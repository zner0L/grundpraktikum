# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

# Fitfunktion defnieren
def fit_linear(x, a):
        return a * x

def joule2ev(joule):
    return joule*6.242e+18

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

figsize = [10, 8]
figs = {}

# Konstanten
c = 299792458
h = 6.62607004e-34
mu_B = 5.788382e-5 # eV/T

# Messreihen benennen, Daten einlesen aus Datei
data_normal = np.genfromtxt('data/messung1.csv', delimiter=' ', usecols=range(5))
data_anomal = np.genfromtxt('data/messung2.csv', delimiter=' ', usecols=range(7))

# Normierung
data_normal = data_normal/math.pi
data_anomal = data_anomal/math.pi

r_formula = ma.uncertainty.Sheet('1/(n*l) * (rp11/(rp11 - rp1) - rp12/(rp12 - rp2)) * (-1)')
r_formula.set_value('n', 1.4519)
r_formula.set_value('l', 3e-3)

# -- normaler Zeeman Effekt --

results_normal = []
b_fields = [0.245, 0.34, 0.37, 0.435, 0.5]
for i, dummy in enumerate(b_fields):
        delta_lamdas = []
        for j in [0, 1, 3, 4]:
            r_formula.set_value('rp11', data_normal[j+1, i]*1e-12, 500*1e-12, tex='r_{(p+1),1}')
            r_formula.set_value('rp1', data_normal[j, i]*1e-12, 500*1e-12, tex='r_{p,1}')
            r_formula.set_value('rp12', data_normal[j+4, i]*1e-12, 500*1e-12, tex='r_{(p+1),2}')
            r_formula.set_value('rp2', data_normal[j+3, i]*1e-12, 500*1e-12, tex='r_{p,2}')
            delta_lamdas.append(r_formula.get_result())
        delta_lamdas = np.array(delta_lamdas)
        results_normal.append(ma.data.weighted_average(delta_lamdas))

results_normal = np.array(results_normal)

l = np.linspace(0, 0.6)
figs['normaler_zeeman.png'] = {'fig': plt.figure(figsize=figsize), 'extra_artists': ()}
plt.xlabel(r'Magnetfeld $B$ [T]')
plt.ylabel(r"Wellenl\"angendifferenz $\Delta \lambda^{-1}$ [$\mathrm{m}^{-1}$]")
params, error = curve_fit(fit_linear, b_fields, results_normal[:, 0])
plt.errorbar(b_fields, results_normal[:, 0], yerr=results_normal[:, 1], label='Messwerte', fmt='+')
plt.plot(l, fit_linear(l, params[0]), label='Linearer Fit')
figs['normaler_zeeman.png']['extra_artists'] = (plt.legend(bbox_to_anchor=(0., -0.22, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.),)
plt.close()

mu_B_exp = joule2ev(ufloat(params[0], np.sqrt(error[0][0])) * c * h)
print('a: ' + str(ufloat(params[0], np.sqrt(error[0][0]))))
print('mu_B: ' + str(mu_B_exp))

# -- anormaler Zeeman Effekt --
r_formula.set_value('n', 1.4519)

results_anomal = []
b_field = [0.275, 0.435, 0.47, 0.5, 0.435, 0.47, 0.5]
for i, dummy in enumerate(b_field):
        delta_lamdas = []
        for j in [0, 2]:
            r_formula.set_value('rp11', data_anomal[j+1, i]*1e-12, 500*1e-12)
            r_formula.set_value('rp1', data_anomal[j, i]*1e-12, 500*1e-12)
            r_formula.set_value('rp12', data_anomal[j+3, i]*1e-12, 500*1e-12)
            r_formula.set_value('rp2', data_anomal[j+2, i]*1e-12, 500*1e-12)
            delta_lamdas.append(r_formula.get_result())
        delta_lamdas = np.array(delta_lamdas)
        results_anomal.append(ma.data.weighted_average(delta_lamdas))

results_anomal = np.array(results_anomal)

l = np.linspace(0, 0.6)
figs['anomaler_zeeman.png'] = {'fig': plt.figure(figsize=figsize), 'extra_artists': ()}
plt.xlabel(r'Magnetfeld $B$ [T]')
plt.ylabel(r"Wellenl\"angendifferenz $\Delta \lambda^{-1}$ [$\mathrm{m}^{-1}$]")
params, error = curve_fit(fit_linear, b_field, results_anomal[:, 0])
plt.errorbar(b_field[:3], results_anomal[:3, 0], yerr=results_anomal[:3, 1], label=r'$0^{\circ}$', fmt='+')
plt.errorbar(b_field[4:], results_anomal[4:, 0], yerr=results_anomal[4:, 1], label=r'$90^{\circ}$', fmt='+')
plt.plot(l, fit_linear(l, params[0]), label='Linearer Fit')
figs['anomaler_zeeman.png']['extra_artists'] = (plt.legend(bbox_to_anchor=(0., -0.30, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.),)
plt.close()

a = ufloat(params[0], np.sqrt(error[0][0]))

proportionality_factors = []
for i, b in enumerate(b_field):
    delta_lambda = ufloat(results_anomal[i, 0], results_anomal[i, 1])
    propf = a * b / delta_lambda
    proportionality_factors.append([propf.n, propf.s])

proportionality_factors = np.array(proportionality_factors)

tbl = ma.latex.Table()
tbl.add_column(results_anomal*1e-2, 'num($0,$1)', '$\Delta \lambda^{-1}$ [$10^2 \mathrm{m}^{-1}$]')
tbl.add_column(proportionality_factors, 'num($0,$1)', 'Bestimmte Landé-Faktoren $g_J$')
tbl.set_caption('Aus dem Fit bestimmte Landé-Faktoren.')
tbl.set_label('tab:lande')
tbl.set_mode('hline')
tbl.set_placement('[h]')
tbl.export('lande_factors.tex')

for key in figs.keys():
    figs[key]['fig'].savefig(key, bbox_extra_artists=figs[key]['extra_artists'], bbox_inches='tight')