# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

# Fitfunktion defnieren
def fit_linear(x, a, b):
        return a * x + b

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

# Messreihen benennen, Daten einlesen aus Datei
data_luft = np.genfromtxt('data/luft.csv', delimiter=' ', usecols=range(2))
data_polyacryl = np.genfromtxt('data/polyacryl.csv', delimiter=' ', usecols=range(3))
data_wasser = np.genfromtxt('data/wasser.csv', delimiter=' ', usecols=range(3))

# Normalisierung
data_luft[:, 0] = (data_luft[:, 0] - 20.5)*2 # relativer Abstand von der Maximumsposition (doppelt wegen Reflexion)
data_luft[:, 1] = data_luft[:, 1] # 1/1000 mus = 1e-9 s

data_polyacryl[:, 1] = (data_polyacryl[:, 1] - 110)*1e-2 # relativer Abstand (2 kürzt sich mit später raus, deshalb gleich weggelassen)
data_wasser[:, 1] = (data_wasser[:, 1] - 110)*1e-2 # relativer Abstand (-"-)

# Farben array anlegen für plots
cmap = plt.cm.get_cmap('Accent', 2)

#_________________________________________________________________________________________
# Plot: Abstand gegen Pulsreisedauer
fig1 = plt.figure(1, figsize=[10, 7]) # figsize = Bildauflösung
# r macht texbar
plt.title(r'Luft')

# Fit
param, errors = curve_fit(fit_linear, data_luft[:, 1], data_luft[:, 0], [0, 0])
l = np.linspace(2, 11)

plt.errorbar(data_luft[:, 1], data_luft[:, 0], xerr=0.02, yerr=0.2, label='Messwerte', fmt='.', color=cmap(0))
plt.plot(l, fit_linear(l, param[0], param[1]), color=cmap(1), label='linearer Fit')
plt.axis([2, 11, 70, 300])
plt.ylabel(r'zur\"uckgelegte Strecke $x$ [cm]')
plt.xlabel(r'Durchlaufzeit $\Delta t$ [ns]')
plt.legend(loc='lower right', fancybox=1)
plt.close()
# Plotende
#_________________________________________________________________________________________

v_luft = ufloat(param[0], np.sqrt(errors[0, 0]))*1e+7
print('V_Luft: ' + str(v_luft))

# Brechungsindex
distances = np.array([ma.data.weighted_average(data_polyacryl[:, (1, 2)]), ma.data.weighted_average(data_wasser[:, (1, 2)])]) # Zeilen: 0 Polyacryl, 1 Wasser
distances = np.column_stack([distances, np.array([[490e-3], [500e-3]])])
brechungsindex = ma.uncertainty.Sheet('(lm + dx)/lm')
brechungsindex.set_value('lm', tex='l_m')
brechungsindex.set_value('dx', tex='\Delta x')
n_results = brechungsindex.batch(distances, 'dx|dx%|lm')
print('-- Brechungsindizes --')
print(n_results)
print(ma.data.literature_value(1.43, n_results[0, 0], n_results[0, 1]))
print(ma.data.literature_value(1.3311, n_results[1, 0], n_results[1, 1]))

# Daraus Lichtgeschwindigkeit
v = ma.uncertainty.Sheet('vl/n')
v.set_value('vl', v_luft.n, v_luft.s)
print('-- Lichtgeschwindigkeiten --')
print(v.batch(n_results, 'n|n%'))

# Save Assets
fig1.savefig('luft.png')