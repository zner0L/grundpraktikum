\subsection{Erzeugung von Röntgenstrahlung}
Röntgenstrahlen lassen sich in einem Röntgenspektrometer erzeugen. Dieses besteht aus einer Röntgenröhre, einer Blende und einem Detektor. Eine Skizze des Aufbaus ist in Abbildung \ref{img:aufbau} zu sehen.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{img/aufbau.png}
	\caption{Versuchsaufbau bestehend aus Röntgenröhre, Blende, Probe und Detektor.  Aus: https://lp.uni-goettingen.de/get/bigimage/4817, aufgerufen am 28.05.2018.}
	\label{img:aufbau}
\end{figure}
Die Röntgenröhre besteht aus einer Kathode, über die Elektronen in eine evakuierte  Glasröhre eintreten und mit einer Anodenspannung $U$ zu einer gegenüberliegende Anode beschleunigt werden \cite[S.857]{gerthsen10}. Durch Auftreffen auf die Anodenplatte, die aus massivem Eisen besteht, werden die Elektronen abgebremst und es entstehen zwei Arten von elektromagnetischer Strahlung:  Bremsstrahlung und charakteristische Strahlung, die durch eine Blende hindurch auf die Probe treffen  \cite[S.184]{gerthsen10}. 

\subsection{Charakteristische Strahlung} 
Die charakteristische Röntgenstrahlung entsteht, wenn die schellen Elektronen auf die Anodenplatte treffen und Elektronen aus der Elektronenhülle der Atome der Platte herausschlagen. Ein herausgeschlagenes Elektron wird durch ein Elektron aus einer höheren Energiestufe ersetzt, da dieser Zustand energetisch günstiger ist. Die durch diesen Vorgang frei werdende Energie wird in Form von elektromagnetischer Strahlung freigesetzt. Die Frequenz dieser Strahlung lässt sich über das Moseleysche Gesetz berechnen:
\begin{align}
	 \nu = R \cdot (Z-1)^2 \cdot  \left( \frac{1}{n^2} - \frac{1}{m^2}\right) \; . \label{eq:charstrahlung}
\end{align}
Hierbei ist $Z$ die Ordnungszahl des Atoms, $R$ die Rydberg-Konstante, $n$ und $m$ die Schalennummer im Bohrschen Atommodell \cite[S.863f]{gerthsen10}. 
 
\subsection{Bremsstrahlung}
Die plötzlich abgebremsten Elektronen müssen nach der klassischen Elektrodynamik elektromagnetische Strahlung senkrecht zu ihrer Beschleunigungsrichtung aussenden. Die Energie der Elektronen berechnet sich über die Beziehung $E= e\cdot U_A$, wobei $e$ die Elementarladung bezeichnet. Die Energie der entstehenden Photonen lässt sich über $E = \hbar\cdot \nu$ berechnen, wobei $\hbar$ das  Planck'sche Wirkumsquantum ist. Wird die gesamte kinetische Energie des Elektrons in ein Photon derselben Energie umwandelt, folgt daraus mit der Lichtgeschwindigkeit $c$ über $c= \lambda \cdot \nu$ und Gleichsetzen der beiden Energieterme das Gesetz von  Duane und Hunt \cite[S.862 f]{gerthsen10}
\begin{align}
	\lambda_{Gr} = \frac{h \cdot c}{e \cdot U_A} \;.
	\label{eq:bremsstrahlung}
\end{align}
Dabei bezeichnet $\lambda_{Gr}$ die Grenzwellenlänge. Die Intensität einer charakteristischen Linie $I_K$ ist abhängig vom Anodenstrom $I_A$, der Anodenspannung $U_A$ und dem Ionistationspotential $U_K$ der jeweiligen Schale gemäß \cite[S. 82]{knetter17}
\begin{align}
	I_K \propto I_A \cdot(U_A - U_K)^{\frac{3}{2}} \;. 
	\label{eq:intesitaet}
\end{align}

\subsection{Röntgenabsorbtion}
Treffen elektromagnetische Strahlen auf Materie, so werden diese dort materialabhängig absorbiert. Röntgenstrahlung wird genau wie sichtbares Licht nach dem  Lambert-Beer-Gesetz absorbiert \cite[S. 865 f]{gerthsen10}
\begin{align}
	I(x) = I_0 \cdot e^{- \mu x} \;. \label{lambert-beer}
\end{align}
$\mu$ ist der Absorptionskoeffizient, $I_0$ die Intensität an der Oberfläche des Material und $x$ die Endringtiefe in das Material.
Es gibt unterschiedliche Arten von Absorption, hier soll jedoch nur der Photoeffekt behandelt werden. Über das Moselysche Gesetz ergibt sich dadurch für die Wellenlänge $\lambda$ und die Dichte $\rho$ des Materials die Beziehung \cite[S.866]{gerthsen10}
\begin{align}
	\lambda^3 \propto \frac{\mu}{\rho} \;. \label{eq:lambda}
\end{align}

\subsection{Die Bragg-Reflexion}
Beim Auftreffen elektromagnetischer Wellen auf eine Kristallstruktur unter einem Winkel $\theta$ tritt Bragg-Reflexion auf. Die Wellen werden an unterschiedlichen Schichten des Kristalls reflektiert, wobei für konstruktive Interferenz der Gangunterschied ein ganzzahliges Vielfaches der Wellenlänge betragen muss:
\begin{align}
	2 \cdot \sin(\theta)\cdot d = \lambda \cdot n \; , \hspace{2 cm } n \in \mathbb{N}. \label{eq:bragg}
\end{align}
Dies ist die Bragg-Bedingung \cite[S.858]{gerthsen10}. Der Faktor 2 rührt daher, dass der Winkel $\theta$, sowohl als Eintrittswinkel als auch als Ausfallswinkel auftritt. Eine Skizze dieses Effekts ist in Abbildung \ref{img:bragg} dargestellt.
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{img/bragg.png}
	\caption{Schematische Darstellung der Bragg-Reflexion an einem Kristall.  Aus: https://lp.uni-goettingen.de/get/bigimage/4806, aufgerufen am 28.05.2018.}
	\label{img:bragg}
\end{figure}

\subsection{Das Geiger-Müller-Zählrohr}
Ein Geiger-Müller-Zählrohr besteht aus einem mit Edelgas gefüllten Rohr. Auf der Achse der Röhre ist eine positive Anodenspannung angelegt, während am Rand eine negative Kathodenspannung anliegt. Die Edelgasmoleküle werden durch die Röntgenstrahlung ionisiert und es fließt ein Strom. Dieser wird  verstärkt und die Anzahl der Stromstöße von einem Zähler gemessen \cite[S.467]{gerthsen10}. Ein schematischer Aufbau  ist in Abbildung \ref{fig:geiger} dargestellt.
\begin{figure}[h!]
	\centering
	\includegraphics[scale=0.7]{img/geiger.png}
	\caption{Schematischer Aufbau des Geiger-Müller-Zählrohrs. Aus: https://lp.uni-goettingen.de/get/bigimage/4795, aufgerufen am 28.05.2018.}
	\label{fig:geiger}
\end{figure}
Die Strahlung, die in diesem Zeitraum ankommt, wird Totzeit $\tau$ genannt. Während dieses Intervalls kann keine eintreffende Strahlung registriert werden. Die gemessenen Werte der Zählrate lassen sich allerdings über folgende Formel korrigieren \cite[S.81]{knetter17}:
\begin{align}
	N_{korrigiert} = \frac{N_{gemessen}}{1-\tau \cdot N_{gemessen}} \;. \label{eq:geigerkorr}
\end{align}