\subsection{Korrektur der Messwerte}

Zur Auswertung wird das Programm \texttt{measure} verwendet. Die gemessenen Zählraten $N$ werden nach Gleichung \ref{eq:geigerkorr} zu $N_k$ korrigiert. Nach \cite[S. 85]{knetter17} beträgt die Totzeit des verwendeten Zählrohres $ \tau = 100 \, \mu\text{s}$. \\

\subsection{Energie der charakteristischen Strahlung}
\label{sec:energie}

In Abbildung \ref{fig:messung1} sind die korrigierten Zählraten $N_k$ gegen die Wellenlängen $\lambda$ aufgetragen. Mit dem Programm \texttt{measure} werden die Maxima bestimmt. Aus der Bragg-Beziehung \ref{eq:bragg} folgt für die Wellenlänge 
\begin{equation}
	\lambda = 2d \sin \theta \; .
	\label{eq:lambdamax}
\end{equation}
Der Gitterabstand $d = 201 \text{pm}$ ist nach \cite[S. 85]{knetter17} gegeben. Hieraus ergeben sich die zugehörigen Energien zu
\begin{equation}
	E = h \cdot \nu = h \cdot \frac{c}{\lambda} \; .
\end{equation}
Die Ergebnisse sind in Tabelle \ref{tab:maxima} aufgeführt.

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|} \hline
		Linie & gemessene Wellenlängen $\lambda$ [pm] & Energien [$10^{3}$ eV] \\ \hline\hline
		$\text{K}_{\alpha,1}$ & $64.4 \pm 0.05$ & $1.925 \pm 0.001$ \\ \hline
		$\text{K}_{\beta,1}$ & $72.02 \pm 0.05$ & $1.721 \pm 0.001$ \\ \hline
		$\text{K}_{\alpha,2}$ & $143.04 \pm 0.05$ & $8.668 \pm 0.001$ \\ \hline
		$\text{K}_{\beta,2}$ & $155.44 \pm 0.05$ & $7.977 \pm 0.001$ \\ \hline
	\end{tabular}
	\caption{Energien und Wellenlängen der Maxima in Röntgenspektrum einer Molybdän-Röntgenröhre.}
	\label{tab:maxima}
\end{table}

\begin{figure}
	\centering
	\includegraphics[height=0.35\textwidth]{evaluation/messung1_lambdas.png}
	\caption{Charakteristisches Röntgenspektrum einer Molybdän-Röntgenröhre.}
	\label{fig:messung1}
\end{figure}

\subsection{Intensität in Abhängigkeit von der Anodenspannung}

\begin{figure}[h!]
	\centering
	\includegraphics[height=0.6\textwidth]{evaluation/intensity_charakteristische_linien.png}
	\caption{Intensitäten der charakteristischen Röntgenlinien bei verschiedenen Spannungen.}
	\label{fig:intensity}
\end{figure}

In Abbildung \ref{fig:messung2} sind die korrigierten Messwerte der zweiten Messreihe im entsprechenden Intervall für die verschiedenen Spannungen aufgetragen. Mithilfe der Python-Bibliothek \texttt{peakdetect} werden die Maxima und Minima bestimmt, aus diesen werden Grenzwellenlängen und charakteristische Wellenlängen bestimmt. In Abbildung \ref{fig:intensity} sind diese gegen die entsprechenden Anodenspannungen aufgetragen. Hieran wird nach Gleichung \ref{eq:intesitaet} mit
\begin{equation}
	f(U) = a(U - U_k)^{\frac{3}{2}}
\end{equation}
gefittet. Die Kurven sind ebenfalls in \ref{fig:intensity} eingezeichnet. Es ergeben sich folgende Fitparameter: 
\begin{equation}
	U_{K_{\alpha}} = (174 \pm 9) \, \frac{\text{pm}}{\text{kV}}; \qquad U_{K_{\beta}} = (1.32 \pm 0.19) \cdot 10^{3} \, \frac{\text{pm}}{\text{kV}} \; .
	\label{eq:holla}
\end{equation}
Der Fehler stammt aus dem Fit. 

\begin{figure}[h!]
	\centering
	\includegraphics[height=0.7\textwidth]{evaluation/messung2.png}
	\caption{Röntgenspektren der Molybdän-Röntgenröhre bei verschiedenen Beschleunigungsspannungen.}
	\label{fig:messung2}
\end{figure}

\subsection{Berechnung des Planckschen Wirkungsquantums}

Aus den aus Abbildung \ref{fig:messung2} bestimmten Minima lassen sich über Gleichung \ref{eq:lambdamax} die entsprechen Grenzwellenlängen berechnen. Mit Gleichung \ref{eq:bremsstrahlung} folgt für das Planck'sche Wirkungsquantum
\begin{align}
	h = \frac{\lambda_{Gr} \cdot e \cdot U_A}{c} \;.
\end{align}
Die Ergebnisse sind in Tabelle \ref{tab:planck} aufgeführt. 

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|} \hline
		Spannung $U_A$ [kV] & Grenzwellenlänge $\lambda$ [pm] & Wirkungsquantum $h$ [$10^{-34}$ Js] \\ \hline\hline
		29 & $35.04 \pm 0.01$ & $5.430 \pm 8$ \\ \hline
		32 & $36.43 \pm 0.01$ & $6.231 \pm 9$ \\ \hline
		35 & $35.74 \pm 0.01$ & $6.684 \pm 10$ \\ \hline
	\end{tabular}
	\caption{Planck'sches Wirkungsquantum aus den gemessenen Grenzwellenlängen.}
	\label{tab:planck}
\end{table}

\subsection{Absorptionskonstanten und Rydbergkonstante}

\begin{table}
	\centering
	\begin{tabular}{|c|c|c|c|} \hline
		Absorber & Wellenlängen $\lambda$ [pm] & Energien [eV] & Rydberg-Konstante $R$ [$10^{15} \frac{1}{\text{s}}$] \\ \hline\hline
		Cu & $136.8 \pm 0.3$ & $9062 \pm 22$ & $3.726 \pm 0.009$ \\ \hline
		Ni & $147.3 \pm 0.3$ & $8416 \pm 19$ & $3.722\pm 0.008$ \\ \hline
	\end{tabular}
	\caption{Rydberg-Konstante aus den Absorptionskanten.}
	\label{tab:rydberg}
\end{table}

In Abbildung \ref{fig:absorptionskanten} sind die Werte der dritten Messreihe halblogarithmisch aufgetragen. Analog zum Vorgehen in Auswertungspunkt \ref{sec:energie} werden die Wellenlängen und Energien bestimmt. Aus Gleichung \ref{eq:charstrahlung} lässt sich die Rydberg Konstante $R$ berechnen. Dazu wird die Frequenz $\nu = c/\lambda$ durch die Wellenlänge ausgedrückt und für den Übergang der Elektronen zwischen $K$ und $L$-Schale $n=1$ und $m=2$ gesetzt. Es ergibt sich
\begin{equation}
	R = \frac{4c}{3\lambda(Z-1)^2}, 
\end{equation}
wobei die Ordnungszahlen $Z$ dem Periodensystem entnommen werden. Die theoretische Wellenlängen ergeben sich mit gegebenem $R$ zu 
\begin{equation}
	\lambda = \frac{4c}{eR(Z-1)^2} \; .
\end{equation}
Die Ergebnisse sind in Tabelle \ref{tab:rydberg} aufgeführt. 

\begin{figure}[h!]
	\centering
	\includegraphics[height=0.7\textwidth]{evaluation/absorptionskanten.png}
	\caption{Absorptionskanten für verschiedene Absorbermaterialen.}
	\label{fig:absorptionskanten}
\end{figure}

\subsection{Absorptionskoeffizienten}

\begin{figure}[h!]
	\centering
	\includegraphics[height=0.7\textwidth]{evaluation/messung4.png}
	\caption{Absorptionsspektren zu verschiedenen Absorbermaterialien.}
	\label{fig:messung4}
\end{figure}

Die in der vierten Messung bestimmten Absorptionsspektren sind in Abbildung \ref{fig:messung4} dargestellt. Aus den gemessenen Intensitäten $I$, hier die gemessenen Zählraten $N$, lässt sich dann mithilfe des Lambert-Beerschen-Absorptionsgesetzes \cite[Kap. 10.2.6]{demtroeder05} der Absorptionskoeffizient $\frac{\mu}{\rho}$ bestimmen:

\begin{equation}
	\frac{\mu}{\rho} = - \frac{1}{x \, \rho} \cdot \ln(\frac{I}{I_0}) \; .
\end{equation}

Als Nullintensität $I_0$ wird hier die Intensität bei leerer Röntgenröhre verwendet. Die Ergebnisse sind in Abbildung \ref{fig:absorptionskoeffizienten} gegen die Wellenlänge aufgetragen. An sie wurde ein Polynom dritten Grades gefittet:

\begin{equation*}
	f(x) = a x^3 + b x^2 + c x + d
\end{equation*}

\begin{figure}[h!]
	\centering
	\includegraphics[height=0.7\textwidth]{evaluation/absorptionskoeffizienten.png}
	\caption{Absorptionskoeffizienten $\frac{\mu}{\rho}$ aufgetragen gegen die Wellenlängen $\lambda$.}
	\label{fig:absorptionskoeffizienten}
\end{figure}