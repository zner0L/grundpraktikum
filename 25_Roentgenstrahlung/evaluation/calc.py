# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math
from peakdetect import peakdetect
from matplotlib.legend_handler import HandlerLine2D, HandlerTuple

# Fitfunktion defnieren
def fit_linear(x, a, b):
        return a * x + b

def intensity_law(u, k, uk):
    return k * (u - uk)**(3/2)

def joule2ev(joule):
    return joule*6.242e+18

def fit_cubic(x, a, b, c, d):
    return a * x**3 + b * x**2 + c * x + d

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

figsize = [12, 10]
figs = {}

# Messreihen benennen, Daten einlesen aus Datei
data_maxima = np.genfromtxt('data/messung1_max.dat', delimiter=' ', usecols=range(3))

# Wellenlängen
lamda_bragg = ma.uncertainty.Sheet('2*sin(t)*d')
lamda_bragg.set_value('d', 201e-12)
lamda_bragg.set_value('t', error=np.deg2rad(0.05))
print(u'Wellenlängen: ' + str(data_maxima[:, 1]))

photo_energy = ma.uncertainty.Sheet('h*c/la')
photo_energy.set_value('h', 6.62607004*1e-34)
photo_energy.set_value('c', 299792458)
photo_energy.set_value('la', error=0.05e-12)
energies = photo_energy.batch(data_maxima[:, 1], 'la')
print(u'Energien: ' + str(joule2ev(energies)))

duane_hunt = ma.uncertainty.Sheet('e * u * la/c')
duane_hunt.set_value('c', 299792458)
duane_hunt.set_value('e', 1.60217662e-19)
duane_hunt.set_value('u', error=0.01e+3)
duane_hunt.set_value('la', error=0.05e-12)

grenzfrequenz = []
charakteristische_linien_1 = []
charakteristische_linien_2 = []
# Messung 2
cmap = plt.cm.get_cmap('Accent', 3)

figs['messung2.png'] = {'fig': plt.figure(figsize=figsize), 'extra_artists': ()}
plt.ylabel(r'Z\"ahlrate $N$ [$\frac{1}{\mathrm{s}}$]')
plt.xlabel(r'Wellenl\"ange $\lambda$ [pm]')
for i, voltage in enumerate([29, 32, 35]):
    data = np.genfromtxt('data/messung2_' + str(voltage) + 'v.dat', delimiter=' ', usecols=(0,2))
    data[:, 0] = np.deg2rad(data[:, 0])
    data[:, 0] = lamda_bragg.batch(data[:, 0], 't')[:, 0]
    maxima, minima = peakdetect(data[:, 1], data[:, 0], lookahead=4)
    maxima = np.array(maxima)
    minima = np.array(minima)
    plt.scatter(data[:, 0]*1e+12, data[:, 1], color=cmap(i), label=str(voltage) + ' kV')
    plt.scatter(maxima[:, 0]*1e+12, maxima[:, 1], color="green")
    plt.scatter(minima[:, 0]*1e+12, minima[:, 1], color="red")
    grenzfrequenz.append([minima[0, 0], minima[0, 1], voltage*1e+3])
    charakteristische_linien_1.append([maxima[0, 0], maxima[0, 1], voltage])
    charakteristische_linien_2.append([maxima[1, 0], maxima[1, 1], voltage])
figs['messung2.png']['extra_artists'] = (plt.legend(bbox_to_anchor=(0., -0.23, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.),)
plt.close()


grenzfrequenz = np.array(grenzfrequenz)
charakteristische_linien_1 = np.array(charakteristische_linien_1)
charakteristische_linien_2 = np.array(charakteristische_linien_2)

params1, errors1 = curve_fit(intensity_law, charakteristische_linien_1[:, 2], charakteristische_linien_1[:, 1])
params2, errors2 = curve_fit(intensity_law, charakteristische_linien_2[:, 2], charakteristische_linien_2[:, 1])
l = np.linspace(28, 36)
figs['intensity_charakteristische_linien.png'] = {'fig': plt.figure(figsize=figsize), 'extra_artists': ()}
plt.xlabel(r'Spannung $U$ [kV]')
plt.ylabel(r'Wellenl\"ange $\lambda$ [pm]')
plt.scatter(charakteristische_linien_1[:, 2], charakteristische_linien_1[:, 1], color="blue", label=r'$K_{\alpha}$')
plt.scatter(charakteristische_linien_2[:, 2], charakteristische_linien_2[:, 1], color="red", label=r'$K_{\beta}$')
plt.plot(l, intensity_law(l, params1[0], params1[1]), color="blue")
plt.plot(l, intensity_law(l, params2[0], params2[1]), color="red")
figs['intensity_charakteristische_linien.png']['extra_artists'] = (plt.legend(bbox_to_anchor=(0., -0.18, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.),)
plt.close()
u_k_1 = ufloat(params1[0], np.sqrt(errors1[0][0]))
u_k_2 = ufloat(params2[0], np.sqrt(errors2[0][0]))

print('U_K Linie 1: ' + str(u_k_1))
print('U_K Linie 2: ' + str(u_k_2))

print(grenzfrequenz[:, (2,0)])
h_exp = duane_hunt.batch(grenzfrequenz[:, (2,0)], 'u|la')
print('h exp: ' + str(h_exp))

cmap = plt.cm.get_cmap('Accent', 2)

figs['absorptionskanten.png'] = {'fig': plt.figure(figsize=figsize), 'extra_artists': ()}
plt.ylabel(r'Z\"ahlrate $N$ [$\frac{1}{\mathrm{s}}$]')
plt.xlabel(r'Reflexionswinkel $\theta$ [\textdegree]')
for i, metal in enumerate(['Cu', 'Ni']):
    data = np.genfromtxt('data/messung3_' + metal.lower() + '.dat', delimiter=' ', usecols=(0, 1))
    #data[:, 0] = np.deg2rad(data[:, 0])
    #data[:, 0] = lamda_bragg.batch(data[:, 0], 't')[:, 0]
    plt.scatter(data[:, 0], np.log(data[:, 1]), color=cmap(i), label=metal)
figs['absorptionskanten.png']['extra_artists'] = (plt.legend(bbox_to_anchor=(0., -0.23, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.),)
plt.close()

absorp = np.array([[19.9, 0.05], [21.5, 0.05]])
absorp = np.deg2rad(absorp)
absorp_lambda = lamda_bragg.batch(absorp, 't|t%')
absorp_energy = photo_energy.batch(absorp_lambda, 'la|la%')

print('Absorptionskanten (Cu, Ni):')
print(' --- Wellenlängen:')
print(absorp_lambda)
print(' --- Energien:')
print(joule2ev(absorp_energy))

rydberg = ma.uncertainty.Sheet('4*c/(3*la*(z-1)**2)')
rydberg.set_value('c', 299792458)
constants = rydberg.batch(np.column_stack((absorp_lambda, [29, 28])), 'la|la%|z')
print(' --- Rydberg-Konstante:')
print(constants)

cmap = plt.cm.get_cmap('Accent', 5)

metals = {'Al': [0.08, 2700], 'Ni': [0.025, 8908], 'Sn': [0.025, 7310], 'Zn': [0.025, 7140]}
data_koeff = {}
figs['messung4.png'] = {'fig': plt.figure(figsize=figsize), 'extra_artists': ()}
plt.ylabel(r'Z\"ahlrate $N$ [$\frac{1}{\mathrm{s}}$]')
plt.xlabel(r'Wellenl\"ange $\lambda$ [pm]')
for i, metal in enumerate(['Al', 'Ni', 'Sn', 'Zn', 'leer']):
    data_koeff[metal] = np.genfromtxt('data/messung4_' + metal.lower() + '.dat', delimiter=' ', usecols=(0, 1))
    data_koeff[metal][:, 0] = np.deg2rad(data_koeff[metal][:, 0])
    data_koeff[metal][:, 0] = lamda_bragg.batch(data_koeff[metal][:, 0], 't')[:, 0]
    plt.scatter(data_koeff[metal][:, 0]*1e+12, data_koeff[metal][:, 1], color=cmap(i), label=metal)
figs['messung4.png']['extra_artists'] = (plt.legend(bbox_to_anchor=(0., -0.27, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.),)
plt.close()

mu = ma.uncertainty.Sheet('(-1/x * log(i/i0))/rho')
l = np.linspace(50, 120)
cubic_plots = []

figs['absorptionskoeffizienten.png'] = {'fig': plt.figure(figsize=figsize), 'extra_artists': ()}
plt.xlabel(r'Wellenl\"ange $\lambda$ [pm]')
plt.ylabel(r"Absorptionskoeffizient $\frac{\mu}{\rho}$ [$\frac{\mathrm{m}^2}{\mathrm{kg}}$]")
for i, metal in enumerate(['Al', 'Ni', 'Sn', 'Zn']):
    mu.set_value('x', metals[metal][0])
    mu.set_value('rho', metals[metal][1])
    koeff = mu.batch(np.column_stack((data_koeff[metal][:, 1], data_koeff['leer'][:, 1])), 'i|i0')
    params, error = curve_fit(fit_cubic, data_koeff[metal][:, 0]*1e+12, koeff[:, 0])
    plt.scatter(data_koeff[metal][:, 0]*1e+12, koeff[:, 0], color=cmap(i), label=metal)
    plt.plot(l, fit_cubic(l, params[0], params[1], params[2], params[3]), color=cmap(i))
figs['absorptionskoeffizienten.png']['extra_artists'] = (plt.legend(handler_map={tuple: HandlerTuple()}, bbox_to_anchor=(0., -0.22, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.),)
plt.close()

for key in figs.keys():
    figs[key]['fig'].savefig(key, bbox_extra_artists=figs[key]['extra_artists'], bbox_inches='tight')