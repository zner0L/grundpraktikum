import numpy as np
import math
from scipy.optimize import curve_fit, OptimizeWarning

### Math Functions ###

def linear_function(x, m, b):
    return m * x + b

def exponential_function(x, a, b, c):
    return a*np.exp(x * (-b))+c

### Special Physikcs Functions ###

def phi0(omega, n, omega_0, beta):
    return n/(np.sqrt((omega_0**2-omega**2)**2+4*beta**2*omega**2))

def phase_shift_func(x, g):
    return np.arctan((g*x)/(1-x**2))/math.pi if x < 1 else np.arctan((g*x)/(1-x**2))/math.pi+1

def phase_shift_func_vec(x, g):
  y = np.zeros(x.shape)
  for i in range(len(y)):
    y[i]=phase_shift_func(x[i],g)
  return y