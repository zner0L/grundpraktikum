# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq, fsolve
import math
from peakdetect import peakdetect


def fit_linear(x, a, b):
    return a * x + b

def findIntersection(fun1,fun2,x0):
 return fsolve(lambda x : fun1(x) - fun2(x),x0)

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

figsize = [10, 8]
figs = {}

data = np.genfromtxt('data/data.csv', usecols=range(4))

# Bestimmung der Maxima mit der Wunderfunktion
maxima, minima = peakdetect(data[:, 2], data[:, 0], lookahead=4)
maxima = np.array(maxima)
minima = np.array(minima)

indices_max = []
indices_min = [14]
for maximum in maxima[:, 0]:
    indices_max.append(np.where(data[:, 0] == maximum)[0][0])
for minimum in minima[:, 0]:
    indices_min.append(np.where(data[:, 0] == minimum)[0][0])
indices_min.append(244)

# Plot
l = np.linspace(0, 100)
figs['plot.png'] = plt.figure(figsize=figsize)
plt.title('Photostrom')

upward_lines = []
downward_lines = []

for k in range(len(indices_min)-1):
    i_min = indices_min[k]
    i_max = indices_max[k]
    p, e = curve_fit(fit_linear, data[i_min:i_max, 0], data[i_min:i_max, 2])
    l = np.linspace(data[i_min, 0] - 2, data[i_max, 0] + 2)
    plt.plot(l, fit_linear(l, p[0], p[1]))
    upward_lines.append(p)

for k in range(len(indices_max)):
    i_min = indices_min[k + 1]
    i_max = indices_max[k]
    p, e = curve_fit(fit_linear, data[i_max:i_min, 0], data[i_max:i_min, 2])
    l = np.linspace(data[i_max, 0] - 2, data[i_min, 0] + 2)
    plt.plot(l, fit_linear(l, p[0], p[1]))
    downward_lines.append(p)

maxima = []

for k in range(len(downward_lines)):
    p_up = upward_lines[k]
    p_down = downward_lines[k]
    x_intersection = findIntersection(lambda x: p_up[0] * x + p_up[1], lambda x: p_down[0] * x + p_down[1], 5)[0]
    maxima.append([x_intersection, p_up[0] * x_intersection + p_up[1]])

maxima = np.array(maxima)

prev_max = 0
delta_max = []
for max in maxima[:, 0]:
    delta_max.append(max-prev_max)
    prev_max = max
maxima = np.column_stack((maxima, delta_max))
average_delta = ufloat(maxima[:, 2].mean(), stats.sem(maxima[:, 2]))
delta_E = average_delta * 1.60217662e-19
lambda_emit = (6.62607004e-34 * 299792458)/delta_E
print('Mittelwert der Differenzen: ' + str(average_delta) + ' V')
print('Anregungsenergie: ' + str(delta_E) + ' J')
print('Lambda: ' + str(lambda_emit*1e+9) + ' nm')

plt.scatter(maxima[:, 0], maxima[:, 1], marker='o', label='Maxima', color="red", zorder=2)
plt.errorbar(data[:, 0], data[:, 2], xerr=data[:, 1], yerr=data[:, 3], fmt='.', label='Photostrom', zorder=1)
plt.xlabel('Beschleunigungsspannung $U_1$ [V]')
plt.ylabel('Auff\\"angerspannung $U_A$ $\propto$ Photostrom [mV]')
plt.axis([0, 100, 0, 2500])
box = plt.axes().get_position()
plt.axes().set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
plt.legend(bbox_to_anchor=(0., -0.32, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.)
plt.close()

# Tabellen
tbl = ma.latex.Table()
tbl.add_column(range(1, len(maxima)+1, 1), False, 'Ordnung')
tbl.add_column(maxima[:, 0], 'num($0,0.05)', 'Beschleunigungsspannung $U_1$ [V]')
tbl.add_column(maxima[:, 1], 'num($0,1,sigceil,1)', '$U_A$ [mV]')
tbl.add_column(maxima[:, 2], 'num($0,0.05)', '$\\Delta U$ [V]')
tbl.set_caption('Lokale Maxima des Photostroms aus Abbildung \\ref{fig:plot} sowie Differenz der Beschleugnigungsspannungen.')
tbl.set_label('tab:maxima')
tbl.set_mode('hline')
tbl.set_placement('[h!]')
tbl.export('maxima.tex')

for key in figs.keys():
    figs[key].savefig(key)