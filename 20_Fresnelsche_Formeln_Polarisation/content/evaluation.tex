\subsection{Intensität des Lasers nach senkrechter und paralleler Polarisation}

\subsubsection{Experimentelle Werte}

Der Winkel zwischen dem einfallenden Strahl und dem Lot der reflektierenden Fläche des Prismas ist $\alpha$. Nach dem Reflexionsgesetz ist der Winkel zwischen Lot und reflektiertem Strahl ebenfalls $\alpha$. Gemessen wurde der Winkel $\varphi$ zwischen reflektiertem Strahl und der geraden Verlängerung des einfallenden Lasers hinter dem Prisma, dessen Fehler auf einen halben Skalenteil geschätzt wird. Es ergibt sich 

\begin{equation}
	\alpha + \alpha + \phi = 180^\circ \Leftrightarrow \alpha = 90^\circ -\phi/2
\end{equation}
Der Fehler von $\alpha$ berechnet sich nach Gauß zu $\sigma_\alpha=\sigma_\varphi/2$. \\

Die Intensität des an der Fotodiode auftreffenden Lichts ist proportional zur am Multimeter gemessenen Spannung. Daher wird in den folgenden Rechnungen die Spannung betrachtet. Die gemessenen Spannungen $U_{\text{M}}$ werden mit der durch das Umgebungslicht verursachten Spannung $U_{\text{U}}$ normiert auf die allein durch das Licht verursachte Spannung. Der Fehler ist nach Gauß
\begin{equation*}
	\sigma_{U_\text{L}}=\sqrt{\sigma^2_{U_\text{M}}+\sigma^2_{U_\text{U}}}=\sqrt{2}\cdot\sigma_{U_\text{M}} \; .
\end{equation*}
Mit der Spannung $U_0$, die bei einer Auslenkung des Schwenkarms von $\varphi = 0 ^{\circ}$ für zwei Einstellungen der Polarisation gemessen wurde, ergibt sich für die normierte Spannung 
\begin{equation}
	U_{\text{n}} = \frac{U_{\text{L}}}{U_0} \; .
\end{equation}
Der Fehler berechnet sich zu 
\begin{equation}
\sigma_{U_\textrm{n}}=\frac{1}{U_0}\cdot\sqrt{\sigma_{U_\textrm{L}}^2+\left(\sigma_{U_0}\frac{U_\textrm{L}}{U_0}\right)^2} \; .
\end{equation}
In Abbildung \ref{img:ev_intensity}  sind die Messwerte der normierten Spannung $U_{\text{n}}$ über den Einfallswinkel $\alpha$ jeweils für senkrecht und parallel polarisiertes Licht aufgetragen. 



\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{evaluation/intensity_on_angle.png}
	\caption{Normierte Spannung aufgetragen über dem Einfallswinkel bei zwei verschiedenen Richtungen der Polarisation. Sowohl theoretisch erwartete Verläufe als auch Messwerte sind eingezeichnet.}
	\label{img:ev_intensity}
\end{figure}


\subsubsection{Theoriewerte}

Einsetzen des Snellius'schen Brechungsgesetzes \ref{eq:snellius} in die Fresnel'schen Formeln \ref{eq:fresnelformeln} ergibt nach Umstellen eine Gleichung für den Theoriewert der normierten Spannung $U_{\text{n}}$. Für senkrecht polarisiertes Licht gilt
\begin{equation*}
	U_{\textrm{n, s}}=\varrho^2_\textrm{s}=\left(-\frac{\sin(\alpha-\beta)}{\sin(\alpha+\beta)}\right)^2=\left(-\frac{\sin\left(\alpha-\arcsin\left(\frac{\sin(\alpha)}{1.63}\right)\right)}{\sin\left(\alpha+\arcsin\left(\frac{\sin(\alpha)}{1.63}\right)\right)}\right)^2 \; ,
\end{equation*}
für parallel polarisertes Licht
\begin{equation*}
	U_{\textrm{n, p}}=\varrho^2_\textrm{p}=\left(-\frac{\tan(\alpha-\beta)}{\tan(\alpha+\beta)}\right)^2=\left(-\frac{\tan\left(\alpha-\arcsin\left(\frac{\sin(\alpha)}{1.63}\right)\right)}{\tan\left(\alpha+\arcsin\left(\frac{\sin(\alpha)}{1.63}\right)\right)}\right)^2 \; .
\end{equation*}
Der als fehlerfrei angenommene Brechungsindex des Glasprismas beträgt nach \cite[Kap. 20.7]{knetter17} $n = 1.63$. Die Verläufe sind ebenfalls in Abbildung \ref{img:ev_intensity} eingezeichnet. 

\subsection{Brechungsindex über den Brewster-Winkel}
\label{sec:brewster}

Der niedrigste Wert der normierten Intensitäten in Abbildung \ref{img:ev_intensity} wird für den Brewster-Winkel $\alpha_B = (62.5 \pm 0.5) ^{\circ}$. Nach Gleichung \ref{eq:brewster_winkel} gilt für eine Brechung an Luft für den Brechungsindex des Prismas $n_{\text{P}}$
\begin{equation}
	n_{\text{P}} =  \tan(\alpha_B) = 1.92 \pm 0.04 \; .
	\label{eq:n_eins}
\end{equation}
Der Fehler berechnet sich zu
\begin{equation}
	\sigma_{n_{\text{P}}}=\frac{\sigma_{\alpha_B}}{\cos(\alpha_B)} \; .
	\label{eq:bummms}
\end{equation}

\subsection{Drehung der Schwingungsebene}

Um aus dem gemessenen Winkel $\vartheta_0$ die Polarisationswinkel des einfallenden Lichts $\gamma$ zu berechnen, müssen hiervon $45^{\circ}$ subtrahiert werden. In Abbildung \ref{img:drehwinkel} sind die so normierten Einstellungen am Analysator über dem Einfallswinkel des Lichts aufgetragen. Da das Minimum der Intensität jeweils in einem Intervall angenommen wird, sind für jeden Einfallswinkel $\alpha$ je zwei Werte aufgetragen, welche die Intervallgrenzen darstellen. Zudem ist ebenfalls die aus dem Snellius'schen Brechungsgesetz folgende Kurve für den Theoretischen Verlauf nach \cite[Kap. 20.8]{knetter17} aufgetragen:
\begin{equation}
	\gamma=\arctan\left(-\frac{\cos(\alpha-\arcsin(\sin(\alpha)/n))}{\cos(\alpha+\arcsin(\sin(\alpha)/n))}\right)-\frac{\pi}{4}.
\label{eq:theta_null}
\end{equation}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{evaluation/drehwinkel.png}
	\caption{Drehwinkel der Schwingungsebene aufgetragen über den Einfallswinkel $\alpha$. Es sind sowohl die linken als auch rechten Intervallgrenzen der Messung aufgetragen.}
	\label{img:drehwinkel}
\end{figure}

\subsection{Brechungsindex über den Drehwinkel}

Einsetzen von $\gamma = \pi/4$ in Gleichung \ref{eq:theta_null} kommt dem Nullsetzen des Nenners gleich, da der Tangens für diesen Wert divergiert. Wird erneut $n_1 = 1$ für Luft gesetzt, so folgt 
\begin{equation}
	n_2 = \tan \alpha \; .
	\label{eq:bumms}
\end{equation}
Offenbar bezeichnet $\alpha$ hier ebenfalls des Brewster-Winkel. Aus Abbildung \ref{img:drehwinkel} wird zu $\gamma = \pi/4$ gehörige Wert von $\alpha = (62 \pm 2)^{\circ}$ abgelesen, dessen Fehler aufgrund der Ableseungenauigkeiten auf $2 ^{\circ}$ geschätzt wird. Aus Gleichung \ref{eq:bumms} wird der Brechungsindex berechnet, mit dem Fehler nach Gleichung \ref{eq:bummms}. Es ergibt sich
\begin{equation}
	n_{\text{P}} = 1.9 \pm 0.16 \;. 
\end{equation}