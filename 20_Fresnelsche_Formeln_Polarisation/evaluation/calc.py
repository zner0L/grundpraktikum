# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

# Fitfunktion defnieren
def fit_senkrecht(x, n):
    return (-(np.sin(x*math.pi/180 - np.arcsin(np.sin(x*math.pi/180) / n))) / (np.sin(x*math.pi/180 + np.arcsin(np.sin(x*math.pi/180) / n))))**2

def fit_parallel(x, n):
    return (-(np.tan(x*math.pi/180 - np.arcsin(np.sin(x*math.pi/180) / n))) / (np.tan(x*math.pi/180 + np.arcsin(np.sin(x*math.pi/180) / n))))** 2

def fit_drehwinkel(x, n):
    return (np.arctan(- (np.cos(x*math.pi/180 - np.arcsin(np.sin(x*math.pi/180) / n))) / (np.cos(x*math.pi/180 + np.arcsin(np.sin(x*math.pi/180) / n)))))*180/(math.pi)

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

# Messreihen benennen, Daten einlesen aus Datei
data_norm = np.genfromtxt('data/norm.csv', delimiter=' ', usecols=range(4))
data_p1 = np.genfromtxt('data/prism_p1.csv', delimiter=' ', usecols=range(6))
data_p2 = np.genfromtxt('data/prism_p2.csv', delimiter=' ', usecols=range(7))

# Normalisierung
ausfallswinkel = ma.uncertainty.Sheet('90 - (d)/2')
data_p1[:, [0,1]] = ausfallswinkel.batch(data_p1[:, [0,1]], 'd|d%')
data_p2[:, [0,1]] = ausfallswinkel.batch(data_p2[:, [0,1]], 'd|d%')

data_p1_0 = data_p1[data_p1[:, 2] == 0]
data_p1_90 = data_p1[data_p1[:, 2] == 90]

norm = ma.uncertainty.Sheet('v/n')
norm.set_value('n', data_norm[1, 2], data_norm[1, 3])
data_p1_0[:, [4, 5]] = norm.batch(data_p1_0[:, [4, 5]], 'v|v%')
norm.set_value('n', data_norm[2, 2], data_norm[2, 3])
data_p1_90[:, [4, 5]] = norm.batch(data_p1_90[:, [4, 5]], 'v|v%')


# Plot
l = np.linspace(15, 85)

fig1 = plt.figure(figsize=[15, 10])
plt.errorbar(data_p1_0[:, 0], data_p1_0[:, 4], xerr=data_p1_0[:, 1], yerr=data_p1_0[:, 5], fmt='.', label='Messdaten (senkrechte Polarisation)')
plt.errorbar(data_p1_90[:, 0], data_p1_90[:, 4], xerr=data_p1_90[:, 1], yerr=data_p1_90[:, 5], fmt='.', label='Messdaten (parallele Polarisation)')
plt.plot(l, fit_parallel(l, 1.63), label='Theoriekurve (parallele Polarisation)')
plt.plot(l, fit_senkrecht(l, 1.63), label='Theoriekurve (senkrechte Polarisation)')
plt.xlabel(r'Reflexionswinkel $\alpha$ [$^{\circ}$]')
plt.ylabel(r'normierte Intentsit{\"a}t [1]')
plt.legend(loc="upper left")
plt.close()

brewster = data_p1_90[np.argmin(data_p1_90[:, 4]), [0, 1]]
print('Brewster-Winkel: ' + str(brewster))
n = ma.uncertainty.Sheet('n * tan(b*(pi/180))')
n.set_value('n', 1)
n.set_value('b', brewster[0], brewster[1])
print('Brechungsindex aus Brewster: ' + str(n.get_result()))

fig2 = plt.figure(figsize=[12, 10])
plt.errorbar(data_p2[:, 0], data_p2[:, 4]-45, xerr=data_p2[:, 1], yerr=data_p2[:, 6], fmt='.', label='Drehwinkel linke Intervallgrenze')
plt.errorbar(data_p2[:, 0], data_p2[:, 5]-45, xerr=data_p2[:, 1], yerr=data_p2[:, 6], fmt='.', label='Drehwinkel rechte Intervallgrenze')
plt.plot(l, fit_drehwinkel(l+45, 1.63), label='Theoriekurve')
plt.ylabel(r'Drehwinkel $\gamma$ [$^{\circ}$]')
plt.xlabel(r'Einfallswinkel $\alpha$ [$^{\circ}$]')
plt.legend()
plt.close()

fig1.savefig('intensity_on_angle.png')
fig2.savefig('drehwinkel.png')