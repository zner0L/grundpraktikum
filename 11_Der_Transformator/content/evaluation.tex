\subsection{Primärspannung bei unbelastetem Transformator}
In der ersten Messreihe wurde die Primärspannung in Abhängigkeit vom Primärstrom gemessen. Die Messwerte sind in Abbildung \ref{fig:u_f_i} graphisch dargestellt. Es ist ein Proportionaler Zusammenhang der Stromstärke und der Spannung zu erkennen. Dies entspricht den Erwartungen für einen idealen Transformator für Spannungen $<$ 20 V.

\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{evaluation/u_f_i.png}
    \caption{$U_1$ in Abhängigkeit von $I_1$.} 
    \label{fig:u_f_i}
\end{figure}

\subsection{Übersetzungsverhältnisse des unbelasteten Transformators}

Nun ist die Primärspannung $U_1$ gegen die Sekundärspannung $U_2$ aufgetragen.
Aus dem linearen Fit beider Kurven konnte so das Übersetzungsverhältnis beider Messreihen ermittelt werden. Zum einen wurden bei genau der Hälfte der Windungszahlen und zum anderen bei gleicher Windungszahl von Primär- und Sekundärspannung Messwerte genommen. Alle Messwerte sind in Abbildung \ref{fig:u_f_u} aufgetragen. Die Steigungen der Geraden sind in Tabelle \ref{tab:fits} eingetragen. 
    
\begin{figure}[h!]
    \centering
    \includegraphics[width=0.6\textwidth]{evaluation/u_f_u.png}
    \caption{$U_2$ in Abhängigkeit von $U_1$ bei verschiedenen Windungszahlen der Primär- und Sekundärspule.} 
    \label{fig:u_f_u}
\end{figure}
    
\begin{table}[h!]
\centering
\begin{tabular}{|l|l|l|l|}
	\hline
 	Schalterstellung & Steigung m & Achsenabschnitt t \\
	\hline\hline
	b1 $</>$ a2; b1 $<>$ b2 & 0.498 $\pm$ 0.001 & -0.011 $\pm$ 0.005 \\
	\hline
	b1 $<>$ a2; b2 $<>$ a1 & 0.991 $\pm$ 0.001  & 0.002 $\pm$ 0.005 \\
	\hline
\end{tabular}
\caption{Konstanten für die Modellierung von $U_2$ gegen $U_1$}
\label{tab:fits}
\end{table}

\subsection{Berechnung des Phasenwinkels}

Das in  Abbildung \ref{pic:zeiger} dargestellte Diagramm zeigt den Zusammenhang zwischen dem Phasenwinkel $\phi$, dem Gesamtstrom $I_{ges}$ und dem gemessenden Primärstrom. Dabei ist $I_{1,T}$, der mit dem Transformator gemessene Primärstrom und $I_{1,R}$, der mit dem Widerstand gemessene Primärstrom \cite{knetter16}.
Der Zusammenhang zwischen Phasenwinkel und Gesamtstrom lautet demnach (Abb. \ref{pic:zeiger}):

\begin{equation}
cos\left(\frac{\phi}{2}\right) = \frac{I_{ges}}{2 I_1}
\label{eq:phase_angle}
\end{equation}

Die berechneten Winkel sind in Tabelle \ref{tab:angles} aufgelistet, die sich nach Umstellen von Gleichung \ref{eq:phase_angle} nach $\phi$ bestimmen lassen. 

\begin{table}[h!]
	\centering
	\begin{tabular}{|l|l|l|l|}
		\hline
		$I_2$ [A] &  $I_{1}$ [mA] & $I_{ges}$ [mA] & $\phi$ [Rad] \\
		\hline\hline
		$0.00$ & $20.6 \pm 0.01$ & $30 \pm 10$ & $1.51 \pm 0.71$ \\
		\hline
		$0.50$ & $513.0 \pm 1.0$ & $1014 \pm 1$ & $0.31 \pm 0.03$ \\
		\hline
		$0.75$ & $738.0 \pm 1.0$ & $1446 \pm 1$ & $0.40 \pm 0.02$ \\
		\hline
		$1.00$ & $1013.0 \pm 2.0$ & $1960 \pm 2$ & $0.51 \pm 0.02$ \\
		\hline
		$1.50$ & $1515.0 \pm 2.0$ & $2950 \pm 10$ & $0.46 \pm 0.03$ \\
		\hline
	\end{tabular}
	\caption{Phasenwinkel zwischen Primärspannung $U_1$ und Primärstrom $I_1$ in Abhängigkeit von $I_2$}
	\label{tab:angles}
\end{table}

Die Lissajous-Figuren besitzen eine so große Unsicherheit, dass sich hieraus keine Ergebnisse bestimmen lassen. Auch die graphische Darstellung der Ergebnisse (Abb. \ref{fig:angles}) folgt keinem erkennbaren Zusammenhang.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{evaluation/phase_angle.png}
	\caption{Phasenwinkel in Abhängigkeit von $I_2$.} 
	\label{fig:angles}
\end{figure}

\subsection{Wirk- und Verlustleistung bei 1.5 A}

Die Wirk- und Verlustleistung lässt sich über folgende Gesetzmäßigkeiten bestimmen \cite[S. 156]{demtroeder06}:

\begin{align}
P_w = U_1 \cdot I_1 \cdot cos(\phi) \notag\\
P_v = U_1 \cdot I_1 \cdot sin(\phi)
\end{align}

So ist zu erkennen, dass sich die Wirk- und Verlustleistung sich je nach Phasenwinkel verändern. Für die Maximallast 1.5 A ergibt sich der Phasenwinkel $\phi$ zu 0.46 Rad (aus dem Zeigerdiagramm).
Der Fehler für den Strom $\sigma_{I_1}$ entspricht 0.002 A. Da aus den Messwerten für den Phasenwinkel kein Fehler errechnet werden konnte, da ein Verhältnis berechnet wurde, bei dem sowohl der Divisor als auch der Dividend den gleichen Fehler haben, wird an dieser Stelle keine weitere Fehlerberechnung benötigt.\\
Man erhält nach Einsetzen der Werte $I_1 = 1515 \mathrm{mA}$ und dazugehörigem $U_1 = 15.2 \mathrm{V}$ für
$P_w = 20.63 \mathrm{W}$ und $P_v = 10.22 \mathrm{W}$. 


\subsection{Energieverschwendung eines Handyladegeräts}

Um zu ermitteln, wie viel Geld pro Jahr bei einem Preis von $0.25 \frac{\text{€}}{\mathrm{kWh}}$ des Elektrizitätsversorgers gezahlt werden muss, wenn ein Handyladegerät in der Steckdose verbleibt, lässt sich folgender Zusammenhang nutzen:

\begin{equation}
\mathrm{Preis_{ges}} = {365 \, \mathrm{Tage} \cdot 24 \, \mathrm{Stunden} \cdot P_v\cdot 0.25 \text{€}} \cdot \frac{1}{1000}\,\,.
\end{equation}

Es ergibt sich für den Preis: $\mathrm{Preis_{ges}} = 0.045 \text{€}$. 