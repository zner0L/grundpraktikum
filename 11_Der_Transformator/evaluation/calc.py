# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

def fit_linear(x, a, b):
    return a * x + b

def fit_quadratic_prime(x, a, b):
    return 2 * a * x + b

def fit_quadratic(x, a, b, c):
    return a * x**2 + b * x + c

def fit_function(x, a, b, c, d, e, f, g):
    return g * x**6 + f * x**5 + e * x**4 + d * x**3 + c * x**2 + b * x + a

def fit_function_prime(x, b, c, d, e, f, g):
    return 6 * g * x**5 + 5 * f * x**4 + 4 * e * x**3 + 3 * d * x**2 + 2 * c * x + b

data_u_f_I = np.genfromtxt('data/u_1_f_I.csv', delimiter=' ', usecols=range(3))
data_u_f_u = np.genfromtxt('data/u_2_f_u.csv', delimiter=' ', usecols=range(4))
data_i_f_i = np.genfromtxt('data/I_f_i.csv', delimiter=' ', usecols=range(5))
data_lissajous = np.genfromtxt('data/lissajous.csv', delimiter=' ', usecols=range(4))


cmap = plt.cm.get_cmap('Paired', 6)

fig1 = plt.figure(figsize=[10,7])
plt.errorbar(data_u_f_I[:, 0], data_u_f_I[:, 1], yerr=data_u_f_I[:, 2], fmt='+', label=r'Messwerte zu $U_1$', color=cmap(1))
plt.xlabel(r'Stromst{\"a}rke $I$ [mA]')
plt.ylabel(r'Spannung $U$ [V]')
plt.legend(loc='lower right')
plt.axis([0, 20, 0, 15])
fig1.savefig('u_f_i.png')

l = np.linspace(0, 10)
params_1, errors_1 = curve_fit(fit_linear, data_u_f_u[:, 0], data_u_f_u[:, 1])
params_2, errors_2 = curve_fit(fit_linear, data_u_f_u[:, 0], data_u_f_u[:, 2])

fig2 = plt.figure(figsize=[10,7])
plt.errorbar(data_u_f_u[:, 0], data_u_f_u[:, 1], yerr=data_u_f_u[:, 3], fmt='+', label=r'ge{\"o}ffneter Schalter', color=cmap(0))
plt.plot(l, fit_linear(l, params_1[0], params_1[1]), color=cmap(1))
plt.errorbar(data_u_f_u[:, 0], data_u_f_u[:, 2], yerr=data_u_f_u[:, 3], fmt='+', label=r'geschlossener Schalter', color=cmap(2))
plt.plot(l, fit_linear(l, params_2[0], params_2[1]), color=cmap(3))
plt.xlabel(r'Spannung $U_1$ [V]')
plt.ylabel(r'Spannung $U_2$ [V]')
plt.legend(loc='upper left')
plt.axis([0, 10, 0, 10])
fig2.savefig('u_f_u.png')

print params_1
print str(np.sqrt(errors_1[0][0])) + ' ' + str(np.sqrt(errors_1[1][1]))
print params_2
print str(np.sqrt(errors_2[0][0])) + ' ' + str(np.sqrt(errors_2[1][1]))

phase_angle = ma.uncertainty.Sheet('2 * acos(ig/(2 * i))', name='\phi')
phase_angle.set_value('i', tex='I_1')
phase_angle.set_value('ig', tex='I_{ges}')
angles = phase_angle.batch(data_i_f_i[:, (1,2,3,4)], 'i|i%|ig|ig%')

print angles

fig3 = plt.figure(figsize=[10,7])
plt.errorbar([0, 0.3, 0.5, 0.75, 1.0, 1.5], angles[:, 0], fmt='+', label=r'berechnete Winkel')
plt.legend()
plt.ylabel(r'Phasenwinkel $\phi$ [rad]')
plt.xlabel(r'$I_2$ [A]')
fig3.savefig('phase_angle.png')