\documentclass[12pt,a4paper,titlepage,headinclude,bibtotoc]{scrartcl}

\usepackage[komastyle]{scrpage2}
\pagestyle{scrheadings}
\setheadsepline{0.5pt}[\color{black}]
\setkomafont{captionlabel}{\sffamily\bfseries}
\setcapindent{0em}
\usepackage[ngerman]{babel}
\usepackage[utf8]{inputenc}
\usepackage{latexsym,exscale,stmaryrd,amssymb,amsmath}
\usepackage{eurosym}
\usepackage{color}
\usepackage{graphicx}
\usepackage{wrapfig}
\usepackage{subfigure}
\usepackage{sidecap}
\usepackage{here}
\usepackage[backend=biber]{biblatex}
\usepackage[nice]{units}

\addbibresource{Literatur.bib}

\graphicspath{{Abbildungen/}}

\newcommand{\corresponds}{\ensuremath{\mathrel{\widehat{=}}}}
\makeatletter
\DeclareRobustCommand{\chemical}[1]{%
  {\(\m@th
   \edef\resetfontdimens{\noexpand\)%
       \fontdimen16\textfont2=\the\fontdimen16\textfont2
       \fontdimen17\textfont2=\the\fontdimen17\textfont2\relax}%
   \fontdimen16\textfont2=2.7pt \fontdimen17\textfont2=2.7pt
   \mathrm{#1}%
   \resetfontdimens}}
\makeatother


\begin{document}

\begin{titlepage}
\centering
\textsc{\Large Praktikum der Fakultät für
  Physik,\\[1.5ex] Universität Göttingen}

\vspace*{4.2cm}

\rule{\textwidth}{1pt}\\[0.5cm]
{\huge \bfseries
  Versuch 11 \\ [1ex]
  Der Transformator}\\[1.5ex]
\rule{\textwidth}{1pt}

\vspace*{3.5cm}

\begin{Large}
\begin{tabular}{ll}
Praktikanten: &  Kira Herff, Sven Meienberg\\
E-Mail: & sven.meienberg@stud.uni-goettingen.de\\
& kira.herff@stud.uni-göttingen.de\\
Betreuer: & Robert Gruhl\\
Datum: & 11.5.2017  \\ 
\end{tabular}
\end{Large}

\vspace*{0.8cm}

\begin{Large}
\fbox{
  \begin{minipage}[t][2.5cm][t]{6cm} 
    Testat:
  \end{minipage}
}
\end{Large}

\end{titlepage}

\tableofcontents

\newpage

\section{Einleitung}
\label{sec:einleitung}
Um auf die Welt möglichst überall zu elektrisieren, müssen vor allem Spannungen über riesige Distanzen transportiert werden - mit möglichst wenig Stromverlust.\\
Realisiert wird diese flächendeckende Eltrizität durch Transformatoren, die zwischen unterschiedlichen Spannungsebenen übersetzen können. Im Alltag wird dabei beispielsweise eine Spannung von 220 bis 380 kV (in Europa) über regionale Umspannwerke auf eine Niederspannung von 400V für die Verbraucher gebracht.\\
Doch selbst kurz vor Gebrauch, von zum Beispiel elektrischen Küchengeräten, wird die Netzspannung aus der Steckdose abermals auf die üblichen 230V transformiert.\\
In diesem Versuch haben wir verschiedene Experimente zu dem Thema durchgeführt und sie im folgenden ausgewertet.
\newpage


\section{Theorie}
\label{sec:theorie}
\subsection{Aufbau}
\label{subsec:aufbau}
Ein Transformator besteht grundsätzlich erst einmal aus zwei Spulen, die durch einen gemeinsamen Eisenkern induktiv verbunden sind. Dabei unterscheidet man in Primär- und Sekundärspule je nach Einbau in den Schaltplan.\\
Die Wechselstrom-Eingangspannung der Primärspule wird dabei in die Ausgangspannung transformiert.\\
Durch den sich ändernden Stromfluss $\Phi$ in Spule 1 wird ein Magnetfeld im Eisenkern induziert, welches dazu führt, dass an Spule 2 wiederum eine Spannung auftritt, die anschließend besser genutzt werden kann.\\
Es kann dabei beliebig hoch- und heruntertransformiert werden, wobei dabei die Regulierung durch die Wahl der Windungszahlen der Spulen erfolgt. Diese Windungen bestehen üblicherweise aus Kupfer und sind bei beiden gleichsinnig gewickelt.
\begin{figure}[H]
\centering
\includegraphics[width=9cm]{Aufbauskizze.png}
\caption{shematische Darstellung eines Transformators mit orangenem Eisenkern \cite{UNI}} 
\label{fig:aufbau}
\end{figure}


\subsection{Idealer Transformator}
\label{subsec:idTrafo}
Nun werden wir ein paar Vereinfachungen vornehmen und aus den bisherigen Kenntnissen einen idealen Transformator modellieren.\\
Sein größter Unterschied zur Realität besteht darin, dass er verlustfrei arbeitet. Dies ist äußerst hilfreich bei der physikalisch-mathematischen Betrachtung des Versuchs, indem viel Zusammenhänge einfacher werden. Im Zuge dessen treten auch mehr oder weniger große Abweichungen auf, die im Anschluss analysiert werden.\\
Wird an der Primärspule ($N_1$ Windungen) die Spannung $U_1$ angelegt, so wird dort nach \cite[155]{DEM} eine Spannung
\begin{equation}
    U_{ind} = -U_1 = -N_1 \* \frac{\mathrm{d}\Phi_1}{\mathrm{dt}}
\end{equation}
induziert, welche der von außen angelegten Spannung entgegengesetzt ist, da nach dem Kirchoff'schen Gesetz
\begin{equation}
    U_1 + U_{ind} = 0
\end{equation}
gelten muss. Dabei ist $\Phi_1$ der elektrische Fluss in der Primärspule (Länge $L_1$, Querschnitt $A_1$)
\begin{equation}
    \Phi_1 = \frac{\mu_0  \mu_r  A_1  N_1}{L_1} \cdot I_1(t) \,\, .
\end{equation}

\subsubsection{Idealer unbelasteter Transformator}
Die Besonderheit des idealen unbelasteten Transformators ist, dass der Sekundärstromkreis nicht geschlossen ist und somit kein Strom $I_2$ fließen kann.
Analog zu Gleichung (1) wird auch eine Spannung $U_2$ in der Sekundärspule induziert. Fässt man diese beiden anschließend zusammen, kommt man auf
\begin{equation}
    \frac{U_1}{U_2} = -\frac{N_1}{N_2} ,
\end{equation}
was eine wesentliche Vereinfachung zum realen Transformator darstellt. Dabei sei explizit erwähnt, dass hier die Spulen gleichsinnig gewickelt sind, sodass es zu einer Phasenverschiebung um 180$^\circ$ kommt, sodass die Induktionsspannung der Erregerspannung entgegenwirkt (Lenz'sche Regel). Man kann an der Gleichung außerdem ohne Weiteres erkennen, dass man die Spannungsübersetzungen lediglich mit den Windungszahlen der verwendeten Spulen reguliert.\\
Hierbei ist zu beachten, dass ein idealisiertes Modell verlustfrei arbeitet, sodass genau die Leistung sekundär abgenommen wird. Es gilt mit $P=UI$ für diesen Fall $P_1=P_2$, sodass man
\begin{equation}
    \frac{I_2}{I_1} = -\frac{N_1}{N_2} = u
\end{equation}
feststellen kann. Diese Stromstärken-/Spannungsverhältnisse seien als Übersetzungsverhältnis $u$ bezeichnet, welches für Werte kleiner 1 hoch- und für Werte größer 1 heruntertransformiert.\\
Im unbelasteten Fall nimmt der Primärstromkreis auch keine Stromstärke auf.

\subsubsection{Idealer belasteter Transformator}
Nun wird der Sekundärstromkreis geschlossen, indem ein Verbraucher angeschlossen wird. Dadurch fließt Strom, das heißt $I_2$ sinkt, während der Primärstrom $I_1$ ebenfalls ansteigt. Dieses Phänomen liegt außerdem der Tatsache zugrunde, dass bei Transformatoren die angegebene Nennlast immer höher ist als im unbelasteten Fall.\\
Laut Lenz wird hier der Stromfluss $\Phi_1$ geschwächt, aber er ist durch die fest angelegte Spannungsquelle invariant. Daher muss, wie eben bereits erwähnt, ein zu $I_1$ phasenverschobener Anteil $I_1^{\prime}$ dazustoßen.\\
Da wir hierbei von einem idealen Transformator sprechen, gibt es keinen Leistungsverlust, sodass für das System der Zusammenhang
\begin{equation}
    \int_0^T \! U_1\underbrace{(I_1 + I_1^{\prime})}_{I_{ges}} \mathrm{d}t =  \int_0^T \! U_2I_2\mathrm{d}t
\end{equation}
gilt.
Wie eben bereits festgestellt, ist $I_1$ um $\pi/2$ phasenverschoben und es folgt direkt:
\begin{equation}
    U_1I_1^{\prime} = U_2I_2
\end{equation}
, wobei $U_1$ und $I_1^{\prime}$ in Phase sind.
Wir wollen die Phasenverschiebung ermitteln und nutzen dabei nur die Kenntnis über $I_1$ und $I_{ges}$. $U_1$ ist dabei nicht weiter relevant. Dieser Zusammenhang ist in Abb. \ref{fig: zeiger} anschaulich gezeigt.
 \begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{zeiger.png}
    \caption{Schematisches Zeigerdiagramm des Transformators für Messreihe 3 \cite{UNI4, Handbuch}} 
    \label{fig: zeiger}
\end{figure}
\noindent Anhand der dort gezeigten trigonometrischen Zusammenhänge kann man mit 
$$I_{1,R} = I_1^{\prime} = I_{1,R} + I_{1,T} \cos\phi$$
auf 
\begin{equation}
    \tan\phi = \frac{I_{1,T}\sin\phi}{I_{1,R}+I_{1,T}\cos\phi}
\end{equation}
schließen.\\
Da sowohl der Primärstrom $I_1$, als auch dessen phasenverschobenes Gegenstück $I_1^{\prime}$ dieselbe Amplitude besitzen kommt man auf:
\begin{equation}
    \cos\left(\frac{\phi}{2}\right) = \frac{I_{ges}}{2I_1^{\prime}}
\end{equation}


\subsection{Realer Transformator}
Beim realen Transformator müssen wesentlich  mehr Einflüsse in die gerechte Beschreibung integriert werden. Dazu zählen vor allem die Verlusteffekte, wie zum Beispiel die Streuung des Magnetfeldes des Eisenkerns um diesen herum. Diese ist zwar verhältnismäßig gering, jedoch nicht vernachlässigbar und man modelliert demzufolge den Idealfall mit einer relativen Permeabilität $\mu_r$ des Kerns, die unendlich groß ist.\\
Dazu kommen eventuelle Wirbelströme in der Aparatur, die zu beachten sind.\\
\\
Des Weiteren sei zu erwähnen, dass die aus Kupferdraht bestehenden Windungen der Spule ebenfalls einen Ohmschen Widerstand besitzen.\\
\\
Ein letzter Punkt ist die sogenannte Hysterese. Im Allgemeinen bezeichnet sie den Fakt, dass das Produkt eines Sytsems nicht nur vom Input sondern auch von den Ausgangssituationen des Systems abhängt. Bei Transformatoren sind diese stets unterschiedlich, da es sich hierbei um ein stetig veränderndes System handelt. Daraus folgen Abweichungen der Messwerte von der Realität, wenn von  gleichen Startmodalitäten ausgegangen wird.\\
Beschrieben wird dieses Phänomen durch eine sogenannte Hysteresekurve. Diese soll bei Transformatoren möglichst steil und schmal gehalten werden, um einen guten Wirkungsgrad zu erzielen, denn die dabei eingeschlossene Fläche ist ein Maß für die während des Prozesses abgegebene thermische Energie.
\begin{figure}[H]
\centering
\includegraphics[width=9cm]{hk.png}
\caption{Abtragung der Flussdichte gegen die Feldstärke als Hysteresekurve \cite{UNI1}} 
\label{fig: hysterese}
\end{figure}

\noindent Der Wirkungsgrad liegt im Allgemeinen in einem Argumentbereich von 0 bis 1. Transformatoren haben teilweise eine relativ hohe Nennleistung ($\ge99\%$) bzw. gewöhnliche mit über 95\%. Bei Kleintransformatoren liegt der Wert bei circa 80\% und bei sogenannten Kleinsttransformatoren bei kaum 50\%.\\
\\
Ein Beispiel für die Anwendung von (realen) Transformatoren ist Stromzange, welche wir auch während des Versuchs verwendet haben. Sie dient der berührungsfreien Messung von Strömen, indem sie den Leiter umschließende magnetische Felder detektiert und die dadurch in der Primärspule der Stromzange induzierte Spannung in messbare Spannungen transformiert.\\
\begin{figure}[H]
\centering
\includegraphics[width=9cm]{Stromzange.jpg}
\caption{im Versuch verwendete Stromzange \cite{UNI2}} 
\label{fig:stromzange}
\end{figure}

Eine ähnliche Wirkungsweise weißt der Spannungswandler auf. Er transformiert hingegen sehr hohe Spannungen von auch bis über 1MV auf messbare Spannungsbereiche um die 100V.
\begin{figure}[H]
\centering
\includegraphics[width=7.1cm]{Spannungswandler.jpg}
\caption{Spannungswandler von Hochspannungen \cite{UNI3}} 
\label{fig:spannungswandler}
\end{figure}
\newpage



\section{Durchführung}
\label{sec:durchfuehrung}
\subsection{Aufbau}
\label{subsec: aufbau}

\begin{figure}[H]
\centering
\includegraphics[width=11cm]{schaltplan.jpg}
\caption{Schaltplan des Versuchsaufbaus \cite{Handbuch}} 
\label{fig: schaltplan}
\end{figure}

Zentral in diesem Versuch ist der Transformator, welcher in der Mitte des Schaltplans (Abb. \ref{fig: schaltplan}) zu sehen ist. Dieser besteht aus einer Primär- und Sekundärspule, die um einen Eisenring gewickelt sind, um die Induktionsströme zu verstärken.\\
Insgesamt beinhaltet der Aufbau vier Multimeter, vier Schalter und drei Widerstände, wovon zwei Schiebewiderstände sind. Die gesamte Schaltung ist an eine Wechselspanungsquelle angeschlossen, die eine regulierbare Spannung zwischen \unit[0]{V} und \unit[260]{V} erzeugen kann.\\ \par
Im Stromkreis der Primärspule liegt ein Multimeter an, mit dem die Primärspannung $U_1$ gemessen werden kann. Mit dem zweiten Multimeter wird der Primärstrom $I_1$ abgelesen. Des Weiteren kann über Schalter 1 und 2 der Stromkreis der Primärspule geschlossen werden. Dabei kann der Transformator unabhängig von dem Schiebewiderstand $R_1$ geschalten werden und umgekehrt. Der letzte Schalter des Primärstromkreises hat die Aufgabe, den Widerstand $R_2$ zu überbrücken, wenn er während des Versuchs nicht gebraucht wird.\\
\indent Der Stromkreis der Sekundärspule ist einfacher aufgebaut. Durch den Schalter kann der Stromkreis geschlossen werden und somit über den Schiebewiderstand $R_3$ die Stromstärke $I_2$ geregelt werden, welche über ein Multimeter gemessen wird. Wenn der Stromkreis geschlossen ist, ist der Transformator belastet. Mit dem letzten zu erwähnenden Multimeter wird die Sekundärspannung $U_2$ ermittelt.
\newpage



\subsection{Messreihen mit unbelastetem Transformator}
\label{subsec: unbtrafo}
In beiden folgenden Messreihen ist der Sekundärkreislauf offen, das heißt, dass der Transformator unbelastet ist.

\subsubsection{Messreihe 1}
\label{subsubsec: 1}
Als erstes wird ohne Inbetriebnahme jeglicher Widerstände, also bei geschlossenem Schalter 1 und offenem Schalter 2, die Stromstärke $I_1$ in Abhängigkeit von der Spannung $U_1$ gemessen. Dabei wird die Spannung im Bereich von \unit[0]{V} bis \unit[20]{V} variiert. Insgesamt wurden so 18 Messwertpaare aufgenommen. 

\subsubsection{Messreihe 2}
\label{subsubsec: 2}
Nun wird die Sekundärspannung $U_2$ in Abhängigkeit von der Primärspannung $U_1$ gemessen. Hierbei ist $U_1$ wieder im Bereich zwischen \unit[0]{V} und \unit[20]{V}.\\
Durch das Umstecken der Anschlüsse des Transformators kann man die Windungszahlen halbieren. So nutzen wir zunächst nur die Hälfte der Windungen der Sekundärspule und danach die gesamte Windungszahl.  \\
Für beide Fälle nahmen wir 20 Messwertpaare auf.

\subsection{Messreihen mit belastetem Transformator}
\label{subsec: belastrafo}
Für die letzten beiden Messreihen ist der Sekundärkreislauf geschlossen. So ist der Transformator bis zum Ende der Versuchsdurchführung belastet.

\subsubsection{Messreihe 3}
\label{subsubsec: 3}

Ziel dieser Messreihe ist es den Gesamtstrom $I_{ges}$ zu ermitteln, welcher im Primärkreislauf gemessen wird.
In Abbildung \ref{fig: teil3} ist die Reihenfolge der zu öffnenden und zu schließenden Schalter dargestellt.\\
Zu Beginn wird der Sekundärstrom $I_2$ mit Hilfe des Schiebewiederstandes $R_3$ auf einen Wert zwischen \unit[0]{A} und \unit[1.5]{A} eingestellt. Währenddessen wird sich der Strom im Primärkreislauf $I_1$ gemerkt.\\
Im zweiten Schritt ist nur der Primärkreislauf von Bedeutung. Hier ist nun der Schiebewiederstand $R_1$ in Reihe mit dem Multimeter, welches die Stromstärke misst, geschalten. der Transformator wird durch den offenen Schalter 1 nicht mehr relevant. Jetzt kann durch den Schiebewiederstand $R_1$ die Stromstärke auf den sich vorher zu merkenden Wert geregelt werden.\\
Abschließend werden alle Schalter geschlossen. So kann $I_{ges}$ am Multimeter des Primärkreislaufs abgelesenen werden.\\
Alle drei Schritte werden für sieben verschiedene Anfangs Sekundärstromwerte $I_2$ durchgeführt. Wir haben die Messungen für \unit[0.24]{A}, \unit[0.31]{A}, \unit[0.5]{A}, \unit[0.7]{A}, \unit[0.99]{A}, \unit[1.2]{A} und \unit[1.4]{A} durchgeführt. So erhält man am Ende sieben verschiedene Gesamtstromwerte.

    \begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{teil3.png}
    \caption{Schaltplan zur Veranschaulichung der dritten Messreihe \cite{UNI4}} 
    \label{fig: teil3}
    \end{figure}

\subsubsection{Messreihe 4}
\label{subsubsec: 4}

In der letzten Messreihe werden anhand eines Oszilloskops die Phasenverschiebung anhand des Zeitunterschieds zweier Signale. Hierbei wurden Diagramme im x-t-Modus und x-y-Modus erstellt und ausgedruckt.\\
Im Prinzip wird die Messung aus Messreihe 3 wiederholt mit den gleichen Werten für $I_2$, aber ohne den Widerstand $R_1$ im Primärkreis. Dieser wird durch Schalter 2 aus dem Schaltkreis ausgeschlossen. Die Stromsträrke wird nun über die verstellbare Spannungsquelle geregelt.\\
Die beiden Messsignale, die das Oszilloskop empfängt, ist zum einen die Primärspannung, welche über einen Tastkopf des Oszilloskop erfasst wird, und den Primärstrom, der mit Hilfe einer Stromzange gemessen wird.\\
Am Ende erhält man so jeweils 6 Bilder für beide Koordinatenachsenwahlen, also insgesamt 12 Oszilloskop ausdrucke.

\section{Auswertung}
\label{sec:auswertung}
\subsection{Primärspannung in Abhängigkeit vom Primärstrom bei unbelastetem Transformator}
In der ersten Messreihe haben wir die Primärspannung in Abhängigkeit vom Primärstrom gemessen. Die Messwerte sind in Abbildung \ref{fig: ui} graphisch dargestellt. Es ist ein lineares Wachstum der Stromstärke bei Erhöhung der Spannung zu beobachten. In Hinblick auf die Funktionsweise eines idealen Transformators ist zu sagen, dass dies den Erwartungen eines idealen Transformators entspricht für Spannungen $< \unit[20]{V}$.

\begin{figure}[H]
    \centering
    \includegraphics[width=11cm]{uiplot.png}
    \caption{$U_1$ in Abhängigkeit von $I_1$} 
    \label{fig: ui}
    \end{figure}

\subsection{Übersetzungsverhältnisse des unbelasteten Transformators}

Nun tragen wir die Primärspannung $U_1$ gegen die Sekundärspannung $U_2$ ab.
Aus dem linearen Fit beider Kurven konnten wir so das Übersetzungsverhältnis beider Messreihen ermitteln. Zum einen haben wir bei genau der Hälfte der Windungszahlen (Abb. \ref{fig: u1}) und zum anderen bei gleicher Windungszahl von Primär- und Sekundärspannung (Abb. \ref{fig: u2})  Messwerte genommen. Die Steigungen der Ausgleichsgeraden sind in Tabelle \ref{fits} nachzulesen. \\
So sind die Übersetzungsverhältnisse $u_1$ und $u_2$ für beide Messungen $u_1 = 0.496 \pm 0.0002$ und $u_2 = 0.993 \pm 0.0002$, was genau den ermittelten Steigungen entspricht. Da der Achsenabschnitt t für beide Messungen sehr klein ist, vernachlässigen wir diesen Faktor.

\begin{figure}[H]
    \centering
    \includegraphics[width=10.5cm]{u1plot.png}
    \caption{$U_2$ in Abhängigkeit von $U_1$ bei unterschiedlicher Windungszahlen der Primär- und Sekundärspule} 
    \label{fig: u1}
    \end{figure}
    
\begin{figure}[H]
    \centering
    \includegraphics[width=10.5cm]{u2plot.png}
    \caption{$U_2$ in Abhängigkeit von $U_1$ bei gleicher Windungszahl der Primär- und Sekundärspule} 
    \label{fig: u2}
    \end{figure}
    
\begin{table}[]
\centering
\caption{Ergebnis des Linearen Fits zum Zusammenhang zwischen $U_2$ und $U_1$}
\label{fits}
\begin{tabular}{l|l|l|l}
Zusammenhang            & Steigung m          & Achsenabschnitt t  & \\
$U_2$ = $m_1 \cdot U_1$ + t & 0.4960 $\pm$ 0.0002 & -0.012 $\pm$ 0.002 & Abbildung \ref{fig: u1}\\
$U_2$ = $m_2 \cdot U_1$ + t& 0.993 $\pm$ 0.0002  & -0.006 $\pm$ 0.005 & Abbildung \ref{fig: u2}
\end{tabular}
\end{table}


\subsection{Berechnung des Phasenwinkels $\phi$}


\subsubsection{Berechnung Anhand Messreihe 3}

 \begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{zeiger.png}
    \caption{Schematisches Zeigerdiagramm des Transformators für Messreihe 3 \cite{UNI4, Handbuch}} 
    \label{fig: zeiger}
    \end{figure}

Das in  Abbildung \ref{fig: zeiger} dargestellte Diagramm zeigt den Zusammenhang zwischen dem Phasenwinkel $\phi$, dem Gesamtstrom $I_{ges}$ und dem gemessenden Primärstrom. Dabei ist $I_{1,T}$, der mit dem Transformator gemessene Primärstrom und $I_{1,R}$, der mit dem Widerstand gemessene Primärstrom \cite{Handbuch}.
Der Zusammenhang zwischen Phasenwinkel und Gesamtstrom lautet demnach (Abb. \ref{fig: zeiger}):
\begin{equation}
    cos\left(\frac{\phi}{2}\right) = \frac{I_{ges}}{2 I_1}
\end{equation}

\noindent Daraus folgt sofort, dass das Verhältnis $\frac{I_{ges}}{2 I_2}$ kleiner oder gleich 1 sein muss, da der $\cos$ nur Werte zwischen -1 und 1 annimmt und in unserem Fall nur positive Werte betrachtet werden.\\
Aufgrund kleiner Messungenauigkeiten sind 3 unserer Messwerte um 0.04 bis 0.08 über 1. Die restlichen Winkel sind in Tabbelle \ref{tab: Winkel1} aufgelistet, die sich nach Umstellen von Gleichung 6 nach $\phi$ bestimmen lassen. 

\begin{table}[]
\centering
\caption{Phasenwinkel zwischen Primärspannung $U_1$ und Primärstrom $I_1$ in Abhängigkeit von $I_2$}
\label{tab: Winkel1}
\begin{tabular}{l|l|l|l|l|l|l}
$I_2$ in [A] &  $I_{ges}$ in [A] & $\frac{I_{ges}}{2I_2}$ & $\phi$ [Rad]\\
0.24 & 0.52   &  1.084            & -           \\
0.31 &   0.67   &            1.081               & -          \\
0.5  &       1.04   &               1.040                           & -           \\
0.7  &          1.39   &              0.992              & 0.24 \\
0.99 &          1.95   &              0.985               & 0.35   \\
1.2  &       2.26   &           0.942            & 0.67 \\
1.4  &          2.6    &              0.929           & 0.76
\end{tabular}
\end{table}

\subsubsection{Berechnung Anhand Messreihe 4 bzw. Lissajous-Figuren}

Nun bestimmen wir den Phasenwinkel anhand der Oszilloskopausdrucke, die diesem Protokoll beigefügt sind. Die Bilder im x-y Modus werden auch als Lissajous-Figuren bezeichnet.\\
Die Werte für $a$ und $b$, welche dem Primärstrom bzw- -spannung entsprechen. Dabei haben wir den maximalen Ausschlag auf den Lissajous-Figuren gemessen.\\
Dabei haben wir den Fehler $\sigma_{I_2}=0{,}2$ geschätzt.\\
Die Fehler von den Messungen für $a$ und $b$ haben wir aufgrund der unscharfen Ausdrucke der Figuren den Fehler $\sigma_a=\sigma_b=0{,}3$ genutzt.\\
\begin{table}[H]
\centering
\caption{Messwerte zur Bestimmung des Winkels $\phi$}
\label{phi}
\begin{tabular}{l|l|l|l}
$I_2 [A]$ & $a$ & $b$& $\phi \mathrm{[rad]}$ \\
1.4       & 2         & 7.5       & 0.27         \\
1.2       & 4         & 7         & 0.608        \\
1         & 3.25      & 7         & 0.483        \\
0.7       & 2.5       & 8         & 0.318        \\
0.5       & 3.5       & 8         & 0.453        \\
0.24      & 3.25      & 8         & 0.418       
\end{tabular}
\end{table}

Der Fehler für $\phi$ ergibt sich aus folgender Fehlerformel:
\begin{equation}
    \sigma_{\phi}=\frac{1}{b}\sqrt{\frac{a^2\sigma_b^2+b^2\sigma_a^2}{b^2-a^2}}\,\,.
\end{equation}


\subsubsection{Graphische Darstellung und Vergleich der Ergebnisse für $\phi$}
Nachdem auf zwei Weisen der Phasenwinkel $\phi$ bestimmt wurde, werden nun beide Ergebnisse \ref{fig: shit} aufgetragen.\\
Der theoretische Verlauf der Kurve ergibt sich aus:

\begin{equation}
    \tan(\phi) = \frac{I_0 \sin(\phi_{0})}{I_1 + I_0 \cos(\phi_{0})}
\end{equation}
Dabei sind $I_0$ und $\phi_0$ der Strom und die Phase bei unbelastetem Transformator. $I_1$ lässt sich aus dem zweiten Übersetzungsverhältnis 

\begin{equation}
    I_1 = \frac{N_1}{N_2} \cdot I_2
\end{equation}
 bestimmen, welches im zweiten Auswertungspunkt ermittelt wurde. Da die Messung bei $I_2 = \unit[0]{A}$ nicht möglich war, können wir leider keinen theoretischen Verlauf der Kurve berechnen, da wie mit den Schiebewiderständen $I_1$ nicht soweit herunter regeln konnten (vgl. \ref{subsubsec: 3}).
 \begin{figure}[H]
    \centering
    \includegraphics[width=10cm]{plot.png}
    \caption{Vergleich der ermittelten Phasenwinkel aus Messreihe 3 und 4 ohne theoretische Kurve} 
    \label{fig: shit}
    \end{figure}

 
\subsection{Wirkleistung und Verlustleistung bei \unit[1.4]{A}}

Die Wirk- und Verlustleistung lässt sich über folgende Gesetzmäßigkeiten bestimmen \cite[S. 156]{DEM}:
\begin{align}
    P_w = U_1 \cdot I_1 \cdot cos(\phi) \\
    P_v = U_1 \cdot I_1 \cdot sin(\phi)
\end{align}

\noindent So ist zu erkennen, dass sich die Wirk- und Verlustleistung sich je nach Phasenwinkel verändern. Für unsere Maximallast \unit[1.4]{A} ergibt sich ein Phasenwinkel $\phi$ zu 0.76 Rad (bestimmt aus dem Zeigerdiagramm).\\
Als Fehler für den Strom $\sigma_{I_1}$ wählen wir \unit[0.02]{A}. Da aus unseren Messwerten für den Phasenwinkel kein Fehler errechnet werden konnte, da wir ein Verhältnis berechnet haben, bei dem sowohl der Divisor als auch der Dividend den gleichen Fehler hatten, benötigen wir an dieser Stelle keine weiter Fehlerberechnung.\\
Man erhält nach Einsetzen der Werte $I_1 = \unit[2.6]{mA}$ und dazugehörigem $U_1 = \unit[15.2]{V}$ für
 $P_w = \unit[17.69]{mW}$ und $P_v = \unit[16.81]{mW}$. 



\subsection{Energieverschwendung eines Handyladegeräts}

Um zu ermitteln, wie viel Geld pro Jahr bei einem Preis von $0.25 \frac{\text{\euro}}{\mathrm{kWh}}$ des Elektrizitätsversorgers gezahlt werden muss, wenn ein Handyladegerät in der Steckdose verbleibt, nutzen wir folgenden Zusammenhang:

\begin{equation}
\mathrm{Preis_{ges}} = {365 \mathrm{Tage} \cdot 24 \mathrm{Stunden} \cdot P_v\cdot 0.25} \cdot \frac{1}{1000}\,\,.
\end{equation}


\noindent Da wir keine Werte für eine Leerlaufleistung $I_2$ aufnehmen konnten, können wir hier leider keinen sinnvollen Wert für $P_v$ einsetzen, um einen Wert für $\mathrm{Preis}_{ges}$ zu bestimmen. 

\newpage

\section{Diskussion}
\label{sec:diskussion}
Im Allgemeinen kann man sagen, dass die Messwerte bei der Arbeit mit Elektrizität, wie in diesem Fall, meist genauer sind, als beispielsweise in der Mechanik. Aufgrund der der fehlenden großen Anzahl an Störeinflüssen der Umgebung, waren die gezeigten Auswertungen häufig theorienah.\\
Gut zu sehen waren diese geringen Fehler bei den Plots in der Auswertung, wo die gefitteten Geraden nahezu perfekt die Punkte enthalten. Die im Theorieteil beschriebenen Verlusteffekte des realen Transformators sind dabei noch am signifikantesten, wie zum Beispiel die Wirbelströme um den Eisenkern oder die magnetische Streuung um eben diesen.\\
\\
Im zweiten Teil ging es ja um eine ähnliche Messung, sodass es hierbei zu den gleichen störenden Phänomenen kam.\\
Die daraus zu bestimmenden Übersetzungsverhältnisse $u$ sollten laut der Theorie $1$ und $1/2$ sein. Bei uns erreichen sie diese Werte nicht genau, was damit in Zusammenhang zu bringen ist, dass die im Transformator verwendete Spulen auch einen Ohmschen Widerstand besitzen. Daraus ergeben sich die geringen Abweichungen bei den Windungszahlen.\\
Auch die bestimmte Verschiebung des Plots in y-Richtung wurde hier vernachlässigt, da sie im Größenverhältnis zu den anderen Messgrößen verschwindend klein ist. Davon völlig unabhängig entstehen diese nicht signifikanten Änderungen auch nur aus der mangelnden Präzision beim Messvorgang.\\
\\
Im dritten Versuchsteil waren die auftretenden Fehler erstmals wirklich stark ergebnisbeeinflussend. Es kam vermutlich daher, dass während des Versuchsverlaufes die Spannung manuell nachgeregelt werden musste. Damit einher treten ebenfalls (wenn auch geringe) Induktionsströme auf, die das Messergebnis mitbeeinflussen. Genau wie weitere technische Fehler zum Beispiel bei der Eichung der Widerstände, die sich auch nicht einwandfrei bedienen lassen haben.\\
Speziell in unserem Fall kam es während der Versuchsdurchführung dazu, dass wir nach der anfänglichen analogen Messung bei einem Gerät auf ein digitales wechseln mussten.\\
Des Weiteren musste dieser Abschnitt des Versuchs mehrmals wiederholt werden, wobei viele Veränderungen des Schaltplans und der Spannungen und Ströme die Folge war. Diese Veränderungen waren natürlich von uns nicht zu 100\% reproduzierbar, sodass es eine gewisse Abweichung vom Erstversuch gegeben haben wird. Ein Stichwort sei in diesem Zusammenhang auch genannt: Hysterese. Sie wurde schon im Theorieteil erläutert.\\
Im Anschluss waren dazu Verhältnisse $I_{ges}/2I_2$ zu berechnen. In der Theorie sollte dieser Wert maximal 1 betragen. Hier war es jedoch der Fall, dass die Errechneten um $0{.}04$ bis $0{.}08$ darüber liegen. Daraus lässt sich schlussfolgern, dass es auch hier zu wirklich signifikanten Messfehlern kam.\\
In Folge dessen sind die daraus bestimmten Winkel $\phi$ sehr ungenau und entsprechen nicht der Erwartung. Teilweise konnte er gar nicht bestimmt werden, was mit dem falschen zuvor festgestellten Verhältnis zu erklären ist. In den Bereichen $0{.}5$ bis $1{.}5$A landete man zumindest in der gleichen Größenordnung, die auch realitätsnah ist.\\
\\
Die Auswertung der Lissajous-Figuren war auch dementsprechend ungenau. Hinzu kam hierbei die Unschärfe der Ausdrucke, sodass die beim Ablesen auftretenden Fehler groß sind.\\
In diesem Versuchsteil ging es auch darum einen theoretischen Verlauf Winkels in Abhängigkeit von dem Strom abzutragen, jedoch waren für die genaue Bestimmung dieser Kurve die Kenntnis über $I_0$ (Strom bei unbelastetem Transformator) und $\phi_0$ (Phasenverschiebung bei unbelastetem Transformator) nötig. Sie fehlte uns allerdings, sodass dieser Auswertungspunkt leider nicht weiter bearbeitet werden konnte.\\
Generell kann man bei so einer Art Auswertung davon ausgehen, dass die resultierenden Fehler groß sind, da hierbei zu den technischen Mängeln zusätzlich die fehlende Perfektion des Menschen beim Ablesen dazukommt. In Diagramm \ref{fig: shit} sind aufgrund der stark von der Realität abweichenden Werte keine Fehlerbalken eingetragen, da diese Werte nicht zur weiteren Berechnung genutzt werden können.\\
\\
Bei der Bestimmung der Wirk- und Verlustleistung kam es in diesem Zuge zu genauso starken Abweichungen. Da die Winkelbestimmung sehr ungenau und fehlerbehaftet aufgrund von Messgenauigkeiten ist, sprechen wir diesen Ergebnissen keine hohen Wahrheitsgehalt zu, da die Wirkleistung viel höher sein sollte als die Verlustleistung.\\
\\
Wie schon weiter oben erwähnt, blieb uns die Messung des Leerlaufs verwehrt, sodass auch im letzten Abschnitt der Auswertung kein sinnvoller Wert ermittelt werden kann.\\
 
\newpage

\printbibliography

\end{document}


