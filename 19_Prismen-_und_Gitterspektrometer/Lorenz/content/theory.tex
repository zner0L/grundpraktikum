\subsection{Spektrum eines Prismas}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{img/prisma.pdf}
	\caption{Strahlengang durch ein Prisma \cite{lp_prisma_svg}.}
	\label{fig:prisma}
\end{figure}

Prismen sind homogene, optische Medien mit dreieckiger Grundfläche. Der Brechungsindex $n(\lambda)$ eines Prismas ist höher als der des Umgebungsmediums, sodass eintreffende Lichtstrahlen zum Prisma hin gebrochen werden. Die Abhängigkeit dieses Brechungsindexes von der Wellenlänge des eintreffenden Lichts $\lambda$, also die Dispersion des Prismas $\frac{\mathrm{d}n}{\mathrm{d}\lambda}$, wird genutzt, um mithilfe eines Prismas Lichtspektren zu erzeugen. 

\subsubsection{Winkeldispersion}

Ist der Strahlengang durch das Prisma symmetrisch (s. Abbildung \ref{fig:prisma}), also $\beta_1 = \beta_2$, so wird der Ablenkwinkel $\delta$ und damit auch die optische Weglänge minimal \cite[Kap. 3.2]{zinth11}. Mit dem Snellius'schen Brechungsgesetz lässt sich somit aus dem minimalen Winkel $\delta_{min}$ der Brechungsindex $n$ für ein Prisma bestimmen. Es gilt die Fraunhofer-Formel \cite[Kap. 10.1.5]{gerthsen10}

\begin{equation}
n = \frac {\sin \left( \frac{\delta_{\text{min}} + \varepsilon} {2}\right)}
{\sin \left( \frac{\varepsilon}						{2}\right)} \; .
\label{eq:fraunhofer}
\end{equation}

Da der Brechungsindex im Allgemeinen von der Wellenlänge abhängt und nach der Fraunhofer-Formel also auch der minimale Ablenkwinkel, ändert sich mit der Wellenlänge auch $\delta_{min}$. Diese Beziehung lässt sich in der Winkeldispersion $D$ eines Prismas, der Änderung des Winkels mit der Wellenlänge, zusammenfassen \cite[Kap. 11.5]{demtroeder13}:

\begin{equation}
D = 	\frac{\text{d} 	\delta}		{\text{d} \lambda}
= 	\left(\frac{\partial \delta}{\partial n}\right) 
\cdot 	\frac{\text{d} 	n}			{\text{d} \lambda}
= \frac{2 \cdot \sin \left( \frac{\varepsilon}					{2}\right)}{\cos \left( \frac{\delta_{\text{min}} + \varepsilon} {2}\right)} \cdot 	\frac{\text{d} 	n}			{\text{d} \lambda} \; .
\label{eq:winkeldispersion}
\end{equation}

\subsubsection{Auflösungsvermögen eines Prismas}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{img/resolution.pdf}
	\caption{Geometrie eines teilbestrahlten Prismas \cite[bearbeitet]{lp_prisma_resolution_svg}.}
	\label{fig:resolution}
\end{figure}

Die Auflösung $A$ eines Spektrometers beschreibt, ob bestimmte Wellenlängen noch von einander zu unterscheiden sind, oder sie scheinbar auf denselben Punkt abgebildet werden. Quantitativ wird sie definiert durch das Rayleigh-Kriterium \cite[Kap. 4.5.1]{zinth11}:

\begin{equation}
	A = \frac{\lambda}{\Delta \lambda} \; .
	\label{eq:rayleigh}
\end{equation}

Definiert ist so die benötigte Auflösung, um bei der Wellenlänge $\lambda$ noch Spektrallinien mit einem Wellenlängenunterschied von $\Delta \lambda$ auflösen zu können. Für ein Prisma gilt mit der Strahlbreite $S$ nach \cite[Kap. 11.5.3]{demtroeder13} für das Auflösungsvermögen:

\begin{equation}
	A = S \cdot D \; .
	\label{eq:resolution_prisma}
\end{equation}

Es folgt mit Gleichung \ref{eq:winkeldispersion} dann:

\begin{equation}
	A = 2 \, S \cdot\frac{ \sin \left( \frac{\varepsilon}					{2}\right)}{\cos \left( \frac{\delta_{\text{min}} + \varepsilon} {2}\right)} \cdot 	\left| \frac{\text{d} n}{\text{d} \lambda} \right|
	= : B \left| \frac{\text{d} n}{\text{d} \lambda} \right| \; ,
	\label{eq:resolution_exp}
\end{equation}

wobei $B$ die effektive Basisbreite bezeichnet, wie sie in Abbildung \ref{fig:resolution} eingezeichnet ist. 

\subsection{Spektralbeugung am Gitter}

Im Bild eines Interferenzgitters mit Gitterkonstante $g$ können auch Spektren beobachtet werden. Licht der Wellenlänge $\lambda$ besitzt ein Intensitätsmaximum $m$-ter Ordnung bei einem Gangunterschied von $\Delta r = m \cdot \lambda$. Daraus folgt für den Winkel $\alpha_m$ des $m$-ten Maximums \cite[Kap. 4.3.5]{zinth11}:

\begin{equation}
\sin \alpha_{\text{m}} = \frac{m \, \lambda}{g} \; , \qquad m \in \mathbb{Z} \; .
\label{eq:gitter_max}
\end{equation}

Für das Auflösungsvermögen von Spektrallinien an der Ordnung $m$ eines Gitters mit $N$ beleuchteten Spalten gilt schließlich \cite[Kap. 4.5.1]{zinth11}:

\begin{equation}
	A = m \cdot N \; .
	\label{eq:gitta_resolution}
\end{equation}
