\subsection{Prismenspektrometer}

\subsubsection{Strahlengang}
\label{sec:schematic}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.3\textwidth]{img/strahlengang_prismenspektrometer.pdf}
	\caption{Schematischer Strahlengang im Prismenspektrometer.}
	\label{img:strahlengang}
\end{figure}


Abbildung \ref{img:strahlengang} zeigt schematisch den Strahlengang im Prismenspektrometer. Die drei vermessenen Spektrallinien sind beispielhaft eingezeichnet, um die Dispersion im Prisma zu verdeutlichen.

\subsubsection{Normierung der Winkel}
\label{sec:angle_norm}

Die mit dem Nonius von der Skala abgelesenen Winkel sind mit einem geschätzten Ablesefehler von $\sigma_\delta = 2'$ belastet. Relevant sind nicht die absoluten abgelesenen Winkel, sondern die Winkel $\delta$ in Bezug zur optischen Achse. Da diese bei $\delta_0 = 90^\circ$ verläuft, werden alle Winkel mit $\delta_0$ normiert. Aus den je drei Messwerten lässt sich schließlich ein Mittelwert bilden, sodass die verwendeten Winkel sich ergeben zu:

\begin{align}
\delta_{ge} & = (49.29 \pm 0.02)\, ^{\circ} \; \notag\\
\delta_{gr} & = (49.63 \pm 0.01)\, ^{\circ} \; .
\label{eq:delta_diff}
\end{align}

Die Fehler ergeben sich aus dem quadratischen Fehler des Mittelwerts.

\subsubsection{Dispersion der Spektralanteile}
\label{sec:ev_dispersion}

Mithilfe der Fraunhofer-Formel (Gleichung \ref{eq:fraunhofer}) lassen sich aus den bestimmten Winkeln die Brechungsindizes für die jeweiligen Spektrallinien bestimmen. Für den Scheitelwinkel gilt $\varepsilon = 60^\circ$, da ein Prisma mit gleichseitiger Grundfläche beleuchtet wurde. Somit ergeben sich:

\begin{align}
n_{ge} 	& = 1.9156 \pm 0.0003 \notag\\
n_{gr}		& = 1.9209 \pm 0.0002 \; .
\end{align}

Nach der Gauß'schen Fehlermethode ergibt sich der Fehler aus

\begin{equation*}
\sigma_n = \sigma_{\delta} \cdot
\frac   {\cos \left( \frac{\delta_{\text{min}} + \varepsilon} {2}\right)}
{2 \, \sin \left( \frac{\varepsilon}						  {2}\right)} \; . 
\end{equation*}

Unter der Voraussetzung eines proportionalen Zusammenhangs von Wellenlänge $\lambda$ und Brechungsindex $n$, kann die Dispersion als konstant angenommen werden. Diese ergibt sich aus den Differenzen der Wellenlängen $\Delta \lambda$ und berechneten Brechungsindizes $\Delta n$. Für die Wellenlängen werden $\lambda_{ge} = 579.07 \,  \text{nm}$ für die gelbe und $\lambda_{gr}  = 546.07 \,  \text{nm}$ für die grüne Spektrallinie verwendet \cite[Kap. 19.7]{knetter17}. Diese Werte werden als fehlerfrei angenommen. Es gilt dann:

\begin{equation}
 \frac{\Delta n}{\Delta \lambda} = \frac{n_{gr} - n_{ge}}{\lambda_{gr} - \lambda_{ge}} = (1.62 \pm 0.10) \cdot 10^5 \, \frac{1}{\text{m}} \; ,
\label{eq:dispersion_exp}
\end{equation}

wobei für den Fehler gilt:

\begin{equation*}
\sigma_{\frac{\text{d} 	n}{\text{d} \lambda}} 
= \sqrt{\left(\frac{\sigma_{n_{ge}}}{\lambda_{ge} - \lambda_{gr}}\right)^2 +\left(\frac{\sigma_{n_{gr}}}{\lambda_{ge} - \lambda_{gr}}\right)^2} \; .
\label{eq:disp_exp}
\end{equation*}

Unter den gleichen Annahmen lässt sich die theoretische Dispersion im Flintglasprisma mit Literaturwerten berechnen. Betrachtet wird die Differenz des Brechungsindex $\Delta n = 0.017$ zwischen der Natrium-D-Linie bei $\lambda_{\text{D}} = 589.3 \, \text{nm}$ und der Wasserstoff-$\beta$-Linie $\lambda_{\beta} = 486.1 \text{nm}$ \cite[Kap. 19.7]{knetter17}. Also ergibt sich für den als fehlerfrei angenommenen Literaturwert der Dispersion:

\begin{equation}
\frac{\Delta 	n}{\Delta \lambda} = \frac{\Delta n}{\lambda_{\text{D}} - \lambda_{\beta}} = 1.65 \cdot 10^5 \, \frac{1}{\text{m}} \; .
\label{eq:dispersion_theo}
\end{equation}

\subsubsection{Auflösungsvermögen des Prismas}
\label{sec:resolution_prisma}

Über das Rayleigh-Kriterium (Gleichung \ref{eq:rayleigh}) lässt sich das minimale Auflösungsvermögen  $A_{min}$ bestimmen, das nötig ist um die gelbe Doppellinie des Hg-Spektrums aufzulösen. Dazu werden die Literaturwerte aus Abschnitt \ref{sec:ev_dispersion} sowie ein weiterer Wert für die äußere gelbe Spektrallinie $\lambda_{ge,2} = 576.96 \; \text{nm}$ verwendet \cite[Kap. 19.7]{knetter17}:

\begin{equation}
A_{\text{min}} = \frac{\lambda_{ge,1}}{\lambda_{ge,1} - \lambda_{ge,2}} = 274.4 \; .
\label{eq:a_theo_prisma}
\end{equation}

Experimentell ergibt sich das Auflösungsvermögen nach Gleichung \ref{eq:resolution_exp} über die effektive Basisbreite $B$ des beleuchteten Prismas. Gemessen wurde dazu die Strahlbreite $S$ über die Spaltöffnung des Auflösungsspalts. Es wird ein Skalenfehler von einem Skalenteil, also $\sigma_S = 1 \cdot 10^{-5} \, \text{m}$, angenommen. Für diese gilt dann mit dem Winkel der inneren gelben Linie $\delta_{ge}$:

\begin{equation}
B 	= 2 S \cdot \frac	{\sin \left(\frac{\varepsilon}{2} \right)}
{\cos\left(\frac{\delta_{ge} + \varepsilon} {2}\right)}
= \frac	{S}
{\cos\left(\frac{\delta_{ge} + \varepsilon} {2}\right)} \; .
\label{eq:spaltbreite_eff}
\end{equation}

Für den Fehler gilt:

\begin{equation*}
\sigma_B = \sqrt{
	\left(\sigma_S \cdot   
	\frac{1}
	{\cos\left(\frac{\delta_{ge} + \varepsilon} {2}\right)} \right)^2 + 
	\left( \sigma_{\delta_{ge}} \cdot 
	\frac{S \sin\left(\frac{\delta_{ge} + \varepsilon} {2}\right)}
	{2 \, \cos^2\left(\frac{\delta_{ge} + \varepsilon} {2}\right)} \right)^2
} \; .
\end{equation*}

Mit der in Abschnitt \ref{sec:ev_dispersion} bestimmten Dispersion und der zuvor bestimmten effektiven Basisbreite lässt sich dann nach Gleichung \ref{eq:resolution_exp} das minimale experimentelle Auflösungs\-vermögen bestimmen, das nötig ist, um die gelbe Doppellinie aufzulösen:

\begin{equation}
A_{\text{exp,min}} = B \left| \frac{\text{d} n}{\text{d} \lambda} \right| = (1.07 \pm 0.07) \cdot 10^3  \; ,
\label{eq:a_exp_prisma}
\end{equation}

wobei sich der Fehler ergibt zu

\begin{equation*}
\sigma_A = \sqrt{
	\left(\sigma_B \left| \frac{\text{d} n}{\text{d} \lambda} \right| \right)^2 + 
	\left( \sigma_{\left| \frac{\text{d} n}{\text{d} \lambda} \right|} B \right)^2
} \; .
\end{equation*}

Aus dem Literaturwert für die Dispersion und der tatsächlichen Basislänge des Prismas von $B_{tat} = 33 \, \text{mm}$ \cite[Kap. 19.7]{knetter17}, die hier als fehlerfrei angenommen wird, lässt sich das maximale Auflösungsvermögen des Prismas nach Gleichung \ref{eq:resolution_exp} bestimmen:

\begin{equation}
A_{\text{max}} = B_{\text{tat}} \left| \frac{\text{d} n}{\text{d} \lambda} \right| \approx 5436 \; .
\label{eq:a_max_prisma}
\end{equation}

Und aus dem Rayleigh-Kriterium lässt sich dann die kleinste noch trennbare Wellenlänge rund um die gelbe Doppellinie bestimmen. Es gilt für die kleinste auflösbare Wellenlängendifferenz $\Delta \lambda$:

\begin{equation}
\Delta \lambda = \frac{\lambda_{ge}}{ A_{\text{max}} } = 0.107 \, {\text{nm}}  \; .
\label{eq:kleinstes_lambda_prisma}
\end{equation}

Am Rand des Spektralbereichs des Hg-Spektrums an der kleinsten, noch sichtbaren Wellenlänge $\lambda_{min} = 404.66 \, \text{nm}$ \cite[Kap. 19.7]{knetter17} lässt sich die global kleinste überhaupt auflösbare Wellenlängendifferenz ausrechnen, sie ergibt sich zu:

\begin{equation*}
	\Delta \lambda_{min} = 0.074 \text{nm}
\end{equation*}

% ----------- Gitterspektrometer ---------- %

\subsection{Gitterspektrometer}

\subsubsection{Bestimmung der Gitterkonstante aus den Beugungswinkeln}

Nach Gleichung \ref{eq:gitter_max} besteht ein Zusammenhang zwischen den gemessenen Beugungswinkeln $\alpha_{m}$ an der Ordnung $m$ des Lichtes der Wellenlänge $\lambda$. Die Messwerte zu den Beugungswinkeln werden wie in Abschnitt \ref{sec:angle_norm} normiert, es gilt der gleiche Fehler für $\alpha_{m}$. Es gilt:

\begin{equation}
g = \frac{m \, \lambda}{\sin \alpha_{\text{m}}} \; ,
\label{eq:gitter_max2}
\end{equation}

die Ergebnisse sind in Tabelle \ref{tab:g_ergebnisse} aufgeführt. Für $\lambda$ werden die Literaturwerte aus \cite[Kap. 19.7]{knetter17} verwendet, diese sind somit fehlerfrei. Für den Fehler der Gitterkonstante gilt demnach:

\begin{equation*}
\sigma_{g} = \sigma_{\alpha_{m}} \, m \, \lambda \, \frac{\cos \alpha_{m}}{\sin^2\alpha_{m}}	\; .
\end{equation*}

Aus den Ergebnissen für alle Ordnungen lässt sich schließlich ein gewichtetes Mittel bilden, sodass sich für die Gitterkonstante ergibt:

\begin{equation}
g = 1.654 \pm 0.006 \, \mu \text{m} \; .
\label{eq:g_exp}
\end{equation}

Der Fehler ist der quadratische Fehler des Mittelwertes. \\ 

\begin{table}[!htb]
	\centering
	\caption{Gitterkonstante $g$ aus Gleichung \ref{eq:gitter_max2} für drei gemessene Spektrallinien des Hg-Spektrums. Wellenlängen aus \cite[Kap. 19.7]{knetter17}}.
	\begin{tabular}{|ll|c|c|}
		\hline
		&& \multicolumn{2}{c|}{Gitterkonstante $g$ [$\mu \text{m}$]} 	\\
		\hline
		\multicolumn{2}{|l|}{Spektrallinie} & 1. Ordnung & 2. Ordnung \\
		\hline\hline
		gelb & $\lambda_{ge} = 579.07 \, \text{nm}$ & $1.666 \pm 0.004$ & $1.661 \pm 0.002$\\
		\hline
		grün & $\lambda_{gr} = 546.07 \, \text{nm}$ & $1.652 \pm 0.003$ & $1.648 \pm 0.001$ \\
		\hline
		violett  & $\lambda_{vi} = 407.66 \, \text{nm}$ & $1.686 \pm 0.003$ & $1.651 \pm 0.009$ \\
		\hline
	\end{tabular}
	\label{tab:g_ergebnisse}
\end{table}

\subsubsection{Wellenlängendifferenz}
\label{sec:delta_lambda}

Aus Gleichung \ref{eq:gitter_max} lässt sich die Wellenlängendifferenz von zwei Spektrallinien $\Delta \lambda$, hier die Differenz der grünen und gelben Linie, berechnen:
\begin{equation}
\Delta \lambda = \lambda_{gr} - \lambda_{ge}
= \frac{g \cdot \left( \sin (\alpha_{gr,m}) -  \sin (\alpha_{ge,m})\right) }{m} \; .
\label{eq:lambda_diff}
\end{equation}
Die Gitterkonstante ist dabei $g = 1/600 \, \text{mm}$, der Literaturwert aus \cite[Kap. 19.7]{knetter17}. Für den Fehler gilt:
\begin{equation*}
\sigma_{\Delta \lambda} 
= \sqrt{ 
	\left( \sigma_{\alpha_{gr,m}} \cdot \frac{g}{m} \cos (\alpha_{gr,m})\right)^2 +
	\left( \sigma_{\alpha_{ge,m}} \cdot \frac{g}{m} \cos (\alpha_{ge,m})\right)^2
} \; .
\end{equation*}

Tabelle \ref{tab:lambda_diff_exp} führt die Ergebnisse der Wellenlängendifferenz für beide Beugungsordnungen auf. Ein gewichteter Mittelwert ergibt mit dem quadratischen Fehler:

\begin{equation}
\Delta \lambda = 
	30.7 \pm 0.5 \, \text{nm} \; .
\label{eq:lambda_diff_exp}
\end{equation}

\begin{table}[!htb]
	\centering
	\caption{Ergebnisse für die Wellenlängendifferenz $\Delta \lambda$ zwischen der gelben und grünen Spektrallinie in beiden Ordnungen.}
	\begin{tabular}{|l|c|}
		\hline
		& Wellenlängendifferenz $\Delta\lambda$ [$\text{nm}$]\\
		\hline\hline
		1. Ordnung 	&  $21 \pm 1.2$ \\
		\hline
		2. Ordnung 	&  $32.0 \pm 0.5$\\
		\hline
	\end{tabular}
	\label{tab:lambda_diff_exp}
\end{table}

\subsubsection{Auflösungsvermögen des Gitters}

Für das minimale Auflösungsvermögen eines Gitters gilt nach Gleichung \ref{eq:gitta_resolution} mit der Anzahl der beleuchteten Spalte, bestimmt aus der Strahlbreite $S$ über $N = \frac{S}{g}$:

\begin{equation}
A = m \cdot N = \frac{m\, S}{g}
\end{equation}

und somit dem Fehler:

\begin{equation*}
\sigma_A = \sqrt{
	\left(\sigma_S \frac{m}{g}\right)^2
	+ \left(\sigma_g \frac{mS}{g^2}\right)^2
} \; .
\end{equation*}

Der Fehler der Strahlbreite bleibt wie in Abschnitt \ref{sec:resolution_prisma}, für die Gitterkonstante wird der Mittelwert aus Gleichung \ref{eq:g_exp} verwendet. Für beide Ordnungen ergibt sich dann :

\begin{equation}
A_{\text{exp},1}
	= (222 \pm 4)  \quad \text{und} \quad	
A_{\text{exp},2}
	= (169 \pm 7)  \; .
\label{eq:a_exp_gitter}
\
\end{equation}

Das theoretische Auflösungsvermögen nach Abschnitt \ref{sec:resolution_prisma} gilt unabhängig vom Spektralapparat und ist deshalb auch für das Gitter unverändert.

Aus dem Durchmesser der Kreisblende des Gitters $d_{\text{aper}} = 1.9 \, \text{cm}$ und der Gitterkonstante $g = 1/600 \, \text{mm}$ \cite[Kap. 19.7]{knetter17} kann aus Gleichung \ref{eq:gitta_resolution} das maximale Auflösungsvermögen in der 1. Ordnung bestimmt werden:

\begin{equation}
A_{\text{max}} = m \cdot N = g \cdot d_{\text{aper}}  = 11400 \; .
\label{eq:a_max_gitta}
\end{equation}

\subsubsection{Violette Spektrallinie}

Die Wellenlänge der violetten Spektrallinie $\lambda_{vi}$ kann ähnlich wie in Abschnitt \ref{sec:delta_lambda} durch Umstellen von Gleichung \ref{eq:gitter_max} bestimmt werden. Für $g$ wird der bereits erwähnte Literaturwert verwendet, $\alpha_m$ ist ein Messwert, ebenfalls wie zuvor:

\begin{equation}
\lambda_{vi} = \frac{g \,\sin \alpha_{m}}{m} .
\label{eq:lambda_vio}
\end{equation}

Für den Fehler gilt:

\begin{equation*}
\sigma_{\lambda_{vi}} =
\sqrt{
	\left( \sigma_g \frac{\sin \alpha_{m}}{m} \right)^2 + 
	\left( \sigma_{\alpha_{m}} \frac{g}{m} \cos \alpha_{m} \right)^2 
} \; .
\end{equation*}

Tabelle \ref{tab:lambda_v} enthält die Ergebnisse für beide Ordnungen. Das gewichtete Mittel mit quadratischem Fehler ergibt sich zu

\begin{equation}
\lambda_{vi} = (408.9 \pm  0.4) \, \text{nm} \; .
\label{eq:violett_exp}
\end{equation}

\begin{table}[!htb]
	\centering
	\caption{Wellenlänge $\lambda_{vi}$ der mittelhellen violetten Linie des Hg-Spektrums aus Messwerten für beide Ordnungen.}
	\begin{tabular}{|l|c|}
		\hline
		& Wellenlänge $\lambda_{vi}$ [$\text{nm}$]\\
		\hline\hline
		1. Ordnung 	&  $408 \pm 1$ \\
		\hline
		2. Ordnung 	&  $409.1 \pm 0.4$\\
		\hline
	\end{tabular}
	\label{tab:lambda_v}
\end{table}