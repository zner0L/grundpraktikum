# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

# Fitfunktion defnieren
def fit_linear(x, a, b):
        return a * x + b

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

# Messreihen benennen, Daten einlesen aus Datei
data_prisma = np.genfromtxt('data/prisma.csv', delimiter=' ', usecols=range(3))
data_gitta = np.genfromtxt('data/gitta.csv', delimiter=' ', usecols=range(4))

# Mittelwert bilden
angle_prism_yellow = [np.mean(data_prisma[:, 0]), stats.sem(data_prisma[:, 0])]
angle_prism_green = [np.mean(data_prisma[:, 1]), stats.sem(data_prisma[:, 1])]

print(u'---- Winkel ----')
print(u' - \delta_{min} Gelb: ' + str(angle_prism_yellow))
print(u' - \delta_{min} Grün: ' + str(angle_prism_green))


# ---- Prismenspektrometer ----
print('---- Prismenspektrometer ----')
# Fraunhofer-Formel
# Fraunhofer-Formel
fraun = ma.uncertainty.Sheet('sin((dmin*pi/180 + eps)/2)/sin(eps/2)', 'n')
fraun.set_value('eps', math.pi/4, tex='\esiplon')
fraun.set_value('dmin', tex='\delta_{min}')
n = fraun.batch(np.array([angle_prism_yellow, angle_prism_green]), 'dmin|dmin%')

print(u' - Brechungsindizes:')
print(n)

# Dispersion
dispersion = ma.uncertainty.Sheet('(n2 - n1)/(la2 - la1)')
dispersion.set_value('la1', 579.07e-9) # gelb
dispersion.set_value('la2', 546.07e-9) # gruen
dispersion.set_value('n2', n[0, 0], n[0, 1]) # gruen
dispersion.set_value('n1', n[1, 0], n[1, 1]) # gelb

dispersion = dispersion.get_result(mode='ufloat')
print(u' - exp. Dispersion: ' + str(dispersion))

theory_dispersion = 0.017/(589.3e-9-486.1e-9)
print(u' - theo. Dispersion: ' + str(theory_dispersion))

# Auflösungsvermögen
resolution_theory = 579.07/(579.07-576.96)
print(u' - theo. Auflösungsvermögen: ' + str(resolution_theory))

resolution = ma.uncertainty.Sheet('disp*s/cos((d*pi/180+eps)/2)')
resolution.set_value('eps', math.pi/4, tex='\esiplon')
resolution.set_value('d', angle_prism_yellow[0], angle_prism_yellow[1])
resolution.set_value('disp', dispersion.n, dispersion.s)
resolution.set_value('s', 4.52e-3, 0.01e-3)

print(u' - exp. Auflösungsvermögen: ' + str(resolution.get_result(mode='ufloat')))

max_resolution = theory_dispersion * 33e-3 # dn/dlamdba * B
smallest_wavelength_delta = 404.66e-9/max_resolution # lambda_gelb/max_resolution

print(u' - Maximales Auflösungsvermögen: ' + str(max_resolution))
print(u' - kleinste aufgelöste Wellenlängendifferenz (bei 404.66 nm): ' + str(smallest_wavelength_delta))

# ---- Gitterspektrometer ----
print('---- Gitterspektrometer ----')
# Gitterkonstante
gitta_const = ma.uncertainty.Sheet('(la*n)/sin(x*pi/180)', 'g')
gittas_family = gitta_const.batch(data_gitta, 'x|x%|n|la')
print(gittas_family)
print(u'gew. Mittelwert der Gitterkonstanten: ' + str(ma.data.weighted_average(gittas_family, mode='ufloat')))

# TODO: delta alpha und delta beta

# Wellenlängendifferenz
delta_lambda = ma.uncertainty.Sheet('g * (sin(pi/180*a2) - sin(pi/180*a1))/m')
delta_lambda.set_value('g', 1e-3/600)
for_delta = np.array([[1, data_gitta[1, 0], data_gitta[1, 1], data_gitta[2, 0], data_gitta[2, 1]],
                      [2, data_gitta[4, 0], data_gitta[4, 1], data_gitta[5, 0], data_gitta[5, 1]]])
deltas = delta_lambda.batch(for_delta, 'm|a1|a1%|a2|a2%')
print(u' - Delta Lambda für beide Ordnungen:')
print(deltas)
print(u'gewichteter Mittelwert: ' + str(ma.data.weighted_average(deltas)))

# Auflösungsvermögen
resolution_gitta = ma.uncertainty.Sheet('n * s/g')
resolution_gitta.set_value('n', 1)
resolution_gitta.set_value('s', 0.37e-3, 0.01e-3)
resolution_gitta.set_value('g', 1e-3/600)
resolution_gitta_first_order = resolution_gitta.batch(gittas_family[[0,1,2], :], 'g|g%')
resolution_gitta.set_value('s', 0.14e-3, 0.01e-3)
resolution_gitta.set_value('n', 2)
resolution_gitta_second_order = resolution_gitta.batch(gittas_family[[3,4,5], :], 'g|g%')

print(u' - Auflösungsvermögen 1. Ordnung:')
print(resolution_gitta_first_order)
print(u'gew. Mittelwert: ' + str(ma.data.weighted_average(resolution_gitta_first_order)))
print(u' - Auflösungsvermögen 2. Ordnung:')
print(resolution_gitta_second_order)
print(u'gew. Mittelwert: ' + str(ma.data.weighted_average(resolution_gitta_second_order)))

# maximales Auflösungsvermögen
max_resolution_gitta = 19*600
print(u' - maximales Auflösungsvermögen: ' + str(max_resolution_gitta))

# violette Wellenlänge
lambda_violet = ma.uncertainty.Sheet('g*sin(x*pi/180)/n', '\lambda')
lambda_violet.set_value('g', 1e-3/600)
lilalo = lambda_violet.batch(data_gitta[[0, 3], :], 'x|x%|n|f')
print(u' - Wellenlänge Lambda: ' )
print(lilalo)
lambda_violet = ma.data.weighted_average(lilalo)
print(u'gew. Mittelwert: ' + str(lambda_violet))
