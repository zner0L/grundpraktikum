\subsection{Das Prismenspektrometer}

%_________________________________________________________________%

\subsubsection{Strahlengänge und Bestimmung der Winkeldifferenzen}
\label{sec:winkeldiff}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth]{img/strahlengang_fertig.png}
	\caption{Skizze des Strahlengangs im Prismenspektroskop. Beispielhaft wurde der Verlauf von blauem und rotem Licht eingezeichnet. Der Auflösungsspalt ist so weit geöffnet, dass daran keine Beugung stattfindet.}
	\label{img:strahlengang}
\end{figure}


In Abbildung \ref{img:strahlengang} ist der Strahlengang im Prismenspektroskop dargestellt. Der Fehler der mit dem Nonius der Winkelskala gemessenen Werte wird wegen der Schwierigkeiten beim Ablesen auf zwei Skalenteile zu $\sigma_{\delta_0} = 2 \, '$ geschätzt. Für die Auswertung ist der Ablenkwinkel $\delta$ des Lichts von der optischen Achse von Interesse, welcher aus  den gemessenen Winkeln $\delta_0$ bestimmt werden soll. Da die Messskala um $90^{\circ}$ gegenüber der optischen Achse verschoben ist, muss zur Bestimmung der tatsächlichen Winkeldifferenzen von den gemessenen Werten je $90^{\circ}$ subtrahiert werden. Der sich ergebende Fehler ist dabei nach Gauß $\sigma_{\delta} = \sqrt{2} \cdot \sigma_{\delta_0}$. Für die tatsächlichen Winkeldifferenzen der linken gelben und der grünen Spektrallinie ergeben sich als Mittelwert der drei Messungen in Gradmaß
\begin{align}
	\delta_1 & = (49.29 \pm 0.02)\, ^{\circ} \; \notag\\
	\delta_2 & = (49.63 \pm 0.01)\, ^{\circ} \; .
	\label{eq:delta_diff}
\end{align}
Der Fehler ist der quadratische Fehler des Mittelwerts. 

%_________________________________________________________________%

\subsubsection{Dispersion der gelben und grünen Linie}
\label{sec:ev_dispersion}

Die Wellenlängen der linken gelben Spektrallinie $\lambda_1 = 579.07 \,  \text{nm}$ und der grünen Linie $\lambda_2  = 546.07 \,  \text{nm}$ sind der Praktikumsanleitung entnommen \cite[Kap. 19.7]{knetter17} und werden als fehlerfrei angenommen. Das verwendete Prisma ist gleichwinklig, somit beträgt der brechende Winkel $\varepsilon = 60^{\circ}$ und wird ebenfalls als fehlerfrei angenommen. Nach Frauenhofer (Gleichung \ref{eq:frauenhofer}) lassen sich die Brechungsindizes für die beiden Wellenlängen bestimmen:  
\begin{align}
	n_1 	& = (1.9156 \pm 0.0003) \notag\\
	n_2		& = (1.9209 \pm 0.0001) \; .
\end{align}
Aufgrund des geringen Fehlers und zur Erhöhung der Genauigkeit der nachfolgenden Rechnung werden mehr Nachkommastellen angegeben als üblich. Der Fehler berechnet sich dabei zu
\begin{equation*}
	\sigma_n = \sigma_{\delta} \cdot
	\frac   {\cos \left( \frac{\delta_{\text{min}} + \varepsilon} {2}\right)}
			{2 \, \sin \left( \frac{\varepsilon}						  {2}\right)} \; . 
\end{equation*}
Für die als konstant angenommene Dispersion ergibt sich somit
\begin{equation}
	\boxed{ \frac{\text{d} 	n}{\text{d} \lambda} \approx \frac{\Delta n}{\Delta \lambda} = \frac{n_2 - n_1}{\lambda_2 - \lambda_1} = (1.6 \pm 0.1) \cdot 10^5 \, \frac{1}{\text{m}} }
	\label{eq:dispersion_exp}
\end{equation}
mit dem Fehler
\begin{equation*}
 	\sigma_{\frac{\text{d} 	n}{\text{d} \lambda}} 
	= \sqrt{\left(\frac{\sigma_{n_2}}{\lambda_2 - \lambda_1}\right)^2 +\left(\frac{\sigma_{n_1}}{\lambda_2 - \lambda_1}\right)^2} \; .
\end{equation*}
Der theoretische Wert der Dispersion berechnet sich aus den im Praktikumshandbuch gegebene Werten \cite[Kap. 19.7]{knetter17}: Zwischen der Natrium-D-Linie bei $\lambda_{\text{D}} = 589.3 \, \text{nm}$ und der Wasserstoff-$\beta$-Linie bei $\lambda_{\beta} = 486.1 \, \text{nm}$ hat das Prisma eine mittlere Differenz des Brechungsindexes von $\Delta n = 0.017$. Hieraus ergibt sich der Theoriewert der Dispersion zu 
\begin{equation}
	\boxed{ \frac{\text{d} 	n}{\text{d} \lambda} \approx   \frac{\Delta n}{\lambda_{\text{D}} - \lambda_{\beta}} = 1.65 \cdot 10^5 \, \frac{1}{\text{m}}  } \; .
	\label{eq:dispersion_theo}
\end{equation}
Die dem Handbuch entnommenen Werte werden als fehlerfrei angenommen. 

%_________________________________________________________________%

\subsubsection{Auflösungsvermögen des Prismas}

\label{sec:auflvmg_prisma}

Nach Gleichung \ref{eq:vermoegen_theo} berechnet sich das Auflösungsvermögen, das theoretisch mindestens nötig ist, um die beiden gelben Linien voneinander trennen zu können:
\begin{equation}
	\boxed{ A_{\text{theo}} = \frac{\lambda_1}{\lambda_1 - \lambda_3} = 274  } \; .
	\label{eq:a_theo_prisma}
\end{equation}
Dabei ist $\lambda_1$ wie in Abschnitt \ref{sec:ev_dispersion} die Wellenlänge der linken gelben Spektrallinie, $\lambda_3 = 576,96 \, {\text{nm}}$ nach \cite[Kap. 19.7]{knetter17} die Wellenlänge der rechten gelben Linie, die ebenfalls als fehlerfrei angenommen wird. Der Bündelquerschnitt $S$ des auf das Prisma fallenden Strahls wurde mit dem Mikrometertrieb gemessen. Der Fehler wird auf einen ganzen Skalenteil zu $\sigma_S = 10 \, \mu\text{m}$ angenommen. Nach Gleichung \ref{eq:vermoegen_exp} berechnet sich die effektive Basisbreite $B$ zu  
\begin{equation}
	B 	= 2 S \cdot \frac	{\sin \left(\frac{\varepsilon}{2} \right)}
					{\cos\left(\frac{\delta + \varepsilon} {2}\right)}
		= \frac	{S}
		{\cos\left(\frac{\delta_1 + \varepsilon} {2}\right)} \;  ,
		\label{eq:spaltbreite_eff}
\end{equation}
dabei wird für $\delta = \delta_1$ der Winkel zur linken gelben Sepktrallinie verwendet. Der Fehler ergibt sich nach Gauß:
\begin{equation*}
	\sigma_B = \sqrt{
		\left(\sigma_S \cdot   
		\frac{1}
		{\cos\left(\frac{\delta_1 + \varepsilon} {2}\right)} \right)^2 + 
		\left( \sigma_{\delta_1} \cdot 
		\frac{S \sin\left(\frac{\delta_1 + \varepsilon} {2}\right)}
		{2 \, \cos^2\left(\frac{\delta_1 + \varepsilon} {2}\right)} \right)^2
		} \; .
\end{equation*}
Hieraus berechnet sich weiterhin nach Gleichung \ref{eq:vermoegen_exp} mit der zuvor experimentell bestimmten Dispersion aus Gleichung \ref{eq:dispersion_exp} das Auflösungsvermögen, bei dem die beiden Linien gerade noch getrennt werden können zu 
\begin{equation}
	\boxed{A_{\text{exp}} = B \left| \frac{\text{d} n}{\text{d} \lambda} \right| = (1.07 \pm 0.07) \cdot 10^3  } \; ,
	\label{eq:a_exp_prisma}
\end{equation}
mit dem Fehler
\begin{equation*}
	\sigma_A = \sqrt{
		\left(\sigma_B \left| \frac{\text{d} n}{\text{d} \lambda} \right| \right)^2 + 
		\left( \sigma_{\left| \frac{\text{d} n}{\text{d} \lambda} \right|} B \right)^2
		} \; .
\end{equation*}
Das maximal erreichbare Auflösungsvermögen $A_{\text{max}}$ ergibt sich aus der als fehlerfrei angenommenen Basislänge des Prismas $B_{\text{max}} = 33 \, \text{mm}$ \cite[Kap. 19.7]{knetter17}. Nach Gleichung \ref{eq:vermoegen_exp} berechnet sich mit dem Theoriewert der Dispersion aus Gleichung \ref{eq:dispersion_theo}: 
\begin{equation}
	\boxed{ A_{\text{max}} = B_{\text{max}} \left| \frac{\text{d} n}{\text{d} \lambda} \right| = 5436 } \; ,
	\label{eq:a_max_prisma}
\end{equation}
Umstellen von Gleichung \ref{eq:vermoegen_theo} ergibt für die kleinste noch trennbare Wellenlängendifferenz in einer Umgebung der linken gelben Spektrallinie nach dem Rayleigh Kriterium mit den Theoriewerten
\begin{equation}
	\boxed{\Delta \lambda_1 = \frac{\lambda_1}{ A_{\text{max}} } = 107 \, {\text{pm}} } \; .
	\label{eq:kleinstes_lambda_prisma}
\end{equation}
Die insgesamt kleinste trennbare Wellenlängendifferenz wird in einer Umgebung der starken violetten Spektrallinie mit der kleinsten Wällenlänge von $\lambda_{\text{v,s}} = 404.66  \, \text{nm}$ erreicht \cite[Kap. 19.7]{knetter17}:
\begin{equation}
	\lambda_{\text{v,s}} = 74 \, \text{pm}
\end{equation}


%_________________________________________________________________%

%_________________________________________________________________%

\subsection{Das Gitterspektrometer}

%_________________________________________________________________%

\subsubsection{Gitterkonstante}

Umformen von Gleichung \ref{eq:gitter_max} ergibt für die Gitterkonstante $g$
\begin{equation}
	g = \frac{m \, \lambda}{\sin \alpha_{\text{m}}} \; ,
	\label{eq:gitter_max2}
\end{equation}
wobei $m$ die Ordnung des Maximums in Richtung $\sin \alpha_{\text{m}}$ bezeichnet und $\lambda$ die Wellenlänge der betrachteten Linie. Die tatsächlichen Winkeldifferenzen werden aus den Messwerten wie in Abschnitt \ref{sec:winkeldiff} berechnet, auch der Fehler von $\alpha_{\text{m}}$ wird analog behandelt. Die Werte für $\lambda$ aus \cite[Kap. 19.7]{knetter17} werden als fehlerfrei angenommen, somit berechnet sich der Fehler der Gitterkonstante zu 
\begin{equation*}
	\sigma_{g} = \sigma_{\alpha_{\text{m}}} m \lambda	
				 \frac{\cos \alpha_{\text{m}}}{\sin^2\alpha_{\text{m}}}	\; .
\end{equation*}
Die Ergebnisse für $g$ sind in Tabelle \ref{tab:g_ergebnisse} aufgeführt. Als gewichteter Mittelwert aus den sechs Messungen ergibt sich für die Gitterkonstante
\begin{equation}
	\boxed{ g = 1.654 \pm 0.006 \, \mu \text{m} } \; .
	\label{g_exp}
\end{equation}
Der Fehler ist der quadratische Fehler des Mittelwertes. \\ 

\begin{table}[!htb]
	\centering
	\caption{Ergebnisse für die Gitterkonstante $g$ nach Gleichung \ref{eq:gitter_max2} für die einzelnen Spektrallinien und Ordnungen.}
	\begin{tabular}{|l|c|c|}
		\hline
		  & \multicolumn{2}{c|}{Gitterkonstante $g$ [$\mu \text{m}$]} 	\\
		\hline
		Spektrallinie & 1. Ordnung & 2. Ordnung \\
		\hline\hline
		linke gelbe & $1.666 \pm 0.004$ & $1.661 \pm 0.0017$\\
		\hline
		grün & $1.652 \pm 0.003$ & $1.648 \pm 0.001$ \\
		\hline
		violett & $1.686 \pm 0.003$ & $1.651 \pm 0.009$ \\
		\hline
	\end{tabular}
	\label{tab:g_ergebnisse}
\end{table}

%_________________________________________________________________%

\subsubsection{Wellenlängendifferenz}

Umstellen von Gleichung $\ref{eq:gitter_max2}$ liefert für die Wellenlängendifferenz zweier Linien $\lambda_a$ und $\lambda_b$
\begin{equation}
	\Delta \lambda = \frac{g \, \sin (\Delta \alpha_{\text{m}})}{m}
	= \frac{g \cdot \left( \sin (\alpha_{\text{m,b}}) -  \sin (\alpha_{\text{m,a}})\right) }{m}
	 \; ,
	 \label{eq:lambda_diff}
\end{equation}
wobei $\sin (\Delta \alpha_{\text{m}})$ als Kurzschreibweise der Differenz der Sinusse von den Winkeln $\alpha_{\text{m,a}}$ und $\alpha_{\text{m,b}}$ zu den entsprechenden Spektrallinien eingeführt wurde. Für $g = 1/600 \, \text{mm}$ wurde die fehlerfreie Gitterkonstante aus \cite[Kap. 19.7]{knetter17} verwendet. Der Fehler ist dabei
\begin{equation*}
	\sigma_{\Delta \lambda} 
	= \sqrt{ 
		 \left( \sigma_{\alpha_{\text{m,a}}} \cdot \frac{g}{m} \cos (\alpha_{\text{m,a}})\right)^2 +
		\left( \sigma_{\alpha_{\text{m,b}}} \cdot \frac{g}{m} \cos (\alpha_{\text{m,b}})\right)^2
		} \; .
\end{equation*}
Die Ergebnisse für die Differenzen zwischen den linken gelben und grünen Spektrallinien erster bzw. zweiter Ordung sind in Tabelle \ref{tab:lambda_diff_exp} aufgeführt. Als gewichteter Mittelwert ergibt sich
\begin{equation}
	\boxed{\Delta \lambda = 
	(30.7 \pm 0.5) \, \text{nm} } \; .
	\label{eq:lambda_diff_exp}
\end{equation}
Der Fehler ist dabei der Fehler des Mittelwertes. 

\begin{table}[!htb]
	\centering
	\caption{Ergebnisse für die Wellenlängendifferenz $\Delta \lambda$ zwischen den linken gelben und den grünen Spektrallinien erster bzw. zweiter Ordnungen nach Gleichung \ref{eq:lambda_diff}.}
	\begin{tabular}{|l|c|}
		\hline
		& Wellenlängendifferenz $\Delta\lambda$ [$\text{nm}$]\\
		\hline\hline
		1. Ordnung 	&  $21 \pm 1.2$ \\
		\hline
		2. Ordnung 	&  $32.0 \pm 0.5$\\
		\hline
	\end{tabular}
	\label{tab:lambda_diff_exp}
\end{table}

%_________________________________________________________________%

\subsubsection{Auflösungsvermögen des Gitters}

Die Spaltbreite $S$ und ihr Fehler werden wie in Abschnitt \ref{sec:auflvmg_prisma} behandelt. Zusammen mit dem Ergebnis der Gitterkonstante $g$ (Gleichung \ref{g_exp}) ergibt sich aus Gleichung \ref{eq:vermoegen_gitter} für das Auflösungsvermögen des Gitters
\begin{equation}
	A = m \cdot N = \frac{m\, S}{g}
\end{equation}
mit dem Fehler
\begin{equation*}
	\sigma_A = \sqrt{
		\left(\sigma_S \frac{m}{g}\right)^2
		+ \left(\sigma_g \frac{mS}{g^2}\right)^2
		} \; .
\end{equation*}
Für die erste und zweite Ordnung berechnet sich
\begin{equation}
	\boxed{ {A_{\text{exp}}}^{(1)} 
		= (222 \pm 4) }  \qquad	
	\boxed{ {A_{\text{exp}}}^{(2)} 
		= (169 \pm 7) } \; .
	\label{eq:a_exp_gitter}
	\
\end{equation}
Das theoretische Auflösungsvermögen für die Doppellinie berechnet sich wie in Abschnitt \ref{sec:auflvmg_prisma}, der Wert entspricht dem aus Gleichung \ref{eq:a_theo_prisma}: 
\begin{equation}
	A_{\text{theo}} = \frac{\lambda_1}{\lambda_1 - \lambda_3} = 274 \; .
	\label{eq:a_theo_gitter}
\end{equation}

Das maximale Auflösungsvermögen ergibt sich mit dem Durchmesser der Kreisblende vor dem Gitter $d_{\text{aper}} = 1.9 \, \text{cm}$ und der Gitterkonstante $g = 1/600 \, \text{mm}$ (Werte aus \cite[Kap. 19.7]{knetter17} als fehlerfrei angenommen) ebenfalls nach Gleichung \ref{eq:vermoegen_gitter} für die Linien erster Ordnung zu 
\begin{equation}
	\boxed{ A_{\text{max}} = m \cdot N = g \cdot d_{\text{aper}}  = 11400 } \; .
	\label{eq:a_max_gitta}
\end{equation}


%_________________________________________________________________%

\subsubsection{Wellenlänge der violetten Linie}

Zur Berechnung der Wellenlänge der mittleren violetten Linie $\lambda_{\text{v}}$ wird Gleichung \ref{eq:gitter_max2} umgestellt zu 
\begin{equation}
	 \lambda_{\text{v}} = \frac{g \,\sin \alpha_{\text{m}}}{m} .
	 \label{eq:lambda_vio}
\end{equation} 
Der Fehler berechnet sich zu
\begin{equation*}
	\sigma_{\lambda_{\text{v}}} =
	\sqrt{
		\left( \sigma_g \frac{\sin \alpha_{\text{m}}}{m} \right)^2 + 
		\left( \sigma_{\alpha_{\text{m}}} \frac{g}{m} \cos \alpha_{\text{m}} \right)^2 
		} \; .
\end{equation*}
Die Ergebnisse sind in Tabelle \ref{tab:lambda_v} aufgeführt.  Als gewichteter Mittelwert aus den beiden Ordnungen ergibt sich
\begin{equation}
	\boxed{ \lambda_{\text{v}} = (408.9 \pm  0.4) \, \text{nm} } \; .
	\label{eq:violett_exp}
\end{equation}
Der Fehler ist der quadratische Fehler des gewichteten Mittelwertes.

\begin{table}[!htb]
	\centering
	\caption{Ergebnisse für die Wellenlänge $\lambda_{\text{v}}$ der mittelstarken violetten Spektrallinie nach Gleichung \ref{eq:lambda_vio}.}
	\begin{tabular}{|l|c|}
		\hline
			& Wellenlänge $\lambda_{\text{v}}$ [$\text{nm}$]\\
		\hline\hline
		1. Ordnung 	&  $408 \pm 1$ \\
		\hline
		2. Ordnung 	&  $409.1 \pm 0.4$\\
		\hline
	\end{tabular}
	\label{tab:lambda_v}
\end{table}

\newpage