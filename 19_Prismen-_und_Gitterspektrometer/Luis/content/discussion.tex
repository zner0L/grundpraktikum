
\subsection{Das Prismenspektrometer}

\subsubsection{Winkeldifferenzen}

Die Messung der Winkel, aus denen sich die tatsächlichen Winkeldifferenzen in Gleichung \ref{eq:delta_diff} berechnen, führt zu Fehlern. Die durch das Fernrohr beobachteten Spektrallinien haben eine endliche Ausdehnung. Bei der Messung wird versucht, die Mitte der Linie zu treffen, was jedoch nach Augenmaß geschieht und somit zu Messfehlern führt. Zusätzlich erschwert wird die Messung durch die zur Horizontalen verdrehten Stellung des Fadenkreuzes, was anfangs irritiert. Um ein genaueres Ablesen zu ermöglichen, müsste das Fadenkreuz neu justiert werden. 


\subsubsection{Dispersion}

Der Theoriewert der Dispersion $\text{d}n / \text{d} \lambda$ aus Gleichung \ref{eq:dispersion_theo} liegt innerhalb des 1-$\sigma$-Intervalls des experimentell bestimmten Wertes aus Gleichung \ref{eq:dispersion_exp}. Zur Bestimmung des Theoriewertes wurde die mittlere Änderung des Brechungsindexes zwischen der Natrium-D- und der Wasserstoff-$\beta$-Linie verwendet. Das Ergebnis legt nahe, dass der Brechungsindex $n$ von Flintglas innerhalb dieses Intervalls nicht stark schwankt. Auch die bei der gemessenen Dispersion verwendete Näherung des Differentials durch kleine Differenzen in Gleichung \ref{eq:dispersion_exp} erscheint zulässig. Die Dispersion von Flintglas ist im gemessenen Wellenlängenbereich offenbar näherungsweise konstant, der Brechungsindex wird also näherungsweise linear von der Wellenlänge abhängen.

\subsubsection{Auflösungsvermögen}
\label{sec:auflvmg_prisma_discussion}

Das Auflösungsvermögen $A_{\text{theo}}$ aus Gleichung \ref{eq:a_theo_prisma}, welches theoretisch nötig ist, um die beiden gelben Linien trennen zu können, liegt um ein Vielfaches außerhalb des 5-$\sigma$-Intervalls des experimentell bestimmten Wertes $A_{\text{exp}}$ in Gleichung \ref{eq:a_exp_prisma} und beträgt etwa ein Viertel des experimentellen Wertes. Der Fehler $\sigma_{A_{\text{exp}}}$ rührt vom Mikrometertrieb bei der Bestimmung der Strahlenbreite $S$ und von Winkelmessung des Ablenkwinkels $\delta_1$ her. Die Werte für $\delta_1$ liefern bei der Bestimmung der Dispersion sehr gute Ergebnisse (vgl. vorherigen Abschnitt). Die große Abweichung des Auflösungsvermögens wird folglich in der Bestimmung der Spaltbreite $S$ begründet liegen. Die beiden Linien sind nur sehr schwer getrennt voneinander zu erkennen und müssen eine längere Zeit mit dem Auge fixiert werden, bis sie überhaupt zu unterscheiden sind. Hinzu kommt, dass das Rayleigh Kriterium in der Praxis schwer anzuwenden ist, da eine Unterscheidung der Linien auf bloßem Augenmaß beruht. Somit erscheint es plausibel, dass der Spalt nicht so weit verkleinert wurde, wie es dem Rayleigh Kriterium entspräche, und hierdurch ein zu großer Wert für das Auflösungsvermögen ermittelt wurde. \\
 
Die Messung könnte durch ein stärker vergrößerndes Teleskop oder ein Prisma mit höherer Dispersion verbessert werden. Nach der kleinsten noch aufzulösenden Wellen- längendifferenz in einer Umgebung der Doppellinie aus Gleichung \ref{eq:kleinstes_lambda_prisma} sollten  die beiden gelben Linien mit einem Wellenlängenunterschied $\Delta \lambda$ von mehr als 2 nm gut zu unterscheiden sein. Offenbar wird diese Auflösung in der Praxis nicht erreicht. Mögliche Ursachen hierfür sind Verunreinigungen auf dem Prisma oder auf den Linsen des Spalt- und Fernrohres. Da die beiden Rohre einen begrenzten Durchmesser haben, wird es auch innerhalb des Spektralapparates zu zusätzlicher Beugung kommen. Diese Effekte können auch eine Erklärung für das hohe experimentelle Auflösungsvermögen $A_{\text{exp}}$ liefern. 


%_________________________________________________________________%

\subsection{Das Gitterspektrometer}


\subsubsection{Gitterkonstante und Wellenlängendifferenz}
\label{sec:disc_g_gitta}

Der Theoriewert der Gitterkonstante $g = 1/600 \, \text{mm} \approx 1.667 \, \mu \text{m}$ nach \cite[Kap. 19.7]{knetter17} liegt innerhalb des 2-$\sigma$-Intervalls der experimentell bestimmten Gitterkonstante in Gleichung \ref{g_exp}, die prozentuale Abweichung beträgt weniger als 1 \%. Da die einzige Fehlerquelle die der gemessenen Winkel ist, wurde der Fehler der Winkel $\sigma_{\alpha}$ offenbar geringfügig zu klein abgeschätzt. Eine weitere Ablenkung durch Beugung oder chromatische Aberration innerhalb des Fernrohres ist möglich, der Effekt scheint aufgrund der guten Werte für $g$ jedoch gering zu sein. \\

Die theoretische Wellenlängendifferenz zwischen der linken gelben und der grünen Spektrallinie von $\Delta \lambda = 33 \, \text{nm}$  nach \cite[Kap. 19.7]{knetter17} liegt innerhalb des 5-$\sigma$-Intervalls des experimentell bestimmten Wertes aus Gleichungen \ref{eq:lambda_diff_exp}. Für den aus der zweiten Ordnung bestimmten Wert in Tabelle \ref{tab:lambda_diff_exp} liegt sie im 2-$\sigma$-Intervall. Dies lässt sich durch das größere Auflösungsvermögen des Gitters in der zweiten Ordnung begründen (vgl. folgenden Abschnitt), wodurch die Messgenauigkeit erhöht wird.  Entsprechend liegt der experimentelle Wert in der ersten Ordnung etwa 30 \% unterhalb des Theoriewertes. Durch den deutlich größeren Fehler trägt er jedoch nur wenig zum gewichteten Mittelwert bei. 


\subsubsection{Auflösungsvermögen}


Das Auflösungsvermögens $A_{\text{theo}}$ aus Gleichung \ref{eq:a_theo_gitter}, welches theoretisch mindestens nötig ist, um die gelbe Doppellinie aufzulösen, liegt außerhalb des 5-$\sigma$-Intervalls des experimentell bestimmten Wertes in Gleichung \ref{eq:a_exp_gitter}, sowohl in der ersten als auch in der zweiten Ordnung.  In der ersten Ordnung weicht der Wert um 20 \%, in der zweiten Ordnung um 40 \% nach unten ab. Es erscheint nicht sinnvoll, die beiden Werte zu mitteln, da sie unterschiedliche physikalische Größen darstellen. Die Berechnung des Auflösungsvermögens nach Gleichung \ref{eq:a_theo_gitter} liefert hingegen lediglich einen einzigen Wert, unabhängig von der Ordnung. Der Fehler von $A$  liegt wieder in der Bestimmung des Spaltdurchmessers $S$. Laut gemessenen Werten kann die Doppellinie also mit einem geringeren Auflösungsvermögen getrennt werden, als theoretisch dazu nötig ist. Offenbar wurde der Spalt noch weiter zu gedreht, als das Rayleigh Kriterium schon nicht mehr erfüllt war. Dies zeigt erneut die schwierige Umsetzung des Kriteriums in der Praxis. Allerdings ist die Messung beim Gitter durch das insgesamt höhere Auflösungsvermögen präziser als beim Prisma. Somit liegen die theoretischen und experimentellen Werte näher beieinander als bei der Messung in Abschnitt \ref{sec:auflvmg_prisma}. \\

Nach Gleichung $\ref{eq:a_max_gitta}$ kann das Auflösungsvermögen des Gitters theoretisch noch auf das etwa vierzigfache erhöht werden. Hierzu müsste jedoch das ganze Gitter ausgeleuchtet werden, also größere Rohre verwendet werden. Zudem wird aufgrund der Begrenzungen durch Beugung im Apparat und sphärische Aberration an den Linsen nicht das volle theoretische Auflösungsvermögen zu erreichen sein. \\

\subsubsection{Wellenlänge der violetten Linie}

Der theoretische Wert für die Wellenlänge der mittleren violetten Spektrallinie beträgt nach \cite[Kap. 19.7]{knetter17} $\lambda_{\text{v}} = 407.78 \, \text{nm}$. Er liegt innerhalb des 1-$\sigma$-Intervalls des für die erste Ordnung bestimmten Wertes und des 3-$\sigma$-Intervalls des Wertes der zweiten Ordnung (Tabelle \ref{tab:lambda_v}). Wie in Abschnitt \ref{sec:disc_g_gitta} besprochen ist der Fehler der Winkel $\sigma_{\alpha}$ zu gering abgeschätzt. Es mag verwundern, dass der Wert in der zweiten Ordnung ungenauer als in der ersten ist, obwohl das Auflösungsvermögen mit der Ordnung zunimmt. Allerdings beträgt die prozentuale Abweichung vom Literaturwert in der zweiten Ordnung weniger als 0.4 \%. Somit ist die höhere Abweichung der statistischen Schwankung der Messwerte geschuldet, zumal lediglich ein Wert pro Ordnung gemessen wurde. 

%_________________________________________________________________%

\subsection{Vergleich der Spektralapparate}

Nach Gleichungen \ref{eq:a_max_prisma} und \ref{eq:a_max_gitta} ist das theoretische Auflösungsvermögen des Prismas etwa halb so groß wie das des Gitters. Zudem nimmt das Auflösungsvermögen $A$ des Gitters in höheren Ordnungen noch weiter zu. Da die Intensität der Linien mit höherer Ordnung abnimmt, kann $A$ jedoch auf diese Weise nicht beliebig gesteigert werden. Nach Gleichungen \ref{eq:a_exp_prisma} und \ref{eq:a_exp_gitter} genügt beim Gitter ein etwa viermal kleineres Auflösungsvermögen, um die beiden gelben Linien voneinander zu trennen. Dieser große Unterschied ist auf das vergleichsweise geringe Vermögen des Prismas zurückzuführen, die Spektrallinien aufzutrennen, und die damit verbundenen Messfehler. Hierdurch wird wie in Abschnitt \ref{sec:auflvmg_prisma_discussion} besprochen die Messung beim Prisma verfälscht. Grundsätzlich sind mit einem Gitterspektrometer höhere Auflösungsvermögen als mit einem Prisma zu erzielen. Dies zeigt sich beim Blick durch die Spektrometer mit geöffnetem Auflösungsspalt: Beim Aufbau mit Gitter ist die gelbe Doppellinie deutlich besser zu erkennen als bei der Verwendung eines Prismas. 

