\subsection{Das Prismenspektrometer}


\subsubsection{Strahlengänge und Bestimmung der Winkeldifferenzen}
\label{sec:winkeldiff}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.9\textwidth]{img/strahlengang_fertig.png}
	\caption{Skizze des Strahlengangs im Prismenspektroskop. Beispielhaft wurde der Verlauf von blauem und rotem Licht eingezeichnet. Der Auflösungsspalt ist so weit geöffnet, dass daran keine Beugung stattfindet.}
	\label{img:strahlengang}
\end{figure}


In Abbildung \ref{img:strahlengang} ist der Strahlengang im Prismenspektroskop dargestellt. Der Fehler der mit dem Nonius der Winkelskala gemessenen Werte wird wegen der Schwierigkeiten beim Ablesen auf zwei Skalenteil zu $\sigma_{\delta_0} = 2 \, '$ geschätzt. Für die Auswertung ist der Ablenkwinkel $\delta$ des Lichts von der optischen Achse von Interesse, welcher aus  den gemessenen Winkeln $\delta_0$ bestimmt werden soll. Da die Messskala um $90^{\circ}$ gegenüber der optischen Achse verschoben ist, muss zur Bestimmung der tatsächlichen Winkeldifferenzen von den gemessenen Werten je $90^{\circ}$ subtrahiert werden. Der sich ergebende Fehler ist dabei nach Gauß $\sigma_{\delta} = \sqrt{2} \cdot \sigma_{\delta_0}$. Für die tatsächlichen Winkeldifferenzen der linken gelben und der grünen Spektrallinie ergeben sich als Mittelwert der drei Messungen in Gradmaß
\begin{align}
	\delta_1 & = (49.288888890000003 \pm 0.020030841189660267)\, ^{\circ} \; \notag\\
	\delta_2 & = (49.633333333333333 \pm 0.0096225035242428324)\, ^{\circ} \; .
\end{align}
Der Fehler ist der quadratische Fehler des Mittelwerts. 



\subsubsection{Dispersion der gelben und grünen Linie}
\label{sec:ev_dispersion}

Die Wellenlängen der linken gelben Spektrallinie $\lambda_1 = 579.07 \,  \text{nm}$ und der grünen Linie $\lambda_2  = 546.07 \,  \text{nm}$ sind der Praktikumsanleitung entnommen \cite[Kap. 19.7]{knetter17} und werden als fehlerfrei angenommen. Das verwendete Prisma ist gleichwinklig, somit beträgt der brechende Winkel $\varepsilon = 60^{\circ}$ und wird ebenfalls als fehlerfrei angenommen. Nach Frauenhofer (Gleichung \ref{eq:frauenhofer}) lassen sich die Brechungsindizes für die beiden Wellenlängen bestimmen:  
\begin{align}
	n_1 	& = (1.91560609 \pm 0.000310679897) \notag\\
	n_2		& = (1.92093979 \pm 0.000148761588) \; .
\end{align}
Der Fehler berechnet sich dabei zu
\begin{equation*}
	\sigma_n = \sigma_{\delta} \cdot
	\frac   {\cos \left( \frac{\delta_{\text{min}} + \varepsilon} {2}\right)}
			{2 \, \sin \left( \frac{\varepsilon}						  {2}\right)} \; . 
\end{equation*}
Für die als konstant angenommene Dispersion ergibt sich somit
\begin{equation}
	\boxed{ \frac{\text{d} 	n}{\text{d} \lambda} \approx \frac{\Delta n}{\Delta \lambda} = \frac{n_2 - n_1}{\lambda_2 - \lambda_1} = (1.62 \pm 0.10) \cdot 10^5 \, \frac{1}{\text{m}} }
	\label{eq:dispersion_exp}
\end{equation}
mit dem Fehler
\begin{equation*}
 	\sigma_{\frac{\text{d} 	n}{\text{d} \lambda}} 
	= \sqrt{\left(\frac{\sigma_{n_2}}{\lambda_2 - \lambda_1}\right)^2 +\left(\frac{\sigma_{n_1}}{\lambda_2 - \lambda_1}\right)^2} \; .
	\label{eq:disp_exp}
\end{equation*}
Der theoretische Wert der Dispersion berechnet sich aus den in den im Praktikumshandbuch gegebene Werten \cite[Kap. 19.7]{knetter17}: Zwischen der Natrium-D-Linie bei $\lambda_{\text{D}} = 589.3 \, \text{nm}$ und der Wasserstoff-$\beta$-Linie bei $\lambda_{\beta} = 486.1 \, \text{nm}$ hat das Prisma eine mittlere Differenz des Brechungsindexes von $\Delta n = 0.017$. Hieraus ergibt sich der Theoriewert der Dispersion zu 
\begin{equation}
	\boxed{ \frac{\text{d} 	n}{\text{d} \lambda} \approx   \frac{\Delta n}{\lambda_{\text{D}} - \lambda_{\beta}} = 1.65 \cdot 10^5 \, \frac{1}{\text{m}}  } \; .
	\label{eq:dispersion_theo}
\end{equation}
Die dem Handbuch entnommenen Werte werden als fehlerfrei angenommen. 


\subsubsection{Auflösungsvermögen des Prismas}

\label{sec:auflvmg_prisma}

Das theoretische Auflösungsvermögen für die beiden gelben Linien berechnet sich nach Gleichung \ref{eq:vermoegen_theo} zu 
\begin{equation}
	\boxed{ A_{\text{theo}} = \frac{\lambda_1}{\lambda_1 - \lambda_3} = 274.440758294  } \; .
	\label{eq:a_theo_prisma}
\end{equation}
Dabei ist $\lambda_1$ wie in Abschnitt \ref{sec:ev_dispersion} die Wellenlänge der linken gelben Spektrallinie, $\lambda_3 = 576,96 \, {\text{nm}}$ nach \cite[Kap. 19.7]{knetter17} die Wellenlänge der rechten gelben Linie, die ebenfalls als fehlerfrei angenommen wird. Der Bündelquerschnitt $S$ des auf das Prisma fallenden Strahls wurde mit dem Mikrometertrieb gemessen. Der Fehler wird auf einen ganzen Skalenteil zu $\sigma_S = 10 \, \mu\text{m}$ angenommen. Nach Gleichung \ref{eq:vermoegen_exp} berechnet sich die effektive Basisbreite $B$ zu  
\begin{equation}
	B 	= 2 S \cdot \frac	{\sin \left(\frac{\varepsilon}{2} \right)}
					{\cos\left(\frac{\delta + \varepsilon} {2}\right)}
		= \frac	{S}
		{\cos\left(\frac{\delta_1 + \varepsilon} {2}\right)} \;  ,
		\label{eq:spaltbreite_eff}
\end{equation}
dabei wird für $\delta = \delta_1$ der Winkel zur linken gelben Sepktrallinie verwendet. Der Fehler ergibt sich nach Gauß:
\begin{equation*}
	\sigma_B = \sqrt{
		\left(\sigma_S \cdot   
		\frac{1}
		{\cos\left(\frac{\delta_1 + \varepsilon} {2}\right)} \right)^2 + 
		\left( \sigma_{\delta_1} \cdot 
		\frac{S \sin\left(\frac{\delta_1 + \varepsilon} {2}\right)}
		{2 \, \cos^2\left(\frac{\delta_1 + \varepsilon} {2}\right)} \right)^2
		} \; .
\end{equation*}
Hieraus berechnet sich weiterhin nach Gleichung \ref{eq:vermoegen_exp} mit der zuvor experimentell bestimmten Dispersion aus Gleichung \ref{eq:dispersion_exp} das Auflösungsvermögen zu 
\begin{equation}
	\boxed{A_{\text{exp}} = B \left| \frac{\text{d} n}{\text{d} \lambda} \right| = (1.1 \pm 0.5) \cdot 10^3  } \; ,
	\label{eq:a_exp_prisma}
\end{equation}
mit dem Fehler
\begin{equation*}
	\sigma_A = \sqrt{
		\left(\sigma_B \left| \frac{\text{d} n}{\text{d} \lambda} \right| \right)^2 + 
		\left( \sigma_{\left| \frac{\text{d} n}{\text{d} \lambda} \right|} B \right)^2
		} \; .
\end{equation*}
Das maximal erreichbare Auflösungsvermögen $A_{\text{max}}$ ergibt sich aus der als fehlerfrei angenommenen Basislänge des Prismas $B_{\text{max}} = 33 \, \text{mm}$ \cite[Kap. 19.7]{knetter17}. Nach Gleichung \ref{eq:vermoegen_exp} berechnet sich mit dem Theoriewert der Dispersion aus Gleichung \ref{eq:dispersion_theo}: 
\begin{equation}
	\boxed{ A_{\text{max}} = B_{\text{max}} \left| \frac{\text{d} n}{\text{d} \lambda} \right| = 5436.04651163 } \; ,
	\label{eq:a_max_prisma}
\end{equation}
(mit dem Fehler
\begin{align*}
	\sigma_{A_{\text{max}}} & = 
\sqrt{
	\left(\sigma_S \cdot \left| \frac{\text{d} n}{\text{d} \lambda} \right|   
	\frac{1}
	{\cos\left(\frac{\delta_1 + \varepsilon} {2}\right)} \right)^2 + 
	\left( \sigma_{\delta_1} \cdot \left| \frac{\text{d} n}{\text{d} \lambda} \right| 
	\frac{S_{\text{max}} \sin\left(\frac{\delta_1 + \varepsilon} {2}\right)}
	{2 \, \cos^2\left(\frac{\delta_1 + \varepsilon} {2}\right)} \right)^2 +
	\left(\sigma_{\left| \frac{\text{d} n}{\text{d} \lambda} \right|} \cdot
	 \frac{S_{\text{max}}}
	{\cos\left(\frac{\delta_1 + \varepsilon} {2}\right)} \right)^2 
} \\
\sigma_{A_{\text{max}}} & = \sigma_{\left| \frac{\text{d} n}{\text{d} \lambda} \right|} \cdot B_{\text{max}}
\; . \qquad )
\end{align*}
Umstellen von Gleichung \ref{eq:vermoegen_theo} ergibt für die kleinste noch trennbare Wellenlängendifferenz in einer Umgebung der linken gelben Spektrallinie nach dem Rayleigh Kriterium  
\begin{equation}
	\boxed{\Delta \lambda_1 = \frac{\lambda_1}{ A_{\text{max}} } = 106.5241069 \, {\text{pm}} } \; .
	\label{eq:kleinstes_lambda_prisma}
\end{equation}
Die insgesamt kleinste trennbare Wellenlängendifferenz wird in einer Umgebung der starken violetten Spektrallinie mit der kleinsten Wällenlänge von $\lambda_{\text{v,s}} = 404.66  \, \text{nm}$ erreicht \cite[Kap. 19.7]{knetter17}:
\begin{equation}
	\lambda_{\text{v,s}} = 74.4401283422 \, \text{pm}
\end{equation}


%_________________________________________________________________%

\subsection{Das Gitterspektrometer}

\subsubsection{Gitterkonstante}

Umformen von Gleichung \ref{eq:gitter_max} ergibt für die Gitterkonstante $g$
\begin{equation}
	g = \frac{m \, \lambda}{\sin \alpha_{\text{m}}} \; ,
	\label{eq:gitter_max2}
\end{equation}
wobei $m$ die Ordnung des Maximums in Richtung $\sin \alpha_{\text{m}}$ bezeichnet und $\lambda$ die Wellenlänge der betrachteten Linie. Die tatsächlichen Winkeldiffernzen werden aus den Messwerten wie in Abschnitt \ref{sec:winkeldiff} berechnet, auch der Fehler von $\alpha_{\text{m}}$ wird analog behandelt. Die Werte für $\lambda$ aus \cite[Kap. 19.7]{knetter17} werden als fehlerfrei angenommen, somit berechnet sich der Fehler der Gitterkonstante zu 
\begin{equation*}
	\sigma_{g} = \sigma_{\alpha_{\text{m}}} m \lambda	
				 \frac{\cos \alpha_{\text{m}}}{\sin^2\alpha_{\text{m}}}	\; .
\end{equation*}
Die Ergebnisse für $g$ sind in Tabelle \ref{tab:g_ergebnisse} aufgeführt. Als gewichteter Mittelwert aus den sechs Messungen ergibt sich für die Gitterkonstante
\begin{equation}
	\boxed{ g = 1.6542 \pm 0.0006 \, \mu \text{m} } \, . 
	\label{g_exp}
\end{equation} 
Der Fehler ist der Fehler des Mittelwertes. Die Gitterkonstante kann alternativ aus der Winkeldifferenz zwischen den Doppellinien berechnet werden. Dazu wird Gleichung \ref{eq:gitter_max2} nach $\alpha_{\text{m}}$ umgeformt und nach $\lambda$ abgeleitet: 
\begin{equation}
	 \frac{\text{d} }{\text{d} \lambda} \alpha_{\text{m}}
	= \frac{\text{d}}{\text{d} \lambda} \left( \arcsin \left(\frac{m \lambda}{g}\right)\right)
	= \frac{m}{g} \sqrt{\left(1 - \left( \frac{m \lambda}{g} \right)^2\right)^{-1}}
\end{equation}
Mit der Näherung $\lambda \ll g$ ergibt sich
\begin{align}
			\frac{\text{d} }{\text{d} \lambda} \alpha_{\text{m}}
	& \approx \frac{\Delta \alpha_{\text{m}}}{\Delta \lambda}
	= 		\frac{m}{g} \notag\\
	& \Rightarrow g = \frac{m \; \Delta \lambda}{\Delta \alpha_{\text{m}}}
\end{align}
Die mit den aus \cite[Kap. 19.7]{knetter17} entnommenen Wellenlängen berechneten Werte für $g$ finden sich ebenfalls in Tabelle \ref{tab:g_ergebnisse}. 


\subsubsection{Wellenlängendifferenz}

Umstellen von Gleichung $\ref{eq:gitter_max2}$ liefert für die Wellenlängendifferenze zweier Linien $\lambda_a$ und $\lambda_b$
\begin{equation}
	\Delta \lambda = \frac{g \, \sin (\Delta \alpha_{\text{m}})}{m}
	= \frac{g \cdot \left( \sin (\alpha_{\text{m,b}}) -  \sin (\alpha_{\text{m,a}})\right) }{m}
	 \; ,
\end{equation}
wobei $\sin (\Delta \alpha_{\text{m}})$ als Kurzeschreibweise der Differenz der Sinuse von den Winkeln $\alpha_{\text{m,a}}$ und $\alpha_{\text{m,b}}$ zu den entsprechenden Spektrallinien eingeführt wurde. Für $g$ wurde die fehlerfreie Gitterkonstante aus \cite[Kap. 19.7]{knetter17} verwendet. Der Fehler ist dabei
\begin{equation*}
	\sigma_{\Delta \lambda} 
	= \sqrt{ 
		  \left( \sigma_g \cdot \frac{\sin (\Delta \alpha_{\text{m}})}{m}\right)^2
		+ \left( \sigma_{\alpha_{\text{m,a}}} \cdot \frac{g}{m} \cos (\alpha_{\text{m,a}})\right)^2 +
		\left( \sigma_{\alpha_{\text{m,b}}} \cdot \frac{g}{m} \cos (\alpha_{\text{m,b}})\right)^2
		} ZU LANG, OHNE \; g\; .
\end{equation*}
Für die Differenzen zwischen den linken gelben und grünen Sektrallinien erster bzw. zweiter Ordung ergibt sich
\begin{equation}
	\boxed{ \Delta \lambda^{(1)} = 
		(21.4535264 \pm 1.29104196) \, \text{nm} } \qquad
	\boxed{ \Delta \lambda^{(2)} = 
		(32.0805522 \pm 0.501347515) \, \text{nm} }
	\label{eq:lambda_diff_gitter}
\end{equation}


\subsubsection{Auflösungsvermögen des Gitters}

Die Spaltbreite $S$ und ihr Fehler werden wie in Abschnitt \ref{sec:auflvmg_prisma} behandelt. Zusammen mit dem Ergebnis der Gitterkonstante $g$ (Gleichung \ref{g_exp}) ergibt sich aus Gleichung \ref{eq:vermoegen_gitter} für das Auflösungsvermögen des Gitters
\begin{equation}
	A = m \cdot N = \frac{m\, S}{g} MIT FEHLER GENOMMEN
\end{equation}
mit dem Fehler
\begin{equation*}
	\sigma_A = \sqrt{
		\left(\sigma_S \frac{m}{g}\right)^2
		+ \left(\sigma_g \frac{mS}{g^2}\right)^2
		} \; .
\end{equation*}
Für die erste und zweite Ordnung berechnet sich
\begin{equation}
	\boxed{ {A_{\text{exp}}}^{(1)} 
		= (221.7758 \pm 3.469084) }  \qquad	
	\boxed{ {A_{\text{exp}}}^{(2)} 
		= (447.4491 \pm 6.985006) } \; .
	\label{eq:a_exp_gitter}
	\
\end{equation}
Das maximale Auflösungsvermögen ergibt sich mit dem Durchmesser der Kreisblende vor dem Gitter $d_{\text{aper}} = 1.9 \, \text{cm}$ und der Gitterkonstante $g = 1/1600 \, \text{mm}$ (Werte aus \cite[Kap. 19.7]{knetter17} als fehlerfrei angenommen) ebenfalls nach Gleichung \ref{eq:vermoegen_gitter} für die Linien erster Ordnung zu 
\begin{equation}
	\boxed{ A_{\text{max}} = m \cdot N = g \cdot d_{\text{aper}}  = 11400 } \; .
	\label{eq:a_max_gitta}
\end{equation}
(FEHLT: Theoretische Auflösungsvermögen nach Gleichung) \ref{eq:vermoegen_theo} und Vergleich. 
Das theoretische Auflösungsvermögen berechnet sich wie in Abschnitt \ref{sec:auflvmg_prisma}, sein Wert entspricht dem aus Gleichung \ref{eq:a_theo_prisma}: 
\begin{equation}
	A_{\text{theo}} = \frac{\lambda_1}{\lambda_1 - \lambda_3} = 274.440758294 \; .
	\label{eq:a_theo_gitter}
\end{equation}

\subsubsection{Wellenlänge der violetten Linie}

Zur Berechnung der Wellenlänge der mittleren violetten Linie $\lambda_{\text{v}}$ wird Gleichung \ref{eq:gitter_max2} umgestellt zu 
\begin{equation}
	 \lambda_{\text{v}} = \frac{g \,\sin \alpha_{\text{m}}}{m} = 
	(4.0888813707685926e-07, \;  3.8527925327526936e-10) \; .
\end{equation} 
Der Fehler berechnet sich zu
\begin{equation*}
	\sigma_{\lambda_{\text{v}}} =
	\sqrt{
		\left( \sigma_g \frac{\sin \alpha_{\text{m}}}{m} \right)^2 + 
		\left( \sigma_{\alpha_{\text{m}}} \frac{g}{m} \cos \alpha_{\text{m}} \right)^2 
		}
\end{equation*}
Die Ergebnisse sind in Tabelle \ref{tab:blubb} aufgeführt.  Als gewichteter Mittelwert aus den beiden Ordnungen ergibt sich
\begin{equation}
	\boxed{ \lambda_{\text{v}} = (408.88813707685926 \pm  0.38527925327526936) \, \text{nm} } \; .
	\label{eq:violett_exp}
\end{equation}
Der Fehler ist der quadratische Fehler des gewichteten Mittelwertes. 

\subsubsection{Es Fehlt}

ESTO IGNORADLO \\

- alle Werte runden \\
- Komische Näherung und wie tun Gleichung 20 \\
- DISKUSSION NEU CHECKEN, insbesondere Auflösungsvermögen Gitter

- Werte für $\delta$ Gleichung 8 X\\
- Werte für $n$ Gleichung 9 X\\
- Wert effektive Basisbreite Gleichung 13 WEGLASSEN\\
- Dispersion mit Fehler ausrechnen Gleichung 15 und 16 OHNE\\
- kleinste trennbare Linie bei kleinster Wellenlänge Wasserstoff-$\beta$ X ersetzen X\\
- Ergebnistabelle für $g$ 4.2.1 \\
- Ausrechnen Wellenlängendifferenzen und checken ob richtig Gleichung 22 X \\
- Gleichung 24 vielleicht besser mit theoretischem $g$ um vergleichbar zu machen MIT GENOMMEN\\
- Theoretisches Auflösungsvermögen Ende 4.2.3 X\\
- Wert für violett Gleichung 28 Tabelle X \\
- Diskussion \\
