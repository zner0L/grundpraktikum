# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

rc_params = {'font.family' : 'normal',
        'font.weight' : 'normal',
        'font.size'   : 25,
        'legend.fontsize': 25}

matplotlib.rcParams.update(rc_params)

def linear_function(x, m, b):
    return m * x + b

data_eichung = np.genfromtxt('data/eichung.csv', delimiter=' ', usecols=range(3))
data_eichung[:, 0] = data_eichung[:, 0] * 1e-3

param, error = curve_fit(linear_function, data_eichung[:, 0], data_eichung[:, 1])

l = np.linspace(0, 0.6)

figsize = [20, 14]

fig_eich = plt.figure(1, figsize=figsize)
plt.errorbar(data_eichung[:, 0], data_eichung[:, 1], yerr=data_eichung[:, 2], xerr=1e-3, fmt='x', label='Messwerte')
plt.plot(l, linear_function(l, param[0], param[1]), label='Regressionsgerade')
plt.xlabel(u'Pulslänge $\Delta t$ [s]')
plt.ylabel(r'Werte des Stromintegrators $x$ [Skalenteile]')
plt.legend(loc='upper left')
plt.close()

m_eichung = ufloat(param[0], np.sqrt(error[0][0]))

r_kappa = ma.uncertainty.Sheet('r1/r2*(r2+rl+ri)+rl+ri', 'R_{\kappa}')
r_kappa.set_value('r1', 1000, tex='R_1')
r_kappa.set_value('r2', 3.3, tex='R_2')
r_kappa.set_value('rl', 18, 1, tex='R_L')
r_kappa.set_value('ri', 1000, tex='R_Int')

r_kappa = r_kappa.get_result('ufloat')

kappa = 2/(m_eichung*r_kappa)