# -*- coding: utf-8 -*-
from eichung import *

font = {'family' : 'normal',
        'weight' : 'normal',
        'size'   : 25}

matplotlib.rc('font', **font)

def b_feld(z, mu, n, I, l, r):
    return 1.0/2.0 * (mu*n*I)/l * ((z+l/2.0)/np.sqrt((z+l/2.0)**2+r**2) - (z-l/2.0)/np.sqrt((z-l/2.0)**2+r**2))

def b_feld_helmholtz(z, mu, n, I, r):
    return 1.0/2.0 * (mu*n*I*r**2) * (1/(np.sqrt((z+r/2.0)**2+r**2))**3 + 1/(np.sqrt((z-r/2.0)**2+r**2))**3)

data_induktion = np.genfromtxt('data/induktion.csv', delimiter=' ', usecols=range(3))
data_hall_lang = np.genfromtxt('data/hall_lange_spule.csv', delimiter=' ', usecols=range(3))
data_hall_kurz = np.genfromtxt('data/hall_kurze_spule.csv', delimiter=' ', usecols=range(3))
data_hall_helmholtz = np.genfromtxt('data/hall_helmholtz.csv', delimiter=' ', usecols=range(3))


data_induktion[:, 0] = data_induktion[:, 0]*1e-2 # cm to m
data_hall_lang[:, 0] = data_hall_lang[:, 0]*1e-2
data_hall_kurz[:, 0] = data_hall_kurz[:, 0]*1e-2
data_hall_helmholtz[:, 0] = data_hall_helmholtz[:, 0]*1e-2

B = ma.uncertainty.Sheet('(4*k*(rl+ri+r2))/(pi*ni*d^2)*x', 'B')
B.set_value('ni', 369, tex='N_Ind')
B.set_value('d', 59e-3)
B.set_value('r2', 3.3, tex='R_2')
B.set_value('rl', 18, 1, tex='R_L')
B.set_value('ri', 1000, tex='R_Int')
B.set_value('k', kappa.n, kappa.s, tex='\kappa')

B_values = B.batch(data_induktion[:, (1,2)], 'x|x%')
l = np.linspace(-0.1, 0.5)

figsize = [20, 14]

naeherung = ma.uncertainty.Sheet('mu0*n*i/l', 'B')
naeherung.set_value('mu0', 4e-7*math.pi, tex='\mu_0')

naeherung_helmholtz = ma.uncertainty.Sheet('mu0*8*n*i/(125^(1/2)*d)')
naeherung_helmholtz.set_value('mu0', 4e-7*math.pi, tex='\mu_0')

# == lange Spule ==
naeherung.set_value('n', 831)
naeherung.set_value('i', 0.5, 0.0005, tex='I')
naeherung.set_value('l', 45.1e-2)
naeherung_lang = naeherung.get_result()
fig1 = plt.figure(1, figsize=figsize)
plt.errorbar(data_induktion[:, 0], B_values[:, 0]*1e+3, yerr=B_values[:, 1]*1e+3, xerr=0.005, fmt='+', label='Messwerte der Induktionsspule')
plt.errorbar(data_hall_lang[:, 0], (data_hall_lang[:, 1]*1e-4+0.6e-4)*1e+3, yerr=data_hall_lang[:, 2]*1e-4*1e+3, xerr=0.005, fmt='+', label='Messwerte der Hallsonde')
plt.plot(l, b_feld(l, 4e-7*math.pi, 831, 0.5, 45.1e-2, 6.4e-2)*1e+3, label='Theoriekurve')
plt.plot(l, [naeherung_lang[0]*1e+3]*len(l), label=u'Näherung für lange Spulen', color='#E8AD5E')
plt.fill_between(l, [naeherung_lang[0]*1e+3 - naeherung_lang[1]*1e+3]*len(l),
                 [naeherung_lang[0]*1e+3 + naeherung_lang[1]*1e+3] * len(l), color="#FFCE67")
plt.axis([0, 0.45, -0.1, 1.3])
plt.ylabel(r'magnetische Flussdichte $B$ [mT]')
plt.xlabel(r'Abstand zur Spulenmitte $z$ [m]')
plt.legend(loc='lower left')
plt.close()

# == kurze Spule ==
naeherung.set_value('n', 501)
naeherung.set_value('i', 0.5, 0.0005, tex='I')
naeherung.set_value('l', 27e-2)
naeherung_kurz = naeherung.get_result()
fig2 = plt.figure(1, figsize=figsize)
plt.errorbar(data_hall_kurz[:, 0], (data_hall_kurz[:, 1]*1e-4+0.3e-4)*1e+3, yerr=data_hall_kurz[:, 2]*1e-4*1e+3, xerr=0.005, fmt='+', label='Messwerte der Hallsonde')
plt.plot(l, b_feld(l, 4e-7*math.pi, 501, 0.5, 27.0e-2, 11.1e-2)*1e+3, label='Theoriekurve')
plt.plot(l, [naeherung_kurz[0]*1e+3]*len(l), label=u'Näherung für lange Spulen', color='#E8AD5E')
plt.fill_between(l, [naeherung_kurz[0]*1e+3 - naeherung_kurz[1]*1e+3]*len(l),
                 [naeherung_kurz[0]*1e+3 + naeherung_kurz[1]*1e+3] * len(l), color="#FFCE67")
plt.axis([0, 0.45, -0.1, 1.3])
plt.xlabel(r'Abstand zur Spulenmitte $z$ [m]')
plt.ylabel(r'magnetische Flussdichte $B$ [mT]')
plt.legend(loc='lower left')
plt.close()

# == Helmholtz-Spulenpaar ==
naeherung_helmholtz.set_value('n', 501)
naeherung_helmholtz.set_value('i', 0.5, 0.0005, tex='I')
naeherung_helmholtz.set_value('d', 6.4e-2, 0.2e-2)
naeherung_helmholtz = naeherung_helmholtz.get_result()
fig3 = plt.figure(1, figsize=figsize)
plt.errorbar(data_hall_helmholtz[:, 0], (data_hall_helmholtz[:, 1]*1e-4+0.3e-4)*1e+3, yerr=data_hall_helmholtz[:, 2]*1e-4*1e+3, xerr=0.005, fmt='+', label='Messwerte der Hallsonde')
plt.plot(l, b_feld_helmholtz(l, 4e-7*math.pi, 501, 0.5, 6.4e-2)*1e+3, label='Theoriekurve')
plt.plot(l, [naeherung_helmholtz[0]*1e+3]*len(l), label=u'Näherung für Helmholtz-Spulenpaar', color='#E8AD5E')
plt.fill_between(l, [naeherung_helmholtz[0]*1e+3 - naeherung_helmholtz[1]*1e+3]*len(l),
                 [naeherung_helmholtz[0]*1e+3 + naeherung_helmholtz[1]*1e+3] * len(l), color="#FFCE67")
plt.axis([0, 0.45, -0.1, 3.7])
plt.xlabel(r'Abstand zur Spulenmitte $z$ [m]')
plt.ylabel(r'magnetische Flussdichte $B$ [mT]')
plt.legend(loc='center right')
plt.close()

# == Vergleich ohne Normierung ==
fig4 = plt.figure(1, figsize=figsize)
plt.errorbar(data_hall_helmholtz[:, 0], (data_hall_helmholtz[:, 1]*1e-4+0.3e-4)*1e+3, fmt='o', label='Helmholtz-Spule')
plt.errorbar(data_hall_kurz[:, 0], (data_hall_kurz[:, 1]*1e-4+0.3e-4)*1e+3, fmt='o', label='kurze Spule')
plt.errorbar(data_hall_lang[:, 0], (data_hall_lang[:, 1]*1e-4+0.6e-4)*1e+3, fmt='o', label='lange Spule')
plt.axis([0, 0.45, 0, 3.7])
plt.xlabel(r'Abstand zur Spulenmitte $z$ [m]')
plt.ylabel(r'magnetische Flussdichte $B$ [mT]')
plt.legend()
plt.close()

# == Vergleich mit Normierung ==
fig5 = plt.figure(1, figsize=figsize)
plt.errorbar(data_hall_helmholtz[:, 0]/3.2e-2, (data_hall_helmholtz[:, 1]+0.3)/(data_hall_helmholtz[0, 1]+0.3), fmt='o', label='Helmholtz-Spule')
plt.errorbar(data_hall_kurz[:, 0]/23.5e-2, (data_hall_kurz[:, 1]+0.3)/(data_hall_kurz[0, 1]+0.3), fmt='o', label='kurze Spule')
plt.errorbar(data_hall_lang[:, 0]/22.255e-2, (data_hall_lang[:, 1]+0.6)/(data_hall_lang[0, 1]+0.6), fmt='o', label='lange Spule')
plt.axis([0, 2.5, -0.1, 1.1])
plt.xlabel(r'normierter Abstand $\frac{2z}{l}$')
plt.ylabel(r'normierte Flussdichte $\frac{B}{B_0}$')
plt.legend()
plt.close()

# == mu_0 Berechnung ==
mu_0 = ma.uncertainty.Sheet('be/bt', '\mu_0')
mu_0_ind = mu_0.batch(np.column_stack([B_values[:10, (0,1)], b_feld(data_hall_lang[:10, 0], 1, 831, 0.5, 45.1e-2, 6.4e-2)]), 'be|be%|bt')
mu_0_lang = mu_0.batch(np.column_stack([data_hall_lang[:10, (1,2)], b_feld(data_hall_lang[:10, 0], 1, 831, 0.5, 45.1e-2, 6.4e-2)*1e+4]), 'be|be%|bt')
mu_0_kurz = mu_0.batch(np.column_stack([data_hall_kurz[:10, (1,2)], b_feld(data_hall_kurz[:10, 0], 1, 501, 0.5, 27.0e-2, 11.1e-2)*1e+4]), 'be|be%|bt')
mu_0_helmholtz = mu_0.batch(np.column_stack([data_hall_helmholtz[:10, (1,2)], b_feld_helmholtz(data_hall_helmholtz[:10, 0], 1, 501, 0.5, 6.4e-2)*1e+4]), 'be|be%|bt')
mu_0_ind = ufloat(np.mean(mu_0_ind[:, 0]), stats.sem(mu_0_ind[:, 0]))
print '\mu_0 lange Spule (Induktion): ' + str(mu_0_ind)
mu_0_lang = ufloat(np.mean(mu_0_lang[:, 0]), stats.sem(mu_0_lang[:, 0]))
print '\mu_0 lange Spule: ' + str(mu_0_lang)
mu_0_kurz = ufloat(np.mean(mu_0_kurz[:, 0]), stats.sem(mu_0_kurz[:, 0]))
print '\mu_0 kurze Spule: ' + str(mu_0_kurz)
mu_0_helmholtz = ufloat(np.mean(mu_0_helmholtz[:, 0]), stats.sem(mu_0_helmholtz[:, 0]))
print '\mu_0 Helmholtz: ' + str(mu_0_helmholtz)

mu_0_mean = ma.weighted_average([[mu_0_ind.n, mu_0_ind.s], [mu_0_lang.n, mu_0_lang.s], [mu_0_kurz.n, mu_0_kurz.s], [mu_0_helmholtz.n, mu_0_helmholtz.s]])
print '\mu_0 Mittelwert:' + str(mu_0_mean)

# == Create Assets ==
fig1.savefig('lange_spule.png')
fig2.savefig('kurze_spule.png')
fig3.savefig('helmholtz_spule.png')
fig4.savefig('vergleich.png')
fig5.savefig('vergleich_normiert.png')
fig_eich.savefig('eichgerade.png')

print 'R_Kappa: ' + str(r_kappa)
print 'm_Eichung: ' + str(m_eichung)
print 'Kappa: ' + str(kappa)

print 'Näherung lange Spule: ' +str(naeherung_lang)
print 'Näherung kurze Spule: ' +str(naeherung_kurz)
print 'Näherung Helmholtz Spulenpaar: ' +str(naeherung_helmholtz)