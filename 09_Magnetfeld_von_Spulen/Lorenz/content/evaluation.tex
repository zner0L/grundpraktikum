\subsection{Eichkonstante des Stromintegrators}

Mit den Parametern aus Tabelle \ref{tab:widerstaende} und Gleichung \ref{eq:rkappa} ergibt sich für den Eichwiderstand $R_\kappa$ in diesem Versuchsaufbau:

\begin{equation}
	R_\kappa = (310.5 \pm 0.3) \, \mathrm{k\Omega} \; .
\end{equation}

Der Fehler ergibt sich dabei aus:

\begin{equation*}
\sigma_{R_{\kappa}}  = \sqrt{(\sigma_{R_{L}}^2 + \sigma_{R_{Int}}^2) \cdot \left(\frac{R_1}{R_2} + 1\right)^2} \; .
\end{equation*}

Aus den wie in Abschnitt \ref{sec:impl_eichung} beschrieben bestimmten Skalenwerten $x$ für bekannte Zeitintervalle $\Delta t$ lässt sich aufgetragen gegen die Zeit aus einer linearen Regression die Steigung $m$ erhalten. Abbildung \ref{fig:eichgerade} zeigt den Regressionsgraphen. Demnach beträgt die Steigung:

\begin{equation}
	m = (1148 \pm 4) \frac{\mathrm{Sk}}{\mathrm{s}} \; .
\end{equation}

Der Fehler ergibt sich aus der Regression. \\

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{../evaluation/eichgerade.png}
	\caption{$t$-$x$-Diagramm mit linearer Regression und Wert des Stromintegrators $x$ zur Bestimmung der Eichkonstante $\kappa$. Der Fehler der Zeitintervalle wurde auf 1 ms geschätzt.}
	\label{fig:eichgerade}
\end{figure}

Damit ergibt sich für die Eichkonstante $\kappa$ nach Gleichung \ref{eq:kappa}:

\begin{equation}
	\kappa = \frac{U_E}{R_\kappa x} \Delta t = \frac{U_E}{R_\kappa m} = (5.61 \pm 0.02) \cdot 10^{-9} \frac{\mathrm{C}}{\mathrm{Sk}} \; .
\end{equation}


Für den Fehler gilt nach dem \textsc{Gauss}'schen Fehlerfortpflanzungsgesetz:

\begin{equation*}
\sigma_{\kappa} = \sqrt{
	\left(\sigma_{U_E}  \frac{1}{m R_{\kappa}}\right)^2 +
	\left(\sigma_{m} \frac{U_E}{m^2 R_{\kappa}}\right)^2 + 
	\left(\sigma_{R_{\kappa}}  \frac{{U_E}^2}{m R_{\kappa}^2}\right)^2	} \; .
\end{equation*}

\subsection{Feldverlauf nach der Induktionsspule}

Aus Gleichung \ref{eq:integrator} folgt mit dem Widerstand $R_{Ind}$ aus dem Induktionsschaltkreis in Abbildung \ref{fig:induktionsschaltung} für den Betrag der Flussdichte:

\begin{equation}
	B = \frac{2 R_{Ind}}{n \pi r^2} \kappa x \; .
\end{equation}

Abbildung \ref{fig:lange_spule} zeigt den daraus erhaltenen Feldverlauf. Der Fehler ergibt sich aus:

\begin{equation*}
\sigma_{B} = B \; \sqrt{ \frac{\sigma_{\kappa}^2}{\kappa^2} 
	+ \frac{\sigma_{x}^2}{x^2} + \frac{\sigma_{R_{Ind}}^2}{R_{Ind}^2}} \; .
\end{equation*}

\subsection{Offset der Hall-Sonde}
\label{sec:offset}

Um das Umgebungsfeld zu kompensieren wurde für jede Messung ein Offset der Werte der \textsc{Hall}-Sonde $B_0$ notiert. Dieser wurde von den gemessenen Werten abgezogen, um sie an einen umgebungsfeldfreien Verlauf anzugleichen. Die Werte sind in Tabelle \ref{tab:spulen_ergebnisse} aufgeführt. Für den Fehler wird $\sigma_{Hall} = 0.1 \; \mathrm{Gs}$ angenommen. Die Abbildungen \ref{fig:lange_spule}, \ref{fig:kurze_spule} und \ref{fig:helmholtz_spule} zeigen den Verlauf der so gemessenen magnetischen Flussdichte in Abhängigkeit vom Abstand zum Spulenmittelpunkt $z$.

\begin{table}
	\centering
	\renewcommand{\arraystretch}{1.5}
	\begin{tabular}{|c|c|c|}\hline
		\textbf{gemessene Spule} & \textbf{Offset $B_0$ [Gs]} & \textbf{Näherung $B_{app}$ [mT]}	\\\hline
		lange Spule & $- 0.6	\pm 0.1$ & $1.158 \pm 0.001$	\\\hline
		kurze Spule	& $- 0.3	\pm 0.1$ & $1.165 \pm 0.002$	\\\hline
		Helmholtz-Spulenpaar & $- 0.3 \pm 0.1$ & $3.52 \pm 0.12$ \\\hline
	\end{tabular}
	\caption{Offset der mit der \textsc{Hall}-Sonde bestimmten Werte $B_0$ und Näherung des Feldverlaufs für die verschiedenen Spulen.}
	\label{tab:spulen_ergebnisse}
\end{table}

\subsection{Vergleich mit dem theoretischen Feldverlauf}

Aus Gleichung \ref{eq:cylindrical_coil} lässt sich der Feldverlauf für die lange und die kurze Spule theoretisch berechnen. Mithilfe von Gleichung \ref{eq:helmholtz_coil} lässt sich auch der Feldverlauf des \textsc{Helmholtz}-Spulenpaars theoretisch berechnen. Diese sind in den Abbildungen \ref{fig:lange_spule}, \ref{fig:kurze_spule} und \ref{fig:helmholtz_spule} aufgetragen.  \\

Zum Vergleich sind außerdem die Näherungswerte nach Gleichung \ref{eq:cylindrical_naeherung} für die lange und die kurze Spule sowie der Näherungswert nach Gleichung \ref{eq:helmholtz_naeherung} für das \textsc{Helmholtz}-Spulenpaar in die Abbildungen eingezeichnet. Die bestimmten Werte sind in Tabelle \ref{tab:spulen_ergebnisse} aufgeführt. Für den Fehler der langen und kurzen Spule gilt:

\begin{equation*}
\sigma_B = \frac{B}{I} \cdot \sigma_I\; .
\end{equation*}

Für den Fehler des \textsc{Helmholtz}-Spulenpaars gilt:

\begin{equation*}
\sigma_{B} = B \cdot \sqrt{\left(\frac{\sigma_I}{I} \right)^2 + \left( \frac{\sigma_R}{R} \right)^2} \; .
\end{equation*}

\subsection{Vergleich der drei Spulen}

Zum Vergleich der Felder sind die Messwerte der Messungen mit der \textsc{Hall}-Sonde aller drei Spulen in Abbildung \ref{fig:vergleich} eingezeichnet. In Abbildung \ref{fig:vergleich_normiert} wurde außerdem $z$ mit der halben Spulenlänge $l/2$ sowie $B$ mit der Flussdichte bei $z=0$ $B(0)$ normiert, um so die Homogenität der Felder besser beurteilen zu können. Die Fehlerbalken wurden zur besseren Vergleichbarkeit in diesen Abbildungen nicht eingezeichnet.


\subsection{Bestimmung der magnetischen Feldkonstante}

Zur Bestimmung der magnetischen Feldkonstante $\mu_0$ lässt sich Gleichung \ref{eq:H_to_B} verwenden. Unter der Annahme, dass für die Luft im Labor gilt, dass $\mu_r = 1$, folgt:

\begin{equation}
	\mu_0 = \frac{B}{H} = \frac{B_{exp}}{H_{theo}} \; .
\end{equation}

Somit lässt sich aus den experimentell bestimmten Flussdichten $B_{exp}$ und den theoretisch berechneten magnetischen Feldern $H_{theo}$ die magnetische Feldkonstante bestimmen. In Tabelle \ref{tab:mu0} sind die Mittelwerte der berechneten Feldkonstanten für jede der Messreihen aufgeführt. Da mit zunehmender Entfernung und abnehmendem magnetischen Feld davon auszugehen ist, dass das Feld zunehmend inhomogen und die Messwerte entsprechend ungenauer sind, wurde für diese Berechnung nur das Intervall der Messerwerte bis einschließlich 18 cm Entfernung vom Spulenmittelpunkt betrachtet.

\begin{table}
	\centering
	\renewcommand{\arraystretch}{1.5}
	\begin{tabular}{|c|c|c|c|}\hline
		\textbf{Messreihe }		& \textbf{Feldkonstante $\mu_0$ [$10^{-6}\;  \text{N}/\text{A}^2$]}& \textbf{Abweichung [\%]}\\\hline
		lange Spule (Induktionsmessung) 	& $1.27 \pm 0.03$		& $1.1$		\\\hline
		lange Spule (\textsc{Hall}-Messung)	& $1.22 \pm 0.01$		& $2.9$ 	\\\hline
		kurze Spule				& $1.24 \pm 0.01$		& $1.3$		\\\hline
		Helmholtz-Spulenpaar 	& $1.14 \pm 0.04$ 		& $9.3$		\\\hline\hline
		gewichteter Mittelwert 	& $1.22 \pm 0.01$ 		& $2.9$		\\\hline
		Literaturwert 			& $4 \pi /10 \approx 1.257$ & ---	\\\hline
	\end{tabular}
	\caption{Mittelwerte für die Feldkonstante $\mu_0$, berechnet aus jeder der vier Messreihen sowie Abweichung vom Literaturwert.}
	\label{tab:mu0}
\end{table}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{../evaluation/lange_spule.png}
	\caption{Verlauf der experimentell mittels Induktionsspule und \textsc{Hall}-Sonde bestimmten und theoretisch berechneten magnetischen Flussdichte $B$ in der langen Spule in Abhängigkeit vom Abstand $z$ zum Spulenmittelpunkt sowie Näherungswert für lange Spulen.}
	\label{fig:lange_spule}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{../evaluation/kurze_spule.png}
	\caption{Verlauf der experimentell mittels \textsc{Hall}-Sonde bestimmten und theoretisch berechneten magnetischen Flussdichte $B$ in der kurzen Spule in Abhängigkeit vom Abstand $z$ zum Spulenmittelpunkt sowie Näherungswert für lange Spulen.}
	\label{fig:kurze_spule}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{../evaluation/helmholtz_spule.png}
	\caption{Verlauf der experimentell mittels \textsc{Hall}-Sonde bestimmten und theoretisch berechneten magnetischen Flussdichte $B$ im \textsc{Helmholtz}-Spulenpaar in Abhängigkeit vom Abstand $z$ zum Spulenmittelpunkt sowie Näherungswert für \textsc{Helmholtz}-Spulen inklusive Fehlerbalken.}
	\label{fig:helmholtz_spule}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{../evaluation/vergleich.png}
	\caption{Vergleich der Felder der drei Spulen abhängig vom Abstand $z$ zum Spulenmittelpunkt. Die Fehlerbalken wurden zur besseren Vergleichbarkeit weggelassen.}
	\label{fig:vergleich}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.8\textwidth]{../evaluation/vergleich_normiert.png}
	\caption{Vergleich der Felder der drei Spulen. $z$ ist dabei mit der halben Spulenlänge $l/2$ normiert, die Flussdichte mit $B(0)$. Auch hier sind aufgrund der besseren Vergleichbarkeit keine Fehler eingezeichnet.}
	\label{fig:vergleich_normiert}
\end{figure}