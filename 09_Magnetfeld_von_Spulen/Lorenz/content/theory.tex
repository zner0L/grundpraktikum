\subsection{Der magnetische Fluss}

Durchsetzt ein \textit{magnetisches Feld} $\vec{H}$ eine Fläche, so stellt sich durch sie ein \textit{magnetischer Fluss} $\Phi$ ein. Steht dieser in Verhältnis zur durchflossenen Fläche, ergibt sich die \textit{magenetische Flussdichte} $\vec{B}$. Durchfließt nun ein externes magnetisches Feld ein sog. lineares Material, so wird dieses proportional zur Stärke des Feldes magnetisiert. Daraus lässt sich folgern, dass in linearen Materialien $\vec{B}$ eine Proportionalität zu $\vec{H}$ aufweist. Durch die Einführung einer Materialkonstante, der \textit{relativen Permeabilität} $\mu_r$ lässt sich erhalten \cite[Sec. 6.4.1]{griffiths08}:

\begin{equation}
	\vec{B} = \mu_0 \mu_r \vec{H} \; .
	\label{eq:H_to_B}
\end{equation}

Wobei $\mu_0$ die Permeabilität des Vakuums, die \textit{magnetische Feldkonstante}, meint. Sie ist eine Naturkonstante und für sie gilt: $\mu_0 = 4 \, \pi \cdot 10^{-7} \frac{\mathrm{H}}{\mathrm{m}} \approx 1.257 \cdot 10^{-6} \frac{\mathrm{H}}{\mathrm{m}}$. In Räumen ohne Materie beträgt $\mu_r = 1$, in der Luft ist die Näherung $\mu_r \approx 1$ ebenfalls zulässig. \\

\textsc{Faraday} erkannte, dass die Änderung des magnetischen Flusses durch einen Leiter dort eine Spannung induziert. Quantifiziert ergibt diese Aussage das \textsc{Faraday}'sche Gesetz der Induktion:

\begin{equation}
	U_{Ind} = - \frac{\mathrm{d}\Phi}{\mathrm{d}t} = - \frac{\mathrm{d}}{\mathrm{d}t} \int \vec{B} \mathrm{d} \vec{A}
	\label{eq:farraday_induction}
\end{equation}

\subsection{Magnetfelder bewegter Ladungen}

Bewegte Ladungen bzw. Ströme erzeugen magnetische Wirbelfelder rund um den durchströmten Leiter. Nach dem Gesetz von \textsc{Biot-Savart} lässt sich feststellen, dass für einen Strom $I$ entlang von $\vec{l}$ für das magnetische Feld am Ort $\vec{r}$ gilt \cite[Sec. 5.2.2]{griffiths08}:

\begin{equation}
	\vec{H}(\vec{r}) = \frac{1}{4\pi} I \int \frac{\mathrm{d}\vec{l} \times \vec{r}}{r^2} \; .
	\label{eq:biot-savart}
\end{equation}

Für geschlossene Leiterschleifen $\mathcal{C}$ gilt außerdem das \textsc{Ampère}'sche Gesetz, nach welchem das Integral über das Feld entlang der Schleife dem eingeschlossenem Strom $I_{innen}$ entspricht \cite[Sec. 5.3.3]{griffiths08}:

\begin{equation}
	\oint\limits_{\mathcal{C}} \vec{B} \cdot d\vec{l} = \mu_0 I_{innen} \; .
	\label{eq:ampere}
\end{equation}

\subsubsection{Magnetfeld einer Spule}

Mithilfe des Gesetztes von \textsc{Biot-Savart} ergibt sich zunächst das Magnetfeld eines geschlossenen Kreisleiters und schließlich, mit der Erkenntnis, dass eine Spule eine Aneinanderreihung von Kreisleitern ist, auch das Magnetfeld einer Spule. So folgert \cite[Sec. 3.2.6.4]{demtroeder06} für das Magnetfeld einer Zylinderspule am Ort $z$:

\begin{equation}
	H(z) =  \frac{n I}{2l} \left( \frac{z+\frac{l}{2}}{\sqrt{R^2 + \left(z+\frac{l}{2}\right)^2}} - \frac{z-\frac{l}{2}}{\sqrt{R^2 + \left(z-\frac{l}{2}\right)^2}} \right) \; .
	\label{eq:cylindrical_coil}
\end{equation}

Dabei bezeichnet $n$ die Windungszahl, $I$ den durch die Spulen fließenden Strom, $l$ die Länge der Spule und $R$ den Spulenradius. Näherungsweise lässt sich das Feld einer Zylinderspule auch einfacher ausdrücken (vgl. \cite[Sec. 5.3.3]{griffiths08}):
\begin{equation}
B=\frac{\mu_0 n I}{l} \; .
\label{eq:cylindrical_naeherung}
\end{equation}

\subsubsection{Magnetfeld eines Helmholtz-Spulenpaars}
\label{sec:theo_helmholtz}

Ein \textsc{Helmholtz}-Spulenpaar besteht aus zwei Ringspulen, die im Abstand ihres Radius' gegenüber voneinander aufgestellt sind. Werden beide von demselben Strom $I$ durchflossen, so stellt sich ein Magnetfeld ein. Mit der Annahme, dass die Spule dem Vielfachen von Kreisleitern entspricht, ergibt sich dann nach \cite[Sec. 3.2.6.3]{demtroeder06}:

\begin{equation}
	H(z) = \frac{I R^2}{2} \cdot \left\{ \frac{1}{[(z+\frac{d}{2})^2+R^2]^{\frac{3}{2}}} + \frac{1}{[(z-\frac{d}{2})^2+R^2]^{\frac{3}{2}}} \right\}
	\label{eq:helmholtz_coil}
\end{equation}

Für ein \textsc{Helmholtz}-Spulenpaar kann das Feld ebenfalls bei $z = 0$ angenähert werden. Nach \cite[Sec. 3.2.5]{salditt17} ergibt sich demnach:
\begin{equation}
B_H = \mu_0 \frac{8I}{\sqrt{125} \cdot R} \; .
\label{eq:helmholtz_naeherung}
\end{equation}

\section{Messgeräte und -methoden}

\subsection{Hall-Sonde}

Um Magnetfelder zu bestimmen wird sich oft der \textsc{Hall}-Effekt zu Nutze gemacht. Eine \textsc{Hall}-Sonde, die für diesen Zweck entwickelt wurde, besteht aus einem leitenden Plättchen, durch welches ein Strom $I$ geleitet wird (Abbildung \ref{fig:hall}). Im Magnetfeld werden die Elektronen im Leiter durch die \textsc{Lorentz}kraft $F_L$ abgelenkt, sodass eine \textsc{Hall}spannung $U_H$ auftritt. Diese ist dann proportional zur Flussdichte des Magnetfeldes am Ort der Sonde. Ist die Sonde entsprechend klein genug, kann so näherungsweise das Feld an einem Punkt bestimmt werden.

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{img/Hall.png}
	\caption{Skizze einer \textsc{Hall}-Sonde im $B$-Feld.}
	\label{fig:hall}
\end{figure}

\subsection{Stromintegrator}
\label{sec:integrator}

Ein Stromintegrator wird dazu eingesetzt eine in einer bestimmten Zeit geflossene Ladung zu messen, also das Integral über den geflossenen Strom zu bestimmen. Wie in Abbildung \ref{fig:integrator} zu erkennen, besteht dieser aus einem invertierenden Operationsverstärker, einem Kondensator und einem Eichwiderstand, hier mit $R_\kappa$ bezeichnet. \\

\begin{figure}[h]
	\centering
	\includegraphics[width=0.6\textwidth]{img/Analoger_Stromintegrator_bearbeitet.png}
	\caption{Analoger Stromintegrator aus Operationsverstärker, Kondensator und Widerstand \cite{lp_integrator}.}
	\label{fig:integrator}
\end{figure}

Für den Strom am Kondensator $I_C$ gilt dann nach den \textsc{Kirchhoff}'schen Regeln (mit dem Eingangsstrom $I_{R_\kappa}$):

\begin{equation*}
	I_A = \dot{Q}_C = C \dot{U}_A ~~ = ~~ \frac{U_E}{R_\kappa} = I_{R_\kappa} \; .
\end{equation*}

Daraus folgt dann also für die zwischen $t_0$ und $t$ geflossene Ladung $Q$:

\begin{equation}
	U_A = \frac{1}{R_\kappa C} \int_{t_0}^{t} U_E \mathrm{d}t = \frac{Q}{C} \; .
	\label{eq:integrator}
\end{equation} 

Folglich ist die Ausgangsspannung $U_A$ am Stromintegrator proportional zur geflossenen Ladung. Für den im Versuch verwendeten Integrator ist die Skala nicht geeicht, sodass eine Eichkonstante bestimmt werden muss, um von den Skalenwerten $x$ auf die Ladung schließen zu können. Es wird also ein $\kappa$ gesucht, sodass $Q = \kappa x$. Bleibt die Eingangsspannug $U_E$ konstant, so gilt dann für die Eichkonstante in einem bekannten Zeitintervall $\Delta t$:

\begin{equation}
	\kappa = \frac{U_E}{R_\kappa x} \Delta t \; .
	\label{eq:kappa}
\end{equation}

Da $U_E$ und $\Delta t$ direkt aus dem Versuchsaufbau folgen, ist schließlich noch $R_\kappa$ zu bestimmen. Nach dem Schaltbild in Abbildung \ref{fig:eichschaltung} lässt sich nach den \textsc{Kirchhoff}'schen Regeln folgern:

\begin{equation*}
R_2 I_2 = (R_{Int} + R_L) I_{\kappa} \Leftrightarrow I_2 = \frac{R_{Int} + R_L}{R_2} I_{\kappa} \; .
\end{equation*}

Für den Gesamtstrom lässt sich also schließen: 

\begin{equation*}
I_{ges} = I_\kappa + I_2 = \left(1 +  \frac{R_{Int} + R_L}{R_2} \right) I_\kappa \; .
\end{equation*}

Daraus folgt dann:

\begin{equation}
\begin{split}
R_\kappa &= \frac{U}{I_{\kappa}} = \frac{U}{I_{ges}} \left( 1 +  \frac{R_{Int} + R_L}{R_2} \right) = \left(  R_1 + \frac{1}{\frac{1}{R_2} + \frac{1}{R_L + R_{\text{Int}}}} \right)  \left( 1 +  \frac{R_{Int} + R_L}{R_2} \right) \\
&= \frac{R_1}{R_2}(R_2 + R_L + R_{\text{Int}} ) + R_L + R_{\text{Int}} \;. 
\label{eq:rkappa}
\end{split}
\end{equation}

\subsection{Magnetfeldbestimmung mithilfe einer Induktionsspule}
\label{sec:measure_induction}

Neben der \textsc{Hall}-Sonde lässt sich ein Magnetfeld auch mithilfe einer Spule vermessen. Dazu wird das Induktionsgesetz nach \textsc{Faraday} (Gleichung \ref{eq:farraday_induction}) verwendet. Demnach gilt für die geflossene Ladung durch eine Induktionsspule im Magnetfeld mit Windungszahl $n$, Radius $r$ und Spulenwiderstand $R_{Ind}$:

\begin{equation}
	Q = \int I \mathrm{d}t = \int \frac{U_{Ind}}{R_{Ind}} \mathrm{d}t = - \frac{1}{R_{Ind}} \int \frac{\mathrm{d}\Phi}{\mathrm{d}t} \mathrm{d}t = - \frac{n \pi r^2}{2 R_{Ind}} B \; .
\end{equation}

Die geflossene Ladung ist also proportional zur Flussdichte des Magnetfeldes durch den Querschnitt der Spule.