\subsection{Güte der Bestimmung der Eichkonstante}

Mögliche Fehlerquellen bei der Bestimmung der Eichkonstante können die Genauigkeit des Zeitschalters, die mit dem Zeitschalter angelegte Spannung $U_E$, die angegebenen Widerstände $R_i$ sowie der Spulenwiderstand $R_L$ sein. Die Spannung des Zeitschalters wurde mit dem Multimeter überprüft und war im Rahmen der Messgenauigkeit von $\pm \; 5 \, \%$ exakt (Gleichung \ref{eq:eingangsspannung}). Der Induktionsspulenwiderstand $R_L$ wurde mit dem Multimeter gemessen und ist entsprechend sehr genau (Gleichung \ref{eq:widerstaende_eichung}), zudem trägt er nur geringfügig zum Eichwiderstand $R_{\kappa}$ bei, da er sehr viel kleiner als $R_2$ und $R_{\text{int}}$ ist (vgl. \mbox{Tabelle \ref{tab:widerstaende}}). Über die Genauigkeit der angegebenen Widerstände $R_1$, $R_2$ und $R_{\text{int}}$ kann hier nichts ausgesagt werden. \\
 
Die Werte des Stromintegrators in Abbildung \ref{plot:eichgerade} weichen geringfügig von der Regressionsgeraden ab, entsprechend liegt der Fehler der Steigung bei unter 3 \% (Gleichung \ref{eq:steigung}). Der analoge Stromintegrator arbeitet offenbar sehr genau. Insgesamt ist davon auszugehen, dass die Eichkonstante $\kappa$ präzise bestimmt wurde.

\subsection{Güte der Messung mit Induktionsspule}

Der mit der Induktionsspule gemessene Feldverlauf der langen Spule (Abbildung \ref{plot:lange_spule}) liegt innerhalb der Spule über und außerhalb unter dem erwarteten Theoriewert. Die maximale Abweichung beträgt ca. $+ 0,05$ mT (5 \%) innerhalb und ca. $- 0,06$ mT (60 \%) außerhalb der Spule. Grund für die hohe prozentuale Abweichung außerhalb der Spule ist, dass der registrierte Wert ab einer bestimmten Entfernung nahezu null ist. Es überrascht, dass die Messung im Randbereich der Spule am exaktesten mit dem erwarteten Theorieverlauf zusammenfällt, da in diesem Bereich der Abfall der Flussdichte am stärksten ist und entsprechend kleine Fehler im Abstand $x$ zu großen Abweichungen der Messung führen. Möglich ist, dass solche Schwankungen von der Induktionsspule ausgeglichen werden, da die Messung des magnetischen Flusses über die Fläche $\mathcal{A}$ gemittelt stattfindet. \\

Eine mögliche Fehlerquellen könnte darin liegen, dass die Induktionsspule nicht genau parallel in der langen Spule ausgerichtet wurde. Die Orientierung gestaltete sich als schwierig, da sie nur nach Augenmaß erfolgte. Wird die Induktionsspule nicht parallel zur Flussdichte ausgerichtet, so führt dies nach Gleichung \ref{eq:phi_discussion} zu einer Abweichung der Messwerte nach unten, die sich im nahezu homogenen Feld innerhalb der Spule jedoch zunächst nur geringfügig bemerkbar machen würde. Erst außerhalb der Spule im inhomogenen Feld fiele sie stärker ins Gewicht. Hierdurch könnte sich die gegensätzliche Abweichung vom Theoriewert innerhalb und außerhalb der Spule erklären lassen. Die Werte für Windungszahl $N$ und Durchmesser $d$ der Induktionsspule waren an der Apparatur angegeben. Insbesondere bei der Windungszahl kann davon ausgegangen werden, dass sie exakt ist. \\

Auch wenn es zunächst plausibel erscheinen mag, den Abfall der Flussdichte oberhalb von 30 cm durch den Einfluss der Experimente anderer Gruppen zu erklären, so ist dies doch zurückzuweisen, da die Induktionsspule nur die Änderung der Flussdichte erfasst. Ab der Messung bei 30 cm müsste also jedes mal gleichzeitig eine andere Spule ein- bzw. ausgeschaltet worden sein, was sehr unwahrscheinlich erscheint. Wahrscheinlicher ist, dass bei großen Entfernungen die Induktionsspule zunehmend unempfindlicher für Änderungen der Flussdichte wird.

\subsection{Güte der Messung mit Hallsonde}
\label{sec:dis_hall}

Die mit der Hallsonde gemessene Flussdichte der langen Spule (Abbildung \ref{plot:lange_spule}) liegt innerhalb der Spule leicht über und außerhalb leicht unter dem erwarteten Theoriewert. Die maximalen Abweichungen liegen bei $+ 0,03$ mT (2 \%) innerhalb bzw. $- 0,02$ mT (15 \%) außerhalb. Ein ähnliches Bild zeigt sich bei der Vermessung der kurzen Spule (Abbildung \ref{plot:kurze_spule}), wo die Abweichungen zwischen etwa $+ 0,03$ mT (2 \%) und $- 0,04$ mT (13 \%) liegen. Auch bei der Helmholtz-Spule (Abbildung \ref{plot:helmholtz_spule}) zeigt sich dieser Unterschied zu ca. $+ 0,11$ mT (3 \%) bzw. $- 0,07$ mT (4 \%). Allerdings würden bei dieser Messung die beiden Feldverläufe besser zusammenfallen, wenn die experimentellen Messwerte um ca. 0,5 cm entlang der $z$-Achse systematisch nach rechts verschoben würden. Möglicherweise wurde die Messung nicht genau im Mittelpunkt der Spule begonnen. Aufgrund des geringen Abstandes zwischen den beiden Helmholtz-Spulen war eine exakte Ausrichtung hier schwieriger zu bewerkstelligen als bei den beiden anderen Spulen. \\

Eine potenzielle Fehlerquelle liegt in der Kalibrierung der Hallsonde, deren Offset teilweise vor und nach der Vermessung einer Spule unterschiedlich war (Gleichung \ref{eq:offset}). Möglicherweise könnte eine Verbesserung durch die Erfassung des Offsets nach jedem Messwert mit ausgeschaltetem Spulenstrom erreicht werden. Allerdings ist auch nicht auszuschließen, dass die Magnetfelder der anderen Gruppen die Ursache für die Schwankungen des Offsets waren. Die unterschiedlichen Abweichungen innerhalb und außerhalb der Spule könnten sich wie im vorangegangenen Abschnitt erklären lassen. Die geringeren Abweichungen würden dann bedeuten, dass die Hallsonde besser ausgerichtet wurde als die Induktionsspule. 

\pagebreak

\subsection{Vergleich der Messmethoden}
\label{sec:dis_vergleich_mess}

Bei der Vermessung des Feldes der langen Spule liefert die Hallsonde insgesamt genauere Werte, die näher am Theoriewert liegen. Auch wenn die Induktionsspule unanfälliger gegenüber Schwankungen des äußeren Feldes ist (solange diese nicht genau während einer Messung stattfinden), erwies sich die aufwändigere Messung über den Stromintegrator doch als fehleranfälliger. Die integrierte Elektronik der Hallsonde funktioniert offenbar präziser als der experimentelle Aufbau im Labor. Zudem entfällt bei Verwendung der Hallsonde die Kalibrierung des Stromintegrators. Für Messungen zu nichtdidaktischen Zwecken ist die Hallsonde also der Induktionsspule vorzuziehen.


\subsection{Vergleich der Spulenfelder}
\label{sec:dis_vergleich_felder}

In Abbildung \ref{plot:vergleich} sind die Feldverläufe der drei Spulen in einem Graphen dargestellt. Im Spulenmittelpunkt ist das Feld der Helmholtz-Spulen etwa 3,2 mal so stark wie das der langen Spule. Da die Durchmesser $d$ der beiden Spulen sowie der fließende Strom $I$ bei beiden Spulen nahezu identisch sind und das Helmholtzspulenpaar insgesamt weniger als doppelt so viele Windungen wie die lange Spule aufweist (vgl. Tabelle \ref{tab:spulendaten}), ist das starke Magnetfeld auf die besondere Geometrie der Helmholtz-Spulen zurückzuführen. Das Feld der kurzen Spule ist im Mittelpunkt ca. 20 \% schwächer als das der langen Spule, was laut Gleichung \ref{eq:biotsavart} in den Abweichungen in Windungszahl $N$, Durchmesser $d$ und Länge $l$ begründet liegt (Tabelle \ref{tab:spulendaten}). \\

Die Homogenität der Spulenfelder lässt sich anhand der normierten Feldverläufe in Abbildung \ref{plot:vergleich_normiert} vergleichen. Ein Wert von 1 auf der Abszisse entspricht dem $z$-Wert am Spulenrand, auf der Ordinate entspricht 1 dem Wert der Feldstärke in der Mitte der Spule. Durch die Normierung wird besonders deutlich, dass das Feld der Helmholtz-Spulen in einem weiten Bereich um den Spulenmittelpunkt sehr homogen verläuft. Das Feld der langen Spule fällt schneller ab, ist jedoch immer noch deutlich homogener als das der kurzen Spule. Am Spulenrand beträgt die Flussdichte in den Helmholtz-Spulen noch ca. 95 \% des Maximalwertes, während es in der langen Spule auf 65 \% bzw. auf 14 \% in der kurzen Spule abfällt. Das Feld der kurzen Spule ist nur in einem kleinen Bereich um den Mittelpunkt homogen. Entsprechend weicht der Wert der Näherung für lange Spulen (vgl. Gleichung \ref{eq:ergebnis_naeherung} und Abbildung \ref{plot:kurze_spule}) im Mittelpunkt der kurzen Spule um ca. 24 \% von den Messwerten ab, während es in der langen Spule (Gleichung \ref{eq:ergebnis_naeherung} und Abbildung \ref{plot:lange_spule}) nur 4 \% sind. 

\pagebreak 


\subsection{Bestimmung der Feldkonstanten}

Der Literaturwert für die Feldkonstante $\mu_0$ liegt nur im Fehlerbereich der Messung mit der Induktionsspule (Tabelle \ref{tab:mu0}). Dies überrascht, da wie in Abschnitt \ref{sec:dis_vergleich_felder} diskutiert diese Messreihe absolut gesehen am stärksten von den erwarteten theoretischen Werten nach Gleichung \ref{eq:lange_spule} abweicht. Offenbar entspricht sie relativ betrachtet dem Theorieverlauf jedoch genauer als die anderen Messreihen, sodass die Abweichung vom Literaturwert lediglich ca. 1,1 \% beträgt. Der zweitgenaueste Wert (1,3 \% Abweichung) stammt aus der Vermessung der kurzen Spule mit der Hallsonde. In den vergleichsweise homogenen Feldern der langen Spule bzw. der Helmholtz-Spulen wurden mit der Hallsonde deutlich schlechtere Werte für $\mu_0$ bestimmt, die um 2,9 bzw. 9,3 \% vom Literaturwert abweichen. Bei den Helmholtz-Spulen könnte die in Abschnitt \ref{sec:dis_hall} besprochene fehlerhafte Ausrichtung bei $z = 0$ die hohe Abweichung erklären. Der Literaturwert liegt auch außerhalb des Fehlers des gewichteten Mittels der vier Messreihen. Die relative Abweichung liegt bei 2,9 \% (Tabelle \ref{tab:mu0}). \\

Der in Abschnitt \ref{sec:dis_vergleich_mess} diskutierte Vergleich der Messmethoden bezüglich ihrer absoluten Abweichung von den Theoriewerten spiegelt sich also nicht in den Ergebnissen für die Feldkonstante $\mu_0$ wieder. Hierfür ist nach Gleichung \ref{eq:mu0_ausw} die relative Qualität der Messung entscheidend, worin die Methode der Induktionsspule besser abschneidet als die Hallsonde. Möglicherweise ist dabei von Vorteil, dass die Messung in der Induktionsspule über eine größere Fläche gemittelt stattfindet als bei der Hall-Sonde. Um zu untersuchen, ob dieses Ergebnis nur dem Zufall geschuldet ist oder ob die Induktionsspule für relative Messungen tatsächlich prinzipiell besser geeignet ist als die Hallsonde, wären noch weitere Versuche vonnöten.

\pagebreak

\section{Anhang: Berechnung spezieller Magnetfelder}
\label{sec:anhang}

\subsection{Magnetfeld einer langen Spule}
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{img/lange_spule_bearbeitet.png}
	\caption{Skizze einer langen Spule der Länge $l$ mit Integrationsweg und Ampère'scher Schleife zur Bestimmung der magnetischen Flussdichte $B$ \cite[Kap. 3.2.2, bearbeitet]{demtroeder06}.}
	\label{pic:lange_spule}
\end{figure}
Das Magnetfeld einer sehr langen Spule der Länge $L$ mit Windungsdichte $n$ (Abbildung \ref{pic:lange_spule}) ist im Inneren der Spule annähernd konstant. Zur Berechnung der Feldstärke $H$ bietet sich wegen der Symmetrie der Spule das Ampère'sche Gesetz (Gleichung \ref{eq:ampere}) an. Für eine Ampère'sche Schleife außerhalb der Spule verschwindet der eingeschlossene Strom:
\begin{equation*}
	\oint\limits_\mathcal{C} \vec{H} \cdot \text{d}\vec{l} = 0 \;. 
\end{equation*}
Da die Wahl der Schleife beliebig war, ist auch $H = 0$ und da für große Entfernungen $H$ gegen null geht, muss $H$ überall außerhalb der Spule null sein \cite[Kap. 5.3.3]{griffiths08}. Für das Feld im Inneren der Spule lässt sich eine Ampère'sche Schleife wie in Abbildung \ref{pic:lange_spule} gezeigt wählen. Da das Wegintegral nur für die Strecke $\overline{BA}$ einen Beitrag liefert, gilt
\begin{equation*}
	\oint\limits_\mathcal{C} \vec{H} \cdot \text{d}\vec{l} = BL = \mu_0 n I  \; .
\end{equation*}
Es folgt
\begin{equation}
	B=\begin{cases}
	\; \mu_0 n I, & \text{innerhalb der Spule,}\\
	\; 0, & \text{außerhalb der Spule.}
	\end{cases}
	\label{eq:lange_spule_naeherung_anhang}
\end{equation}
Für eine Spule endlicher Dimension mit Länge $l$, Radius $r$ und Windungszahl \mbox{$N = n \cdot l$} lässt sich $B$ unter Berücksichtigung der nun auftretenden Randeffekte über das Gesetz von Biot-Savart (Gleichung \ref{eq:biotsavart}) berechnen. Nach Wahl des Koordinatenurpsrungs im Spulenmittelpunkt und Transformation auf Zylinderkoordinaten zur Erleichterung der Rechnung folgt \cite[Kap. 3.2.6.4]{demtroeder06}
\begin{equation}
	B(z) = \frac{\mu_0  NI }{2l} \cdot \left(
	\frac{z + \frac{l}{2}}{\sqrt{(z + \frac{l}{2})^2 + r^2}} -
	\frac{z - \frac{l}{2}}{\sqrt{(z - \frac{l}{2})^2 + r^2}}
	\right) \; .
	\label{eq:lange_spule_anhang}
\end{equation}

\subsection{Magnetfeld einer Helmholtzspule}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{img/helmholtz_bearbeitet.png}
	\caption{Schematische Darstellung eines Helmholtzspulenpaares bestehend aus zwei Spulen des Radius $r$ im Abstand $d=r$ \cite[Kap. 3.2.6, bearbeitet]{demtroeder06}.}
	\label{pic:helmholtz}
\end{figure}
Die Flussdichte zwischen zwei kreisförmigen Leiterschleifen im Abstand $d$ mit Radius $r$, die in gleicher Richtung vom Strom $I$ durchflossen werden (Abbildung \ref{pic:helmholtz}), lässt sich auf der Symmetrieachse über das Gesetz von Biot-Savart (Gleichung \ref{eq:biotsavart}) berechnen zu \mbox{\cite[Kap 3.2.6.3]{demtroeder06}}
\begin{equation}
	B(z) = \frac{\mu_0 I  r^2}{2} \cdot \left(
	\frac{1}{\sqrt{(z + \frac{d}{2})^2 + r^2}^3} +
	\frac{1}{\sqrt{(z - \frac{d}{2})^2 + r^2}^3}
	\right) \; .
	\label{eq:helmholtz_anhang}
\end{equation}
Nach der Taylorentwicklung um den Ursprung fällt auf, dass für eine Wahl des Abstandes $d = r$ bis zur dritten Ordnung alle Terme verschwinden bis auf \cite[Kap. 3.2.5]{salditt17}
\begin{equation}
	B(z) = \mu_0 \frac{8I}{\sqrt{125} \cdot r} \; .
	\label{eq:helmholtz_naeherung_anhang}
\end{equation}
Für $N$ einzelne Leiterschleifen (Windungen) heißt eine solche Anordnung Helmholtz-Spulenpaar bzw. kurz Helmholtzspule. Sie bietet sich an, um im Experiment möglichst homogene Feldstärken zu erzeugen. 