\subsection{Magnetfelder in Materie}

Wird ein bestimmtes Material mit einem äußeren Magnetfeld der Feldstärke $\vec{H}$ durchsetzt, so stellt sich im Material eine Flussdichte $\vec{B}$ ein. Es besteht der Zusammenhang \cite[Kap. 6.4.1]{griffiths08}
\begin{equation}
	\vec{B} = \mu_0 \, (\vec{H} + \vec{M}) \; .
	\label{eq:felder}
\end{equation}
Dabei bezeichnet \cite[Anhang]{demtroeder06}
\begin{equation}
	\mu_0 = 4\pi \cdot 10^{-7} \; \frac{N}{A^2}
	\label{eq:const}
\end{equation}
die magnetische Feldkonstante und $\vec{M}$ die Magnetisierung des Materials, was dem magnetischen Moment pro Volumeneinheit entspricht. In vielen Materialien ist $\vec{M}$ proportional zu $\vec{B}$. Eine charakteristische Materialeigenschaft ist der als magnetische Suszeptibilität bezeichnete Proportionalitätsfaktor $\chi$. Gleichung \ref{eq:felder} lautet dann
\begin{equation}
		\vec{B} = \mu_0 \, (1 + \chi) \, \vec{H} = \mu_0 \mu_r \vec{H} \; .
		\label{eq:mu_null}
\end{equation}
Dabei ist $\mu_r = 1 + \chi$ die relative Permeabilität des Materials. Sie ist im Vakuum gleich 1 und auch in Luft kann in guter Näherung $\mu_r \approx 1$ angenommen werden.

\subsection{Gesetze von Biot-Savart und Ampère}

Bei Kenntnis des Stroms $I$ durch einen beliebig geformten dünnen Leiter liefert das Gesetz von Biot-Savart die sich am Ort $\vec{r}$ einstellende Flussdichte $\vec{B}$ \cite[Kap. 5.2.2]{griffiths08}:
\begin{equation}
	\vec{B}(\vec{r})= \frac{\mu_0}{4 \pi} I \int\limits_\mathcal{C} \frac{\text{d}\vec{l} \times (\vec{r} - \vec{r}\,')}{|\vec{r} - \vec{r}\,'|^3} \;.
	\label{eq:biotsavart}
\end{equation}
Dabei wird der Leiter durch die Kurve $\mathcal{C}$ parametrisiert. Mit $\text{d}\vec{l}$ wird der infinitesimale Tangentenvektor am Ort $\vec{r} \, '$ an die Kurve $\mathcal{C}$ bezeichnet. Für symmetrische Leiter bietet sich auch das Ampèresche Gesetz zur Bestimmung der Feldstärke an \cite[Kap. 4.6]{demtroeder06}:
\begin{equation}
	\oint\limits_\mathcal{C} \vec{H} \cdot \text{d}\vec{l} = I_{\text{inc}} \; .
	\label{eq:ampere}
\end{equation}
Hierbei bezeichnet $\mathcal{C}$ eine beliebige geschlossene Leiterschleife und $I_{\text{inc}}$ ist der von ihr eingeschlossene Strom.

\subsection{Spezielle Magnetfelder}

Aus Gleichungen \ref{eq:biotsavart} und \ref{eq:ampere} lässt sich für eine vom Strom $I$ durchflossene Spule der Windungszahl $N$ die Flussdichte $B$ auf der Mittelachse im Abstand vom Urpsrung $z$ sowohl exakt wie auch als Näherung berechnen. Zur Herleitung sei auf den Anhang (Abschnitt \ref{sec:anhang}) verwiesen. Für eine Spule der Länge $l$ ergibt sich (Gleichung \ref{eq:lange_spule_anhang} nach \cite[Kap. 3.2.6.4]{demtroeder06})
\begin{equation}
	B(z) = \frac{\mu_0  NI }{2l} \cdot \left(
	\frac{z + \frac{l}{2}}{\sqrt{(z + \frac{l}{2})^2 + r^2}} -
	\frac{z - \frac{l}{2}}{\sqrt{(z - \frac{l}{2})^2 + r^2}}
	\right) \; .
	\label{eq:lange_spule}
\end{equation}
Für ein Helmholtz-Spulenpaar mit Radius $r$ im Abstand $d = r$ folgt (Gleichung \ref{eq:helmholtz_anhang} nach \cite[Kap 3.2.6.3]{demtroeder06})
\begin{equation}
	B_H(z) = \frac{\mu_0 I  r^2}{2} \cdot \left(
	\frac{1}{\sqrt{(z + \frac{d}{2})^2 + r^2}^3} +
	\frac{1}{\sqrt{(z - \frac{d}{2})^2 + r^2}^3}
	\right) \; .
	\label{eq:helmholtz}
\end{equation}
Die Flussdichte im Mittelpunkt einer langen Spule beträgt näherungsweise (Gleichung \ref{eq:helmholtz_naeherung_anhang} nach \cite[Kap. 5.3.3]{griffiths08}):
\begin{equation}
	B=\frac{\mu_0 n I}{l} \; .
	\label{eq:lange_spule_naeherung}
\end{equation}
Das Feld eines Helmholtz-Spulenpaares lässt sich bei $z = 0$ nähern durch (Gleichung \ref{eq:lange_spule_naeherung_anhang} nach \mbox{\cite[Kap. 3.2.5]{salditt17}}):
\begin{equation}
	B_H = \mu_0 \frac{8I}{\sqrt{125} \cdot r} \; .
	\label{eq:helmholtz_naeherung}
\end{equation}


\subsection{Magnetischer Fluss und Induktionsgesetz}
Der magnetische Fluss $\Phi$ ist ein Maß dafür, wie stark eine Fläche $\mathcal{A}$ von einem \mbox{Magnetfeld $\vec{B}$} durchsetzt ist. Er berechnet sich zu 
% Magnetfeld B in einer Zeile schreiben, geht z.B.: mit \mbox{...}
\begin{equation}
	\Phi = \int\limits_\mathcal{A} \vec{B} \cdot \text{d}\vec{A} \; .
	\label{eq:fluss}
\end{equation}
Für die durch eine Leiterschleife berandete Fläche $\mathcal{A}$ gilt das Faraday'sche Induktionsgesetz \cite[Kap. 8.1.2]{gerthsen10}
\begin{equation}
	U_{\text{ind}} = - \frac{\text{d}\Phi}{\text{d}t} \; .
	\label{eq:faraday}
\end{equation}
Eine zeitliche Änderung des magnetischen Flusses durch die Fläche $\mathcal{A}$ induziert in der Leiterschleife eine ihrer Ursache entgegengerichtete Spannung $U_{\text{ind}}$.

\section{Materialien und Methoden}

\subsection{Messmethoden}

Magnetfelder lassen sich mit einer Induktionsspule unter Ausnutzung des Faraday'schen Induktionsgesetztes (Gleichung \ref{eq:felder}) vermessen. Im unbekannten Magnetfeld der Flussdichte $\vec{B}$ wird eine kreisförmige Spule mit Radius $r$ und Windungszahl $N$ plaziert, so dass die Spulenachse parallel zu $\vec{B}$ ausgerichtet ist. Da die magnetische Flussdichte senkrecht zum Spulenquerschnitt steht, ergibt sich nach Gleichung \ref{eq:fluss} für den magnetischen Fluss durch die Spule:
\begin{equation}
	\Phi = \int_{\mathcal{A}} \vec{B} \cdot \text{d}\vec{A} = \frac{\pi}{2}Nr^2 B \; .
	\label{eq:phi_discussion}
\end{equation}
Wird das äußere Magnetfeld ausgeschaltet, so gilt nach Gleichung \ref{eq:faraday} mit der Induktionsspulenquerschnittsfläche $\mathcal{A}$ für die induzierte Spannung: 
\begin{equation*}
	U_{\text{ind}} = - \frac{\text{d}\Phi}{\text{d}t} \; = - N \cdot \frac{\text{d}}{\text{d}t}\int\limits_\mathcal{A} \vec{B} \cdot \text{d}\vec{A} \;.
\end{equation*}
Für die geflossene Ladung $Q$ folgt dann mit dem Widerstand im Induktionsstromkreis $R_{\text{ind}}$:
\begin{equation}
	Q = \int I \text{d}t = \int \frac{U_{\text{ind}}}{R_{\text{ind}}} \, \text{d}t
	= - \frac{1}{R_{\text{\text{ind}}}} \int \frac{\text{d}\Phi}{\text{d}t}\, \text{d}t
	= - \frac{\pi N r^2 B}{2 R_{\text{ind}}} \; ,
	\label{eq:ladung}
\end{equation}
% was ist Q, was ist I und ind nicht kursiv schreiben und was ist r²

\subsection{Hallsonde}

Mit einer Hallsonde kann der Betrag der magnetischen Flussdichte $B$ unbekannter Felder bestimmt werden. Ein vom konstanten Strom $I$ durchflossener quaderförmiger Leiter der Breite $b$ wird in ein äußeres, senkrecht zum Strom stehendes Magnetfeld der Flussdichte $B$ gebracht (Abbildung \ref{pic:hall}). 
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.5\textwidth]{img/Hallsonde_bearbeitet.jpg}
	\caption{Schematischer Aufbau einer Hallsonde zur Messung eines Magnetfeldes unbekannter Flussdichte \cite{lp_hall}.}
	\label{pic:hall}
\end{figure}
Durch die Lorentz-Kraft werden die fließenden Ladungen an entgegengesetzten Enden des Quaders getrennt. Es entsteht ein elektrisches Feld $E_H$, das eine der Lorentz-Kraft entegegengesetzte Kraft auf die Ladungen ausübt. Im Gleichgewicht kann die Hallspannung $U_H$ gemessen werden, welche proportional zum Betrag der Flussdichte ist \cite[Kap. 7.7.4]{gerthsen10}
\begin{equation*}
	U_H = E_H \cdot b = v \cdot b \cdot B \; .
\end{equation*}
Der Proportionalitätsfaktor ergibt sich als Produkt aus Quaderbreite $b$ und konstanter Ladungsgeschwindigkeit $v$, welche sich bei Kenntnis der Materialeigenschaften und des Stroms $I$ bestimmten lässt.


\subsection{Analoger Stromintegrator}
\label{sec:integrator}

Der analoge Stromintegrator dient der Messung von Ladungen $Q$. Er besteht aus einem invertierenden Operationsverstärker (OPV, engl. OpAmpl), einem Kondensator der Kapazität $C$ und einem Eichwiderstand $R_{\kappa}$, geschaltet wie in Abbildung \ref{pic:integrator} dargestellt. Für einen idealen Stromintegrator ist nach den kirchhoff'schen Regeln der durch den Widerstand fließende Strom $I_{R_{\kappa}}$ vom Betrag her gleich dem auf den Kondensator auffließenden Strom $I_C$. Zudem gilt am Kondensator mit den zeitlichen Ableitungen von Ladung und Spannung
\begin{equation*}
	I_C= \dot{Q}_C = C \dot{U}_A \; .
\end{equation*}
Umstellen und Integrieren liefert mit $I_C= I_{R_{\kappa}}=U_E/R_{\kappa}$ \cite{lp_integrator}
\begin{equation}
	U_A = - \frac{1}{R_{\kappa}C} \, \int_{t_1}^{t_2} U_E \, \text{d}t = \frac{Q}{C} \; .
	\label{eq:opvherleitung}
\end{equation}
Damit ist die vom OPV angezeigte Ausgangsspannung $U_A$ proportional zur auf den Kondensator geflossene Ladung $Q$. Für einen realen Stromintegrator muss zur Vorbereitung der Messung zunächst die für den Stromintegrator charakteristische Eichkonstante $\kappa$ bestimmt werden. Sie gibt den Zusammenhang zwischen geflossener Ladung $Q$ und vom OPV angezeigten Ausgangswert $x$ an:
\begin{equation*}
	Q = \kappa \, x \; .
\end{equation*}
Wird für ein Zeitintervall $\Delta t$ der Kondensator mit konstanter Eingangsspannung $U_E$ geladen, so folgt durch Einsetzen in Gleichung \ref{eq:opvherleitung} und Umstellen für die Eichkonstante
\begin{equation}
	\kappa = \frac{U_E}{xR_{\kappa}}\, \Delta t \; .
	\label{eq:kappa}
\end{equation}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.6\textwidth]{img/Analoger_Stromintegrator_bearbeitet.png}
	\caption{Analoger Stromintegrator bestehend aus Operationsverstärker, Kondensator und Widerstand \cite[bearbeitet]{lp_integrator}.}
	\label{pic:integrator}
\end{figure}

\subsection{Berechnung des Eichwiderstandes}
\label{sec:rkappa}

Die Eichkonstante $\kappa$ eines analogen Stromintegrators soll mit einem Schaltplan wie in Abbildung \ref{pic:schaltung1} dargestellt bestimmt werden. Zunächst muss der in Abschnitt \ref{sec:integrator} eingeführte Eichwiderstand $R_{\kappa}$ aus den festen Widerständen $R_1$, $R_2$, dem Induktionsspulenwiderstand $R_L$ sowie dem Innenwiderstand des Stromintegrators $R_{\text{int}}$ bestimmt werden. Für den Gesamtwiderstand $R_{\text{ges}}$ der Schaltung folgt aus den kirchhoff'schen Regeln \cite[Kap. 2.4]{demtroeder06}
\begin{equation}
R_{\text{ges}} = \frac{U}{I_{\text{ges}}} = R_1 + \frac{1}{\frac{1}{R_2} + \frac{1}{R_L + R_{\text{int}}}} \; .
\label{eq:rges}
\end{equation}
Weiterhin gilt für den Strom durch den Integrator $I_{\kappa}$
\begin{equation*}
	R_2 \cdot I_2 = (R_{\text{int}} + R_L) \cdot I_{\kappa} \; 
\end{equation*}
und nach der Knotenregel
\begin{equation*}
	I_{\text{ges}} = I_{\kappa} + I_2 = I_{\kappa} \left( 1+ \frac{R_L + R_{\text{int}}}{R_2} \right) \; .
\end{equation*}
Daraus folgt für den Eichwiderstand durch Einsetzen und Umformen
\nopagebreak
\begin{align}
	R_{\kappa} &= \frac{U}{I_{\kappa}}
	=\frac{U}{I_{\text{ges}}} \left( 1+ \frac{R_L + R_{\text{int}}}{R_2} \right) =R_{\text{ges}} \left( 1+ \frac{R_L + R_{\text{int}}}{R_2} \right) \notag\\
	&= \frac{R_1}{R_2}(R_2 + R_L + R_{\text{int}} ) + R_L + R_{\text{int}} \;. 
	\label{eq:rkappa}
\end{align}




