\subsection{Feldverlauf an den Polschuhen}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{evaluation/feldverlauf.png}
	\caption{Verlauf der magnetischen Flussdichte $B(h)$ in der Mitte zwischen den Polschuhen in Abhängigkeit zur Höhe $h$.}
	\label{fig:feldverlauf}
\end{figure}

Abbildung \ref{fig:feldverlauf} zeigt den Verlauf der Messwerte der Hall-Sonde zwischen den Polschuhen der Elektromagneten. Um den Feldverlauf zu modellieren lässt sich ein Polynomfit 6. Grades verwenden. \code{python} liefert nach der $\chi^2$-Methode als Modell für das magnetische Feld und den Gradienten:

\begin{align}
	B_0(h) =& -15135908 \, h^6 + 4424553 \, h^5 -523932 \, h^4\notag\\
			&+ 32207 \, h^3 -1085 \, h^2 + 16 \, h + 0.41 \; , \\
	\frac{\partial B_0(h)}{\partial h} =& -90815449 \, h^5 + 22122762  \, h^4 -2095729 \, h^3\notag\\
			&+ 96620 \, h^2 -2170 \, h + 16 \; .
\end{align}

Aus den Modellen für Feld und Gradienten lässt sich das Produkt dieser berechnen. Dieses ist in der Abbildung \ref{fig:gradient_product} gegen die Höhe aufgetragen. Da die Kraft auf einen Probekörper nach Gleichung \ref{eq:para_force} proportional zu diesem Produkt ist, lässt sich zum Einen feststellen, dass diese im Bereich der Polschuhe wie erwartet für paramagnetische Medien negativ ist, zum Anderen verläuft das Produkt wie also die Kraft im Bereich zwischen der Höhe von $0.02$ m bis $0.07$ m nahezu linear.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{evaluation/produkt_feld_gradient.png}
	\caption{Produkt der magnetischen Flussdichte mit dem Gradienten $B(h) \cdot \frac{\partial B_0(h)}{\partial h}$ in Abhängigkeit zur Höhe $h$.}
	\label{fig:gradient_product}
\end{figure}

\subsection{Suszeptibilitäten der Probekörper}

Aus den Gewichtsdifferenzen $\Delta m$ der Waage lassen sich die Kräfte auf die Probekörper mithilfe der Ortskonstante $g$ berechnen:

\begin{equation*}
	F = \Delta m * g
\end{equation*}

Die Ergebnisse sind in Tabelle \ref{tab:probe_params} aufgezeichnet. Für die Kraft ergibt sich der Fehler aus dem Fehler der Gewichtsdifferenz:

\begin{equation*}
	\sigma_F = g \cdot \sigma_{\Delta m}
\end{equation*}

\begin{table}[!htb]
	\centering
	\begin{tabular}{|l|c|c|c|}
		\hline
		 & Masse $m$ [mg] & Dichte $\varrho$ [$10^3 \frac{\mathrm{kg}}{\mathrm{m}^3}$] & Kraft $F$ [$10^{-5}$ N] \\
		\hline\hline
		$\mathrm{MnO}_2$ & $532$ & $5.03$ & $-22.46 \pm 0.49$ \\
		\hline
		$\mathrm{Ta}$ & $840$ & $16.40$ & $-1.085 \pm 0.49$ \\
		\hline
		$\mathrm{Bi}$ & $685$ & $9.78$ & $1.67 \pm 0.49$ \\	
		\hline
	\end{tabular}
	\caption{Parameter der und Kraft auf die Probekörper (der Fehler ergibt sich ). Die Masse und Dichte (\cite[Kap. 10.6]{knetter16}) entstammen der Vorgabe und sind somit als fehlerfrei angenommen.}\label{tab:probe_params}
\end{table}

\begin{table}[!htb]
	\centering
	\begin{tabular}{|l|c|c|c|}
		\hline
		& Position $h$ [mm] & mag. Flussdichte $B_0(h)$ [T] & Gradient $\frac{\partial B_0(h)}{\partial h}$ \\
		\hline\hline
		$\mathrm{MnO}_2$ & $56.25 \pm 0.01$ & $0.36 \pm 0.11$ & $-3.01 \pm 0.11$ \\
		\hline
		$\mathrm{Ta}$ & $54.00 \pm 0.01$ & $0.38 \pm 0.11$ & $-3.02 \pm 0.11$ \\
		\hline
		$\mathrm{Bi}$ & $44.00 \pm 0.01$ & $0.41 \pm 0.11$ & $-3.01 \pm 0.11$ \\	
		\hline
	\end{tabular}
	\caption{Position der Probekörper sowie Flussdichte und Gradient an dieser.}\label{tab:field_params}
\end{table}

Durch Umstellung von Gleichung \ref{eq:para_force} lässt sich mithilfe der Masse $m$ und der Dichte $\varrho$ schließlich die Suszeptibilität berechnen:

\begin{equation}
	\chi = \frac{F \cdot \mu_0}{V \cdot B_0(h) \cdot B_0'(h)} = \frac{F \cdot \mu_0 \cdot \varrho}{m \cdot B_0(h) \cdot B_0'(h)} \; .
\end{equation}

Die Ergebnisse hierfür sind neben den spezifischen Suszeptibilitäten, erhalten aus \\
$\chi_{spez} = \frac{\chi}{\varrho}$, in Tabelle \ref{tab:susceptibilities}. Für die Fehler gelten jeweils nach der Gauss'schen Methode:

\begin{align*}
	\sigma_{\chi}=\frac{\mu_{0} \cdot \rho}{B_{0}^{2} \cdot B_0'^{2} \cdot m} \cdot \sqrt{B_{0}^{2} \cdot B_0'^{2} \cdot \sigma_{F}^{2} + B_{0}^{2} \cdot F^{2} \cdot \sigma_{B_0'}^{2} + B_0'^{2} \cdot F^{2} \cdot \sigma_{B_0}^{2}} \; , \\
	\sigma_{\chi_{spez}}=\frac{\mu_{0}}{B_{0}^{2} \cdot B_0'^{2} \cdot m} \cdot \sqrt{B_{0}^{2} \cdot B_0'^{2} \cdot \sigma_{F}^{2} + B_{0}^{2} \cdot F^{2} \cdot \sigma_{B_0'}^{2} + B_0'^{2} \cdot F^{2} \cdot \sigma_{B_0}^{2}} \; .
\end{align*}

\begin{table}[!htb]
	\centering
	\begin{tabular}{|l|c|c|}
		\hline
		& Suszeptibilität $\chi$ [$10^{-4}$] & spezifische Suszeptibilität $\chi_{spez}$ [$\frac{10^{-8} \, \mathrm{m}^3}{kg}$]  \\
		\hline\hline
		$\mathrm{MnO}_2$ & $23.65 \pm 6.37$ & $47.02 \pm 12.67$ \\
		\hline
		$\mathrm{Ta}$ & $2.29 \pm 1.21$ & $1.40 \pm 0.74$ \\
		\hline
		$\mathrm{Bi}$ & $-2.42 \pm 9.26$ & $-2.47 \pm 0.95$ \\	
		\hline
	\end{tabular}
	\caption{(Spezifische) Suszeptibilitäten der Probekörper.}\label{tab:susceptibilities}
\end{table}

\subsection{Magnetisches Feld für verschiedene Ströme}
\label{sec:field_by_i}

Aus der zweiten Messreihe lassen sich die Feldverläufe zwischen den Polschuhen für die vier verschiedenen Stromstärken erkennen. Dazu sind die magnetischen Flussdichten $B(h)$ gegenüber der Höhe $h$ für jede Stromstärke in Abbildung \ref{fig:flux_by_i} aufgetragen. Der Verlauf der Messwerte lässt sich mit einem quadratischen Polynom annähern:

\begin{equation*}
	B(h) = a h^2 + b
\end{equation*}

Die Parameter wurden mit der in \code{python} implementierten $\chi^2$-Methode bestimmt. Aus diesem Modell lässt sich der Verlauf des Gradienten bestimmen, welcher in Abbildung \ref{fig:gradient_by_i} zu finden ist. Wie aus den Abbildungen zu entnehmen ist, scheint sich die Flussdichte und deren Gradient mit der Stromstärke linear zu ändern.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{evaluation/flussdichte_nach_i.png}
	\caption{Verlauf der Flussdichte $B(h)$ zwischen den Polschuhen für verschiedene Stromstärken.}
	\label{fig:flux_by_i}
\end{figure}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{evaluation/gradient_nach_i.png}
	\caption{Verlauf des Gradienten $B'(h)$ zwischen den Polschuhen für verschiedene Stromstärken.}
	\label{fig:gradient_by_i}
\end{figure}

\subsection{Kräfte auf Mangandioxid}

Der in Abschnitt \ref{sec:field_by_i} gefundene Zusammenhang von Feld und Gradienten zur Stromstärke $I$ lässt sich entsprechend der aus Gleichung \ref{eq:para_force} folgenden Proportionalität der Kraft zur Feld und Gradient auch ein proportionaler Zusammenhang zwischen $I$ und der Kraft auf den Probekörper $F_x$ vermuten. Zur Überprüfung wurde in Abbildung \ref{fig:force_probe} die gemessene Kraft zur Stromstärke aufgetragen. Der Fit einer linearen Funktion

\begin{equation*}
	F_x(I) = (-0.33 \pm 0.02) \, I + (0.17 \pm 0.02) \; ,
\end{equation*}

scheint die Daten entsprechend der Abbildung gut zu beschreiben. Der vermutete Zusammenhang lässt sich also bestätigen.

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{evaluation/kraft.png}
	\caption{Die Kraft $F_x$ auf $\mathrm{MnO}_2$ aufgetragen zur Stromstärke mit einem linearen Fit $f(I)$.}
	\label{fig:force_probe}
\end{figure}