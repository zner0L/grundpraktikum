# -*- coding: utf-8 -*-
from calc_field import *

fig1.savefig('feldverlauf.png')
fig2.savefig('produkt_feld_gradient.png')
fig3.savefig('flussdichte_nach_i.png')
fig4.savefig('gradient_nach_i.png')
fig5.savefig('kraft.png')

#newton.print_result()
print 'Kräfte: '
print data_forces
print 'Params: '
print calc_temp
print 'Chi: '
print chi_results
#chi.print_result()
print 'Chi Specific: '
print chi_specific_results
#chi_specific.print_result()
print 'Kräfte Fit: '
print params_f
print np.sqrt(errors_f[0,0])
print np.sqrt(errors_f[1,1])