# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

def fit_linear(x, a, b):
    return a * x + b

def fit_quadratic_prime(x, a, b):
    return 2 * a * x + b

def fit_quadratic(x, a, b, c):
    return a * x**2 + b * x + c

def fit_function(x, a, b, c, d, e, f, g):
    return g * x**6 + f * x**5 + e * x**4 + d * x**3 + c * x**2 + b * x + a

def fit_function_prime(x, b, c, d, e, f, g):
    return 6 * g * x**5 + 5 * f * x**4 + 4 * e * x**3 + 3 * d * x**2 + 2 * c * x + b

data_probe_field = np.genfromtxt('data/feld_probekoerper.csv', delimiter=' ', usecols=range(2))
data_forces = np.genfromtxt('data/kraefte.csv', delimiter=' ', usecols=range(3))
data_materials = np.array([
    [-0.0229e-3, 0.0005e-3, 532e-6, 56.25e-3, 5.03e+3], # MnO2
    [-0.0011e-3, 0.0005e-3, 840e-6, 54e-3, 16.4e+3], # Ta
    [0.0017e-3, 0.0005e-3, 685e-6, 44e-3, 9.78e+3] # Bi
])
data_field_c1 = np.genfromtxt('data/feld_c1.csv', delimiter=' ', usecols=range(5))
currents = [0.8, 1.0, 1.2, 1.4]

field_params, field_errors = curve_fit(fit_function, data_probe_field[:, 0] * 1e-3, data_probe_field[:, 1])
l = np.linspace(-0.005, 0.085)

fig1 = plt.figure(1, figsize=[10, 7])
plt.errorbar(data_probe_field[:, 0]*1e-3, data_probe_field[:, 1], yerr=0.001, label='Messdaten', fmt='x')
plt.plot(l, fit_function(l, field_params[0], field_params[1], field_params[2], field_params[3], field_params[4], field_params[5], field_params[6]), label=r'$\chi^2$-Fit 6. Ordnung')
plt.axis([-0.005, 0.082, 0.3, 0.5])
plt.legend(ncol=1, fancybox=True, shadow=True)
plt.xlabel(r'H{\"o}he $h$ [$\mathrm{m}$]')
plt.ylabel(r'magnetische Flussdichte $B(h)$ [$\mathrm{T}$]')

fig2 = plt.figure(2, figsize=[10, 7])
plt.plot(l, fit_function(l, field_params[0], field_params[1], field_params[2], field_params[3], field_params[4], field_params[5], field_params[6])
         * fit_function_prime(l, field_params[1], field_params[2], field_params[3], field_params[4], field_params[5], field_params[6]), label=r'$B(h) \cdot \frac{\partial B(h)}{\partial h}$ \\ aus Fitfunktion')
plt.axis([-0.005, 0.082, -2, 8])
plt.legend(ncol=1, fancybox=True, shadow=True)
plt.xlabel(r'H{\"o}he $h$ [$\mathrm{m}$]')
plt.ylabel(r'Produkt $B(h) \cdot \frac{\partial B(h)}{\partial h}$')

newton = ma.uncertainty.Sheet('g * m')
newton.set_value('g', 9.81)
data_materials[:, (0, 1)] = newton.batch(data_materials[:, (0, 1)], 'm|m%')
data_forces[:, (1,2)] = newton.batch(data_forces[:, (1,2)], 'm|m%')


calc_temp = np.column_stack([
    data_materials[:, (0, 1, 2, 4)],
    fit_function(data_materials[:, 3], field_params[0], field_params[1], field_params[2], field_params[3], field_params[4], field_params[5], field_params[6]),
    np.array([0.1, 0.1, 0.1]),
    fit_function_prime(data_materials[:, 3], field_params[1], field_params[2], field_params[3], field_params[4], field_params[5], field_params[6]),
    np.array([0.1, 0.1, 0.1])
])

chi = ma.uncertainty.Sheet('(f * mu0 * rho)/(m * b0p * b0)', name='\\chi')
chi_specific = ma.uncertainty.Sheet('(f * mu0)/(m * b0p * b0)', name='\\chi_{spez}')
chi.set_value('mu0', 4*math.pi*1e-7)
chi_specific.set_value('mu0', 4*math.pi*1e-7)
chi_results = chi.batch(calc_temp, 'f|f%|m|rho|b0|b0%|b0p|b0p%')
chi_specific_results = chi_specific.batch(calc_temp, 'f|f%|m|rho|b0|b0%|b0p|b0p%')

fig3 = plt.figure(3, figsize=[10, 7])
param_list = []
cmap = plt.cm.get_cmap('Paired', len(currents)*2)
for i, c in enumerate(currents):
    l = np.linspace(0.03, 0.07)
    params, errors = curve_fit(fit_quadratic, data_field_c1[:, 0]*1e-3, data_field_c1[:, i+1], p0=(9, -3, 0.5))
    plt.errorbar(data_field_c1[:, 0]*1e-3, data_field_c1[:, i+1], yerr=0.001, xerr=0.2e-3, label=str(c) + ' A', fmt='+', color=cmap(i*2-1))
    plt.plot(l, fit_quadratic(l, params[0], params[1], params[2]), color=cmap(i*2))
    param_list.append(params)

plt.xlabel(r'H{\"o}he $h$ [$\mathrm{m}$]')
plt.ylabel(r'magnetische Flussdichte $B(h)$ [$\mathrm{T}$]')
plt.legend(ncol=1, fancybox=True, shadow=True)
plt.axis([0.04, 0.068, 0.27, 0.47])

fig4 = plt.figure(4, figsize=[10, 8])
print param_list
for i, params in enumerate(param_list):
    plt.plot(l, fit_quadratic_prime(l, params[0], params[1]), color=cmap(i*2), label=r"$B'(h)$ bei " + str(currents[i]) + ' A')

plt.xlabel(r'H{\"o}he $h$ [$\mathrm{m}$]')
plt.ylabel(r"Gradient der Flussdichte $B'(h)$")
plt.legend(loc='lower right', ncol=1, fancybox=True, shadow=True)
plt.axis([0.03, 0.07, -4.5, -1.5])

fig5 = plt.figure(5, figsize=[10, 7])
l = np.linspace(0.75, 1.45)
params_f, errors_f = curve_fit(fit_linear, data_forces[:, 0], data_forces[:, 1])
plt.errorbar(data_forces[:, 0], data_forces[:, 1], yerr=data_forces[:, 2], label=r'Kraft auf $\mathrm{MnO}_2$', fmt='+')
plt.plot(l, fit_linear(l, params_f[0], params_f[1]), label=r'lineares Modell')
plt.xlabel(r'Stromst{\"a}ke $I$ [$\mathrm{A}$]')
plt.ylabel(r"Kraft auf den Probek{\"o}rper $F_x$ [$\mathrm{N}$]")
plt.legend(loc='lower right', ncol=1, fancybox=True, shadow=True)
plt.axis([0.75, 1.45, -0.075, -0.325])