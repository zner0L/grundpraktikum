\subsection{Spezifische Wärme der Luft}
Nach Gleichung (\ref{eq:kondensator}) ist die im Kondensator gespeicherte elektrische Energie $\Delta Q$ gegeben durch 
\begin{equation*}
	\Delta Q = \frac{1}{2}CU^2 = \frac{1}{2}(C_1+C_2)U^2 \; .
\end{equation*}
Die Werte befinden sich in Tabelle \ref{tab:results_luft} mit dem Fehler 
\begin{equation*}
	\sigma_{\Delta Q} = \sigma_U \cdot CU
\end{equation*}
Der Fehler der Kapazität $C$ ist null, da dieser Wert an der Apparatur gegeben war. 

\begin{table}[h!]
	\centering
	\begin{tabular}{||c|c|c|c|c||}\hline\hline
		Größe	&	Bezeichnung & Wert		&	Fehler	&	Einheit \\\hline\hline
		Durchmesser	&		$d$	&	8.8		&	0.2	& cm	\\\hline
		Länge		&		$l$	&	39.9	&	0.1	& cm	\\\hline
		Radius Schenkel links		&		$r_1$	&	2.0	&	-	& mm	\\\hline
		Radius Schenkel rechts		&		$r_2$	&	9.2	&	-	& mm	\\\hline
		Kapazität	&		$C$	&	10		&	-	& $\mu$F 	\\\hline\hline
	\end{tabular}
	\caption{Spezifische Wärme von Luft: Maße des verwendeten Zylinders und weitere relevante Größen.}
	\label{spezwaerme}
\end{table}

\input{evaluation/luft_table}

Aus den Zylindermaßen in Tabelle (\ref{spezwaerme}) ergibt sich das Zylindervolumen $V$ zu
\begin{equation*}
	V=\pi \left(\frac{d}{2}\right)^2 l = 2430 \pm 220 ~\text{cm}^3
\end{equation*}
mit dem Fehler
\begin{equation*}
\sigma_{V} =\sqrt{
	\left({ \sigma_{d} \cdot \pi \frac{d}{2}l}\right)^2 + 
	\left({ \sigma_{l} \cdot \pi \frac{d^2}{4}}\right)^2
} \; . 
\end{equation*}
Das in den Manometerschenkeln verdrängte Volumen ist $\Delta V = \pi {r_1}^2 h_1 = \pi {r_2}^2 h_2$. Mit $\Delta h = h_1 + h_2$ folgt durch Einsetzen und Umstellen 
\begin{equation*}
	\Delta h = h_1 \left(1+ \frac{{r_1}^2}{{r_2}^2}\right)
\end{equation*}
und folglich für die Druckdifferenz
\begin{equation*}
\Delta p =\varrho g \Delta h= \varrho g h_1 \left(1+ \frac{{r_1}^2}{{r_2}^2}\right) \; .
\end{equation*}
Nun lässt sich die Druckänderung $\Delta p$ als Funktion der elektrischen Energie $\Delta Q$ aus den Kondensatoren auftragen (siehe Abb. \ref{fig:energy_to_pressure}). 

\begin{figure}
	\centering
	\includegraphics[scale=0.3]{evaluation/luft_energy_to_pressure}
	\caption{Die Druckänderung $\Delta p$ in Bezug auf die elektrische Energie $\Delta Q$. Ein linearer Fit lässt den linearen Zusammenhang erkennen.}
	\label{fig:energy_to_pressure}
\end{figure}

Nach Gleichung (\ref{eq:freiheitsgrade}) berechnet sich die mittlere Anzahl der Freiheitsgrade des Gasgemisches Luft zu
\begin{equation*}
	f=2 \left(\frac{\text{d} Q - p\, \text{d} V}{(V\text{d} p + p\, \text{d} V)}\right) = 6.3 \pm 0.8
\end{equation*}
Aus den Freiheitsgraden lässt sich ebenfalls aus Gleichung (\ref{eq:freiheitsgrade}) die gemittelte Molwärme bestimmen:
\begin{equation*}
	c_V = \frac{Rf}{2} = (26.3 \pm 3.1) \frac{\text{J}}{\text{mol}\,\text{K}}
\end{equation*}


\subsection{Adiabatenexponent nach Rüchardt}
Nach Gleichung (\ref{kappa}) gilt
\begin{equation*}
	\kappa  = \frac{64 \,V m_{\text{eff}}}{d^4 p {T^2}}
\end{equation*}
wobei der Druck im Kolben $p$ gegeben ist durch 
\begin{equation*}
	p  = b + \frac{mg}{A}
\end{equation*}
und die effektive Masse durch
\begin{equation*}
	m_{\text{eff}} = m + \varrho a A \; ,
\end{equation*}

dabei ergibt sich der Fehler nach dem Gesetz der Fehlerfortpflanzung zu 
\begin{equation*}
	\sigma_{\kappa} =\sqrt{
		\left({ \sigma_{p} \cdot \frac{64 \,V m_{\text{eff}}}{(d^2 pT)^2}}\right)^2 + 
		\left({ \sigma_{T} \cdot \frac{64 \,V m_{\text{eff}}}{d^4 p {T^3}}}\right)^2 +
		\left({ \sigma_{m_{\text{eff}}} \cdot \frac{64 \,V }{d^4 p {T^2}}}\right)^2
	} \; . 
\end{equation*}

\begin{table}[h!]
	\centering
	\begin{tabular}{||c|c|c|c|c||}\hline\hline
		Größe	&	Bezeichnung & Wert			&	Einheit \\\hline\hline
		Volumen Kolben	&	$V$	&	2402			& cm$^3$	\\\hline
		Durchmesser Zylinder		&	$d$		&	9.745		& mm\\\hline
		Masse Schwingkörper	&		$m$	&	4.734			& g 	\\\hline
		Luftdruck		&	$b$		&	1024.4		& hPa	\\\hline\hline
	\end{tabular}
	\caption{Adiabatenexponent nach Rüchardt: relevante Größen.}
	\label{tab:ruechardt}
\end{table}


Die relevanten Größen in Tabelle \ref{tab:ruechardt} werden als nicht fehlerbehaftet angenommen, da sie an der Apparatur gegeben sind. Die Ergebnisse für die arithmetischen Mittel der Adiabatenexponenten der vermessenen Gase sind in Tabelle \ref{tab:results_ruechard} aufgeführt.

\input{evaluation/ruechard_table} 

\subsection{Adiabatenexponent nach Clement-Desormes}
Nach Gleichung \ref{eq:blaa} gilt für den Adiabatenexponent $\kappa$ 
\begin{equation*}
	k=\frac{\Delta h_1}{\Delta h_1- \Delta h_2}
\end{equation*}
und folglich für den Fehler
\begin{equation*}
\sigma_{\kappa} =\sqrt{
	\left({ \sigma_{\Delta h_1} \cdot \frac{\Delta h_2}{(\Delta h_1- \Delta h_2)^2}}\right)^2 + 
	\left({ \sigma_{\Delta h_2} \cdot \frac{\Delta h_1}{(\Delta h_1- \Delta h_2)^2}}\right)^2 }
\end{equation*}

Die erhaltenen Werte für $\kappa$ der verschiedenen Öffnungszeiten sind in Tabelle \ref{tab:results_clement} aufgeführt. Im Mittel ergibt sich dann:

$$ \kappa = 1.372 \pm 0.008 $$

\input{evaluation/clement_table}