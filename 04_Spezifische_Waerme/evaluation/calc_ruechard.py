# -*- coding: utf-8 -*-
import scipy.stats as stats
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning
import math

data = [
    ['luft', ufloat(0.115, 0.01), 1.293],
    ['ar', ufloat(0.11, 0.01), 1.78],
    ['co2', ufloat(0.125, 0.01), 1.98]
]

kappa = ma.uncertainty.Sheet('(64*V*(m+o*a*A))/(d**4*(b+(m*g)/A)*T**2)', '\\kappa')
kappa.set_value('V', 2402e-6) # m**3
kappa.set_value('m', 4.734e-3) # kg
kappa.set_value('A', (9.745e-3)**2/4*math.pi) # m
kappa.set_value('b', 1024.4e+2, tex='p_{Luft}') # Pa
kappa.set_value('d', 9.745e-3) # m
kappa.set_value('g', 9.81) # m/s**2

results_ruechard = []
gas_names = ['Luft', 'Argon', '$\\text{CO}_2$']

for d in data:
    raw_data = np.genfromtxt('data/ruechard_' + d[0] + '.csv', delimiter=' ', usecols=range(0,3), skip_header=1)
    more_data = np.genfromtxt('data/ruechard_' + d[0] + '.csv', delimiter=' ', usecols=range(0,11), skip_footer=4)
    values = [ufloat(more_data.mean(), stats.sem(more_data))]

    for i, n in enumerate([10, 20, 50, 100]):
        values.append(ufloat(np.mean(raw_data[i,:]), stats.sem(raw_data[i, :]))/n)

    period = np.mean(values)/1000 # s
    kappa.set_value('a', d[1].n, d[1].s) # m
    kappa.set_value('T', period.n, period.s)
    kappa.set_value('o', d[2], tex='\\rho')  # kg/m**3

    results_ruechard.append([period.n, period.s, kappa.get_result()[0], kappa.get_result()[1]])