# -*- coding: utf-8 -*-
from calc_luft import *

plt.savefig('luft_energy_to_pressure.png')

tbl = ma.latex.Table()
tbl.add_column(charges, "num($0,$1)", 'el. Energie $\\Delta Q$ [J]')
tbl.add_column(median_luft[:, [2, 3]], "num($0,$1)", 'Steighöhe $h_1$ [mm]')
tbl.add_column(delta_ps, "num($0,$1)", 'Druckänderung $\\Delta p$ [Pa]')
tbl.set_caption('Wertetabelle zur Energie und Druckänderung')
tbl.set_label('tab:results_luft')
tbl.set_mode('hline')
tbl.export('luft_table.tex')

from calc_ruechard import *

results_ruechard = np.array(results_ruechard)

tbl = ma.latex.Table()
tbl.add_column(gas_names, title='Gas')
tbl.add_column(results_ruechard[:, [0,1]], "num($0,$1)", 'Schwingungsperiode $T$ [s]')
tbl.add_column(results_ruechard[:, [2,3]], "num($0,$1)", '$\\kappa$')
tbl.set_caption('Adiabatenexponenten und gemessene Schwingungsperioden für verschiedene Gase (Luft, Argon, $\\text{CO}_2$).')
tbl.set_label('tab:results_ruechard')
tbl.set_mode('hline')
tbl.export('ruechard_table.tex')

from calc_clement import *

tbl = ma.latex.Table()
tbl.add_column([0.1, 1, 5], "$0", 'Öffnungszeit [s]')
tbl.add_column(kappas2, "num($0,$1)", 'Koeffizient $\\kappa$')
tbl.set_caption('Adiabatenkoeffizienten aus verschiedenen Öffnungszeiten des Entlüftungsventils.')
tbl.set_label('tab:results_clement')
tbl.set_mode('hline')
tbl.export('clement_table.tex')