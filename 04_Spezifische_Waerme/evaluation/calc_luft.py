# -*- coding: utf-8 -*-
import scipy.stats as stats
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning
import math

matplotlib.rcParams.update({'font.size': 25})

def linear_function(x, m, b):
    return m * x + b

raw_luft = np.genfromtxt('data/luft.csv', delimiter=' ', usecols=range(0,5), skip_header=2)

scale_constant = 2e-3  # 2mm pro Streifen

median_luft = []
for row in raw_luft:
    row_mean = ufloat(np.mean(row[1:]), stats.sem(row[1:])+0.1)*scale_constant
    median_luft.append([row[0], 5, row_mean.n, row_mean.s])

median_luft = np.array(median_luft)

charge = ma.uncertainty.Sheet('0.5 * 2*C * U**2', '\\Delta Q')
charge.set_value('C', 10e-6)
charges = charge.batch(median_luft[:, [0, 1]], 'U|U%')

cylinder_volume = ma.uncertainty.Sheet('pi * r**2 * l', 'V')
cylinder_volume.set_value('r', 4.4e-2, 0.2e-2)
cylinder_volume.set_value('l', 39.9e-2, 0.1e-2)
cylinder_volume = cylinder_volume.get_result()

print 'Zylindervolumen: $' + str(cylinder_volume[0]) + ' \\pm ' + str(cylinder_volume[1]) + ' \\text{m}^3$'

delta_p = ma.uncertainty.Sheet('o * g * h * (1 + r1**2/r2**2)', '\\Delta p')
delta_p.set_value('r1', 2.0e-3, tex='r_1') # m
delta_p.set_value('r2', 9.2e-3, tex='r_2') # m
delta_p.set_value('o', 998.20, tex='\\rho') # kg/m**3
delta_p.set_value('g', 9.81) # m/s**2
delta_ps = delta_p.batch(median_luft[:, [2, 3]], 'h|h%')

params, errors = curve_fit(linear_function, charges[:, 0], delta_ps[:, 0])

plt.figure(figsize=(20,10))
plt.axis([0, 3.5, 0, 500])
plt.ylabel(r'Druckunterschied $\Delta p$ [Pa]')
plt.xlabel(r'Energie $\Delta Q$ [J]')
plt.errorbar(charges[:, 0], delta_ps[:, 0], xerr=charges[:, 1],  yerr=delta_ps[:, 1], fmt='x')
l = np.linspace(0, 3.5, 100)
plt.plot(l, linear_function(l, params[0], params[1]))

deg_of_freedom = ma.uncertainty.Sheet('2*((q - p*(pi*r1**2*h))/(V * d + p*(pi*r1**2*h)))', 'f')
deg_of_freedom.set_value('p', 1024.4*100) # Pa
deg_of_freedom.set_value('r1', 2.0e-3, tex='r_1') # m
deg_of_freedom.set_value('V', cylinder_volume[0], cylinder_volume[1])
deg_of_freedom.set_value('d', tex='\\text{d}p')
degrees = deg_of_freedom.batch(np.column_stack((charges, delta_ps, median_luft[:, [2, 3]])), 'q|q%|d|d%|h|h%')

deg_of_freedom = degrees.mean(axis=0)
print 'Freiheitsgrade: $' + str(deg_of_freedom[0]) + ' \\pm ' + str(deg_of_freedom[1]) + '$'

spez = ma.uncertainty.Sheet('R*f/2', 'c_v')
spez.set_value('R', 8.3145) # allgemeine Gaskonstante [J/(mol*K)]
spez.set_value('f', deg_of_freedom[0], deg_of_freedom[1])
spez = spez.get_result()

print 'spezifische Wärme: $' + str(spez[0]) + ' \\pm ' + str(spez[1]) + ' \\frac{\\text{J}}{\\text{mol}\\,\\text{K}}$'