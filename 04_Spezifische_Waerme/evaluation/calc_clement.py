# -*- coding: utf-8 -*-
import scipy.stats as stats
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning
import math

data = [
    np.genfromtxt('data/clement.csv', delimiter=' ', usecols=range(0, 2)),
    np.genfromtxt('data/clement.csv', delimiter=' ', usecols=range(2, 4), skip_footer=1),
    np.genfromtxt('data/clement.csv', delimiter=' ', usecols=range(4, 6), skip_footer=1)
]

kappa2 = ma.uncertainty.Sheet('h1/(h1-h2)', '\\kappa')
kappa2.set_value('h1', error=0.25, tex='\\Delta h_1')
kappa2.set_value('h2', error=0.25, tex='\\Delta h_2')

kappas2 = []
for o in data:
    kappas2.append(kappa2.batch(o, 'h1|h2').mean(axis=0))

kappas2 = np.vstack(kappas2)
kappa_mean = kappas2.mean(axis=0)
print 'Kappa aus Clement: ' + str(kappa_mean[0]) + ' \\pm ' +  str(kappa_mean[1])