\subsection{Oberflächenspannung}

Eine Flüssigkeit lässt sich modellhaft als eine Menge frei gegeneinander bewegbarer Moleküle beschreiben, welche anziehende Kräfte aufeinander ausüben. Diese intermolekularen Kräfte treten in der Form von Dipol- oder Van-der-Waals-Kräften auf. Für ein Molekül tief im inneren einer Flüssigkeit kompensieren sich die Kräfte aller umliegenden Teilchen, die Resultierende ist Null. Befindet sich ein Teilchen hingegen an der Oberfläche einer Flüssigkeit, so werden die Kräfte einseitig und die resultierende Kraft zeigt in Richtung der Flüssigkeit. Es lässt sich die Oberflächenenergie $E_{S}$ definieren, welche proportional zur Oberfläche $A$ ist \cite[Kap. 4.7.2]{gerthsen10}:
\begin{equation}
	E_{s}= \sigma A \; .
		\label{eq:spannung}
\end{equation}
Die Proportionalitätskonstante $\sigma$ heißt spezifische Oberflächenenergie oder Oberflächen-spannung. Die Energie einer Grenzfläche ist also minimal, wenn die Fläche $A$ minimal wird.

\subsection{Kapillarität}

Neben den Kohäsionskräften zwischen gleichartigen Molekülen innerhalb der Flüssigkeit treten auch Adhäsionskräfte zwischen Molekülen unterschiedlicher Stoffe auf, in einer Flasche etwa zwischen Wasser- und Glasmolekülen (Vgl. Abbildung \ref{pic:kap}). Je nach Verhältnis zwischen Kohäsions- und Adhesionskräften werden die Teilchen am Rand der Flüssigkeit aufwärts oder herab gezogen. 
\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{img/theorie_kap.png}
	\caption{Schematische Darstellung der Oberflächenspannungen an den Grenzflächen unterschiedlicher Stoffe. online: https://lp.uni-goettingen.de/get/bigimage/4883, aufgerufen am 23.06.2017}
	\label{pic:kap}
\end{figure}
In einem engen Rohr (Kapillare) mit Radius $r$ steigt eine Flüssigkeit mit Oberflächenspannung $\sigma$ aufgrund der Kohäsions- und Adhesionskräfte um $h$ an. Nach Gleichung \ref{eq:spannung} gilt für die hierfür benötigte Arbeit
\begin{equation}
	\text{d} W_S = \sigma \text{d}A \; .
\end{equation}
Für ein Gleichgewicht muss diese Kraft gleich der Gewichtskraft der angehobenen Flüssigkeit $F_{g}= mgh$ sein. Mit der Flüssigkeitsdichte $\rho$ folgt \cite[Kap 6.4.3]{demtroeder15}
\begin{align}
	\notag
	F_S & = F_g \\ \notag
	\sigma A & = mgh \\ \notag
	2 \pi r \sigma &=  \pi r^2 \rho g h \\ 
	\Rightarrow h &= \frac{2 \sigma}{\rho g r} \; .
	\label{eq:sigma}
\end{align}

\subsection{Viskosität}

Die Viskosität $\eta$ eines Stoffes ist ein Maß für seine Dickflüssigkeit bzw. Fließfähigkeit. Für den Strom $I = \dot{V}$ einer Flüssigkeit der Viskosität $\eta$ durch eine Kapillare der Länge $l$ und Radius $r$ gilt das Gesetz von Hagen Poiseuille \cite[Kap. 3.5.2]{gerthsen10} 
\begin{equation}
	I = \dot{V} = \frac{\pi r^4}{8 \eta l}\Delta p \; ,
	\label{eq:hagen}
\end{equation}
wobei $\Delta p$ den Druckunterschied zwischen oberem und unterem Rohrende bezeichnet, gegeben durch den Schweredruck der Flüssigkeitssäule. Für aus einem Zylinder mit Radius $R$ ausströmende Flüssigkeit gilt $\dot{V} = A\dot{h}$ mit der Zylinderquerschnittsfläche $A = \pi R^2$ und es ergibt sich die Differenzialgleichung
\begin{equation*}
	\pi R^2 \dot{h} = -\frac{ \pi \rho g r^4}{8 \eta l} h \; .
\end{equation*}
Trennung der Variablen und Integration liefert die Gleichung
\begin{equation}
	\log h(t) = - \frac{\rho g}{8 \eta l R^2}r^4 t \; .
	\label{eq:log_h}
\end{equation}

\subsection{Mohrsche Waage}

Mit einer wie in Abbildung \ref{pic:waage} gezeigten Mohrschen Waage lässt sich die Dichte $\rho$ einer Flüssigkeit bestimmen. Das auf der rechten Seite einer Balkenwaage hängende Gewicht wird vollständig in die zu vermessende Flüssigkeit eingetaucht. Durch Anhängen der Massen $m_i$ an der rechten Seite im Abstand $r_i$ vom Aufhängepunkt wird die Waage wieder in Gleichgewichtslage gebracht. Nun gleicht die Summe der Drehmomente $D$ der angehhängten Massen gerade das durch den Auftrieb erzeugte Drehmoment $-D$ aus. Für die Auftriebskraft gilt mit Volumen $V$ und Dichte $\rho$:
\begin{equation*}
	F_A = V \rho g \; .
\end{equation*} 
Die Messung wird zunächst mit einer Flüssigkeit bekannter Dichte $\rho_2$ und anschließend mit der Flüssigkeit unbekannter Dichte $\rho_1$ durchgeführt. Da das Verhältnis der Drehmomente gleich dem Verhältnis der Dichten ist, folgt mit den angehängte Massen $m_i$ für die zu bestimmende Dichte
\begin{equation}
	\rho_1 = \frac{D_1}{D_2}= \frac{\sum_{i = 1}^{k} m_{i1} r_i }{\sum_{j = 1}^{l} m_{j2} r_j} \cdot \rho_2\; .
	\label{eq:waage}
\end{equation}


\begin{figure}[h]
	\centering
	\includegraphics[width=0.5\textwidth]{img/m_waage.png}
	\caption{Skizze des Versuchsaufbaus der Mohrschen Waage. online: https://lp.uni-goettingen.de/get/bigimage/4259, aufgerufen am 23.06.2017}
	\label{pic:waage}
\end{figure}