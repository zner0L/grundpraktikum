# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

def linear_function(x, m, b):
    return m * x + b

data_kapillaren = np.genfromtxt('data/kapillaren_diameter.csv', delimiter=' ', usecols=range(4))
data_steighoehen = np.genfromtxt('data/steighoehen.csv', delimiter=' ', usecols=range(9))
data_wassersaeule = np.genfromtxt('data/wassersaeule.csv', delimiter=' ', usecols=range(4))

data_50_45 = np.array([ma.data.weighted_average(np.genfromtxt('data/50_45.csv', delimiter=' ', usecols=(0, 1))),  # grün
                       ma.data.weighted_average(np.genfromtxt('data/50_45.csv', delimiter=' ', usecols=(2, 3))),  # rot
                       ma.data.weighted_average(np.genfromtxt('data/50_45.csv', delimiter=' ', usecols=(4, 5)))]) # blau

steighoehen = []
for i in range(9): steighoehen.append(ma.data.weighted_average(np.column_stack([data_steighoehen[:, i], [0.5,0.5,0.5]])));
steighoehen = np.array(steighoehen)

radius = []
for i in range(3):
    r = ma.data.weighted_average(data_kapillaren[:, [i,3]], 'ufloat') / 2
    radius.append([r.n, r.s])
radius_filled = np.array([radius[i%3] for i in range(9)])
radius = np.array(radius)

length_kapillaren = np.array(
    [[23.1, 0.1],
     [23.2, 0.1],
     [22.4, 0.1]]
)

density = np.array(
    [[1, 0],[1, 0],[1, 0],
    [0.84, 0.04],[0.84, 0.04],[0.84, 0.04],
    [1.10, 0.06],[1.10, 0.06],[1.10, 0.06]]
)

sigma = ma.uncertainty.Sheet('0.5*o*h*g*r', '\\sigma')
sigma.set_value('o', tex='sigma')
sigma.set_value('g', 9.81) # kg m/s^2
sigma_results = sigma.batch(np.column_stack([steighoehen, radius_filled, density]), 'h|h%|r|r%|o|o%')

sigma_results = [ma.data.weighted_average(sigma_results[[0,1,2], :]),
                 ma.data.weighted_average(sigma_results[[3,4,5], :]),
                 ma.data.weighted_average(sigma_results[[6,7,8], :])]

for i, s in enumerate([72.75, 22.60, 48.4]):
    print ma.data.literature_value(s, sigma_results[i][0], sigma_results[i][1])


dV = ma.uncertainty.Sheet('pi * (d/2)^2 * h', '\\Delta V')
dV.set_value('h', 5e-2, tex='\\Delta h')
dV.set_value('d', 2.45e-2, 0.05e-2)
volume = dV.get_result()

eta = ma.uncertainty.Sheet('(pi * dp)/(8 * dv * l) * r^4 * t', '\\eta')
eta.set_value('dp', 46.60e+2, tex='\\Delta p')
eta.set_value('dv', volume[0], volume[1], tex='\\Delta V')
eta.set_value('t', tex='t_A')

eta_results = eta.batch(np.column_stack([length_kapillaren*1e-2, radius*1e-3, data_50_45]), 'l|l%|r|r%|t|t%')
eta_bar = ma.data.weighted_average(eta_results)
eta_results = np.row_stack([eta_results, eta_bar])

l = np.linspace(-50, 350)
params1, error1 = curve_fit(linear_function, data_wassersaeule[:, 1], np.log(data_wassersaeule[:, 0]))
params2, error2 = curve_fit(linear_function, data_wassersaeule[:, 2], np.log(data_wassersaeule[:, 0]))
plt.errorbar(data_wassersaeule[:, 1], np.log(data_wassersaeule[:, 0]), xerr=data_wassersaeule[:, 3], fmt='+', color='orange', label='1. Messung')
plt.errorbar(data_wassersaeule[:, 2], np.log(data_wassersaeule[:, 0]), xerr=data_wassersaeule[:, 3], fmt='x', color='green', label='2. Messung')
plt.plot(l, linear_function(l, params1[0], params1[1]), color='orange', label='linearer Fit')
plt.plot(l, linear_function(l, params2[0], params2[1]), color='green', label='linearer Fit')
plt.legend()
plt.xlabel(r'$t$ [s]')
plt.ylabel(r'$\log{\frac{h}{[\mathrm{m}]}}$')
plt.title(u'Logarithmischer Plot der Ausflusszeiten einer Wassersäule')

m = ma.weighted_average(np.array([[params1[0], np.sqrt(error1[0][0])], [params2[0], np.sqrt(error2[0][0])]]), 'ufloat')

print m

eta_koeff = ma.uncertainty.Sheet('-(o*g*r^4)/(8*m*l*(d/2)^2)', '\\eta')
eta_koeff.set_value('o', 1000, tex='\\rho')
eta_koeff.set_value('g', 9.81)
eta_koeff.set_value('r', radius[1][0]*1e-3, radius[1][1]*1e-3)
eta_koeff.set_value('m', m.n, m.s)
eta_koeff.set_value('l', length_kapillaren[1][0]*1e-2, length_kapillaren[1][1]*1e-2)
eta_koeff.set_value('d', 2.45e-2, 0.05e-2)
eta_koeff.print_result()