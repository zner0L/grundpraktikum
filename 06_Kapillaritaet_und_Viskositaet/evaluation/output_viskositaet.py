# -*- coding: utf-8 -*-
from calc_viskositaet import *

tbl = ma.latex.Table()
tbl.add_column(['grün', 'rot', 'blau'], title='Kapillare')
tbl.add_column(radius, "num($0,$1)", 'mittlerer Radius $r$ [mm]')
tbl.add_column(length_kapillaren, "num($0,$1)", 'Länge $l$ [mm]')
tbl.set_caption('Radien und Längen der verschiedenen Kapillaren.')
tbl.set_label('tab:radius_kapillaren')
tbl.set_mode('hline')
tbl.export('radius_kapillaren.tex')

lit = [1.0, 0.79, 1.11]
dev = []
for i, d in enumerate(density[[0,3,6], :]):
    dev.append((100*ma.data.literature_value(lit[i], d[0], d[1], 'ufloat')).n)

tbl = ma.latex.Table()
tbl.add_column(['Wasser', 'Methanol', 'Ethylenglykol'], title='Flüssigkeit')
tbl.add_column(density[[0,3,6], :], "num($0,$1)", 'gemessene Dichten [$\\frac{\\text{g}}{\\text{cm}^3}$]')
tbl.add_column(lit, title='Literaturwerte [$\\frac{\\text{g}}{\\text{cm}^3}$]')
tbl.add_column(dev, title='Abweichung [\\%]')
tbl.set_caption('Mess- und Literaturwerte für die Dichten nach \cite{gestis}.')
tbl.set_label('tab:densities')
tbl.set_mode('hline')
tbl.export('densities.tex')

tbl = ma.latex.Table()
tbl.add_column(['Wasser', 'Methanol', 'Ethylenglykol'], title='Flüssigkeit')
tbl.add_column(sigma_results, "num($0,$1)", 'Oberflächenspannung $\\sigma$ [$10^{-3} \\frac{\\text{J}}{\\text{m}^2}$]')
tbl.set_caption('Ergebnisse der Berechnung der Oberflächenspannung $\\sigma$ verschiedener Stoffe.')
tbl.set_label('tab:sigmas')
tbl.set_mode('hline')
tbl.export('sigmas.tex')

print 'Sigma Fehler:'
sigma.print_result()

tbl = ma.latex.Table()
tbl.add_column(['grün', 'rot', 'blau', 'Mittelwert'], title='Kapillare')
tbl.add_column(eta_results, "num($0,$1)", 'Viskosität $\\eta$ [$\\frac{\\text{m}^2}{\\text{s}}$]')
tbl.set_caption('Ergebnisse zur Viskosität $\\eta$ aus der ersten Messreihe.')
tbl.set_label('tab:eta_1')
tbl.set_mode('hline')
tbl.export('eta_1.tex')

print 'Fehler Volumen:'
dV.print_result()
print 'Fehler eta:'
eta.print_result()

plt.savefig('log_plot.png')