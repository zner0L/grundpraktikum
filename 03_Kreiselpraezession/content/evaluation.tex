\subsection{Theoretische Trägheitsmomente}
\begin{table}
	\centering
	\renewcommand{\arraystretch}{1.5}
	\begin{tabular}{||c||c||c||c||}\hline\hline
		\textbf{Größe} & \textbf{Bezeichnung} & \textbf{Wert} & \textbf{Fehler} \\\hline\hline
		Masse Rad & $m_R$ & 1326 g & 0 \\\hline\hline
		Höhe Rad & $h_R$ & 28 mm & 0 \\\hline\hline
		Radius Rad & $r_R$ & 122.5 mm & 0 \\\hline\hline
		Masse Ausgleichsgewicht & $m_A$ & 938 g & 0 \\\hline\hline
		Höhe Ausgleichsgewicht & $h_A$ & 43 mm & 0.5 mm \\\hline\hline
		Radius Ausgleichsgewicht & $r_A$ & 30 mm & 0.5 mm \\\hline\hline
		Abstand Schwerpunkt $m_A$ - Unterstützungspunkt & $d_A$ & 155 mm & 0.5 mm \\\hline\hline
		Abstand Kerbe - Unterstützungspunkt & $d_Z$ & 271 mm & 0.5 mm \\\hline\hline
		Abstand Rad - Unterstützungspunkt & $d_R$ & 112.5 mm & 0.5 mm \\\hline\hline
	\end{tabular}
	\caption{Übersicht der gemessenen Größen mit Fehler}
	\label{tab:parameter}
\end{table}

Das Trägheitsmoment $\theta_{theo}$ des Schwungrades um die horizontale Achse sowie das der gesamten Apparatur $\theta_{theo}'$ um die vertikale Achse lassen sich aus den in Tabelle \ref{tab:parameter} aufgeführten Werten theoretisch berechnen. Die an der Apparatur gegebenen Werte werden als fehlerfrei angenommen, zu den gemessenen Werten ist der jeweilige Fehler in der Tabelle (ebd.) angegeben. \\

Für das Trägheitsmoment des Rades als homogenen Zylinder um die horizontale Achse gilt nach \cite[Kap 10.4]{bergmann98} und mit den wie in Tabelle \ref{tab:parameter} bezeichneten Größen

\begin{equation}
    \theta_{theo} = \frac{1}{2} \, m_R \cdot {r_{R}}^2  =  0.00995 ~\text{kg m}^2 \; .
    \label{eq:theo_theta}
\end{equation}


Das Trägheitsmoment der gesamten Apparatur um die vertikale Achse durch den Unterstützungspunkt $\theta'_{theo}$ setzt sich aus den Trägheitsmomenten der beiden als Zylinder angenommenen Körper Rad und Ausgleichsgewicht zusammen. Für einen Zylinder mit Masse $m$ und Höhe $h$ ist das Trägheitsmoment durch seinen Schwerpunkt parallel zu Ober- und Unterseite (Querachse) \cite[Kap 10.4]{bergmann98}

\begin{equation}
	\theta = \frac{1}{4}mr^2 + \frac{1}{2} m h^2 \; .
\end{equation}
Für den verwendeten Aufbau ergibt sich unter Anwendung des Steinerschen Satzes mit Bezeichnungen entsprechend Tabelle \ref{tab:parameter} die Gleichung
\begin{align}
	\theta'_{theo} & = 
	\frac{1}{4}{m_R} \left({r_R}^2 + \frac{1}{3}{h_R}^2 + 4 {d_R}^2\right) +
	\frac{1}{4}{m_A} \left({r_A}^2 + \frac{1}{3}{h_A}^2 + 4 {d_A}^2\right) \notag\\
	&= (0.0447 \pm 0.0002) ~\text{kg m}^2 \; .
	\label{eq:theta_prime}
\end{align}
Der Fehler ist nach Fehlerfortpflanzungsgesetz
\begin{equation*}
	\sigma_{\theta_{theo}'} = \; \sqrt{\left(2 \, \sigma_{d_R} \, m_R \, {d_R}\right)^2 
	+ \left(\frac{1}{2}\sigma_{r_A}r_A\right)^2
	+ \left(\frac{1}{6}\sigma_{h_A}h_A\right)^2
	+ \left(2\sigma_{d_A}d_A\right)^2	} \; .
\end{equation*}
Die Masse des verbindenden Stabes konnte nicht gemessen werden, da er fest in die Apparatur eingebaut ist. Deshalb wurde sein Trägheitsmoment vernachlässigt. 



\subsection{Teil A: Trägheitsmoment des Schwungrades}

Aus den gemessenen Schwingungsperioden $T$ lässt sich analog zum vorangegangenen Versuch \cite{siebenpeters16} das horizontale Trägheitsmoment $\theta_{exp}$ berechnen. Es gilt mit den Bezeichnungen aus Tabelle \ref{tab:parameter} und Erdbeschleunigung $g$ die Gleichung 
\begin{equation*}
	\theta_{exp} = \frac{T^2gr_R m_Z}{4 \pi^2}-m_Z {r_R}^2 \; .
	\label{eq:exp_theta}
\end{equation*}
Der Fehler ist 

\begin{equation*}
\sigma_{\theta_{exp}}
= \sqrt{
	\left(\sigma_{T}\frac{2Tg r_R m_Z}{4\pi^2}\right)^2 +
	\left(\sigma_{r_R}\left(\frac{T^2gm_Z}{4\pi^2}-2m_Z{r_R}\right)\right)^2} \; .
\end{equation*}

\input{evaluation/table_traegheitsmoment}

Die berechneten Ergebnisse der Messung sowie ein Vergleich mit den theoretischen Werten finden sich in Tabelle \ref{tab:exp_theta}. 



\subsection{Teil B: Präzession der Drehachse}
\label{sec:precession}

Rotationsfrequenz $\omega_R$ und Präzessionsfrequenz $\omega_P$ berechnen sich aus den gemessenen Periodendauern $T_R$ und $T_P$. Dabei ist $T_R$ der Mittelwert aus der jeweiligen Messung vor und nach der Präzession und $T_P$ das Doppelte der gemessenen halben Perioden. Es ist
\begin{equation}
	\omega_R = \frac{2 \pi}{T_R} \quad \text{und} \quad \omega_P = \frac{2 \pi}{T_P} \; ,
\label{eq:omega}	
\end{equation}
für die Fehler gilt
\begin{equation*}
	\sigma_{\omega_R} =	\sigma_{T_R}\frac{2 \pi}{{T_R}^2} \quad \text{und} 
	\quad 
	\sigma_{\omega_P} = \sigma_{T_P}\frac{2 \pi}{{T_P}^2} \; .
\label{eq:omegafehler}
\end{equation*}
Der Fehler der Stoppuhr ist (unter Berücksichtigung menschlicher Fehler) $\sigma_{\omega_P}=0.1 \; s$ und der der Lichtschranke $\sigma_{\omega_R}=0.0005 \; s$. \\

Aus Gleichung \ref{nutation} folgt mit dem Gewicht der angehängten Zusatzmasse $m$ und dessen Abstand zum Unterstützungspunkt $d_Z$, dass
\begin{equation}
\theta = \frac{mgd_Z}{\omega_P \omega_R} \; .
\label{eq:theta_from_omega_p}
\end{equation}
Dabei wurde der gewichtete Mittelwert für die Messungen bei unterschiedlichen Zusatzgewichten gebildet. Der Fehler ist
\begin{equation*}
	\sigma_{\theta}
	= \sqrt{
	\left(\sigma_{d_Z}\frac{mg}{\omega_P \omega_R}\right)^2 + 
	\left(\sigma_{\omega_P}\frac{mgd_Z}{{\omega_P}^2 \omega_R}\right)^2 +
	\left(\sigma_{\omega_R}\frac{mgd_Z}{{\omega_P} {\omega_R}^2}\right)^2 
		} \; .
\end{equation*}

\begin{figure}
	\centering
	\includegraphics[scale=0.2]{evaluation/precession.png}
	\caption{Die Winkelgeschwindigkeit der Präzession $\omega_P$ gegenüber der reziproken Winkelgeschwindigkeit der Rotation $\omega_R$ für verschiedene Zusatzmassen. Es lässt sich jeweils ein klarer linearer Zusammenhang erkennen.}
	\label{fig:precession_to_rotation}
\end{figure}

In Abbildung \ref{fig:precession_to_rotation} ist $\omega_P$ gegen $1/\omega_R$ aufgetragen, es zeigt sich ein linearer Zusammenhang. Die Ergebnisse sind in Tabelle \ref{eq:theta_from_omega_p} aufgeführt und mit den theoretisch berechneten Werten verglichen.

\input{evaluation/table_precession}


\subsection{Teil C: Nutation der Drehachse}

\begin{figure}
	\centering
	\includegraphics[scale=0.2]{evaluation/nutation.png}
	\caption{Die Rotationsfrequenz $\omega_R$ gegenüber der Nutationsfrequenz $\omega_N$. Es wurde an die Messwerte aus allen Messungen linear gefittet.}
	\label{fig:rotation_vs_nutation}
\end{figure}

Die Werte für die Rotationsfrequenz $\omega_R$ berechnen sich wie in \ref{sec:precession}  beschrieben nach Gleichung \ref{eq:omega}, der Fehler nach Gleichung \ref{eq:omegafehler}. Für die Nutationsfrequenz $\omega_N$ lässt sich die Nutationsperiode $T_N$ berechnen, indem die gemessene Zeit durch die Anzahl der Perioden $n$ geteilt wird. Für $\omega_N$ gelten ebenfalls Gleichungen \ref{eq:omega} und \ref{eq:omegafehler}. In Abbildung \ref{fig:rotation_vs_nutation} sind Nutationsfrequenz $\omega_N$ und Rotationsfrequenz $\omega_R$ gegeneinander aufgetragen und linear gefittet. \\ 

Nach Gleichung \ref{eq:omega_n} ist die Steigung $k$ der Ausgleichsgerade das Verhältnis zwischen vertikalem und horizontalem Drehmoment:
\begin{equation*}
	k_{theo} = \frac{\theta}{\theta'} = 0.222 \pm 0.001 \; .
\end{equation*}

Der Fit ergibt ein $k_{exp} = 0.160 \pm 0.003$, welches um $28.22 \; \%$ vom theoretischen Wert abweicht.
