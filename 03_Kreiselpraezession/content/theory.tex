Ein physikalischer Kreisel ist ein um freie Achsen rotierender, nur an einem Punkt mit völliger Drehfreiheit gelagerter starrer Körper, dessen Rotationsachse also ihre räumliche im Laufe der Zeit ändern kann. Im Allgemeinen ist hierbei das Drehmoment $\vec{D}$ die zeitliche Ableitung des Drehimpulses $\vec{L}$.
\begin{equation}
\vec{D}=\vec{r} \times \vec{F} = \frac{\text{d}\vec{L}}{\text{d}t}=\dot{\vec{L}}
\label{drehimpuls}
\end{equation}
Die Bewegung eines Kreisels lässt sich einerseits aus dem raumfesten Koordinatensystem $R$, andererseits aus dem aus den Hauptachsen des Körpers bestehenden Koordinatensystems $K$ betrachten, welches gegenüber $R$ mit der Winkelgeschwindigkeit $\omega$ rotiert. Ähnlich der Galilei-Transformation der Geschwindigkeit bei bewegten Bezugssystemen ergibt sich die Eulersche Kreiselgleichung des Drehmoments $\vec{D}$ mit Drehimpuls $\vec{L}$, Winkelgeschwindigkeit $\omega$ und Zeit $t$ zu 
\begin{equation}
	\vec{D}=\left(\frac{\text{d}\vec{L}}{\text{d}t}\right)_R=\left(\frac{\text{d}\vec{L}}{\text{d}t}\right)_K+(\vec{\omega} \times \vec{L}) \; .
	\label{euler}
\end{equation} 
Hierbei ist zu beachten, dass $\dot{\vec{L}}_R$ im Körperfesten System $K$ angegeben ist, $\vec{\omega}$ jedoch im Inertialsystem $R$. \\

\subsection{Nutation}
Die Symmetrieachse eines rotationssymmetrischen Körpers heißt seine Figurenachse $\vec{c}$. Fallen $\vec{c}$ und die momentane Drehachse $\vec{\omega}$ des Körpers nicht zusammen, so ändert sich  die Richtung beider Größen im Laufe der Zeit entlang eines Kegels um den Drehimpuls $\vec{L}$. Nach Impulserhaltungssatz bleibt $\vec{L}$ jedoch konstant. Wir bezeichnen die Bewegung von $\vec{c}$ und $\vec{\omega}$ als Nutationsbewegung um $\vec{L}$. Die Bewegung der jeweiligen Achsen zueinander lassen sich wie in Abbildung \ref{nutation} gezeigt einzeln beschreiben. So wandert $\vec{c}$ entlang des Nutationskegels um $\vec{L}$, während $\vec{\omega}$ auf dem Rastpolkegel um $\vec{L}$ läuft. Dabei bildet $\vec{\omega}$ die Mittelachse des Gangpolkegels, der auf dem Rastpolkegel um $\vec{L}$ herum abrollt. \cite[Kap. 11.3]{bergmann98} \\

Eine Nutationsbewegung tritt z.B. auf, wenn auf einen konstant um $\vec{c}$ rotierenden Kreisel ein Stoß senkrecht zu seiner Drehachse ausgeübt wird. Die Frequenz der Nutationsbewegung $\vec{\omega_{n}}$ berechnet sich mit Drehimpuls $\vec{D}$ und dem Trägheitsmoment bezüglich einer zur Rotation senkrechen Achse $\theta'$ laut \cite[Kap. VI. §35]{landau04} zu
\begin{equation}
	\omega_{n}=\frac{\vec{D}}{\theta'} \; ,
\end{equation}
wenn es sich um einen sogenannten ''schnellen Kreisel'' handelt, d.h. wenn die momentane Rotation $\omega$ groß gegenüber der Nutationsgeschwindigkeit $\omega_{n}$ ist. 
\begin{figure}[h!]
	\centering
	\includegraphics[width=0.4\textwidth]{Nutation.png}
	\caption{Nutationskegel, Rastpolkegel und Gangpolkegel nach \cite[Kap. 5.7.5.]{demtroeder15}. \label{nutation}}
\end{figure}

\subsection{Präzession}
Wenn nun äußere Kräfte auf den rotierenden Körper ein Drehmoment $\vec{D}$ auswirken, so ist nach Gleichung \ref{drehimpuls} $\vec{D}=\dot{\vec{L}}_R \neq0$, der Drehimpuls $\vec{L}$ also nicht mehr zeitlich konstant. Im Spezialfall eines senkrecht zu $\vec{L}$ wirkenden Drehmomentes ändert sich allerdings lediglich die Richtung des Drehimpulses, nicht sein Betrag $L$. Die daraus resultierende Bewegung des Drehimpulses $\vec{L}$ um eine Achse senkrecht zur Ebene von $\vec{D}$ und $\vec{L}$ heißt Präzession des Drehimpulses $\vec{L}$. Mit Präzessionsgeschwindigkeit $\omega_p$ und Drehmoment $\vec{D}$ gilt dabei \cite[Kap. 5.7]{demtroeder15}
\begin{equation}
	\vec{D} = \frac{\text{d}\vec{L}}{\text{d}t} = \omega_p \times \vec{L} \; .
\end{equation}
Für den Betrag der Präzessionsgeschwindigkeit folgt bei $\omega_p \ll \omega$ mit dem Winkel $\alpha$ zwischen $\omega_p$ und $\vec{L}$ 
\begin{equation}
	\omega_p = \frac{\text{d}\varphi}{\text{d}t} = \frac{D}{L \cdot \sin \alpha} = \frac{mgr}{L} = \frac{mgr}{\theta \omega} \; .
	\label{eq:omega_p}
\end{equation}
Analog ist die Nutationsfrequenz gegeben durch 
\begin{equation}
	\omega_n =  \frac{\theta}{\theta'} \omega \; .
	\label{eq:omega_n}
\end{equation}