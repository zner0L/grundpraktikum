# -*- coding: utf-8 -*-
import scipy.stats as stats
import numpy as np
import maabara as ma
from uncertainties import ufloat

raw = np.genfromtxt('data/traegheitsmoment.csv', delimiter=' ', comments='#')
data = {
    0.12: raw[:, 2],
    0.1: raw[:, 1],
    0.06: raw[:, 0],
}

results_period = []
results_theta = []
deviation = []

theta_theo = ma.uncertainty.Sheet('1/2 * m * r**2', '\\theta_{theo}')
theta_theo.set_value('m', 1.326)
theta_theo.set_value('r', 122.5e-3)
theta_theo = theta_theo.get_result('ufloat')

theta_theo_prime = ma.uncertainty.Sheet('1/4 * mr * (rr**2 + 1/3 * hr**2 + 4*dr**2) + 1/4 * ma * (ra**2 + 1/3 * ha**2 + 4*da**2)', '\\theta\'_{theo}')
theta_theo_prime.set_value('mr', 1.326)  # kg
theta_theo_prime.set_value('ma', 0.938)  # kg
theta_theo_prime.set_value('rr', 122.5e-3)  # m
theta_theo_prime.set_value('ra', 30e-3, 0.5e-3)  # m
theta_theo_prime.set_value('hr', 28e-3)  # m
theta_theo_prime.set_value('ha', 43e-3, 0.5e-3)  # m
theta_theo_prime.set_value('dr', 112.5e-3, 0.5e-3)  # m
theta_theo_prime.set_value('da', 155e-3, 0.5e-3)  # m
theta_theo_prime = theta_theo_prime.get_result('ufloat')

theta_exp = ma.uncertainty.Sheet('(T**2 * g * r * m)/(4*pi**2) - m * r**2', '\\theta_{exp}')
theta_exp.set_value('r', 122.5e-3)
theta_exp.set_value('g', 9.81)

for w, p in data.iteritems():
    period = ufloat(np.mean(p)/10, stats.sem(p)+0.1)
    theta_exp.set_value('m', w)
    theta_exp.set_value('T', period.n, period.s)
    theta = theta_exp.get_result()
    results_period.append([period.n, period.s])
    results_theta.append(theta)
    deviation.append(abs(ma.data.literature_value(theta_theo.n, theta[0])[0]*100))

theta_exp = ma.weighted_average(results_theta, 'ufloat')
deviation.append(abs(ma.data.literature_value(theta_theo.n, theta_exp.n)[0]*100))

print 'Theoretisches Theta: (' + str(theta_theo.n) + ' \\pm ' + str(theta_theo.s) + ')'
print 'Theoretisches Theta Strich: (' + str(theta_theo_prime.n) + ' \\pm ' + str(theta_theo_prime.s) + ')'
