# -*- coding: utf-8 -*-
import scipy.stats as stats
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning
import math

matplotlib.rcParams.update({'font.size': 30})

def linear_function(x, m, b):
    return m*x+b

omega = ma.uncertainty.Sheet('(2*pi)/T', '\\omega')

all_omega_r = []
all_omega_n = []

fig_nutation = plt.figure(figsize=(20,15))

for i in range(1,3):
    raw = np.genfromtxt('data/nutation_' + str(i) + '.csv', delimiter=' ', comments='#')
    omega.set_value('T', error=0.005)
    omega_r = omega.batch(raw[:, 0]*1e-3, 'T')

    omega_n = []
    for j, p in enumerate(raw[:, 2]):
        period = ufloat(p, 0.1)/ufloat(raw[j, 1], 1)
        omega.set_value('T', period.n, period.s)
        omega_n.append(omega.get_result())

    omega_n = np.array(omega_n)

    plt.errorbar(omega_r[:, 0], omega_n[:, 0], xerr=omega_r[:, 1], yerr=omega_n[:, 1], fmt='x', label='Messung ' + str(i))
    all_omega_n += omega_n[:, 0].tolist()
    all_omega_r += omega_r[:, 0].tolist()

l = np.linspace(0, 200)
params, errors = curve_fit(linear_function, all_omega_r, all_omega_n)

k_exp = ufloat(params[0], np.sqrt(errors[0][0]))
print('k aus Experiment: ' + str(k_exp.n) + ' \\pm ' + str(k_exp.s))

plt.plot(l, linear_function(l, params[0], params[1]), label='Fit an beide Messungen')
plt.grid()
plt.legend(loc='upper left')
plt.axis([40, 120, 7.5, 18.5])
plt.xlabel(r'$\omega_R$ [$\frac{1}{\mathrm{s}}$]')
plt.ylabel(r'$\omega_N$ [$\frac{1}{\mathrm{s}}$]')