# -*- coding: utf-8 -*-
from calc_traegheitsmoment import *

tbl = ma.latex.Table()
tbl.add_column([60, 100, 120, 'Mittelwert'], '$0', 'Pendelmasse [g]')
tbl.add_column(results_period + [ma.weighted_average(results_period)], 'num($0,$1)', 'Periode [s]')
tbl.add_column(results_theta + [[theta_exp.n, theta_exp.s]], 'num($0,$1)', '$\\theta_{exp}$ [kg $\\text{m}^2$]')
tbl.add_column(deviation, 'num($0)','Abweichung [$\%$]')
tbl.set_caption('Experimentelle Werte für die horizontalen Trägheitsmomente sowie Vergleich mit den theoretisch berechneten Werten.')
tbl.set_label('tab:exp_theta')
tbl.set_mode('hline')
tbl.export('table_traegheitsmoment.tex')

from calc_precession import *

fig_precession.savefig('precession.png')

theta_from_precession = np.array(theta_from_precession + [ma.weighted_average(theta_from_precession)])

tbl = ma.latex.Table()
tbl.add_column([60, 40, 20, 'Mittelwert'], '$0', 'Zusatzmasse $m$ [g]')
tbl.add_column(theta_from_precession, 'num($0,$1)', 'Periode [s]')
tbl.add_column([abs(ma.data.literature_value(theta_theo.n, x)[0] * 100) for x in theta_from_precession[:, 0]], 'num($0)','Abweichung [$\%$]')
tbl.set_caption('Ergebnisse für die vertikalen Trägheitsmomente aus der Präzessionsbewegung sowie Vergleich mit den theoretisch berechneten Werten.')
tbl.set_label('tab:theta_precession')
tbl.set_mode('hline')
tbl.export('table_precession.tex')

from calc_nuation import *

fig_nutation.savefig('nutation.png')

k_theo = theta_theo/theta_theo_prime
print('k aus Theorie: ' + str(k_theo.n) + ' \\pm ' + str(k_theo.s))
print('Abweichung ks: ' + str(abs(ma.data.literature_value(k_theo.n, k_exp.n)[0]*100)) + '\; \%')