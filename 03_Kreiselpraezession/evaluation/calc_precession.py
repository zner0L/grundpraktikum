# -*- coding: utf-8 -*-
import scipy.stats as stats
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning
import math

matplotlib.rcParams.update({'font.size': 30})

def linear_function(x, m, b):
    return m*x+b

# mass, last period
data = [
    [60, 131e-3, 'blue'],
    [40, 171e-3, 'red'],
    [20, 289e-3, 'green']
]

omega = ma.uncertainty.Sheet('(2*pi)/T', '\\omega')
reziprok = ma.uncertainty.Sheet('1/x')
theta_exp_prime = ma.uncertainty.Sheet('(m*g*r)/(wp*wr)')
theta_exp_prime.set_value('g', 9.81)  # m/s**2
theta_exp_prime.set_value('r', 271e-3, 0.5e-3) # m

fig_precession = plt.figure(figsize=(20,15))

theta_from_precession = []

for d in data:
    raw = np.genfromtxt('data/precession_' + str(d[0]) + '.csv', delimiter=' ')
    omega.set_value('T', error=0.1)
    precession = omega.batch(raw[:, 0]*2, 'T')
    raw_rot = (raw[:, 1]*1e-3).tolist()
    raw_rot.append(d[1])
    rotation = []


    for i, p in enumerate(raw_rot):
        if i < len(raw_rot)-1:
            omega.set_value('T', np.mean([p, raw_rot[i+1]]), stats.sem([p, raw_rot[i+1]])+0.0005)
            rotation.append(omega.get_result())

    theta_exp_prime.set_value('m', d[0]*1e-3)  # kg
    result_theta_exp = theta_exp_prime.batch(np.column_stack((rotation, precession)), 'wr|wr%|wp|wp%')
    theta_from_precession.append(ma.data.weighted_average(result_theta_exp))


    x_axis = reziprok.batch(np.array(rotation), 'x|x%')
    params, errors = curve_fit(linear_function, x_axis[:, 0], precession[:, 0])
    l = np.linspace(0, 0.5)

    plt.errorbar(x_axis[:, 0], precession[:, 0], xerr=x_axis[:, 1], yerr=precession[:, 1], fmt='x', color=d[2])
    plt.plot(l, linear_function(l, params[0], params[1]), color=d[2], label=str(d[0]) + ' g')

plt.axis([0.005, 0.05, 0.05, 0.35])
plt.xlabel(r'$\frac{1}{\omega_K}$ [s]')
plt.ylabel(r'$\omega_P$ [$\frac{1}{\mathrm{s}}$]')
plt.legend()
plt.grid()