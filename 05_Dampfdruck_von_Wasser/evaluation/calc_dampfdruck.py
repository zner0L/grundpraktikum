# -*- coding: utf-8 -*-
import scipy.stats as stats
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning
import math

def linear_function(x, m, b):
    return m * x + b

hot_data = np.genfromtxt('data/erwaermung.csv', delimiter=' ', usecols=range(2)) # :D
cool_data = np.genfromtxt('data/abkuehlung.csv', delimiter=' ', usecols=range(2)) # 8D

theta = ma.uncertainty.Sheet('- A/(2*B) - sqrt(A**2/(4*B**2) - (R0-R)/(R0*B))', name='\\vartheta')  # \vartheta = - \frac{A}{2B} - \sqrt{\frac{A^2}{4B^2}- \frac{R_0-R}{R_0 B}}
theta.set_value('R0', 1000, tex='R_0')  # Ohm
theta.set_value('A', 3.9083e-3)  # 1/°C
theta.set_value('B', -5.775e-7)  # 1/(°C)^2
theta.set_value('R', error=0.5)  # Ohm
hot_data = np.append(hot_data, theta.batch(hot_data[:, 1], 'R'), 1)
cool_data = np.append(cool_data, theta.batch(cool_data[:, 1], 'R'), 1)
hot_data[:, 0] = hot_data[:, 0]*100000; cool_data[:, 0] = cool_data[:, 0]*100000  # bar to Pascal
hot_data[:, 2] = hot_data[:, 2] + 273.15; cool_data[:, 2] = cool_data[:, 2] + 273.15  # Celsius to Kelvin

hot_line, hot_errors = curve_fit(linear_function, 1/hot_data[3:, 2], np.log(hot_data[3:, 0]/100640))  # don't call this number ;)
cool_line, cool_errors = curve_fit(linear_function, 1/cool_data[:-9, 2], np.log(cool_data[:-9, 0]/100640)) # TODO: We ignore a probably systematic error here…
l = np.linspace(0.0018, 0.0035)

fig, axarr = plt.subplots(2, 1, figsize=(10,10), sharey='col')

axarr[0].set_title(u'Erwärmung')
axarr[0].axis([0.0018, 0.0035, -4, 4])
axarr[0].errorbar(1/hot_data[:, 2], np.log(hot_data[:, 0]/100640), xerr=[(1/ufloat(t, e)).s for t, e in hot_data[:, [2,3]]], yerr=[umath.log(ufloat(p, 10000)/100640).s for p in hot_data[:, 0]], fmt='+', color='red')
axarr[0].plot(l, linear_function(l, hot_line[0], hot_line[1]), color='red')
axarr[0].set_ylabel(r"logarithmischer Druck $\ln(p/p_0)$")
axarr[1].set_title(u'Abkühlung')
axarr[1].axis([0.0018, 0.0035, -4, 4])
axarr[1].errorbar(1/cool_data[:, 2], np.log(cool_data[:, 0]/100640), xerr=[(1/ufloat(t, e)).s for t, e in cool_data[:, [2,3]]], yerr=[umath.log(ufloat(p, 10000)/100640).s for p in cool_data[:, 0]], fmt='x', color='blue')
axarr[1].plot(l, linear_function(l, cool_line[0], cool_line[1]), color='blue')
axarr[1].set_xlabel(r"reziproke Temperatur [$\frac{1}{\mathrm{K}}$]")
axarr[1].set_ylabel(r"logarithmischer Druck $\ln(p/p_0)$")
fig.savefig('arrheniusplot.png')

cool_m = ufloat(cool_line[0], np.sqrt(cool_errors[0, 0]))
cool_b = ufloat(cool_line[1], np.sqrt(cool_errors[1, 1]))
steam_temp_cool = -ufloat(cool_line[0], np.sqrt(cool_errors[0, 0]))*8.3145
boil_temp_cool = cool_m/(-cool_b)
steam_pressure_cool = umath.exp(cool_m/273.15+cool_b)

hot_m = ufloat(hot_line[0], np.sqrt(hot_errors[0, 0]))
hot_b = ufloat(hot_line[1], np.sqrt(hot_errors[1, 1]))
steam_temp_hot = -ufloat(hot_line[0], np.sqrt(hot_errors[0, 0]))*8.3145
boil_temp_hot = hot_m/(-hot_b)
steam_pressure_hot = umath.exp(hot_m/273.15+hot_b)

steam_temp_mean = np.mean([steam_temp_hot, steam_temp_cool])
steam_pressure_mean = np.mean([steam_pressure_cool, steam_pressure_hot])
boil_temp_mean = np.mean([boil_temp_cool, boil_temp_hot])

tbl = ma.latex.Table()
tbl.add_column(['Erwärmen', 'Abkühlen', 'Mittelwert', 'Abweichung [\\%]'])
tbl.add_column(np.array([[steam_temp_hot.n, steam_temp_hot.s], [steam_temp_cool.n, steam_temp_cool.s], [steam_temp_mean.n, steam_temp_mean.s], ma.data.literature_value(40590, steam_temp_mean.n)])*100, 'num($0,$1)', 'Verdampfungswärme $\Lambda_V$ [$\\frac{\\text{J}}{\\text{mol}}$]')
tbl.add_column(np.array([[boil_temp_hot.n, boil_temp_hot.s], [boil_temp_cool.n, boil_temp_cool.s], [boil_temp_mean.n, boil_temp_mean.s], ma.data.literature_value(373.15, boil_temp_mean.n)])*100, 'num($0,$1)', 'Siedetemperatur [K]')
tbl.add_column(np.array([[steam_pressure_hot.n, steam_pressure_hot.s], [steam_pressure_cool.n, steam_pressure_cool.s], [steam_pressure_mean.n, steam_pressure_mean.s], ma.data.literature_value(611.15, steam_pressure_mean.n)])*100, 'num($0,$1)', 'Dampfdruck $p_s$ [Pa]')
tbl.set_caption('Ergebnisse der Auswertung des Arrheniusplots sowie ein Vergleich mit den Literaturwerten.')
tbl.set_label('tab:results')
tbl.set_mode('hline')
tbl.export('results_table.tex')


mean_m = np.mean([hot_m, cool_m])
mean_b = np.mean([hot_b, cool_b])

tbl = ma.latex.Table()
tbl.add_column(['Erwärmen', 'Abkühlen', 'Mittelwert'])
tbl.add_column([[hot_m.n, hot_m.s], [cool_m.n, cool_m.s], [mean_m.n, mean_m.s]], 'num($0,$1)', '$m$')
tbl.add_column([[hot_b.n, hot_b.s], [cool_b.n, cool_b.s], [mean_b.n, mean_b.s]], 'num($0,$1)', '$b$')
tbl.set_caption('Steigung $m$ und Ordinatenabschnitt $b$ aus den Fits an die Arrheniuskurven.')
tbl.set_label('tab:results')
tbl.set_mode('hline')
tbl.export('m_and_b_table.tex')

pressure_zugspitze = 100640*umath.exp((-1.293*9.81*2962)/100640)
boil_temp_zugspitze = mean_m/(np.log(pressure_zugspitze)-mean_b)
print 'Druck auf der Zugspitze: ' + str(pressure_zugspitze)
print 'Siedetemp. auf der Zugspitze: ' + str(boil_temp_zugspitze)