\subsection{Reale Gasgleichung}
\label{sec:gasgl}

Die auch im letzten Versuch verwendete ideale Gasgleichung verknüpft die Größen Druck $p$, Volumen $V$, Temperatur $T$ und Teilchenzahl $n$ mittels der universellen Gaskonstante $R$ unter der vereinfachenden Annahme des idealen Gases (Gleichung \ref{eq:idealgas}).
\begin{equation}
	p_{\text{ideal}} \cdot V_{\text{ideal}} = nRT 
	\label{eq:idealgas}
\end{equation}
Vernachlässigt werden dabei einerseits alle nicht-harten, nicht-elastischen Stöße zwischen Gasteilchen, weshalb der Druck eines realen Gases tatsächlich kleiner ist. Aus Beobachtungen zeigt sich, dass
\begin{equation}
	p_{\text{real}} + \frac{n^2 a}{V^2}= p_{\text{ideal}} \; ,
	\label{eq:preal}
\end{equation}
wobei der Faktor $a$ die sog. Binnendruckkonstante ist. Andererseits werden in der idealen Gasgleichung Gasteilchen vereinfacht als Massenpunkte ohne Ausdehnung betrachtet, das reale Volumen eines Gases ist also größer. Mit der Eigenvolumenkonstante $b$ gilt:
\begin{equation}
	V_{\text{real}} - n \cdot b= V_{\text{ideal}}
	\label{eq:vreal}
\end{equation}
Die beiden Konstanten $a$ und $b$ werden auch als Van-der-Waals-Konstanten bezeichnet. Einsetzen der Gleichungen \ref{eq:preal} und (\ref{eq:vreal}) in die ideale Gasgleichung \ref{eq:idealgas} liefert die reale bzw. Van-der-Waals-Gasgleichung \cite[Kap. 10.4]{demtroeder15}:
\begin{equation}
\left(p + \frac{n^2a}{V^2}\right)(V-nb)	=nRT
\label{realgas}
\end{equation}


\subsection{Dampfdruckformel}
Befindet sich in einem abgeschlossenen System ein Stoff, der zwischen Dampf- und Flüssigkeitsphase im thermodynamischen Gleichgewicht liegt, so stellt sich im System der von der Temperatur $T$ abhängige und für den Stoff charakteristische Dampfdruck $p_S$ ein. Die Verdampfungswärme $\Lambda_V$ ist ein Maß für die Wärmemenge, die benötigt wird, um eine bestimmte Menge eines Stoffes vom flüssigen in den gasförmigen Aggregatzustand zu überführen. Dabei vergrößert sich sein molares Volumen von $V_{\text{fl}}$ auf $V_{\text{D}}$. Die Verdampfungswärme $\Lambda_V$ pro Mol verdampfter Flüssigkeit wird durch die Clausius-Clapeyron-Gleichung \footnote{benannt nach Benoît Paul Émile Clapeyron (1799 -- 1864) und Rudolf Clausius (1822 -- 1888), französischer bzw. deutscher Physiker} beschrieben (Gleichung \ref{eq:claudius}) \cite[Kap. 10.4]{demtroeder15}
\begin{equation}
	\Lambda_V=T\frac{\text{d}p_S}{\text{d}T}(V_D-V_{fl}) \; .
	\label{eq:claudius}
\end{equation}
Sie ist proportional zur Differenz der Molvolumina und zur Steigung der Dampfdruckkurve $p_S(T)$. Eine Stoffmenge von $n=1 \, \text{mol}$ des gasförmigen Volumens $V_{\text{D}}$ wird durch die allgemeine Gasgleichung (Gleichung \ref{eq:idealgas}) mit
\begin{equation*}
	p \cdot V_{\text{D}} = R \cdot T
\end{equation*}
beschrieben. Für nicht zu hohe Drücke ist $V_{\text{fl}} \ll V_{\text{D}}$. Umstellen nach $V_{\text{D}}$, Einsetzen in die Clausius-Clapeyron-Gleichung (\ref{eq:claudius}) und Trennung der Variablen liefert dann:
\begin{equation*}
	\frac{\text{d}p_S}{p} = \frac{\Lambda_V \cdot \text{d}T}{R \cdot T^2}
	\label{eq:steigung}
\end{equation*}
Integration ergibt mit der zusätzlichen Randbedingung $p_S(T_0)=p_0$ nach Umformung die Dampfdruckformel zu
\begin{equation}
	p_S=p_0 \cdot e^{ \left[ \frac{\Lambda_V}{R} \left( \frac{1}{T_0}-\frac{1}{T} \right) \right] } \; .
	\label{eq:dampfdruck}
\end{equation}
Demnach zeigt sich im Arrhenius-Plot von logarithmierten Druck gegen die reziproke Temperatur mit Steigung $m$ und $y$-Achsen-Abschnitt $b$ ein linearer Zusammenhang:
 
\begin{equation}
\ln \left(\frac{p}{p_0}\right) = - \frac{\Lambda_V}{R}\frac{1}{T} + \frac{\Lambda_V}{R} \frac{1}{T_0} = m \frac{1}{T} + b
\label{arrhenius}
\end{equation}


\subsection{Widerstandsthermometer}
Bei der Messung wird ein Pt1000 Widerstandsthermometer verwendet, in dessen Messfühler aus Platin bei $T = 0\, ^{\circ} \text{C}$ einen Widerstand von $\mathtt{R}_0 = 1000 \, \Omega $ gemessen werden kann. Für steigende Temperaturen $ \vartheta > 0\, ^{\circ} \text{C}$ steigt der Widerstand mit der Temperatur an nach \cite[Kap. 5.4.2]{knetter16}
\begin{equation}
	\mathtt{R}(\vartheta)=\mathtt{R}_0 \cdot (1+A \vartheta + B \vartheta ^2) \; .
	\label{eq:thermometer1}
\end{equation}
$A$ und $B$ bilden hierbei apparaturpezifische Konstanten. Durch Auflösen von Gleichung \ref{eq:thermometer1} nach $\vartheta$ lässt sich für $ \vartheta > 0\, ^{\circ} \text{C}$ die Temperatur aus gemessenem Widerstand $R$ berechnen zu
\begin{equation}
	\vartheta = - \frac{A}{2B} - \sqrt{\frac{A^2}{4B^2}- \frac{\mathtt{R}_0-\mathtt{R}}{\mathtt{R}_0 \cdot B}} \;.
	\label{eq:thermometer2}
\end{equation}
