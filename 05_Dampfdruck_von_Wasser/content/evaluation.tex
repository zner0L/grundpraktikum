\subsection{Druckkurven und Verdampfungswärme}
Nach Gleichung (\ref{eq:thermometer2}) lassen sich aus den gemessenen Widerständen $\mathtt{R}_0$ die im Kolben herrschenden Temperaturen berechnen:
\begin{equation*}
\vartheta = - \frac{A}{2B} - \sqrt{\frac{A^2}{4B^2}- \frac{\mathtt{R}_0-\mathtt{R}}{\mathtt{R}_0 \cdot B}} \; .
\end{equation*}
Die spezifischen Konstanten sind in Tabelle \ref{tab:werte} aufgeführt, der systematische Fehler beträgt $\Delta \vartheta = \pm (0.3 \, ^{\circ} \text{C} + 0.005 \, \vartheta)$. \cite{knetter16}
Die Druckkurven sind im Arrheniusplot gezeichnet, dass heißt durch Auftragen von $\ln p$ gegen $1/T$. 

\begin{figure}
	\centering
	\includegraphics[width=1\textwidth]{evaluation/arrheniusplot}
	\caption{Arrheniusplot von Druck $p$ und Temperatur $T$ mit linearem Fit. Bei der Abkühlung wurden systematisch fehlerbehaftete Werte für den Fit nicht berücksichtigt (vgl. Abschnitt \ref{sec:diskussion}).}
	\label{fig:arrheniusplot}
\end{figure}

Nach Gleichung \ref{arrhenius} zeigt sich hierbei ein linearer Zusammenhang und für die Steigung $m$ gilt
\begin{equation*}
	m = \frac{\text{d} \ln p}{\text{d}\,(1/T)} = - \frac{\Lambda_V}{R}
\end{equation*}
und es berechnet sich die Verdampfungswärme $\Lambda_V$ zu
\begin{equation*}
	\Lambda_V = - m \cdot R \, .
\end{equation*}
In Tabelle \ref{tab:results} sind die aus \texttt{gnuplot} durch Regression resultierenden Steigungen $m$ aufgeführt, die zugehörige Verdampfungswärme $\Lambda_V$ befindet sich in Tabelle \ref{tab:results}. Der Fehler ist dabei
\begin{equation*}
	\sigma_{\Lambda_V} = R \cdot \sigma_m \; .
\end{equation*}

\input{evaluation/m_and_b_table}

\input{evaluation/results_table}

\subsection{Siedepunkt}

Der Siedepunkt $T_0$ einer Flüssigkeit ist der Punkt, an dem Dampfdruck und Umgebungsdruck gleich groß sind. Ebenfalls aus Gleichung \ref{arrhenius} folgt mit Ordinatenabschnitt $b$ und $p_0 = 1013 \, \text{hPa}$ durch Umstellen Gleichung \ref{eq:pipapo}. Die Ergebnisse sind in Tabelle \ref{tab:results} aufgeführt.
\begin{align}
	b = \frac{\Lambda_V}{R \cdot T_0} & = - \frac{m}{T_0} \notag\\
	\Rightarrow \; \; \; \; \; T_0 & = \frac{m}{ - b}
	\label{eq:pipapo}
\end{align}
wobei der Fehler gegeben ist durch
\begin{equation*}
	\sigma_T = \sqrt{\left(\frac{\sigma_m}{-b}\right)^2+ \left(\frac{m \cdot \sigma_b}{b^2}\right)^2} \, .
\end{equation*}
 

\begin{table}[h!]
	\centering
	\begin{tabular}{||c||c||c||c||} \hline\hline
		\textbf{Größe} 	& \textbf{Bezeichnung} 	& \textbf{Wert} & \textbf{Einheit} \\\hline\hline
		Raumtemperatur 	& $T_0$ & $19.4$ 		& $^{\circ} \text{C}$ 	\\\hline\hline
		Luftdruck	 	&$p_0$ & $1006.4$ 	& hPa 					\\\hline\hline
		Thermometerkonstante & $A$ & $3.908 \cdot 10^{-3}$ & $\frac{1}{^{\circ} \text{C}}$ 		\\\hline\hline
		Thermometerkonstante & $B$	& $-5.775 \cdot 10^{-7}$ & $\frac{1}{(^{\circ} \text{C})^2}$ 		\\\hline\hline
		Thermometerwiderstand & $\mathtt{R}_0$	& $1000$ & $\Omega$ 		\\\hline\hline
	\end{tabular}
	\caption{Für die Auswertung relevante Daten und Charakteristika der verwendeten Apparaturen}
	\label{tab:werte}
\end{table}


\subsection{Dampfdruck}

Zur Bestimmung des Dampfdrucks $p_S$ wird Gleichung \ref{eq:pipapo} umgestellt und es ergeben sich mit $T = 0 ^{\circ} \text{C}= 273.115 \, \text{K}$ die Werte aus Tabelle \ref{tab:results} aus folgender Gleichung:

\begin{equation*}
	p_S = e^{\frac{m}{T} + b}
\end{equation*}
mit dem Fehler 
\begin{equation*}
	\sigma_{p_S} = \sqrt{\left(\sigma_m \sigma \frac{e^{\frac{m}{T}}+b}{T}\right)^2 + \left(\sigma_b \cdot e^{\frac{m}{T}+b}\right)^2} \, .
\end{equation*}


\subsection{Siedetemperatur auf der Zugspitze}

\begin{figure}[htb!]
	\centering
	\includegraphics[width=0.75\textwidth]{zugspitze.jpg}
	\caption{Blick über das Wasser auf die Zugspitze mit ortskonstanter Cumulus Humilis Bewölkung (lehrbuchverdächtig), links am Fuß des Berges das Hotel Eibsee. [Aus: {https://commons.wikimedia.org/wiki/File:Eibsee\_\%26\_Zugspitze.jpg}, aufgerufen am 24.01.2017] \label{zugspitze}}
\end{figure}

Um die Siedetemperatur auf der Zugspitze in einer Höhe von $h= 2962 \, \text{m}$ über Normalnull zu bestimmen, berechnet sich mit der barometrischen Höhenformel der theoretischen Druck $p_Z$ am Gipfel zu 
\begin{equation*}
	p_Z = p_0 \cdot e^{-\frac{\varrho_0 g h}{p_0}} = 69285 ~\text{Pa} \, .
\end{equation*}

Dabei ist die Dichte der Luft $p_0 = 1.293 ~\frac{\text{kg}}{\text{m}^3}$ und der Ortsfaktor $g = 9.81 \frac{\text{m}}{\text{s}^2}$.
Analog zu Gleichung \ref{eq:pipapo} berechnet sich die Siedetemperatur $T_Z$ zu 
\begin{equation*}
	T_Z  = \frac{m}{ - b} = 355 ~\pm ~3 ~\text{K} \, .
\end{equation*}

