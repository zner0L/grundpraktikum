# [Compiled PDFs](https://gitlab.com/zner0L/grundpraktikum/-/jobs/artifacts/master/browse?job=compile_pdf)

Ein Klick auf die Überschrift führt direkt zu den aktuellen PDFs des Projekts.

# Einleitung

Dieses Projekt beinhaltet alle Protokolle des Praktikums und Dateien, die zu ihrer Erstellung nötig sind. Erstellt und bearbeitet wurden die Protoklle von Lorenz Sieben und Luis Peters.

# Lizenz

Alle Texte unterliegen der Creative-Commoms CC-BY-SA Lizenz. Alle Bilder von Lorenz Sieben (entsprechend gekennzeichnet) unterliegen ebenfalls dieser Lizenz. Der Code ist unter der MIT License lizensiert.

# Vorrausetzungen

Alle Texte wurden mit LaTeX geschrieben und müssen (zweimal) mit `pdflatex` kompiliert werden. Außerdem müssen die Module `epstopdf` sowie `biblatex` sowie alle weiteren inkludierten Packages installiert sein.
Die Pythonskripte laufen unter Pyhton 2.7, alle notwendigen Dependencies können über pip installiert werden. Das Modul `maabara` wurde von @zner0L weiterentwickelt und lässt sich hier finden: https://github.com/zner0L/maabara Geplottet wurde mit Gnuplot, alle `.plot` Dateien können via `load` Befehl eingebunden werden.

# Arbeitsweise

## Dateistruktur

Die Protokolle sind alle in einzelnen Ordnern anglegt, die jeweils den Inhalt in verschiedenen Dateien enthalten. Die Dateistruktur sieht wie folgt aus (das Protokll `example`) bietet ein Beispiel:

```
protokoll_name/
├── content/
│   ├── introduction.tex
│   ├── theory.tex
│   ├── implementation.tex
│   ├── evaluation.tex
│   └── discussion.tex
├── main.tex
└── info.tex
```
### main.tex

Die `main.tex` führt alle Dateien zusammen. Sie enthält immmer das gleiche und muss `/inc/header.tex` und `/inc/base.tex` nacheinander einbinden. Dazwischen können protokollspezifische Pakete geladen werden.

### info.tex

Die `info.tex` enthält Metainformationen zu dem Protokoll (Name des Versuchs, Studierende etc.).

## Kompilieren

Um ein PDF zu erhalten, muss `main.tex` im jeweiligen Protokollordner kompiliert werden. Wichtig ist dabei die Reihenfolge: `pdflatex` -> `biblatex` -> 2x `pdflatex`

## Versionsverwaltung

Um Änderungen an den Dateien mit anderen zu teilen, müssen sie in der Versionsverwaltung eingetragen werden. Folgende Prozedur ist dafür notwendig:

```
 $ git status                               // Zeigt aktuelle Veränderungen an
 $ git add -A                               // Fügt alle Änderungen hinzu
 $ git commit -am 'Was ich gemacht habe'    // Speichert alle Änderungen
 $ git push origin master                   // Lädt die Änderungen auf den Server hoch
```

Bevor mit der Arbeit begonnen wird, müssen die lokalen Datein aktualisiert werden:

```
 $ git pull origin master
```