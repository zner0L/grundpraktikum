\section{Auswertung}
\subsection{Beschreibung des Vorgehens}
Alle im Folgenden angegebenen Fehlerformeln werden mithilfe der \texttt{GAUSS}'schen Fehlerfortpflanzung nach Gleichung \ref{eq:fehlerfort} bestimmt.\\
Im Allgemeinen ist die Intensitätsverteilung hinter dem Schirm eine Funktion einer Variable $\varepsilon$:
\begin{equation}
  I(\varepsilon)=I_0\cdot f(\varepsilon)\;.
\end{equation}
wobei diese folgendermaßen gegeben ist:\footnote{Alle verwendeten Formeln stammen aus \cite[21.4.3, p.~59ff.]{PB}.}
\begin{equation}
  \varepsilon = \frac{\pi D\sin(\alpha)}{\lambda} \quad .
  \label{eq:epsi}
\end{equation}
Hierbei ist $D$ die charakteristische Größe des Objekts, $\alpha$ der Winkel des gemessenen Extrema und $\lambda$ die Wellenlänge des Lasers. Der Winkel berechnet sich leicht durch:
\begin{equation}
  \alpha = \arctan\left(\frac{x}{l}\right) \quad .
  \label{eq:alpha}
\end{equation}
Die relative Position des Extremums zum Hauptmaximum wird mit der Variable $x$ bezeichnet, $l$ ist der Abstand zwischen Objekt und Schirm, welcher hier $l=(1.39\pm 0.01$)\,m beträgt, wobei der Fehler dadurch entsteht, dass wegen der Form des Objekthalters und der Diode der genaue Abstand schwer zu erkennen ist.\\
Für jedes Objekt werden mit der Formel \ref{eq:alpha} die Winkel $\alpha_i$ der gemessenen Maxima bestimmt und über die theoretischen Annahmen der Intensitätsverteilung die Größe $\varepsilon/\pi$ bestimmt. Stellt man die Gleichung \ref{eq:epsi} nach dieser Größe um, so ergibt sich ein linearer Zusammenhang zwischen $\varepsilon/\pi$ und $D$:
\begin{equation}
  \label{eq:e}
  \frac{\varepsilon}{\pi}=\frac{D}{\lambda}\sin{\alpha} \quad .
\end{equation}
Die Auswertung wird für jedes Objekt einzeln ausgeführt. Dazu werden zunächst die Extrema und deren relative Position zum Hauptmaximum bestimmt: Die Werte des Computerprogramms für die Intensität werden geplottet und die Position der Minima und Maxima sowie des 0. Hauptmaximums herausgelesen. Weil dies per Hand sehr ungenau ist, wird stattdessen der Algorithmus \A{peakdetect} des Programms \texttt{Python} verwendet, welcher die Extrema findet. Einige Objekte werden wegen äußeren Störungen doppelt gemessen. Für die Bestimmung der Minima/Maxima wird dann der Plot verwendet, der die deutlicheren Positionen liefert. Im Anhang sind die Plots mit gekennzeichneten Extremstellen für jedes Objekt zu sehen. Für den Fehler der Positionen der Minima und Maxima werden 5 Schritte angenommen. Weil ein Schritt 1/400\,mm entspricht, folgt daraus ein Fehler von $\sigma_x=0.0125$\,mm. Für den Fehler des Winkels bzw. der Sinusfunktionen der Winkel, mit denen anschließend weitergerechnet werden soll, folgen diese Fehler:
\begin{equation}
  \o_\alpha=\sqrt{\o_x^2\left(\frac{1}{\frac{x^2}{l}+l}\right)^2+\o_l^2\left(\frac{1}{l^2(x^2+1)}\right)^2} \quad \text{und}
\end{equation}
\begin{equation}
    \o_{\sin{\alpha}}=\o_\alpha\cos{\alpha} \quad .
\end{equation}
Für diese Maxima und Minima wird über Formel \ref{eq:alpha} der Winkel bestimmt und für alle Winkel eines Objekts eine lineare Interpolarisation durchgeführt. Die Fehler der Computerprogramm gemessenen Intensitäten werden als $\o_I=0.01$\,V angenommen. Für die gefittete Funktion $f=mx+b$ ist $m=D/\lambda$ der Wert der Steigung, über den die charakteristische Größe $D$ berechnet wird. Der Fit verwendet die Methode des kleinsten $\chi^2$.
Schließlich wird für diese Berechnung die Korrelation der Werte bestimmt. Für das Objekt mit der besten Korrelation wird die Steigung der Geraden, also die charakteristische Größe $D$, als gegeben gesetzt und über diese wiederum die Wellenlänge des Laserlichts $\lambda$ berechnet.\\
\subsection{Spalt}
Die Intensität im Fernfeld hinter einem Spalt kann durch:
\begin{equation*}
  I = I_0 \cdot \mathrm{sinc}(\varepsilon)^2
\end{equation*}
beschrieben werden. Hierbei ist $D$ die Spaltbreite. Somit ergibt sich bei Maxima der Intensität für $\varepsilon$:
\begin{equation*}
  \varepsilon = (n+0.5)\cdot \pi \hspace{2cm}\text{für } n\in\mathbb{Z}\setminus\{-1;0\}\;.
\end{equation*}
Bei minimaler Intensität gilt hingegen:
\begin{equation*}
  \varepsilon = n\cdot\pi\hspace{2cm}\text{für } n\in\mathbb{Z}\setminus\{0\}\;.
\end{equation*}
\begin{table}[H]
 \caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima bei Beugung am Spalt.}
  \centering
  \begin{tabular}{|c|c|c|c|}
  \hline
                       &   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
  \hline\hline
   4. Maximum (links)  & $-15.88\pm$0.02 & $-0.01142\pm0.00002$ & -4.5 \\
   3. Maximum (links)  & $-12.13\pm$0.02 & $-0.00872\pm0.00002$ & -3.5 \\
   2. Maximum (links)  & $-8.50\pm$0.02 & $-0.00611\pm0.00001$ & -2.5 \\
   1. Maximum (links)  & $-5.00\pm$0.02 & $-0.00360\pm0.00001$ & -1.5 \\
   Hauptmaximum        & 0     & $0$                  & 0   \\
   1. Maximum (rechts) & $\phantom{ip}5.13\pm$0.02 & $\phantom{ip}0.00369\pm0.00001$   & 1.5 \\
   2. Maximum (rechts) & $\phantom{ip}8.63\pm$0.02 & $\phantom{ip}0.00620\pm0.00001$   & 2.5 \\
   3. Maximum (rechts) & $\phantom{ip}12.00\pm$0.02 & $\phantom{ip}0.00863\pm0.00001$   & 3.5 \\
   4. Maximum (rechts) & $\phantom{ip}15.63\pm$0.02 & $\phantom{ip}0.01124\pm0.00002$   & 4.5 \\
  \hline
  \end{tabular}
  \label{tab:spaltMax}
\end{table}
\begin{table}[H]
  \caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Minima bei Beugung am Spalt.}
  \centering
  \begin{tabular}{|c|c|c|c|}
  \hline
                       &   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
  \hline\hline
   4. Minimum (links)  & $-14.50\pm$0.02 & $-0.01043\pm0.00002$ & -4 \\
   3. Minimum (links)  & $-10.50\pm$0.02 & $-0.00755\pm0.00002$ & -3 \\
   2. Minimum (links)  & $-7.13\pm$0.02 & $-0.00513\pm0.00001$ & -2 \\
   1. Minimum (links)  & $-3.63\pm$0.02 & $-0.00261\pm0.00001$ & -1 \\
   Hauptminimum        & 0     & $0$                  & 0   \\
   1. Minimum (rechts) & $\phantom{ip}3.75\pm$0.02 & $\phantom{ip}0.00270\pm0.00001$   & 1 \\
   2. Minimum (rechts) & $\phantom{ip}7.00\pm$0.02 & $\phantom{ip}0.00504\pm0.00001$   & 2 \\
   3. Minimum (rechts) & $\phantom{ip}10.63\pm$0.02 & $\phantom{ip}0.00764\pm0.00002$   & 3 \\
   4. Minimum (rechts) & $\phantom{ip}14.25\pm$0.02 & $\phantom{ip}0.00103\pm0.00002$   & 3 \\
   5. Minimum (rechts) & $\phantom{ip}17.38\pm$0.02 & $\phantom{ip}0.00125\pm0.00002$   & 3 \\
  \hline
  \end{tabular}
  \label{tab:spaltMin}
\end{table}
Der Fit aus Abbildung \ref{fig:spaltkor} liefert eine Steigung von $m=-397\pm2$ und es folgt: 
\begin{equation}
    D=0.251\pm0.002 \,\text{mm} \quad .
\end{equation}
Die Korrelation ist in diesem Fall: $k=0.99983$
\subsection{Steg}
Der Steg ist das Komplement zum Spalt und hat somit nach dem Babinetschen Theorem die gleiche Intensitätsverteilung wie der Spalt, somit gelten für $\varepsilon$ ebenfalls die gleichen Bedingungen. Die charakteristische Größe $D$ entspricht der Stegbreite.
\begin{table}[H]
  \caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima bei Beugung am Steg.}
  \centering
  \begin{tabular}{|c|c|c|c|}
  \hline
                       &   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
  \hline\hline
   3. Maximum (links)  & $-14.75\pm$0.02 & $-0.01061\pm0.00002$ & -3.5 \\
   2. Maximum (links)  & $-10.63\pm$0.02 & $-0.00764\pm0.00001$ & -2.5 \\
   1. Maximum (links)  & $-5.88\pm$0.02 & $-0.00423\pm0.00001$ & -1.5 \\
   Hauptmaximum        & 0     & $0$                  & 0   \\
   1. Maximum (rechts) & $\phantom{ip}5.38\pm$0.02 & $\phantom{ip}0.00387\pm0.00001$   & 1.5 \\
   2. Maximum (rechts) & $\phantom{ip}9.88\pm$0.02 & $\phantom{ip}0.00710\pm0.00001$   & 2.5 \\
   3. Maximum (rechts) & $\phantom{ip}13.50\pm$0.02 & $\phantom{ip}0.00971\pm0.00002$   & 3.5 \\
  \hline
  \end{tabular}
  \label{tab:stegMax}
\end{table}
\begin{table}[H]
  \caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Minima bei Beugung am Steg.}
  \centering
  \begin{tabular}{|c|c|c|c|}
  \hline
                       &   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
  \hline\hline
   4. Minimum (links)  & $-17.00\pm$0.02 & $-0.01223\pm0.00002$ & -3.5 \\
   3. Minimum (links)  & $-12.63\pm$0.02 & $-0.00908\pm0.00002$ & -3.5 \\
   2. Minimum (links)  & $\phantom{.}-8.88\pm$0.02 & $-0.00638\pm0.00001$ & -2.5 \\
   1. Minimum (links)  & $\phantom{l}-4.63\pm$0.02 & $-0.00333\pm0.00001$ & -1.5 \\
   Hauptminimum        & 0     & $0$                  & 0   \\
   1. Minimum (rechts) & $\phantom{ip0}4.00\pm$0.02 & $\phantom{ip}0.00288\pm0.00001$   & 1.5 \\
   2. Minimum (rechts) & $\phantom{ip0}7.88\pm$0.02 & $\phantom{ip}0.00567\pm0.00001$   & 2.5 \\
   3. Minimum (rechts) & $\phantom{ip}11.75\pm$0.02 & $\phantom{ip}0.00845\pm0.00002$   & 3.5 \\
   \hline
   \end{tabular}
  \label{tab:stegMin}
\end{table}
Der Fit aus Abbildung \ref{fig:stegkor} liefert eine Steigung von $m=-341\pm3$ und es folgt: 
\begin{equation}
    D=0.216\pm0.002 \,\text{mm} \quad .
\end{equation}
Die Korrelation ist in diesem Fall: $k=0.99965$
\subsection{Kreisblende}
Zur Berechnung der Intensität wird die Besselfunktion verwendet. Die sich daraus ergebenden Werte für $\varepsilon/\pi$ können in \cite[21.4.3, p.~61ff.]{PB} nachgeschlagen werden. Die charakteristische Größe $D$ ist der Durchmesser der Kreisblende.
\begin{table}[H]
  \caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima und Minima bei Beugung an der Kreisblende.}
  \centering
  \begin{tabular}{|c|c|c|c|}
  \hline
                       &   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
  \hline\hline
   2. Maximum (links)  & $-8.38\pm$0.02 & $-0.00603\pm0.00001$ & -2.6793 \\
   1. Maximum (links)  & $-7.25\pm$0.02 & $-0.00522\pm0.00001$ & -1.6347 \\
   1. Maximum (rechts) & $\phantom{ip}7.00\pm$0.02 & $\phantom{ip}0.00504\pm0.00001$ & 1.6347 \\
  \hline\hline
   3. Minimum (links)  & $-9.25\pm$0.02 & $-0.00665\pm0.00001$   & -3.2383 \\
   2. Minimum (links)  & $-8.13\pm$0.02 & $-0.00585\pm0.00001$   & -2.2331 \\
   1. Minimum (links)  & $-5.25\pm$0.02 & $-0.00367\pm0.00001$   & -1.2197 \\
   1. Minimum (rechts) & $\phantom{ip}5.13\pm$0.02 & $\phantom{ip}0.00369\pm0.00001$   & 1.2197 \\
   2. Miminum (rechts) & $\phantom{ip}9.00\pm$0.02 & $\phantom{ip}0.00647\pm0.00001$   & 2.2331 \\
  \hline
  \end{tabular}
  \label{tab:kreis}
\end{table}
Der Fit aus Abbildung \ref{fig:lochkor} liefert eine Steigung von $m=340\pm30$ und es folgt: 
\begin{equation}
    D=0.22\pm0.02 \,\text{mm} \quad .
\end{equation}
Die Korrelation ist in diesem Fall: $k=0.98373$
\subsection{Doppellochblende}
Die Intensitätsverteilung ist gegeben durch:
\begin{equation*}
  I = I_0 \cdot \cos(\varepsilon)^2\;.
\end{equation*}
Somit erhält man ein Intensitätsmaximum für 
\begin{equation*}
  \varepsilon = n\cdot\pi\hspace{2cm}\text{für } n\in\mathbb{Z}
\end{equation*}
und ein Intensitätsminimun für
\begin{equation*}
  \varepsilon = (n+0.5)\cdot \pi\hspace{2cm}\text{für } n\in\mathbb{Z}\;.
\end{equation*}
Die charakteristische Größe $D_i$ entspricht hier dem Lochabstand.
Die Fits aus Abbildungen \ref{fig:Doppel1kor}, \ref{fig:Doppel2kor} und \ref{fig:Doppel3kor} liefern eine Steigung von $m_1=-670\pm40$, $m_2=-1150\pm30$ und $m_3=-2730\pm70$ und es folgt: 
\begin{equation}
    D_1=0.42\pm0.03 \,\text{mm} \quad \text{;} \quad D_2=0.73\pm0.02 \,\text{mm} \quad \text{und} \quad  D_3=1.73\pm0.04 \,\text{mm} \quad .
\end{equation}
Die Korrelationen sind in diesem Fall: $k_1=0.98152$; $k_2=0.99350$ und $k_3=0.99403$. 
\subsection{Gitter}
Die Intensitätsverteilung hinter dem Gitter ist gegeben durch:
\begin{equation*}
  I = I_0 \cdot \underbrace{\left( \frac{\sin\left(\frac{\pi\sin(\alpha)B}{\lambda}\right)}{\frac{\pi\sin(\alpha)B}{\lambda}} \right)^2}_{Spaltfunktion} \cdot \underbrace{\left( \frac{\sin(N\varepsilon)}{\sin(\varepsilon)} \right)^2}_{Gitterfunktion}\;.
\end{equation*}
Dabei ist $B$ die Spaltbreite und $D = B + S$ die charaktersistische Größe mit der Stegbreite $S$. 
Es ergibt sich für die Maxima:
\begin{equation*}
  \varepsilon = n\cdot\pi\hspace{2cm}\text{für }n\in\mathbb{Z}\;.
\end{equation*}
Eine Bedinung für $\varepsilon$ der Minima kann mit unbekannter Spaltanzahl $N$ nicht hergeleitet werden.
\begin{table}[H]
  \caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima bei Beugung am Gitter.}
  \centering
  \begin{tabular}{|c|c|c|c|}
  \hline
                       &   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
  \hline\hline
   4. Maximum (links)  & $-7.38\pm$0.02 & $-0.00530\pm0.00001$ & -3.5 \\
   3. Maximum (links)  & $-6.25\pm$0.02 & $-0.00350\pm0.00001$ & -3.5 \\
   2. Maximum (links)  & $-4.25\pm$0.02 & $-0.00306\pm0.00001$ & -2.5 \\
   1. Maximum (links)  & $-1.63\pm$0.02 & $-0.00117\pm0.00001$ & -1.5 \\
   Hauptmaximum        & 0     & $0$                  & 0   \\
   1. Maximum (rechts) & $\phantom{ip}1.50\pm$0.02 & $\phantom{ip}0.00108\pm0.00001$   & 1.5 \\
   2. Maximum (rechts) & $\phantom{ip}3.25\pm$0.02 & $\phantom{ip}0.00234\pm0.00001$   & 2.5 \\
   3. Maximum (rechts) & $\phantom{ip}4.63\pm$0.02 & $\phantom{ip}0.00463\pm0.00001$   & 3.5 \\
  \hline
  \end{tabular}
  \label{tab:GitterMax}
\end{table}
\begin{table}[H]
  \caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Minima bei Beugung am Gitter.}
  \centering
  \begin{tabular}{|c|c|c|c|}
  \hline
                       &   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
  \hline\hline
   5. Minimum (links)  & $-8.38\pm$0.02 & $-0.00603\pm0.00001$ & - \\
   4. Minimum (links)  & $-6.88\pm$0.02 & $-0.00495\pm0.00001$ & - \\
   3. Minimum (links)  & $-5.75\pm$0.02 & $-0.00414\pm0.00001$ & - \\
   2. Minimum (links)  & $-3.75\pm$0.02 & $-0.00270\pm0.00001$ & - \\
   1. Minimum (links)  & $-0.88\pm$0.02 & $-0.00063\pm0.00001$ & - \\
   Hauptminimum        & 0     & $0$                  & -   \\
   1. Minimum (rechts) & $\phantom{ip}0.88\pm$0.02 & $\phantom{ip}0.00629\pm0.00001$   & - \\
   2. Minimum (rechts) & $\phantom{ip}2.88\pm$0.02 & $\phantom{ip}0.00207\pm0.00001$   & - \\
   3. Minimum (rechts) & $\phantom{ip}3.75\pm$0.02 & $\phantom{ip}0.00270\pm0.00001$   & - \\
   4. Minimum (rechts) & $\phantom{ip}5.25\pm$0.02 & $\phantom{ip}0.00378\pm0.00001$   & - \\
  \hline
  \end{tabular}
  \label{tab:GitterMin}
\end{table}
Der Fit aus Abbildung \ref{fig:Gitterkor} liefert eine Steigung von $m=-900\pm30$ und es folgt: 
\begin{equation}
    D=0.57\pm0.02 \,\text{mm} \quad .
\end{equation}
Die Korrelation ist in diesem Fall: $k=0.99402$
\subsection{Die Wellenlänge des Lasers}
Die Korellation des Einfachspalts ist am Größten. Somit wird die Spaltbreite $D$ als gegeben angesehen. Aus dem Fit ist außerdem die Steigung der Geraden $m$ bekannt. Nach Formel \ref{eq:e} lautet dann die Wellenlänge des Lasers:
\begin{equation}
    \lambda=\frac{D}{m}=632\pm6\,\text{nm} \quad .
\end{equation}
\newpage
\subsection{Steigungsgeraden zur Bestimmung der charakteristischen Größen}
\begin{figure}[h]
\centering
\scalebox{0.7}
{\includegraphics  [width=0.75\textwidth]{figures/1-Einfachspalt-correlation.png}}
\caption{Auftragen der $\epsilon_i/\pi$ gegen die jeweiligen $\sin({\alpha_i})$ zur Bestimmung der Größe $D$ und der Korrelation für den Spalt.}
\label{fig:spaltkor}
\end{figure}
\begin{figure}[H]
\centering
\scalebox{0.8}{
\includegraphics [width=0.75\textwidth]{figures/2-Draht-correlation.png}}
\caption{Auftragen der $\epsilon_i/\pi$ gegen die jeweiligen $\sin({\alpha_i})$ zur Bestimmung der Größe $D$ und der Korrelation für den Steg.}
\label{fig:stegkor}
\end{figure}
\begin{figure}
\centering
\scalebox{0.8}{
\includegraphics [width=0.75\textwidth]{figures/3-Punkt-correlation.png}}
\caption{Auftragen der $\epsilon_i/\pi$ gegen die jeweiligen $\sin({\alpha_i})$ zur Bestimmung der Größe $D$ und der Korrelation für die Lochblende.}
\label{fig:lochkor}
\end{figure}
\begin{figure}
\centering
\scalebox{0.8}{
\includegraphics [width=0.75\textwidth]{figures/4-Doppelpunkt-correlation.png}}
\caption{Auftragen der $\epsilon_i/\pi$ gegen die jeweiligen $\sin({\alpha_i})$ zur Bestimmung der Größe $D_1$ und der Korrelation für die Doppellochblende.}
\label{fig:Doppel1kor}
\end{figure}
\begin{figure}
\centering
\scalebox{0.8}{
\includegraphics [width=0.75\textwidth]{figures/5-Doppelpunkt-correlation.png}}
\caption{Auftragen der $\epsilon_i/\pi$ gegen die jeweiligen $\sin({\alpha_i})$ zur Bestimmung der Größe $D_2$ und der Korrelation für die Doppellochblende.}
\label{fig:Doppel2kor}
\end{figure}
\begin{figure}
\centering
\scalebox{0.8}{
\includegraphics [width=0.75\textwidth]{figures/6-Doppelpunkt-correlation.png}}
\caption{Auftragen der $\epsilon_i/\pi$ gegen die jeweiligen $\sin({\alpha_i})$ zur Bestimmung der Größe $D_3$ und der Korrelation für die Doppellochblende.}
\label{fig:Doppel3kor}
\end{figure}
\begin{figure}
\centering
\scalebox{0.8}{
\includegraphics [width=0.75\textwidth]{figures/7-Gitter-correlation.png}}
\caption{Auftragen der $\epsilon_i/\pi$ gegen die jeweiligen $\sin({\alpha_i})$ zur Bestimmung der Größe $D$ und der Korrelation für das Gitter.}
\label{fig:Gitterkor}
\end{figure}