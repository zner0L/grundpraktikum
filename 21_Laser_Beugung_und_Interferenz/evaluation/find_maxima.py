# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math
import os
from peakdetect import peakdetect



# Fitfunktion defnieren
def fit_linear(x, a, b):
        return a * x + b

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

figsize = [10, 10]

def sort_extrema(extrema, xmin, xmax, main_max):
    extrema[0] = np.array([extrema[0][i, :] for i in range(0, len(extrema[0][:, 0])) if (extrema[0][i, 0] > xmin) and (extrema[0][i, 0] < xmax)])
    extrema[1] = np.array([extrema[1][i, :] for i in range(0, len(extrema[1][:, 0])) if (extrema[1][i, 0] > xmin) and (extrema[1][i, 0] < xmax) and (extrema[1][i, 1] < main_max)])

    return extrema

def find_order(extrema, offset=0):
    middle = np.where(extrema < 0)[0][-1]
    order = []
    for i, ex in enumerate(extrema):
        if i > middle:
            order.append((middle - i) - offset)
        else:
            order.append((middle - i +1) + offset)
    return np.array(order)

objekte = {
    1: {
        'name': 'Einfachspalt',
        'use_extrema_of': 'spalt_1',
        'use_main_max_of': 'spalt_1',
        'useful_range': [6500, 21000],
    },
    2: {
        'name': 'Draht',
        'use_extrema_of': 'draht_2',
        'use_main_max_of': 'draht_2_v2',
        'useful_range': [6000, 19000],
    },
    3: {
        'name': 'Lochblende',
        'use_extrema_of': 'punkt_3_v2',
        'use_main_max_of': 'punkt_3_v2',
        'useful_range': [9000, 17000],
    },
    4: {
        'name': 'Doppellochblende',
        'use_extrema_of': 'zwei_punkte_4',
        'use_main_max_of': 'zwei_punkte_4',
        'useful_range': [10000, 17000],
    },
    5: {
        'name': 'Doppellochblende',
        'use_extrema_of': 'zwei_punkte_5',
        'use_main_max_of': 'zwei_punkte_5',
        'useful_range': [10000, 17000],
    },
    6: {
        'name': 'Doppellochblende',
        'use_extrema_of': 'zwei_punkte_6',
        'use_main_max_of': 'zwei_punkte_6',
        'useful_range': [12000, 15000],
    },
    7: {
        'name': 'Gitter',
        'use_extrema_of': 'gitter_7_v2',
        'use_main_max_of': 'gitter_7_v2',
        'useful_range': [10000, 16000],
    }
}

data = {}
extremata = {}

for file in os.listdir('data'):
    series = file.replace('.dat', '')
    data[series] = np.genfromtxt('data/' + file, delimiter="\t", usecols=range(2))

    maxima, minima = peakdetect(data[series][:, 1], data[series][:, 0], lookahead=2)
    minima = np.array(minima)
    maxima = np.array(maxima)

    pos = np.argmax(maxima[:, 1])
    main_max = [maxima[pos, 0], maxima[pos, 1]]

    extremata[series] = [minima, maxima, main_max]

alpha = ma.uncertainty.Sheet('atan(x/l)')
alpha.set_value('l', 139e-2, 0.1e-2)
alpha.set_value('x', error=5*1e-3/400)

sine = ma.uncertainty.Sheet('sin(a)')

epsilon_norm = ma.uncertainty.Sheet('')

figs = {}

for key in objekte.keys():
    objekt = objekte[key]
    series = objekt['use_extrema_of']
    main_max = extremata[objekt['use_main_max_of']][2]
    extrema = sort_extrema(extremata[series], objekt['useful_range'][0], objekt['useful_range'][1], main_max[1])
    rel_maxima = (extrema[1][:, 0] - main_max[0])*1e-3/400
    rel_minima = (extrema[0][:, 0] - main_max[0])*1e-3/400
    alpha_maxima = alpha.batch(rel_maxima, 'x')
    alpha_minima = alpha.batch(rel_minima, 'x')
    sine_alpha_maxima = sine.batch(alpha_maxima, 'a|a%')
    sine_alpha_minima = sine.batch(alpha_minima, 'a|a%')

    order_max = find_order(rel_maxima, 0.5)
    order_min = find_order(rel_minima)


    results = np.column_stack(
        (np.concatenate((extrema[0][:, 0], extrema[1][:, 0])),
         np.concatenate((rel_minima, rel_maxima)),
         np.concatenate((sine_alpha_minima, sine_alpha_maxima)),
         np.concatenate((order_min, order_max)))
    )


    print('----- ' + objekt['name'] + ' -----')
    print('abs. Position | rel. Position | sin(alpha) | Fehler sinus | Ordnung')
    print(results)

    figs[str(key) + '-' + objekt['name'] + '.png'] = plt.figure(figsize=figsize)
    spline = interp1d(data[series][:, 0], data[series][:, 1], kind='linear')
    plt.title(objekt['name'])
    plt.errorbar(data[series][:, 0], data[series][:, 1], xerr=5, yerr=0.01, fmt='.', label='Messwerte')
    plt.scatter(extrema[1][:, 0], extrema[1][:, 1], color='red', label='Maxima', marker='^')
    plt.scatter(extrema[0][:, 0], extrema[0][:, 1], color='green', label='Minima', marker='v')
    plt.axes().set_xlim(objekt['useful_range'][0], objekt['useful_range'][1])
    plt.axes().axvline(main_max[0], ymin=0, ymax=10, label='Hauptmaximum 0. Ordnung', color='orange')
    plt.xlabel('abs. Position [$\\frac{1}{400}$ mm]')
    plt.ylabel('Intenstit{\\"a}t [V]')
    plt.plot(data[series][:, 0], spline(data[series][:, 0]))
    box = plt.axes().get_position()
    plt.axes().set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
    plt.legend(bbox_to_anchor=(0., -0.32, 1., .102), loc=3,
       ncol=2, mode="expand", borderaxespad=0.)
    plt.close()

    params, errors = curve_fit(fit_linear, results[:, 2], results[:, 4])
    m = ufloat(params[0], np.sqrt(errors[0][0]))
    D = m * 632.8e-9
    r = np.corrcoef(results[:, 2], results[:, 4])[1, 0]

    print('Steigung m: ' + str(m))
    print('Konstante D: ' + str(D))
    print('Korrelation r: ' + str(r))
    if round(r, 3) <= -0.997:
        print('Wellenlänge: ' + str(D/m))

    l = np.linspace(-2, 2)
    figs[str(key) + '-' + objekt['name'] + '-correlation.png'] = plt.figure(figsize=figsize)
    plt.title(objekt['name'] + ' Korrelation')
    plt.errorbar(results[:, 2]*1e+2, results[:, 4], xerr=results[:, 4]*1e-2, label='Messwerte', fmt='.')
    plt.plot(l, fit_linear(l, params[0]*1e-2, params[1]), label='Fitgerade')
    plt.axes().set_xlim(np.amin(results[:, 2])*1e+2, np.amax(results[:, 2])*1e+2)
    plt.axes().set_ylim(np.amin(results[:, 4]), np.amax(results[:, 4]))
    plt.ylabel('$\\frac{\\epsilon}{\\pi}$')
    plt.xlabel('$\\sin(\\alpha)$ [$10^{-2}$]')
    plt.legend()
    plt.close()

for key in figs.keys():
    figs[key].savefig(key)