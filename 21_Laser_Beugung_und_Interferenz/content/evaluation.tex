\subsection{Tabellen zu den Beugungsobjekten}
Es wurden verschiedene Beugungsobjekte untersucht: Ein Einzelspalt, ein Steg, eine Lochblende, drei Doppellochblenden und ein Gitter. Für alle diese Objekte ist die Intensitätsverteilung gegeben durch \cite[Kap. 21.4.3]{knetter17}:

\begin{equation}
	I(\varepsilon) = I_0 \cdot  f(\varepsilon) \; ,
\end{equation}

wobei hierbei die Variable $\varepsilon$ vom Beugungswinkel $\alpha$ abhängt. Es gilt:

\begin{equation}
	\varepsilon = \frac{\pi D \sin(\alpha)}{\lambda} \; .
	\label{eq:epsilon}
\end{equation}

$D$ wird die charakteristische Größe des Objektes genannt und ist eine Konstante, die objektabhängig ist. Aus der Geometrie des Versuchsaufbaus lässt sich für $\alpha$ folgern:

\begin{equation}
	\alpha = \arctan(\frac{x}{l}) \; .
\end{equation}

$x$ meint die relative Position eines Extremums zum 0. Hauptmaximum, $l$ bezeichnet den Abstand des Beugungsobjektes zur Messdiode, für welchen $l=(1.39\pm 0.01) \; \text{m}$ gemessen wurde. Der Fehler kommt durch Ablesefehler an der teilweise verdeckten Skala der Schiene zustande. Aus den so bestimmten Winkeln ergibt sich dann auch $\sin(\alpha)$, wobei für die Fehler nach der Gauß'schen Fehlermethode gilt:

\begin{align}
	\sigma_\alpha &= \sqrt{\sigma_x^2\left(\frac{1}{\frac{x^2}{l}+l}\right)^2+\sigma_l^2\left(\frac{1}{l^2(x^2+1)}\right)^2} \\
	\sigma_{\sin{\alpha}} &= \sigma_\alpha\cos{\alpha} \; .
\end{align}

Die Positionen der Extrema ergeben sich aus der gemessenen Intensitätsverteilung durch Ablesen der Maxima und Minima. Hierfür wird die \texttt{python}-Analysebibliothek \texttt{peakdetect} \cite{peakdetect} verwendet, welche auch den Messwerten Maxima und Minima heraussucht. Für manche Beugungsobjekte wurden Messungen bei verschiedenen Verstärkungen durchgeführt, sodass die eingezeichneten Hauptmaxima teilweise (offensichtlich) aus anderen Messwerten bestimmt wurden, als jene, aus denen die Nebenmaxima stammen. Für $x$ wird ein Fehler von fünf Motorschritten angenommen, wobei 1 mm 400 Motorschritten entspricht, also ist $\sigma_x = \frac{5}{400} \, \text{mm} = 0.0125 \, \text{mm}$. \\

Schließlich lässt sich $\varepsilon$ auch aus der Ordnung der Extrema bestimmen. Es gilt:

\begin{align}
	\text{Für Maxima:} \quad \varepsilon &= (n + \frac{1}{2}) \cdot \pi \notag\\ 
	\text{Für Minima:} \quad \varepsilon &= n \cdot \pi
\end{align}

Die aus den Messwerten bestimmten Werte sind im Folgenden in Tabellen aufgeführt.

\subsection{Bestimmung der charakteristischen Größen}

Um die charakteristischen Größen der Beugungsobjekte zu bestimmen wird verwendet, dass der Zusammenhang $\varepsilon \propto \alpha$ gilt, denn aus Gleichung \ref{eq:epsilon} folgt:

\begin{equation}
	\frac{\varepsilon}{\pi} = \frac{D}{\lambda} \sin(\alpha) \; .
\end{equation}

Es lässt sich also $\frac{\varepsilon}{\pi}$ gegen $\sin(\alpha)$ auftragen. Ein Fit mit $f(x) = m \cdot x + b$ ergibt dann $m = \frac{D}{\lambda}$. Für $\lambda$, der Wellenlänge des He-Ne-Lasers, gilt nach \cite[Kap. 21.3]{knetter17} $\lambda_{\text{He-Ne}} = 632.8 \, \text{nm}$. Daraus lässt sich dann $D = m \cdot 632.8 \, \text{nm}$ bestimmen. Für dem Fehler von $D$ gilt:

\begin{equation*}
	\sigma_D = 632.8 \cdot \sigma_m \; ,
\end{equation*}

wobei sich $\sigma_m$, der Fehler von $m$, aus dem $\chi^2$-Fit ergibt.

%-------------------------------------- Werte hier --------------------------------------%
\subsection{Ergebnisse für die Beugungsobjekte}

\subsubsection{Einfachspalt}

\begin{figure}
	\centering
	\scalebox{0.7}
	{\includegraphics[width=0.7\textwidth]{evaluation/1-Einfachspalt.png}}
	{\includegraphics[width=0.5\textwidth]{evaluation/1-Einfachspalt-correlation.png}}
	\caption{Gemessene Intensitätsverteilung des Einfachspalts mit eingezeichnetem Hauptmaximum (links) sowie die bestimmten Werte für $\frac{\varepsilon}{\pi}$ aufgetragen gegen den Sinus des Beugungswinkels $\sin(\alpha)$ (rechts).}
	\label{fig:spalt}
\end{figure}

Die Messwerte des Einfachspalts sind in Abbildung \ref{fig:spalt} aufgeführt. Die gefundenen Maxima sind in Tabelle \ref{tab:spaltMax}, die Minima in Tabelle \ref{tab:spaltMin} aufgeführt.\\

Für der Spalt wird durch seine Spaltbreite $D$ charakterisiert. Die Ausgleichsgerade an die Messwerte in Abbildung \ref{fig:spalt} liefert eine Steigung von $m = -397 \pm 2$ und es folgt: 
\begin{equation}
D=0.251\pm0.002 \,\text{mm} \quad .
\end{equation}

Für die Korrelation ergibt sich nach \cite{knetter17}: $k=0.99983$.

\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima bei Beugung am Spalt.}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		4. Maximum (links)  & $-15.88\pm$0.02 & $-0.01142\pm0.00002$ & -4.5 \\ \hline
		3. Maximum (links)  & $-12.13\pm$0.02 & $-0.00872\pm0.00002$ & -3.5 \\ \hline
		2. Maximum (links)  & $-8.50\pm$0.02 & $-0.00611\pm0.00001$ & -2.5 \\ \hline
		1. Maximum (links)  & $-5.00\pm$0.02 & $-0.00360\pm0.00001$ & -1.5 \\ \hline
		Hauptmaximum        & 0     & $0$                  & 0   \\ \hline
		1. Maximum (rechts) & $\phantom{ip}5.13\pm$0.02 & $\phantom{ip}0.00369\pm0.00001$   & 1.5 \\ \hline
		2. Maximum (rechts) & $\phantom{ip}8.63\pm$0.02 & $\phantom{ip}0.00620\pm0.00001$   & 2.5 \\ \hline
		3. Maximum (rechts) & $\phantom{ip}12.00\pm$0.02 & $\phantom{ip}0.00863\pm0.00001$   & 3.5 \\ \hline
		4. Maximum (rechts) & $\phantom{ip}15.63\pm$0.02 & $\phantom{ip}0.01124\pm0.00002$   & 4.5 \\ \hline
	\end{tabular}
	\label{tab:spaltMax}
\end{table}

\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Minima bei Beugung am Spalt.}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		4. Minimum (links)  & $-14.50\pm$0.02 & $-0.01043\pm0.00002$ & -4 \\ \hline
		3. Minimum (links)  & $-10.50\pm$0.02 & $-0.00755\pm0.00002$ & -3 \\ \hline
		2. Minimum (links)  & $-7.13\pm$0.02 & $-0.00513\pm0.00001$ & -2 \\ \hline
		1. Minimum (links)  & $-3.63\pm$0.02 & $-0.00261\pm0.00001$ & -1 \\ \hline
		Hauptminimum        & 0     & $0$                  & 0   \\ \hline
		1. Minimum (rechts) & $\phantom{ip}3.75\pm$0.02 & $\phantom{ip}0.00270\pm0.00001$   & 1 \\ \hline
		2. Minimum (rechts) & $\phantom{ip}7.00\pm$0.02 & $\phantom{ip}0.00504\pm0.00001$   & 2 \\ \hline
		3. Minimum (rechts) & $\phantom{ip}10.63\pm$0.02 & $\phantom{ip}0.00764\pm0.00002$   & 3 \\ \hline
		4. Minimum (rechts) & $\phantom{ip}14.25\pm$0.02 & $\phantom{ip}0.00103\pm0.00002$   & 3 \\ \hline
		5. Minimum (rechts) & $\phantom{ip}17.38\pm$0.02 & $\phantom{ip}0.00125\pm0.00002$   & 3 \\ \hline
	\end{tabular}
	\label{tab:spaltMin}
\end{table}

\subsubsection{Steg}

\begin{figure}
	\centering
	\scalebox{0.7}
	{\includegraphics[width=0.7\textwidth]{evaluation/2-Draht.png}}
	{\includegraphics[width=0.5\textwidth]{evaluation/2-Draht-correlation.png}}
	\caption{Gemessene Intensitätsverteilung des Stegs mit eingezeichnetem Hauptmaximum (links) sowie die bestimmten Werte für $\frac{\varepsilon}{\pi}$ aufgetragen gegen den Sinus des Beugungswinkels $\sin(\alpha)$ (rechts). Das eingetragene Hauptmaximum stammt aus einer zweiten Messreihe.}
	\label{fig:steg}
\end{figure}

Für den Steg, also das Komplement des Einfachspalts, sind die Messwerte Abbildung \ref{fig:steg} aufgezeichnet. Die daraus bestimmen Maxima sind in Tabelle \ref{tab:stegMax}, die Minima in Tabelle \ref{tab:stegMin} aufgeführt.\\

Der lineare Fit (Abbildung \ref{fig:steg}) liefert eine Steigung von $m=-341\pm3$ und es folgt: 
\begin{equation}
D=0.216\pm0.002 \,\text{mm} \quad .
\end{equation}
Beim Steg entspricht $D$ analog zum Einfachspalt der Stegbreite. Die Korrelation ist in diesem Fall: $k=0.99965$

\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima bei Beugung am Steg.}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		3. Maximum (links)  & $-14.75\pm$0.02 & $-0.01061\pm0.00002$ & -3.5 \\ \hline
		2. Maximum (links)  & $-10.63\pm$0.02 & $-0.00764\pm0.00001$ & -2.5 \\ \hline
		1. Maximum (links)  & $-5.88\pm$0.02 & $-0.00423\pm0.00001$ & -1.5 \\ \hline
		Hauptmaximum        & 0     & $0$                  & 0   \\ \hline
		1. Maximum (rechts) & $\phantom{ip}5.38\pm$0.02 & $\phantom{ip}0.00387\pm0.00001$   & 1.5 \\ \hline
		2. Maximum (rechts) & $\phantom{ip}9.88\pm$0.02 & $\phantom{ip}0.00710\pm0.00001$   & 2.5 \\ \hline
		3. Maximum (rechts) & $\phantom{ip}13.50\pm$0.02 & $\phantom{ip}0.00971\pm0.00002$   & 3.5 \\ \hline
	\end{tabular}
	\label{tab:stegMax}
\end{table}
\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Minima bei Beugung am Steg.}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		4. Minimum (links)  & $-17.00\pm$0.02 & $-0.01223\pm0.00002$ & -3.5 \\ \hline
		3. Minimum (links)  & $-12.63\pm$0.02 & $-0.00908\pm0.00002$ & -3.5 \\ \hline
		2. Minimum (links)  & $\phantom{.}-8.88\pm$0.02 & $-0.00638\pm0.00001$ & -2.5 \\ \hline
		1. Minimum (links)  & $\phantom{l}-4.63\pm$0.02 & $-0.00333\pm0.00001$ & -1.5 \\ \hline
		Hauptminimum        & 0     & $0$                  & 0   \\ \hline
		1. Minimum (rechts) & $\phantom{ip0}4.00\pm$0.02 & $\phantom{ip}0.00288\pm0.00001$   & 1.5 \\ \hline
		2. Minimum (rechts) & $\phantom{ip0}7.88\pm$0.02 & $\phantom{ip}0.00567\pm0.00001$   & 2.5 \\ \hline
		3. Minimum (rechts) & $\phantom{ip}11.75\pm$0.02 & $\phantom{ip}0.00845\pm0.00002$   & 3.5 \\ \hline
	\end{tabular}
	\label{tab:stegMin}
\end{table}

\subsubsection{Lochblende}

\begin{figure}
	\centering
	\scalebox{0.7}
	{\includegraphics[width=0.7\textwidth]{evaluation/3-Lochblende.png}}
	{\includegraphics[width=0.5\textwidth]{evaluation/3-Lochblende-correlation.png}}
	\caption{Gemessene Intensitätsverteilung der Lochblende mit eingezeichnetem Hauptmaximum (links) sowie die bestimmten Werte für $\frac{\varepsilon}{\pi}$ aufgetragen gegen den Sinus des Beugungswinkels $\sin(\alpha)$ (rechts).}
	\label{fig:lochblende}
\end{figure}

Die Messwerte für die Lochblende sind in Abbildung \ref{fig:lochblende} dargestellt. Die daraus bestimmten Extrema sind in der Tabelle \ref{tab:lochblende} zu finden. Erneut ergibt sich mit dem Geradenfit in Abbildung \ref{fig:lochblende} eine Steigung von $m=340\pm30$ und es folgt für den Lochdurchmesser $D$: 
\begin{equation}
D=0.22\pm0.02 \,\text{mm} \quad .
\end{equation}
Die Korrelation ist in hier: $k=0.98373$

\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima und Minima bei Beugung an der Lochblende.}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		2. Maximum (links)  & $-8.38\pm$0.02 & $-0.00603\pm0.00001$ & -2.6793 \\ \hline
		1. Maximum (links)  & $-7.25\pm$0.02 & $-0.00522\pm0.00001$ & -1.6347 \\ \hline
		1. Maximum (rechts) & $\phantom{ip}7.00\pm$0.02 & $\phantom{ip}0.00504\pm0.00001$ & 1.6347 \\
		\hline\hline
		3. Minimum (links)  & $-9.25\pm$0.02 & $-0.00665\pm0.00001$   & -3.2383 \\ \hline
		2. Minimum (links)  & $-8.13\pm$0.02 & $-0.00585\pm0.00001$   & -2.2331 \\ \hline
		1. Minimum (links)  & $-5.25\pm$0.02 & $-0.00367\pm0.00001$   & -1.2197 \\ \hline
		1. Minimum (rechts) & $\phantom{ip}5.13\pm$0.02 & $\phantom{ip}0.00369\pm0.00001$   & 1.2197 \\ \hline
		2. Mininum (rechts) & $\phantom{ip}9.00\pm$0.02 & $\phantom{ip}0.00647\pm0.00001$   & 2.2331 \\ \hline
		\hline
	\end{tabular}
	\label{tab:lochblende}
\end{table}

\subsubsection{Doppellochblenden}

\begin{figure}[p!]
	\centering
	\scalebox{0.7}
	{\includegraphics[width=0.7\textwidth]{evaluation/4-Doppellochblende.png}}
	{\includegraphics[width=0.5\textwidth]{evaluation/4-Doppellochblende-correlation.png}}
	\caption{Gemessene Intensitätsverteilung der nahen Doppellochblende (Objekt 4) mit eingezeichnetem Hauptmaximum (links) sowie die bestimmten Werte für $\frac{\varepsilon}{\pi}$ aufgetragen gegen den Sinus des Beugungswinkels $\sin(\alpha)$ (rechts).}
	\label{fig:doppel4}
\end{figure}

\begin{figure}[p!]
	\centering
	\scalebox{0.7}
	{\includegraphics[width=0.7\textwidth]{evaluation/5-Doppellochblende.png}}
	{\includegraphics[width=0.5\textwidth]{evaluation/5-Doppellochblende-correlation.png}}
	\caption{Gemessene Intensitätsverteilung der mittleren Doppellochblende (Objekt 5) mit eingezeichnetem Hauptmaximum (links) sowie die bestimmten Werte für $\frac{\varepsilon}{\pi}$ aufgetragen gegen den Sinus des Beugungswinkels $\sin(\alpha)$ (rechts).}
	\label{fig:doppel5}
\end{figure}

\begin{figure}[p!]
	\centering
	\scalebox{0.7}
	{\includegraphics[width=0.7\textwidth]{evaluation/6-Doppellochblende.png}}
	{\includegraphics[width=0.5\textwidth]{evaluation/6-Doppellochblende-correlation.png}}
	\caption{Gemessene Intensitätsverteilung der fernen Doppellochblende (Objekt 6) mit eingezeichnetem Hauptmaximum (links) sowie die bestimmten Werte für $\frac{\varepsilon}{\pi}$ aufgetragen gegen den Sinus des Beugungswinkels $\sin(\alpha)$ (rechts).}
	\label{fig:doppel6}
\end{figure}

Es wurden drei Doppellochblenden mit verschieden Lochabständen verwendet, die im folgenden mit den Zahlen 4 (nah), 5 (mittel) und 6 (fern) bezeichnet werden. Die charakteristische Größe $D_i$ entspricht hier dem Lochabstand.
Aus den Abbildungen \ref{fig:doppel4}, \ref{fig:doppel5} und \ref{fig:doppel6} ergeben sich mit einem linearen Fit die Steigungen $m_4=-670\pm40$, $m_5=-1150\pm30$ und $m_6=-2730\pm70$ und es folgt: 
\begin{equation}
D_4=0.42\pm0.03 \,\text{mm} \quad \text{;} \quad D_5=0.73\pm0.02 \,\text{mm} \quad \text{und} \quad  D_6=1.73\pm0.04 \,\text{mm} \quad .
\end{equation}
Für die Korrelationen ergibt sich: $k_4=0.98152$; $k_5=0.99350$ und $k_6=0.99403$.

\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima bei Beugung durch die nahe Doppellochblende (Objekt 4).}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		4. Maximum (links)  & $-7.88\pm$0.02 & $-0.00566\pm0.00001$ & -4.5 \\ \hline
		3. Maximum (links)  & $-6.38\pm$0.02 & $-0.00458\pm0.00001$ & -3.5 \\ \hline
		2. Maximum (links)  & $-3.00\pm$0.02 & $-0.00216\pm0.00001$ & -2.5 \\ \hline
		1. Maximum (links)  & $-1.50\pm$0.02 & $-0.00108\pm0.00001$ & -1.5 \\ \hline
		Hauptmaximum        & 0     & $0$                  & 0   \\ \hline
		1. Maximum (rechts) & $\phantom{ip}1.50\pm$0.02 & $\phantom{ip}0.00108\pm0.00001$   & 1.5 \\ \hline
		2. Maximum (rechts) & $\phantom{ip}6.75\pm$0.02 & $\phantom{ip}0.00486\pm0.00001$   & 2.5 \\ \hline
		3. Maximum (rechts) & $\phantom{ip}8.50\pm$0.02 & $\phantom{ip}0.00611\pm0.00001$   & 3.5 \\ \hline
	\end{tabular}
	\label{tab:Doppel4Max}
\end{table}
\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Minima bei Beugung durch die mittlere Doppellochblende (Objekt 5).}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		4. Minimum (links)  & $-7.25\pm$0.02 & $-0.00523\pm0.00001$ & -4 \\ \hline
		3. Minimum (links)  & $-4.88\pm$0.02 & $-0.00351\pm0.00001$ & -3 \\ \hline
		2. Minimum (links)  & $-2.50\pm$0.02 & $-0.00180\pm0.00001$ & -2 \\ \hline
		1. Minimum (links)  & $-0.88\pm$0.02 & $-0.00063\pm0.00001$ & -1 \\ \hline
		Hauptminimum        & 0     & $0$                  & 0  \\ \hline
		1. Minimum (rechts) & $\phantom{ip}0.88\pm$0.02 & $\phantom{ip}0.00063\pm0.00001$   & 1 \\ \hline
		2. Minimum (rechts) & $\phantom{ip}5.62\pm$0.02 & $\phantom{ip}0.00405\pm0.00001$   & 2 \\ \hline
		3. Minimum (rechts) & $\phantom{ip}8.25\pm$0.02 & $\phantom{ip}0.00593\pm0.00001$   & 3 \\ \hline
	\end{tabular}
	\label{tab:Doppel4Min}
\end{table}

\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima bei Beugung durch die mittlere Doppellochblende (Objekt 5).}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		6. Maximum (links)  & $-8.00\pm$0.02 & $-0.00575\pm0.00001$ & -6.5 \\ \hline
		5. Maximum (links)  & $-6.88\pm$0.02 & $-0.00495\pm0.00001$ & -5.5 \\ \hline
		4. Maximum (links)  & $-5.88\pm$0.02 & $-0.00423\pm0.00001$ & -4.5 \\ \hline
		3. Maximum (links)  & $-3.38\pm$0.02 & $-0.00243\pm0.00001$ & -3.5 \\ \hline
		2. Maximum (links)  & $-2.25\pm$0.02 & $-0.00162\pm0.00001$ & -2.5 \\ \hline
		1. Maximum (links)  & $-1.13\pm$0.02 & $-0.00081\pm0.00001$ & -1.5 \\ \hline
		Hauptmaximum        & 0     & $0$                  & 0   \\ \hline
		1. Maximum (rechts) & $\phantom{ip}1.13\pm$0.02 & $\phantom{ip}0.00081\pm0.00001$   & 1.5 \\ \hline
		2. Maximum (rechts) & $\phantom{ip}2.25\pm$0.02 & $\phantom{ip}0.00162\pm0.00001$   & 2.5 \\ \hline
		3. Maximum (rechts) & $\phantom{ip}3.38\pm$0.02 & $\phantom{ip}0.00243\pm0.00001$   & 3.5 \\ \hline
		4. Maximum (rechts) & $\phantom{ip}5.50\pm$0.02 & $\phantom{ip}0.00396\pm0.00001$   & 4.5 \\ \hline
		5. Maximum (rechts) & $\phantom{ip}6.75\pm$0.02 & $\phantom{ip}0.00486\pm0.00001$   & 5.5 \\ \hline
	\end{tabular}
	\label{tab:Doppel5Max}
\end{table}
\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Minima bei Beugung durch die mittlere Doppellochblende (Objekt 5).}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		7. Minimum (links)  & $-8.63\pm$0.02 & $-0.00620\pm0.00001$ & -7 \\ \hline
		6. Minimum (links)  & $-7.63\pm$0.02 & $-0.00549\pm0.00001$ & -6 \\ \hline
		5. Minimum (links)  & $-6.13\pm$0.02 & $-0.00441\pm0.00001$ & -5 \\ \hline
		4. Minimum (links)  & $-5.13\pm$0.02 & $-0.00369\pm0.00001$ & -4 \\ \hline
		3. Minimum (links)  & $-3.00\pm$0.02 & $-0.00216\pm0.00001$ & -3 \\ \hline
		2. Minimum (links)  & $-1.75\pm$0.02 & $-0.00126\pm0.00001$ & -2 \\ \hline
		1. Minimum (links)  & $-0.50\pm$0.02 & $-0.00036\pm0.00001$ & -1 \\ \hline
		Hauptminimum        & 0     & $0$                  & 0  \\ \hline
		1. Minimum (rechts) & $\phantom{ip}0.63\pm$0.02 & $\phantom{ip}0.00045\pm0.00001$   & 1 \\ \hline
		2. Minimum (rechts) & $\phantom{ip}1.88\pm$0.02 & $\phantom{ip}0.00135\pm0.00001$   & 2 \\ \hline
		3. Minimum (rechts) & $\phantom{ip}3.00\pm$0.02 & $\phantom{ip}0.00216\pm0.00001$   & 3 \\ \hline
		4. Minimum (rechts) & $\phantom{ip}5.25\pm$0.02 & $\phantom{ip}0.00378\pm0.00001$   & 4 \\ \hline
		5. Minimum (rechts) & $\phantom{ip}6.50\pm$0.02 & $\phantom{ip}0.00468\pm0.00001$   & 5 \\ \hline
	\end{tabular}
	\label{tab:Doppel5Min}
\end{table}

\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima bei Beugung durch die ferne Doppellochblende (Objekt 6).}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		6. Maximum (links)  & $-3.25\pm$0.02 & $-0.00234\pm0.00001$ & -6.5 \\ \hline
		5. Maximum (links)  & $-2.75\pm$0.02 & $-0.00198\pm0.00001$ & -5.5 \\ \hline
		4. Maximum (links)  & $-2.13\pm$0.02 & $-0.00153\pm0.00001$ & -4.5 \\ \hline
		3. Maximum (links)  & $-1.63\pm$0.02 & $-0.00117\pm0.00001$ & -3.5 \\ \hline
		2. Maximum (links)  & $-1.13\pm$0.02 & $-0.00081\pm0.00001$ & -2.5 \\ \hline
		1. Maximum (links)  & $-0.50\pm$0.02 & $-0.00036\pm0.00001$ & -1.5 \\ \hline
		Hauptmaximum        & 0     & $0$                  & 0   \\ \hline
		1. Maximum (rechts) & $\phantom{ip}0.63\pm$0.02 & $\phantom{ip}0.00045\pm0.00001$   & 1.5 \\ \hline
		2. Maximum (rechts) & $\phantom{ip}1.13\pm$0.02 & $\phantom{ip}0.00081\pm0.00001$   & 2.5 \\ \hline
		3. Maximum (rechts) & $\phantom{ip}1.63\pm$0.02 & $\phantom{ip}0.00117\pm0.00001$   & 3.5 \\ \hline
		4. Maximum (rechts) & $\phantom{ip}2.75\pm$0.02 & $\phantom{ip}0.00198\pm0.00001$   & 4.5 \\ \hline
	\end{tabular}
	\label{tab:Doppel6Max}
\end{table}
\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Minima bei Beugung durch die ferne Doppellochblende (Objekt 6).}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		6. Minimum (links)  & $-3.00\pm$0.02 & $-0.00216\pm0.00001$ & -6 \\ \hline
		5. Minimum (links)  & $-2.50\pm$0.02 & $-0.00179\pm0.00001$ & -5 \\ \hline
		4. Minimum (links)  & $-1.88\pm$0.02 & $-0.00135\pm0.00001$ & -4 \\ \hline
		3. Minimum (links)  & $-1.38\pm$0.02 & $-0.00099\pm0.00001$ & -3 \\ \hline
		2. Minimum (links)  & $-0.88\pm$0.02 & $-0.00063\pm0.00001$ & -2 \\ \hline
		1. Minimum (links)  & $-0.25\pm$0.02 & $-0.00018\pm0.00001$ & -1 \\ \hline
		Hauptminimum        & 0     & $0$                  & 0  \\ \hline
		1. Minimum (rechts) & $\phantom{ip}0.25\pm$0.02 & $\phantom{ip}0.00018\pm0.00001$   & 1 \\ \hline
		2. Minimum (rechts) & $\phantom{ip}0.88\pm$0.02 & $\phantom{ip}0.00063\pm0.00001$   & 2 \\ \hline
		3. Minimum (rechts) & $\phantom{ip}1.38\pm$0.02 & $\phantom{ip}0.00099\pm0.00001$   & 3 \\ \hline
		4. Minimum (rechts) & $\phantom{ip}2.50\pm$0.02 & $\phantom{ip}0.00179\pm0.00001$   & 4 \\ \hline
	\end{tabular}
	\label{tab:Doppel6Min}
\end{table}

\subsubsection{Gitter}

\begin{figure}
	\centering
	\scalebox{0.7}
	{\includegraphics[width=0.7\textwidth]{evaluation/7-Gitter.png}}
	{\includegraphics[width=0.5\textwidth]{evaluation/7-Gitter-correlation.png}}
	\caption{Gemessene Intensitätsverteilung des Gitters mit eingezeichnetem Hauptmaximum (links) sowie die bestimmten Werte für $\frac{\varepsilon}{\pi}$ aufgetragen gegen den Sinus des Beugungswinkels $\sin(\alpha)$ (rechts).}
	\label{fig:gitter}
\end{figure}

In Abbildung \ref{fig:gitter} sind die Messwerte zur Position der Messdiode aufgetragen. Die bestimmten Extrema sind den Tabellen \ref{tab:gitterMin}, \ref{tab_gitterMax} aufgeführt. Für das Gitter ist $B$ die Spaltbreite und $D = B + S$ die charaktersistische Größe mit der Stegbreite $S$. Mit einem linearen Fit nach Abbildung \ref{fig:gitter} ergibt sich $m=-900\pm30$ und folglich: 

\begin{equation}
D=0.57\pm0.02 \,\text{mm} \quad .
\end{equation}

Die Werte sind mit einer Korrelation von $k=0.99402$ korreliert.

\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Maxima bei Beugung am Gitter.}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		4. Maximum (links)  & $-7.38\pm$0.02 & $-0.00530\pm0.00001$ & -4.5 \\ \hline
		3. Maximum (links)  & $-6.25\pm$0.02 & $-0.00350\pm0.00001$ & -3.5 \\ \hline
		2. Maximum (links)  & $-4.25\pm$0.02 & $-0.00306\pm0.00001$ & -2.5 \\ \hline
		1. Maximum (links)  & $-1.63\pm$0.02 & $-0.00117\pm0.00001$ & -1.5 \\ \hline
		Hauptmaximum        & 0     & $0$                  & 0   \\ \hline
		1. Maximum (rechts) & $\phantom{ip}1.50\pm$0.02 & $\phantom{ip}0.00108\pm0.00001$   & 1.5 \\ \hline
		2. Maximum (rechts) & $\phantom{ip}3.25\pm$0.02 & $\phantom{ip}0.00234\pm0.00001$   & 2.5 \\ \hline
		3. Maximum (rechts) & $\phantom{ip}4.63\pm$0.02 & $\phantom{ip}0.00463\pm0.00001$   & 3.5 \\ \hline
	\end{tabular}
	\label{tab:GitterMax}
\end{table}
\begin{table}
	\caption{Position, Ablenkung und Koeffizient $\varepsilon/\pi$ der Minima bei Beugung am Gitter.}
	\centering
	\begin{tabular}{|c|c|c|c|}
		\hline
		&   relative Position [mm] & $\sin(\alpha)$           &   $\varepsilon/\pi$ \\
		\hline\hline
		5. Minimum (links)  & $-8.38\pm$0.02 & $-0.00603\pm0.00001$ & -5 \\ \hline
		4. Minimum (links)  & $-6.88\pm$0.02 & $-0.00495\pm0.00001$ & -4 \\ \hline
		3. Minimum (links)  & $-5.75\pm$0.02 & $-0.00414\pm0.00001$ & -3 \\ \hline
		2. Minimum (links)  & $-3.75\pm$0.02 & $-0.00270\pm0.00001$ & -2 \\ \hline
		1. Minimum (links)  & $-0.88\pm$0.02 & $-0.00063\pm0.00001$ & -1 \\ \hline
		Hauptminimum        & 0     & $0$                  & 0  \\ \hline
		1. Minimum (rechts) & $\phantom{ip}0.88\pm$0.02 & $\phantom{ip}0.00629\pm0.00001$   & 1 \\ \hline
		2. Minimum (rechts) & $\phantom{ip}2.88\pm$0.02 & $\phantom{ip}0.00207\pm0.00001$   & 2 \\ \hline
		3. Minimum (rechts) & $\phantom{ip}3.75\pm$0.02 & $\phantom{ip}0.00270\pm0.00001$   & 3 \\ \hline
		4. Minimum (rechts) & $\phantom{ip}5.25\pm$0.02 & $\phantom{ip}0.00378\pm0.00001$   & 4 \\ \hline
	\end{tabular}
	\label{tab:GitterMin}
\end{table}

\subsection{Die Wellenlänge des Lasers}
Da die Korrelation des Einfachspalts die höchste ist, kann die Spaltbreite $D$ als gegeben angenommen werden. Die Steigung der Geraden $m$ ist außerdem aus dem vorherigen Auswertungsschritt bekannt. Es gilt also mit den Werten des Einfachspalts:
\begin{equation}
    \lambda=\frac{D}{m}=632\pm6\,\text{nm} \quad .
\end{equation}