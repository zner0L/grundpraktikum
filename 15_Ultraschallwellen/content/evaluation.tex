\subsection{Normierung der Amplitude}

Da die Messwerte der Amplitude für verschiedene Verstärkungen des Receivers vorliegen, müssen diese mit der Verstärkung normiert werden. Aus der Gleichung für die Verstärkung nach \cite[Kap. 15.5]{knetter17} ergibt sich dann die normierte Spannungsamplitude:

\begin{equation}
	v_{dB} = 10 \log(\frac{V_a^2}{V_i^2}) \Leftrightarrow V_i=10^{- \frac{v_{dB}}{10}} \cdot V_a
	\label{eq:norm_amp}
\end{equation}

Für die Fehler gilt dann entsprechend, unter der Annahme, dass die Verstärkung fehlerfrei ist:

\begin{equation*}
	\sigma_{V_i}=\sigma_{V_a} \cdot \left(10^{-\frac{1}{10}}\right)^{v_{dB}}
\end{equation*}

\begin{figure}[h!]
	\centering
	\caption{Nach Gleichung \ref{eq:norm_amp} normierte Amplitude der Ultraschallwellen (gemessen in Volt der Spannung am Sensor) gegenüber dem Winkel der eingebrachten Platte aus Aluminium sowie Polyacryl.}
	\includegraphics[height=0.4\textheight]{evaluation/aluminium.png}
	\includegraphics[height=0.4\textheight]{evaluation/polyacryl.png}
	\label{fig:alu_polyacryl}
\end{figure}

\subsection{Bestimmung der Schallgeschwindigkeit}

In der Abbildung \ref{fig:alu_polyacryl} sind nun für die Polyacryl- und die Aluminiumplatte jeweils die normierten Amplituden und Fehler der transversalen und der longitudinalen Wellen in Abhängigkeit von der Position der Platte aufgetragen. Durch die Messwerte wurde mithilfe von \texttt{SciPy} eine Interpolation 3. Grades gelegt, mit deren Hilfe in den folgenden Abschnitten Maxima und Minima bestimmt werden konnten.

\subsubsection{Aus dem Winkel der Totalreflektion}

Aus der Abbildungen \ref{fig:alu_polyacryl} werden jeweils die beiden Winkel $\alpha_{\text{tot}}$ abgelesen, bei denen die Amplitude verschwindet. Die Werte sind in Tabelle \ref{doener} aufgeführt. Durch Umstellen von Gleichung \ref{eq:tot_ref} lassen sich hieraus die Schallgeschwindigkeiten für die transversale und longitudinale Welle berechnen
\begin{equation}
\nu_{L,T}
=	\frac{\nu_W}{\sin (\alpha_{\text{tot}})} \; , 
\label{eq:ev_tot_ref}
\end{equation}
wobei $\nu_W$ die Schallgeschwindigkeit in Wasser bezeichnet ($1484 \frac{\text{m}}{\text{s}}$,) \cite[Kap. 15.7]{knetter17}. Die Ergebnisse stehen in Tabelle \ref{tab:ergebnisse}. Der Fehler berechnet sich nach der Gauß'schen Fehlerfortpflanzung zu:
\begin{equation*}
		\sigma_{\nu_{L,T}}
	=	\sigma_{\alpha_{\text{tot}}} \cdot \nu_{W} \cdot
		\left( 	\frac	{\cos(\alpha_{\text{tot}})}
						{\sin^2(\alpha_{\text{tot}})} 	\right)
\end{equation*}

\begin{table}[!htb]
	\centering
	\caption{Winkel der Totalreflexion $\alpha_{\text{tot}}$ und der maximalen Transmission $\alpha_{\text{max}}$ für Aluminium und Polyacryl.}
	\begin{tabular}{|l|c|c|c|}
		\hline
		\multirow{2}{*}{Material} 	& Longitudinalwelle 	& \multicolumn{2}{l|}{Transversalwelle} 	\\
		\hhline{~---}
		& $\alpha_{\text{tot}}$ [\textdegree] & $\alpha_{\text{tot}}$ [\textdegree] & $\alpha_{\text{max}}$ [\textdegree] \\
		\hline\hline
		Aluminium 	& $20 \pm 3$ & $32 \pm 3$ & $22 \pm 3$\\
		\hline
		Polyacryl	& $52 \pm 3$ & $90 \pm 3$ & $41 \pm 3$ \\
		\hline
	\end{tabular}
	\label{doener}
\end{table}

\begin{table}[!htb]
	\centering
	\caption{Ergebnisse für die Schallgeschwindigkeit $\nu_{T}$ in $\frac{\text{m}}{\text{s}}$ für Aluminium und Polyacryl und prozentuale Abweichungen von den Literaturwerten aus Tabelle \ref{tab:litwerte}.}
	\begin{tabular}{|l|l|c|c|c|c|}
		\hline
		\multicolumn{2}{|c|}{\multirow{2}{*}{$\nu_{T}$ [$10^{3} \frac{\text{m}}{\text{s}}$] aus}} & \multicolumn{2}{|c|}{Aluminium} 	& \multicolumn{2}{|c|}{Polyacryl}	\\
		\hhline{~~----}
		\multicolumn{2}{|c|}{} & Messergebnis & Abweichung [\%] & Messergebnis & Abweichung [\%] \\
		\hline\hline
		longitudinal 	& $\alpha_{\text{tot}}$ & $4.3 \pm 0.6$ & $32 \pm 8$ & $1.9 \pm 0.7$ & $27 \pm 3$ \\
		\hline
		\multirow{2}{*}{transversal} & $\alpha_{\text{tot}}$ &  $2.8 \pm 0.2$ & $11 \pm 6$ & $1.484 \pm 0.001$ & $24.2 \pm 0.1$ \\
		\hhline{~-----}
		& $\alpha_{\text{max}}$ & $2.8 \pm 0.4$ & $10 \pm 10$ & $1.589 \pm 0.008$ & $33 \pm 7$ \\
		\hline
	\end{tabular}
	\label{tab:ergebnisse}
\end{table}

\subsubsection{Aus dem Winkel der maximalen Transmission}
In der Abbildung \ref{fig:alu_polyacryl} wird das Maximum der Amplitude der transversalen Welle abgelesen. Die Ergebnisse stehen in Tabelle \ref{tab:ergebnisse}. Aus Gleichung \ref{eq:alpha_max} ergibt sich mit der Schallgeschwindigkeit in Wasser $\nu_W$ für die transversale Schallgeschwindigkeit
\begin{equation}
\nu_T
= 	\frac{\nu_W}{\sqrt{2} \cdot \sin (\alpha_{\text{max}})} \; ,
\label{eq:ev_alpha_max}
\end{equation}
Wobei sich der Fehler bestimmen lässt durch
\begin{equation*}
	\sigma_{\nu_{T}}
	=	\sigma_{\alpha_{\text{max}}} \cdot \nu_{W} \cdot
	\left( 	\frac	{\cos(\alpha_{\text{max}})}
	{\sqrt{2} \, \sin^2(\alpha_{\text{max}})} 	\right) \; .
\end{equation*} 

\subsection{Literaturwerte}

In Tabelle \ref{tab:litwerte} sind die Literaturwerte der Materialkonstanten aufgeführt. Nach Gleichungen \ref{eq:vlong} und \ref{eq:vtrans} wurden hieraus die Literaturwerte für die transversale und longitudinale Ausbreitungsgeschwindigkeiten der Ultraschallwellen in Polyacryl und Aluminium berechnet. Die Ergebnisse werden in Tabelle \ref{tab:ergebnisse} mit den Messwerten verglichen. 

\begin{table}[!htb]
	\centering
	\caption{Materialkonstanten der verwendeten Platten: Dichte $\rho$, Elastizitätsmodul $G$, Torsionsmodul $E$ und Poisson-Zahl $\mu$ \cite[Kap. 15.7]{knetter17} sowie aus den Konstanten berechnete Schallgeschwindigkeit für beide Wellentypen.}
	\begin{tabular}{|l|l|c|c|c|}
		\hline
		\multicolumn{2}{|l|}{Größe}				 	& Aluminium	& Polyacryl\\
		\hline\hline
		\multicolumn{2}{|l|}{$\rho$ [g/cm$^3$]} 	& 2.7 	& 1.19	\\
		\hline
		\multicolumn{2}{|l|}{$G$ [MPa]}	 			& 2700 	& 1700	\\
		\hline
		\multicolumn{2}{|l|}{$E$ [MPa]}  			& 72000	& 3300	\\	
		\hline
		\multicolumn{2}{|l|}{$\mu$}					& 0.34	& 0.4	\\
		\hline\hline
		\multirow{2}{*}{$\nu_T$ [$10^{3} \frac{\text{m}}{\text{s}}$]} & long. & 6.407 & 2.597 \\
		\hhline{~---}
		& trans. & 3.162 & 1.195 \\
		\hline
	\end{tabular}
	\label{tab:litwerte}
\end{table}