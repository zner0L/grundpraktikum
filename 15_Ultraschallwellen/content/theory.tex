\subsection{Wellengleichung}

Eine physikalische Welle ist eine sich räumlich ausbreitende Erregung oder Störung einer raumzeitabhängigen Größe, durch die Energie transportiert wird. Mathematisch lässt sich die Ausbreitung einer Welle durch die Wellengleichung beschreiben \cite[Kap. 1.3]{fowles89}:
\begin{equation}
	\frac{\partial ^2\xi_{(x,t)}}{\partial t^2} = \frac{1}{\nu^2} \frac{\partial ^2\xi_{(x,t)}}{\partial x^2}
\end{equation}
Dabei ist $\xi$ die vom Ort $x$ und der Zeit $t$ abhängige Größe, mit $\nu$ wird die Ausbreitungsgeschwindigkeit der Welle bezeichnet. Es lässt sich unterscheiden zwischen longitudinalen einerseits Wellen, deren Störung in Ausbreitungsrichtung erfolgt und Transversalwellen andererseits, bei denen $\xi$ senkrecht auf $\nu$ steht. Im Folgenden sollen diese beiden Wellentypen in Festkörpern betrachtet werden. 


\subsection{Longitudinalwellen}
\label{sec:long}

Auf einen fixierten Festkörper der Querschnittsfläche $A$ wirkt die Zugkraft $F$ in \mbox{$z$-Richtung} senkrecht zur Oberfläche. Im Modell des deformierbaren festen Körpers wird für kleine Auslenkungen eine Proportionalität zwischen Kraft und relativer Dehnung $\epsilon$ angenommen \cite[Kap. 6.2.1]{demtroeder15}:
\begin{equation*}
	F = E \cdot A \cdot \epsilon
\end{equation*}
Die Proportionalitätskonstante $E$ ist eine Materialeigenschaft, das Elastizitätsmodul. Im Falle einer senkrecht auf den elastischen Körper auftreffenden und ihn deformierenden Welle lässt die relative Dehnung sich mit der Ableitung der Wellenfunktion $\xi$ in \mbox{$z$-Richtung} identifizieren. Für eine infinitesimale Kraftänderung gilt dann
\begin{equation}
\label{doener}
		\text{d}F 
	=	E \cdot A \; \text{d}\epsilon 
	= 	E \cdot A \; \text{d} \frac{\partial \xi}{\partial z} 
	=	E \cdot A \cdot \frac{\partial^2 \xi}{\partial z^2} \; \text{d} z \; .
\end{equation}
Andererseits ist nach Newton die Kraft auf das infinitesimale Massenelement $\text{d} m$ mit der Beschleunigung $a$ gegeben durch
\begin{equation}
\label{meile}
		\text{d}F 
	= \text{d} m \cdot a
	= \text{d} m \cdot \frac{\partial^2 \xi}{\partial z^2}
	= \rho \cdot \frac{\partial^2 \xi}{\partial z^2} \; \text{d} V
	= \rho \cdot A \cdot \frac{\partial^2 \xi}{\partial z^2} \; \text{d} z \; .
\end{equation}
Gleichsetzen der Gleichungen \ref{doener} und \ref{meile} liefert nach Kürzen eine Wellengleichung
\begin{equation*}
	\frac{\partial ^2\xi_{(x,t)}}{\partial t^2} = \frac{E}{\rho} \frac{\partial ^2\xi_{(x,t)}}{\partial x^2} \; ,
\end{equation*}
aus der sich die longitudinale Ausbreitungsgeschwindigkeit der Welle im Festkörper ablesen lässt: 
\begin{equation}
	\nu_L = \sqrt{\frac{E}{\rho}}
\end{equation}
Soll die Querkontraktion des Materials mit berücksichtigt werden, so muss die Gleichung für die Ausbreitungsgeschwindigkeit mit der Poissonzahl $\mu$ korrigiert werden zu \cite[Kap. 4.1.1]{gerthsen10}: 
\begin{equation}
	\nu_L = \sqrt{\frac{E\,(1 - \mu)}{\rho\, (1 + \mu)\,(1 - 2\mu)}}
	\label{eq:vlong}
\end{equation}

\subsection{Transversalwelle}
\label{sec:transv}

Trifft die Welle nicht senkrecht auf den Festkörper auf, so entstehen durch Scherkräfte Transversalwellen innerhalb des Materials. Analog zu Abschnitt \ref{sec:long} lässt sich eine Wellengleichung herleiten, wobei lediglich das Elastizitätsmodul $G$ durch das Torsionsmodul (Schermodul) $G$ zu ersetzen ist \cite[Kap. 4.1.2]{gerthsen10}. Für die transversale Ausbreitungsgeschwindigkeit folgt somit:
\begin{equation}
	\nu_T = \sqrt{\frac{G}{\rho}}
	\label{eq:vtrans}
\end{equation}

\subsection{Brechung an einer Grenzfläche}

Bei der Brechung einer Welle an der Grenzfläche zweiter Materialien mit den Brechungsindizes $n_i$ gilt für den Ein- bzw. Ausfallswinkel $\alpha$ bzw. $\beta$ das Snelliussche Brechungsgesetz \cite[Kap. 2.3.1]{zinth11}. Da der Brechungsindex $n$ proportional zur Ausbreitungsgeschwindigkeit $\nu$ ist, gilt für die in Abschnitt \ref{sec:transv} besprochene Transversalwelle an der Grenzfläche zwischen einer Flüssigkeit, in der die Ausbretungsgeschwindigkeit $\nu_F$ beträgt und eines Festkörpers \cite[Kap. 11.11.4]{demtroeder15}: 
\begin{equation*}
		n_1 \sin \alpha 
	= 	n_2 \sin \beta \quad \Rightarrow \quad
	\frac{\nu_T}{\nu_F} 
	= 	\frac{\sin \beta}{\sin \alpha}
	\label{eq:snellius}
\end{equation*}
Die Amplitude der transmittierten Transversalwelle wird maximal für einen Durchgangswinkel von $\beta = \pi / 4$, woraus für den Auftreffwinkel $\alpha$ folgt:
\begin{equation}
		\sin (\alpha_{\text{max}}) 
	= 	\frac{\nu_F}{\nu_T} \cdot \sin \frac{\pi}{4} 
	= 	\frac{\nu_F}{\sqrt{2} \cdot \nu_T}
	\label{eq:alpha_max}
\end{equation}
Totalreflektion tritt sowohl bei der Transversal- als auch der Longitundinalwelle für den Fall $\beta = \pi / 2$ ein, woraus sich für den entsprechenden Auftreffwinkel ergibt: 
\begin{equation}
		\sin (\alpha_{\text{tot}} )
	= 	\frac{\nu_F}{\nu_{L,T}} \cdot \sin \frac{\pi}{4} 
	= 	\frac{\nu_F}{\nu_{L,T}} \;.
	\label{eq:tot_ref}
\end{equation}
