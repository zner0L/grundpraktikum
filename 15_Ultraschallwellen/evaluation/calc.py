# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

data_aluminium = np.genfromtxt('data/aluminium.csv', delimiter=' ', usecols=range(8))
data_polyacryl = np.genfromtxt('data/polyacryl.csv', delimiter=' ', usecols=range(8))

voltage_normalized = ma.uncertainty.Sheet('va/10^(db/10)', name='V_i')
voltage_normalized.set_value('va', error=0.02, tex='V_a')
voltage_normalized.set_value('db', tex='v')
data_aluminium[:, (2,3)] = voltage_normalized.batch(data_aluminium[:, (2,3,6)], 'va|%va|db')
data_aluminium[:, (4,5)] = voltage_normalized.batch(data_aluminium[:, (4,5,6)], 'va|%va|db')
data_polyacryl[:, (2,3)] = voltage_normalized.batch(data_polyacryl[:, (2,3,6)], 'va|%va|db')
data_polyacryl[:, (4,5)] = voltage_normalized.batch(data_polyacryl[:, (4,5,6)], 'va|%va|db')

voltage_normalized.print_result()

cmap = plt.cm.get_cmap('Accent', 4)

fig1 = plt.figure(1, figsize=[10, 7])
plt.title(r'Aluminium')
interpol_kind = 'cubic'

minima_materia = np.zeros((4, 4));
l = np.linspace(0, 90)
spline_amelie = interp1d(data_aluminium[:, 0], data_aluminium[:, 2], kind=interpol_kind)
spline_amelie_max = interp1d(data_aluminium[:, 0], -data_aluminium[:, 2], kind=interpol_kind)
minima_materia[0, 0] = 20
minima_materia[1, 0] = minimize(spline_amelie_max, 20, bounds=[(0, 90)]).x[0]
plt.plot(l, spline_amelie(l), color=cmap(0))
spline_bertram = interp1d(data_aluminium[:, 0], data_aluminium[:, 4], kind=interpol_kind)
spline_bertram_max = interp1d(data_aluminium[:, 0], -data_aluminium[:, 4], kind=interpol_kind)
minima_materia[2, 0] = minimize(spline_bertram, 30, bounds=[(0, 90)]).x[0]
minima_materia[3, 0] = minimize(spline_bertram_max, 20, bounds=[(0, 90)]).x[0]
plt.plot(l, spline_bertram(l), color=cmap(1))

plt.errorbar(data_aluminium[:, 0], data_aluminium[:, 2], xerr=data_aluminium[:, 1], yerr=data_aluminium[:, 3], label='longitudinal', fmt='.', color=cmap(0))
plt.errorbar(data_aluminium[:, 0], data_aluminium[:, 4], xerr=data_aluminium[:, 1], yerr=data_aluminium[:, 5], label='transversal', fmt='.', color=cmap(1))
plt.legend(ncol=1, fancybox=True, shadow=True)
plt.axis([0, 90, 0, 0.15])
plt.xlabel(r'Winkel $\alpha$ [\textdegree]')
plt.ylabel(r'normierte Amplitude $A$ [V]')
plt.close()

fig2 = plt.figure(2, figsize=[10, 7])
plt.title(r'Polyacryl')

spline_amelie = interp1d(data_polyacryl[:, 0], data_polyacryl[:, 2], kind=interpol_kind)
spline_amelie_max = interp1d(data_polyacryl[:, 0], -data_polyacryl[:, 2], kind=interpol_kind)
minima_materia[0, 1] = minimize(spline_amelie, 50, bounds=[(0, 60)]).x[0]
minima_materia[1, 1] = minimize(spline_amelie_max, 20, bounds=[(0, 90)]).x[0]
plt.plot(l, spline_amelie(l), color=cmap(2))
spline_bertram = interp1d(data_polyacryl[:, 0], data_polyacryl[:, 4], kind=interpol_kind)
spline_bertram_max = interp1d(data_polyacryl[:, 0], -data_polyacryl[:, 4], kind=interpol_kind)
minima_materia[2, 1] = 90
minima_materia[3, 1] = minimize(spline_bertram_max, 40, bounds=[(0, 90)]).x[0]
plt.plot(l, spline_bertram(l), color=cmap(3))

plt.errorbar(data_polyacryl[:, 0], data_polyacryl[:, 2], xerr=data_polyacryl[:, 1], yerr=data_polyacryl[:, 3], label='longitudinal', fmt='.', color=cmap(2))
plt.errorbar(data_polyacryl[:, 0], data_polyacryl[:, 4], xerr=data_polyacryl[:, 1], yerr=data_polyacryl[:, 5], label='transversal', fmt='.', color=cmap(3))
plt.legend(ncol=1, fancybox=True, shadow=True, loc='lower left')
plt.axis([0, 90, 0, 0.15])
plt.xlabel(r'Winkel $\alpha$ [\textdegree]')
plt.ylabel(r'normierte Amplitude $A$ [V]')
plt.close()

print(minima_materia)

minima_materia = minima_materia*2*math.pi/360

schall_min = ma.uncertainty.Sheet('v/(sin(a))', 'v_T')
schall_min.set_value('a', tex='\alpha_0', error=2.5*2*math.pi/360)
schall_min.set_value('v', 1484, tex='v_W')
schall_max = ma.uncertainty.Sheet('v/(2^(1/2)*sin(a))', 'v_T')
schall_max.set_value('a', tex='\alpha_{max}')
schall_max.set_value('v', 1484, tex='v_W')

minima_materia[[[0], [2]], [0, 2]] = schall_min.batch(minima_materia[(0,2), 0], 'a')
minima_materia[[[0], [2]], [1, 3]] = schall_min.batch(minima_materia[(0,2), 1], 'a')
schall_max.set_value('a', minima_materia[3, 0], 2.5*2*math.pi/360)
minima_materia[[3], [0, 2]] = schall_max.get_result()
schall_max.set_value('a', minima_materia[3, 1], 2.5*2*math.pi/360)
minima_materia[[3], [1, 3]] = schall_max.get_result()

print(minima_materia)

print(ma.data.literature_value(6407, minima_materia[0, 0], minima_materia[0, 2]))
print(ma.data.literature_value(2597, minima_materia[0, 1], minima_materia[0, 3]))
print(ma.data.literature_value(3162, minima_materia[2, 0], minima_materia[2, 2]))
print(ma.data.literature_value(3162, minima_materia[3, 0], minima_materia[3, 2]))
print(ma.data.literature_value(1195, minima_materia[2, 1], minima_materia[2, 3]))
print(ma.data.literature_value(1195, minima_materia[3, 1], minima_materia[3, 3]))

fig1.savefig('aluminium.png')
fig2.savefig('polyacryl.png')