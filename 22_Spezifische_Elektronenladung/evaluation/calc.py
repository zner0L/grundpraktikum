# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math
import os

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

figsize = [10, 8]

data = {}
results = {}
figs = {}
eme_theo = 1.7588e+11

for file in os.listdir('data'):
    series = file.replace('.csv', '')
    name = series.split('_')
    data[series] = np.genfromtxt('data/' + file, usecols=range(6))
    results[series] = np.zeros([data[series].shape[0], 8])

    # Radius
    radius = ma.uncertainty.Sheet('(pr-pl)/2', 'r')
    radius.set_value('pl', 29.33, 0.025)
    results[series][:, (0, 1)] = radius.batch(data[series][:, (4, 5)], 'pr|pr%')*1e-3

    # B-Feld
    helmholtz_field = ma.uncertainty.Sheet('(4/5)^(3/2)*mu0*n*i/r', 'B')
    helmholtz_field.set_value('mu0', 4e-7 * math.pi, tex='\mu_0')
    helmholtz_field.set_value('r', 12.2e-2)
    helmholtz_field.set_value('n', 200)
    results[series][:, (2, 3)] = helmholtz_field.batch(data[series][:, (2, 3)]*1e-2, 'i|i%')

    # Elektronenladung
    spec_charge = ma.uncertainty.Sheet('2*u/(r^2*b^2)', '\frac{e}{m_e}')
    results[series][:, (4, 5)] = spec_charge.batch(np.column_stack((results[series][:, (0, 1, 2, 3)], data[series][:, (0, 1)])), 'r|r%|b|b%|u|u%')

    # experimentelles B-Feld
    b_field_exp = ma.uncertainty.Sheet('(2*u/eme)^(1/2)*1/r', 'B')
    b_field_exp.set_value('eme', eme_theo)
    results[series][:, (6, 7)] = b_field_exp.batch(np.column_stack((results[series][:, (0, 1)], data[series][:, (0, 1)])), 'r|r%|u|u%')

    # Plot Ladung zu Radius
    figs[series + '.png'] = plt.figure(figsize=figsize)
    plt.title(name[0] + ' ' + name[1])
    plt.errorbar(results[series][:, 0]*1e+2, results[series][:, 4], xerr=results[series][:, 1]*1e+2, yerr=results[series][:, 5], fmt='.', label='berechnete Ladung')
    plt.xlabel('Radius [cm]')
    plt.ylabel('spez. Eletronenladung [$\\frac{\\text{C}}{\\text{kg}}$]')
    plt.axhline(y=eme_theo, color='g', linestyle='-', label='Literaturwert')
    box = plt.axes().get_position()
    plt.axes().set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
    plt.legend(bbox_to_anchor=(0., -0.32, 1., .102), loc=3,
               ncol=2, mode="expand", borderaxespad=0.)
    plt.close()

    # Datentabellen
    tbl = ma.latex.Table()
    tbl.add_column(results[series][:, (0, 1)]*1e+2, 'num($0,$1)', 'Radius [cm]')
    tbl.add_column(data[series][:, (0, 1)], 'num($0,$1)', 'Spannung $U_B$ [V]')
    tbl.add_column(results[series][:, (2, 3)]*1e+3, 'num($0,$1)', 'mag. Feldstärke $B$ [mT]')
    tbl.add_column(results[series][:, (4, 5)]*1e-11, 'num($0,$1)', '$\\frac{e}{m_e}$ [$\\frac{\\text{C}}{\\text{kg}} \cdot 10^{11}$]')
    tbl.set_caption('Spezifische Elektronenladung aus den Radien der Elektronenbahnen '
                    + ('bei konstaner Stromstärke ' if name[1] == "mA" else 'bei konstanter Spannung ') + '(' + name[0] + ' ' + name[1] + ').')
    tbl.set_label('tab:' + series)
    tbl.set_mode('hline')
    tbl.set_placement('[h]')
    tbl.export(series + '.tex')

    # Datenreihe rauspicken
    if series == '55_mA':
        deviation = []
        for i, x in enumerate(results[series][:, 2]):
            deviation.append(ma.data.literature_value(results[series][i, 2], results[series][i, 6]))
        tbl = ma.latex.Table()
        tbl.add_column(results[series][:, (6, 7)] * 1e+3, 'num($0,$1)', 'exp. Feldstärke $B_{\\text{exp}}$ [mT]')
        tbl.add_column(np.array(deviation)*100, 'rnd($0, 2)', 'Abweichung [\\%]')
        tbl.set_caption('Experimentell bestimmte  Feldstärke $B$ im Vergleich mit den Theoriewerten.')
        tbl.set_label('tab:b_field_' + series)
        tbl.set_mode('hline')
        tbl.set_placement('[h]')
        tbl.export('b_field_' + series + '.tex')

for key in figs.keys():
    figs[key].savefig(key)