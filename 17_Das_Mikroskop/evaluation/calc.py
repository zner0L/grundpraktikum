# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

# Messreihen benennen, Daten einlesen aus Datei
data_teil_a = np.genfromtxt('data/teil_a.csv', delimiter=' ', usecols=range(8))
data_teil_b = np.genfromtxt('data/teil_b.csv', delimiter=' ', usecols=range(2))

# Gewichteten Mittelwert über alle 3er Pärchen bilden
averages_teil_a = np.zeros([4, 8])  # Zeilen: 0 10mal; 1 40mal
                                    # Spalten: (0,1) V_ges und Fehler; (2,3) V_obj und Fehler; (4,5) V_kurz und Fehler; (6,7) V_lang und Fehler
for i in range(0, 12, 3):
        for j in range(0, 8, 2):
                averages_teil_a[i/3, [j, j+1]] = ma.data.weighted_average(data_teil_a[[[i], [i+1], [i+2]], [j, j+1]])

print("-- Weighted Averages: --")
print(averages_teil_a)

# -- Teil A --

print("-- Vergroesserungen: --")
vergroesserungen = np.zeros([2, 8])
vergroesserung = ma.uncertainty.Sheet('2/n * b')
vergroesserungen[:, (0, 1)] = vergroesserung.batch(np.stack([averages_teil_a[0, (0,1,2,3)], averages_teil_a[0, (4,5,6,7)]]), 'n|n%|b|b%') # V_ges
vergroesserungen[:, (2, 3)] = vergroesserung.batch(np.stack([averages_teil_a[1, (0,1,2,3)], averages_teil_a[1, (4,5,6,7)]]), 'n|n%|b|b%') # V_obj
vergroesserungen[:, (4, 5)] = vergroesserung.batch(np.stack([averages_teil_a[2, (0,1,2,3)], averages_teil_a[2, (4,5,6,7)]]), 'n|n%|b|b%') # V_kurz
vergroesserungen[:, (6, 7)] = vergroesserung.batch(np.stack([averages_teil_a[3, (0,1,2,3)], averages_teil_a[3, (4,5,6,7)]]), 'n|n%|b|b%') # V_lang

print(vergroesserungen)

v_ok = ma.uncertainty.Sheet('vg/vo')
v_ok = ma.data.weighted_average(v_ok.batch(vergroesserungen[:, (0, 1, 2, 3)], 'vg|vg%|vo|vo%')) # V_ok
print("--- V_okular: ---")
print(v_ok)

delta_l = ufloat(212.30e-3, 0.025e-3) - ufloat(39.25e-3, 0.025e-3)
delta_l = np.array([[delta_l.n, delta_l.s],
         [delta_l.n, delta_l.s]])

brennweite = ma.uncertainty.Sheet('dl/(vl-vk)')
f = brennweite.batch(np.column_stack([delta_l, vergroesserungen[:, (4, 5, 6, 7)]]), 'dl|dl%|vk|vk%|vl|vl%')
print("--- Brennweiten: ---")
print(f)

# -- Teil B --

print("--- Teil B ---")
average_teil_b = ma.data.weighted_average(data_teil_b)
print("d: " + str(average_teil_b))

a_exp = ma.data.Sheet('d/(2*l*la)')
a_exp.set_value('d', average_teil_b[0]*1e-2, average_teil_b[1]*1e-2)
a_exp.set_value('l', 37.3, 0.1)
a_exp.set_value('la', 720e-6, 70e-6)
print("A_exp Glassmassstab: " + str(a_exp.get_result()))

apertur = ma.uncertainty.Sheet('n*(d/2)/(sqrt(d^2/2 + l^2))')
apertur.set_value('d', 7.5e-3, 0.5e-3)
apertur.set_value('l', 50e-3)
apertur.set_value('n', 1.49)
apertur = apertur.get_result()
print("Apertur: " + str(apertur))

a_exp2 = ma.uncertainty.Sheet('n/la')
a_exp2.set_value('n', apertur[0], apertur[1])
a_exp2.set_value('la', 720e-6, 70e-6)
print("A_exp Glassmassstab: " + str(a_exp2.get_result()))