\subsection{Teil A: Motic-Mikroskop}

\subsubsection{Strahlengang}

\begin{figure}[h!]
	\centering
	\includegraphics[width=0.7\textwidth]{img/mikroskop.png}
	\caption{Schematischer Strahlengang im Mikroskop: https://lp.uni-goettingen.de/get/bigimage/4995}
	\label{img:mikr}
\end{figure}

Der Strahlengang im Mikroskop ist in Abbildung \ref{img:mikr} dargestellt. 

\subsubsection{Okularvergrößerung}
\label{sec:okvergr}

\begin{table}[!htb]
	\centering
	\caption{Ergebnis für die Okularvergrößerung $V_{\text{Ok}}$ aus den Gesamtvergrößerungen $V_{\text{Ges}}$ und den Objektivvergrößerungen $V_{\text{Obj}}$ und die Vergrößerungen bei kurzem ($V_{\text{kurz}}$) und langem Okulartubus ($V_{\text{lang}}$).}
	\begin{tabular}{|c|c|c||c|c|}\hline
		&   $V_{\text{Ges}}$ 	& $V_{\text{Obj}}$ & $V_{\text{kurz}}$ &  $V_{\text{lang}}$	\\\hline\hline
		Objektiv A (10x) 	&   $103 \pm 2$ 		& $8.5 \pm 0.2$ & $7.8 \pm 0.2$ & $11.4 \pm 0.2$\\\hline
		Objektiv B (40x)    &   $409 \pm 10$ 		& $31.6 \pm 0.4$ & $32.2 \pm 0.2$ & $45 \pm 2$	\\\hline\hhline{===~~}
		$V_{\text{Ok}}$ 	& \multicolumn{2}{|c|}{$2.4 \pm 0.2$}   & \multicolumn{2}{c}{}	\\ \hhline{---~~}
		
		
	\end{tabular}
	\label{tab:vergr}
\end{table}

Aus den beiden Messreihen soll zunächst die Gesamtvergrößerung und anschließend die Objektivvergrößerung bestimmt werden, um hieraus die beiden Okularvergrößerung zu berechnen. \\

Um die Gesamtvergrößerung $V_{\text{ges}}$ zu bestimmen, wird die Gegenstandsgröße $G$ mit der Bildgröße $B$ verglichen. Auf der Skala des Objektmikrometer sind 200 Striche für 10 mm abgetragen. Aus der Anzahl vermessener Striche $N$ ergibt sich die Gegenstandsgröße zu 
\begin{equation*}
	G = \frac{N}{20} \; \text{mm} \; .
\end{equation*} 
Der Fehler beim Ablesen der Gegenstandsgröße hängt von der Vergrößerung ab. Für das Okular mit 10-facher Vergrößerung (im Folgenden A) wird ein Fehler von einem halben Skalenteil angenommen, für das Okular mit 40-facher Vergrößerung (im Folgenden B) von einem fünftel Skalenteil:
\begin{equation*}
	\sigma_{N_A} = 0.5 \qquad , \qquad \sigma_{N_B} = 0.2
\end{equation*}
Der Fehler beim Ablesen der Bildgröße auf dem Vergleichsmaßstab ist für beide Okulare gleich groß. Wegen der Schwierigkeiten, das Gegenstandsbild mit dem Vergleichsmaßstab exakt zur Deckung zu bringen, wird er auf ein ganzes Skalenteil zu
\begin{equation*}
	\sigma_B = 1 \; {mm}
\end{equation*}
angenommen. Für die drei Messungen wird jeweils der gewichtete Mittelwert von Bild- und Gegenstandsgröße gebildet. Die Gesamtvergößerung ergibt sich nach bekannter Formel zu
\begin{equation}
	V_{\text{ges}} = \frac{B}{G} = 20 \cdot \frac{B}{N} \; .\\
	\label{eq:vges}
\end{equation}
Der Fehler ist gegeben nach 
\begin{equation*}
	\sigma_{V_{\text{ges}}} = 20 \cdot \sqrt{	\left( \sigma_B \cdot \frac{1}{N}   \right)^2 +
												\left( \sigma_N \cdot \frac{B}{N^2} \right)^2
									} \; .
\end{equation*}
Die Vergrößerung des Objektivs $V_{\text{Obj}}$ ergibt sich aus der mit der Schieblehree gemessenen Gegenstandsgröße $G'$, deren Ablesefehler $\sigma_{G'} = 0.025 \; \text{mm}$ konstant zu einem Halben Skalenteil des Nonius angenommen wird und der Bildgröße auf der Mattscheibe $B'$, deren Fehler wiederum von der Vergrößerung des Okulars abhängt. Für das Okular A wird er auf einen Skalenteil, für das Okular B zu einem zehntel Skalenteil angenommen: 
\begin{equation*}
	\sigma_{N_{A'}} = 1 \qquad , \qquad \sigma_{N_{B'}} = 0.1
\end{equation*}
Analog zur Bestimmung der Gesamtvergrößerung in Gleichung \ref{eq:vges} ergibt sich auch die  Objektivvergrößerung:  
\begin{equation*}
	V_{\text{Obj}} = \frac{B'}{G'} = 20 \cdot \frac{B}{N} \; . 
\end{equation*}
Die Okularvergrößerung berechnet sich folglich aus
\begin{equation}
	V_{\text{Ok}} = \frac{V_{\text{ges}}}{V_{\text{Obj}}}, 
\end{equation}  
wobei die gewichteten Mittelwerte der zuvor bestimmten Werte verwendet wurden. Der Fehler berechnet sich zu 
\begin{equation*}
	\sigma_{V_{\text{Ok}}} = \sqrt{	
		\left( \sigma_{V_{\text{ges}}} \cdot \frac{1}{V_{\text{Obj}}} \right)^2 +
		\left( \sigma_{V_{\text{Obj}}} \cdot \frac{V_{\text{ges}}}{V_{\text{Obj}}^2} \right)^2
		} \; .
\end{equation*}
Die Ergebnisse sind in Tabelle \ref{tab:vergr} zusammengefasst.

\subsubsection{Brennweite der Objektive}
\label{sec:brennweite}

Umstellen von Gleichung \ref{eq:vges} ergibt für die Brennweite des Objektivs
\begin{equation*}
	f = \frac{\Delta l}{V_{\text{lang}}-V_{\text{kurz}}} \; ,
\end{equation*}
wobei $\Delta l$ die Differenz der Tubuslängen bezeichnet. Die Vergrößerungen berechnen sich wie in Abschnitt \ref{sec:okvergr} beschrieben. Der Fehler der mit der Schieblehree gemessene Gegenstandsgröß $G$e beträgt wie zuvor ein halbes Skalenteil von $\sigma_{G'} = 0.025 \; \text{mm}$. Aufgrund der unterschiedlichen Größen der Projektion des Objektmikrometers auf die Mattscheibe werden für die Anzahl gemessener Striche für jeden der beiden verwendeten Tubi und jedes der beiden Okulare erneut unterschiedliche Fehler angenommen: 
\begin{align*}
	& \sigma_{N_{\text{kurz}_A}} = 1 \; ,     &\sigma_{N_{\text{lang}_A}} = 0.5 \;  \\
	&\sigma_{N_{\text{kurz}B}} = 0.1 \; ,     &\sigma_{N_{\text{lang}B}} = 0.2 \; .
\end{align*}
Der Fehler der Tubuslängen beträgt ein halbes Skalenteil der Schieblehreenskala \mbox{ $\sigma_{l} = 0.025 \; \text{mm}$}. Somit ergibt sich für die beiden Brennweiten je als gewichtetes Mittel 
\begin{align}
	f_A & = (48 \pm 3) \; \text{mm} \, ,\\
	f_B & = (13 \pm 1) \; \text{mm} \; .
\end{align}
Der Fehler ist hierbei der Fehler des Mittelwerts. 

\subsection{Teil B: Optische Schiene}

\subsubsection{Auflösungsvermögen mit Glasmaßstab}
\label{sec:GLASMASSSTAB}

Die Skala des Glasmaßstabes wird als gleich der des Objektmikrometers angenommen. Damit beträgt der als fehlerfrei angenommene Abstand zwischen zwei Strichen $ 1/20 \;  \text{mm}$, was dem kleinsten theoretisch auflösbaren Abstand $x_{\text{min}}$ entspricht. Somit ergibt sich das theoretische Auflösungsvermögen zu 
\begin{equation}
	A_{\text{theo}} = \frac{1}{x_{\text{min}}} = 20 \; \text{mm}^{-1} \; .
\end{equation}
Für den mit der Schieblehre gemessenen Abstand zwischen Glasmaßstab und Spalt $L$ wird aufgrund der Größe der vermessenen Geräte ein Fehler von einem Skalenteil angenommen. Der Wert des Abstandes ergibt sich als der Mittelwert der beiden Messungen zu 
\begin{equation}
	L = (37.3 \pm 0.1) \; \text{mm} \; .
\end{equation}
Die Spaltbreite der Blende ergibt sich ebenfalls als Mittelwert der drei Messungen zu 
\begin{equation}
	d = (13.2 \pm 0.3) \cdot 10^{-5} \; \text{m} \; ,
\end{equation}
wobei der Fehler auf ein halbes Teil der Ableseskala geschätzt wird. Für den Sinus des Öffnungswinkel $\phi$ gilt mit Spaltbreite $d$
\begin{equation*}
	\sin \phi = \frac{d}{2 \, L},
\end{equation*}
woraus mit dem Brechungsindex von Luft $n  \approx 1$ \cite[Kap. 17.6]{knetter17} für das Auflösungsvermögen folgt:
\begin{equation}
	A_{\text{exp}} = \frac{\sin{\phi}}{\lambda} = \frac{d}{2 \, L \cdot \lambda} = (2.5 \pm 0.3) \; \text{mm}^{-1} \, .
\end{equation}
Die Wellenlänge des vom Rotlichtfilter durchgelassenen Lichts wurde auf  \mbox{ $\lambda = (720 \pm 70) \; \text{nm}$ } geschätzt. Der Fehler ergibt sich zu
\begin{equation*}
	\sigma_{A_{\text{exp}}} = \sqrt{ \left( \sigma_d	     \cdot\frac{1}{L   \cdot \lambda  } \right)^2 + 
					  \left( \sigma_L		 \cdot\frac{d}{L^2 \cdot \lambda  } \right)^2 +
					  \left( \sigma_{\lambda}\cdot\frac{d}{L   \cdot \lambda^2} \right)^2
		} \; .
\end{equation*}

\subsubsection{Auflösungsvermögen mit Plexiglasstab}

Die Breite des Strahlendurchgang wurde zu $15 \pm 1$ Strichen bestimmt, woraus sich mit einem Strichabstand von $ 0.5 \; \text{mm}$  folgende Breite ergibt: 
\begin{equation*}
	d = (15 \pm 1) \cdot 0.5 \; \text{mm} = (7.5 \pm 0.5) \; \text{mm}
\end{equation*} 
Für die numerische Apertur ergibt sich nach Definition mit dem Strahlengang innerhalb des Plexiglasstabs und dem Satz des Pythagoras
\begin{equation*}
	N = n \sin \alpha = n \cdot \frac{d/2}{\sqrt{d^2/2 + L^2}} =  (0.111 \pm 0.008) \; ,
\end{equation*} 
wobei $ n\approx 1.49$ den Brechungsindex von Plexiglas bezeichnet und die Länge des Plexiglasstabes $ L = 50 \;  \text{mm}$ beträgt. Die Fehler der beiden Werte werden nach \cite[Kap. 17.6]{knetter17} als vernachlässigbar angenommen. Das Auflösungsvermögen ergibt sich mit der Wellenlänge $\lambda$ wie in Abschnitt \ref{sec:GLASMASSSTAB} entsprechend zu
\begin{equation}
	A = \frac{N}{\lambda} = (154 \pm 18) \; \text{mm}^{-1} \; .
\end{equation}

Der Vergleich der Auflösungsvermögen von Lochblende und Okular zeigt, dass mit der Lochblende eine deutlich größere Auflösung erreicht werden kann, als mit Okular und Spaltblende. 