# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 14,
        'text.usetex' : True,
        'legend.fontsize': 14}

matplotlib.rcParams.update(rc_params)


def linear_function(x, m, b):
    return m * x + b


def scatter_and_fit(x, y, err=0.0, color='blue', label=''):
    params, errors = curve_fit(linear_function, x, y, sigma=[err for i in range(len(y))])
    m = ufloat(params[0], np.sqrt(errors[0, 0]))
    b = ufloat(params[1], np.sqrt(errors[1, 1]))
    plt.errorbar(x, y, yerr=err, fmt='+', label=label, color=color)
    l = np.linspace(0, max(x))
    plt.plot(l, linear_function(l, m.n, b.n), color=color)
    return m, b

data_approaching = np.genfromtxt('data/auf_20cm.csv', delimiter=' ', usecols=range(10))*1e-1
data_spheres = np.genfromtxt('data/kugeln.csv', delimiter=' ', usecols=range(3))*1e-1
spheres_radii = np.array([2, 4, 12])*1e-2
spheres_voltages = np.array([1, 2, 3, 4, 5])*1e+3
approaching_distances = np.array([[50, 0.01], [45, 0.1], [40, 0.01], [35, 0.01], [30, 0.1], [25, 0.01], [23, 0.01], [21, 0.01], [20.5, 0.01], [20, 0.01]])*1e-2 # cm to m
approaching_voltages = np.array([1, 2, 3, 4, 5, 6, 7, 8, 9, 10])*1e+3 # kV to V
slopes = []

cmap = plt.cm.get_cmap('Accent', len(approaching_distances))

for i, distance in enumerate(approaching_distances):
    error = 0.01
    if distance[0] <= 0.23:
        error = 0.1
    m, b = scatter_and_fit(approaching_voltages/1e+3, data_approaching[:, i], error, cmap(i), '$d = ' + str(distance[0]) + '\; \mathrm{m}$')
    slopes.append([m.n, m.s, b.n, b.s])

slopes = np.array(slopes)
plt.legend(loc='upper left', bbox_to_anchor=(1.02, 1.02),
          ncol=1, fancybox=True, shadow=True)
plt.xlabel(r'Spannung $U$ [kV]')
plt.ylabel(r'Feldst{\"a}rke $E(U)$ [$\frac{\mathrm{kV}}{\mathrm{m}}$]')
plt.savefig('approaching.png', bbox_inches="tight")
plt.close()

log = ma.uncertainty.Sheet('log(x)')
logged_slopes = log.batch(slopes[:, (0,1)], 'x|x%')
logged_distances = log.batch(approaching_distances, 'x|x%')

params, errors = curve_fit(linear_function, logged_distances[:, 0], logged_slopes[:, 0], sigma=logged_slopes[:, 1])
m = ufloat(params[0], np.sqrt(errors[0, 0]))
b = ufloat(params[1], np.sqrt(errors[1, 1]))

l = np.linspace(-1.8, -0.6)
plt.errorbar(logged_distances[:, 0], logged_slopes[:, 0], yerr=logged_slopes[:, 1], xerr=logged_distances[:, 1], fmt='+', label=r'logarithmische Steigung')
plt.plot(l, linear_function(l, m.n, b.n), label='linearer Fit')
plt.legend(ncol=1, fancybox=True, shadow=True)
plt.ylabel(r'$\log(\frac{E(U)}{U} \; \cdot \; \mathrm{m})$')
plt.xlabel(r'$\log(\frac{r}{\mathrm{m}})$')
plt.savefig('log_field.png', bbox_inches="tight")
plt.close()

print 'm: ' + str(m)
print 'b: ' + str(b)

cmap = plt.cm.get_cmap('Accent', len(spheres_radii))
slopes = []

for i, radius in enumerate(spheres_radii): # Oha, schlechte Werte
    m, b = scatter_and_fit(spheres_voltages/1e+3, data_spheres[:, i], 0.01, cmap(i), r'Kugel mit $R =' + str(radius) + '\; \mathrm{m}$')
    print 'm: ' + str(m)
    lambda_r = (m*0.25**2)/radius
    print 'lamba: ' + str(lambda_r)
    print 'Abweichung: ' + str(ma.data.literature_value(2, lambda_r.n, lambda_r.s, mode='ufloat'))

plt.xlabel(r'Spannung $U$ [$\mathrm{kV}$]')
plt.ylabel(r'Feldst{\"a}rke $E(U)$ [$\frac{\mathrm{kV}}{\mathrm{m}}$]')
plt.legend(loc='upper left', bbox_to_anchor=(1.02, 1.02),
          ncol=1, fancybox=True, shadow=True)
plt.savefig('spheres.png', bbox_inches="tight")
plt.close()

