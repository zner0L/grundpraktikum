\subsection{Differentialgleichung einer Schwinung}
Zur Beschreibung einer gedämpften Schwingung bietet sich das vereinfachte Modell des Massenpunktes an, nach dem auf eine Masse $m$ Kräfte $F_i$ einwirken, wodurch die Masse eine Beschleunigung erfährt. Bei einer gedämpften mechanischen Schwingung wirkt auf einen Körper der Masse $m$ einerseits die Rückstellkraft $F_R=-Dx$ mit der Federkonstante $D$ proportional zu und entgegen der Auslenkung $x$ (Hook'sches Gesetz), andererseits die Dämpfkraft $F_D=-bv=-b\dot{x}$, proportional zu und entgegengesetzt der Geschwindigkeit $v$, wobei $b$ der Proportionalitätsfaktor zwischen Dämpfkraft und Geschwindigkeit ist. Da die Summe beider Kräfte zu jedem Zeitpunkt gleich der Gesamtkraft $F=ma=m \ddot{x}$, die am Körper angreift, sein muss, ergibt sich die homogene Differenzialgleichung zweiter Ordnung:
\begin{equation}
m\ddot{x}=-b\dot{x}-Dx \; .
\label{eq_freig}
\end{equation}
Bei einer erzwungenen gedämpften Schwingung kommt noch eine weitere auf den Körper wirkende Kraft hinzu, die periodische Kraft $F_E=F_{0}\cos \omega t$ mit Amplitude $F_0$ und Erregerfrequenz $w$ in Abhängigkeit von der Zeit $t$. Somit ergibt sich die nun inhomogene Differenzialgleichung zweiter Ordnung der erzwungenen Schwingung zu:
\begin{equation}
m\ddot{x}=F_{0}\cos \omega t-b\dot{x}-Dx \; .
\label{eq_erz}
\end{equation}
Mit entsprechenden Größen gilt für das Pohlsche Rad mit Bezeichnungen nach Tabelle \ref{tab1} die Differenzialgleichung:
\begin{equation}
\varTheta \ddot{\varphi}=M\cos \omega t-\dot{\rho\varphi}-D^* \varphi \; .
\label{eq_po1}
\end{equation}
Gleichung \ref{eq_po1}, wird mittels Teilen durch $\varTheta$ auf Normalform gebracht, und nach weiterer Umstellung ergibt sich:
\begin{equation}
\ddot{\varphi}+2 \beta \dot{\varphi}+\omega_{0}^2=N\cos \omega t
\label{eq_po2}
\end{equation}
Hierbei wurden folgende Größen definiert: $2 \beta := \frac{\rho}{\varTheta}$ zur Beschreibung der Stärke der Dämpfung (der Vorfaktor 2 vereinfacht die Lösung der Differentialgleichung) und $\omega_{0}^2:=\frac{D^*}{\varTheta}$ als die Eigenfrequenz des Pohlschen Rades ohne Dämpfung sowie die Amplitude der Erregerfrequenz im Verhältnis zum Trägheitsmoment $N:=\frac{M}{\varTheta}$. Vgl. bisher \cite[Kap 11.1-4]{demtroeder15}\\

\subsection{Lösung der Differentialgleichung}

Nun werden Lösungen dieser Gleichung gesucht, zunächst für den Fall $M=0$, also den der freien gedämpfte Schwingung. Der Ansatz $\varphi = \varphi_0 \cdot e^{\lambda t}$ liefert nach Einsetzen der Ableitungen da $ \varphi \neq 0$ eine quadratische Gleichung mit den Lösungen 
\begin{equation}
	\lambda_{1/2}=\beta \pm \sqrt{\beta^2 - {\omega_0}^2}
\end{equation}
womit sich für den Schwingfall $\omega_0 > \beta$ die Lösung in Gleichung \ref{eq:phi} ergibt \cite[Kap II, §10.5]{fischerkaul07}.
\begin{equation}
	\varphi_{(t)}=\varphi_0 \cdot e^{- \gamma t} \cdot e^{i(\omega_e t - \varPhi)}
	\label{phi}
\end{equation}
Die Eigenfrequenz $\omega_e$ ist dabei gegeben durch Gleichung \ref{eq:eigenfreq}.
\begin{equation}
	\omega_e = \sqrt{{\omega_0}^2 - \beta^2}
	\label{eq:eigenfreq}
\end{equation}
Graphisch interpretiert ist Gleichung \ref{phi} ein Cosinus mit Periodendauer $T$ und abnehmender Amplitude. Zur Beschreibung des Verhältnis zweier aufeinanderfolgender Maxima in Abhängigkeit von der Dämpfung $\beta$ bietet sich das logarithmische Dekrement $\Lambda$ an, gegeben durch 
\begin{equation}
	\Lambda = \ln \left[ \frac{\varphi_{(t)}}{\varphi_{(t+T)}} \right]= \ln \left[ e^{+\beta T} \right]= \beta T
	\label{Lambda}
\end{equation}

Für $M \neq 0$ werden zusätzlich die Lösungen der inhomogenen Differentialgleichung gesucht, wobei der Ansatz
\begin{equation}
	\varphi=\frac{\varphi_0}{2\cdot e^{i(\omega t - \phi)}}+c
\end{equation}
die folgende Lösung für eine stationäre erzwungene Schwingung liefert:
\begin{equation}
	\varphi = \frac{N}{\sqrt{{(\omega_0}^2 - \omega^2)^2+4 \beta^2 \omega^2}} \cdot \left( \omega t -\arctan\left( \frac{2 \beta \omega}{{ \omega_0}^2 - \omega^2}\right)\right)
	\label{eq_erz_in}
\end{equation}
Die einzelnen Teile der Gleichung \ref{eq_erz_in} lassen sich physikalisch interpretieren. So ist der Vorfaktor $\varphi_0$ (Gleichung \ref{eq:amplitude}) die Amplitude dieser Schwingung.
\begin{equation}
\varphi_0(\omega) = \frac{N}{\sqrt{{(\omega_0}^2 - \omega^2)^2+4 \beta^2 \omega^2}}
\label{eq:amplitude}
\end{equation}
Der konstante Term in der Klammer heißt Phasenverschiebung $\phi$ mit
\begin{equation}
	\phi=\arctan\left( \frac{2 \beta \omega}{{ \omega_0}^2 - \omega^2}\right)
	\label{eq:phasenverschiebung}
\end{equation}
Bei einer bestimmten Erregerfrequenz wird die Amplitude $\varphi_0$ maximal und es kann zur Resonanzkatastrophe kommen. Diese Frequenz des Erregers wird die Resonanzfrequenz $\omega_r$ genannt. Für sie gilt:
\begin{equation}
	\omega_r = \sqrt{{\omega_0}^2 - 2\beta^2}
	\label{eq:resonanz}
\end{equation}
In der Schwingungsgleichung stehen also die bisher behandelten Größen in Beziehung zueinander und können aus einer geeigneten Messung experimentell bestimmt werden.


