\subsection{Freie Schwingung}
\label{sec:evaluation_free}

\subsubsection{Dämpfungskonstante}

\begin{figure}
	\centering
	\includegraphics[scale=0.2]{evaluation/beta_fits.png}
	\caption{Schwingungskurven und gefittete Abklingkurven der freien Schwingungen}
	\label{fig:abklingkurven}
\end{figure}

Zunächst soll aus den Messungen der freien gedämpften Schwingung die Eigenfrequenz $\omega_e$ des Pohlschen Rades bestimmt werden. Diese ergibt sich aus den aufgezeichneten Schwingungskurven, wobei der Abstand zweier aufeinanderfolgender Maxima bzw. Minima der Amplitude $\varphi$ im Mittel der Schwingungsperiode $T$ entsprechen. Nun lässt sich die Eigenfrequenz durch $\omega_e = \frac{2 \pi}{T}$ berechnen. Der Fehler der Eigenfrequenz ergibt sich zu 
\begin{equation*}
\sigma_{\omega_e} =\sqrt{\left({ \sigma_{T} \cdot \frac{2 \pi}{T^2}}\right)^2}
\label{eq:fehlereigenfreq}
\end{equation*}
wobei $\sigma_T$ der statistische Fehler des Mittelwertes ist. Die erhaltenen Werte sind in Tabelle \ref{tab:results_free_osz} aufgeführt. 

Für die gedämpften Schwingungen lassen sich außerdem sog. Abklingkurven \mbox{$\varphi(t) = a e^{-\beta x} + c$} an die Maxima bzw. Minima der sinkenden Amplituden legen (siehe Abbildung \ref{fig:abklingkurven}). Aus diesen lässt sich nach Gleichung \ref{Lambda} das logarithmische Dekrement $\Lambda$ und die Dämpfungskonstante $\beta$ bestimmen. Aus den Kurven werden die Werte in Tabelle \ref{tab:results_free_osz} ermittelt. Der Fehler von $\beta$ ist dabei der Fehler aus dem Fit mit der $\chi^2$-Methode, der Fehler von $\Lambda$ ergibt sich aus dem statistischen Fehler aller gemessenen Dekremente für eine Kurve.

\input{evaluation/free_osz_table}

\subsubsection{Eigenfrequenz}

Mithilfe der Dämpfungskonstanten kann die ungedämpfte Eigenfrequenz des Oszillators bestimmt werden. Aus Gleichung \ref{eq:eigenfreq} folgt

\begin{equation}
\omega_0 = \sqrt{\omega_e^2 + \beta^2} \; .
\label{eq:eigenfreq_un}
\end{equation}

Damit ergeben sich die Werte aus Tabelle \ref{tab:results_free_osz} und der Fehler laut Fehlerfortpflanzungsgesetz zu 

\begin{equation*}
	\sigma_{\omega_{0}} =\sqrt{ \left(  \sigma_{\omega_{e}} \cdot \frac{\omega_e}{\sqrt{\omega_e^2 + \beta^2}} \right) ^2 + \left( \sigma_{\beta} \cdot \frac{\beta}{\sqrt{\omega_e^2 + \beta^2}} \right)^2 } \; .
\label{eq:fehlerbeta}
\end{equation*}

Die ungedämpfte Eigenfrequenz des Aufbaus berechnet sich aus dem gewichteten Mittelwert über die berechneten Eigenfrequenzen zu

\begin{equation*}
\omega_0 = 2.036 \pm 0.004 ~\text{Hz} \; .
\label{eq:omega_0}
\end{equation*}

\subsection{Erzwungene Schwingung}

\subsubsection{Resonanzkurve}

Zunächst soll für jede Dämpfung aus den gemessenen Daten die Resonanzfrequenz $\omega_r$ ermittelt werden. Dazu wird in ein Diagramm auf der Abszisse der Quotient aus anregender Frequenz $\omega$ und zuvor bestimmter ungedämpfter Eigenfrequenz $\omega_0$ aufgetragen. Auf der Ordinate steht die Amplitude der  $\varphi_0(\omega)$, jeweils normiert durch die Amplitude bei geringer Anregungsfrequenz (hier: $150$ Hz). Die Amplitude $\varphi_0(\omega)$ ergibt sich als das Mittel der Maxima der Schwingung nach dem Einschwingvorgang. Anhand eines Fits mit gnuplot lassen sich wie in Abbildung \ref{fig:resonanzfrequenz} die Maxima der Resonanzkurven bestimmen, deren Werte auf der Abszisse unter Berücksichtigung der Normierung die gesuchte Resonanzfrequenz $\omega_r$ ergeben. Die Ergebnisse sind in Tabelle \ref{tab:resonanzfrequenz} aufgeführt. \\

Andererseits lässt sich die Resonanzfrequenz nach Gleichung \ref{eq:resonanz} auch theoretisch aus der zuvor gemessenen ungedämpften Eigenfrequenz $\omega_0$ und der Dämpfungskonstante $\beta$ bestimmen. In Tabelle \ref{tab:resonanzfrequenz} sind theoretisch und praktisch bestimmte Werte gegenübergestellt. 
\begin{figure}
	\centering
	\include{evaluation/results/resonanz}
	\caption{Fit an die Amplitude in Abhängigkeit von der normierten Anregungsfrequenz lässt die Resonanzfrequenz erkennen.}
	\label{fig:resonanzfrequenz}
\end{figure}

\input{evaluation/theo_forced_osz_table}

\subsubsection{Phasenverschiebung}

Nun soll die Phasenverschiebung $\phi$ ebenfalls auf zwei verschiedene Arten bestimmt werden. In den Messdaten ist der Zeitpunkt des Nulldurchgangs des Exzenters registriert, woraus sich über die Erregerfrequenz $\omega$ auf den Zeitpunkt dessen Maxima schließen lässt. Die Differenz zwischen Erreger- und Schwingungsmaxima ergibt im Mittel den Zeitversatz $\Delta t$. Nach 
\begin{equation*}
	\phi = \frac{2 \pi}{T} \cdot \Delta t =  \omega \cdot \Delta t  
\end{equation*}
lässt sich daraus die Phasenverschiebung $\phi$ berechnen. Wird in einem Diagramm auf der Abszisse wie zuvor $\frac{\omega}{\omega_0}$ aufgetragen und auf der Ordinate nun die Phasenverschiebung in Anteilen des Winkels $\frac{\phi}{\pi}$ ergibt sich zusammen mit einem Fit die Abbildung \ref{fig:phasenverschiebung1}. \\


\begin{figure}
	\centering
	\include{evaluation/results/phasenverschiebung}
	\caption{Fit an die Phasenverschiebung in Abhängigkeit von der normierten Anregungsfrequenz. Für $4$ mm ergab sich kein sinnvoller Fit, da die Kurve durch einen systematischen Fehler verschoben ist.}
	\label{fig:phasenverschiebung1}
\end{figure}
