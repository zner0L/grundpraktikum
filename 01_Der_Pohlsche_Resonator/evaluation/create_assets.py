# -*- coding: utf-8 -*-
from calc_forced_osz import *

# Plot
fig_free_osz.tight_layout()
fig_free_osz.savefig('beta_fits.png')

# Table
tbl = ma.latex.Table()
tbl.add_column([0, 4, 6, 8], "$0", 'Dämpfung [mm]')
tbl.add_column([[result.n, result.s] for result in results_omega], "num($0,$1)", '$\\omega_e$ [Hz]')
tbl.add_column([[result.n, result.s] for result in results_log_decr], "num($0,$1)", '$\Lambda$')
tbl.add_column([[result.n, result.s] for result in results_beta], "num($0,$1)", '$\\beta$ [Hz]')
tbl.add_column(results_omega_0,  "num($0,$1)", '$\\omega_0$ [Hz]')
tbl.set_caption('Ergebnisse der Auswertung der Schwingungskurven der freien Schwingungen')
tbl.set_label('tab:results_free_osz')
tbl.set_mode('hline')
tbl.export('free_osz_table.tex')

print 'Mittelwerte:'
print ' - Eigenfrequenz: ' + str(omega_0_total)

tbl = ma.latex.Table()
tbl.add_column(omega_r_theo, "num($0, $1)", 'theoretisches $\\omega_r$ [Hz]')
tbl.add_column([[result.n, result.s] for result in omega_r_exp], "num($0,$1)", 'experimentelles $\\omega_r$ [Hz]')
tbl.add_column(deviation_omega_r, "$0", 'Abweichung')
tbl.set_caption('Gegenüberstellung von theoretischen und experimentellen Werten der Resonanzfrequenz $\\omega_r$')
tbl.set_label('tab:theo_forced_osz')
tbl.set_mode('hline')
tbl.export('theo_forced_osz_table.tex')