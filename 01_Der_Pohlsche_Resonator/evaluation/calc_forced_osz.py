# -*- coding: utf-8 -*-
from calc_free_osz import *
plt.close()

def phi0(omega, n, omega_0, beta):
    return n/(np.sqrt((omega_0**2-omega**2)**2+4*beta**2*omega**2))

def phase_shift_func(x, g):
    return np.arctan((g*x)/(1-x**2))/math.pi if x < 1 else np.arctan((g*x)/(1-x**2))/math.pi+1

def phase_shift_func_vec(x, g):
  y = np.zeros(x.shape)
  for i in range(len(y)):
    y[i]=phase_shift_func(x[i],g)
  return y

def get_omega(max, min=[]):
    periods = []
    for i, index in enumerate(max):
        if i + 1 < len(max):
            periods.append(time[max[i + 1]] - time[index])

    for i, index in enumerate(min):
        if i + 1 < len(min):
            periods.append(time[min[i + 1]] - time[index])

    return (2 * math.pi) / ufloat(np.mean(periods), stats.sem(periods))  # Winkelgeschwindigkeit

def find_nearest(array,value):
    idx = (np.abs(array-value)).argmin()
    return array[idx]

# Dämpfung, Index, Erregerfrquenzen (mHz)
data = [
    [4, 1, [150, 200, 250, 275, 310, 325, 350, 400, 600], 'red'],
    [6, 2, [150, 250, 275, 290, 310, 350, 450, 600], 'green'],
    [8, 1, [150, 250, 275, 290, 310, 350, 450, 600], 'blue']
]

for d in data:
    amplitudes = []
    amplitudes_error = []
    freq_quotient = []
    shifts = []
    shifts_error = []
    for f in d[2]:
        print 'Dämpfung: ' + str(d[0]) + ' Erregerfrequenz: ' + str(f)
        measurements = np.genfromtxt('messwerte/Erzwungen/' + str(d[0]) + 'mm/Messung_' + str(f) + '_' + str(d[0]) + '.txt', delimiter=' ', usecols=(0, 1, 2), skip_header=2)
        null = measurements[:, 2] # third column mark if there is a zero
        elongation = measurements[:, 1]  # second column is the amplitude column
        time = measurements[:, 0]
        if d[0] == 4: null = null[11000:]; elongation = elongation[11000:]; time = time[11000:]

        null_index = np.where(null == 1)[0]

        if f > 500:
            delta = [70, 10]
        elif f > 400:
            delta = [70, 15]
        elif f < 200:
            delta = [150, 15]
        else:
            delta = [70, 30]

        all, max, min = find_peaks(elongation, delta[0], delta[1])

        null_value = ufloat(np.mean(elongation[null_index]), stats.sem(elongation[null_index]))
        max_value = ufloat(np.mean(elongation[max]), stats.sem(elongation[max]))
        min_value = ufloat(np.mean(elongation[min]), stats.sem(elongation[min]))

        zero_line = np.mean([max_value, min_value])
        amplitude = np.mean([max_value-zero_line, -1*min_value-zero_line])
        if len(amplitudes) >0:
            amplitudes.append(amplitude.n/amplitudes[0])
            amplitudes_error.append(amplitude.s)
        else:
            amplitudes.append(amplitude.n)
            amplitudes_error.append(amplitude.s)
        freq_quotient.append(((2*math.pi*f)/(1000*omega_0_total)).n)     # Mal 2 pi, weil f kein omega

        exzenter_min = []
        for i in null_index:
            exzenter_min.append(time[i]+0.25*10**6/f)

        exzenter_min = np.array(exzenter_min[:-1])

        minimum_times = time[min]
        phase_shift = []
        prev_shift = 0
        for i, e_min in enumerate(exzenter_min):
            if f > 150:
                candidates = minimum_times[minimum_times > e_min]
                if candidates.any():
                    shift = abs(np.min(candidates)-e_min)
                    phase_shift.append(2*shift/(10**6/f))
            else:
                shift = abs(find_nearest(time[min], e_min)-e_min)
                phase_shift.append(2*shift/(10**6/f))

        error = stats.sem(phase_shift)

        shifts.append(np.mean(phase_shift))
        shifts_error.append(stats.sem(phase_shift))


        # plt.plot(time, elongation)
        # plt.scatter(time[null_index], elongation[null_index])
        # plt.scatter(time[max], elongation[max], color='red')
        # plt.scatter(time[min], elongation[min], color='green')
        # plt.plot(time, [null_value.n for i in range(0, len(time))], color='#0520b7')
        # plt.plot(time, [max_value.n for i in range(0, len(time))], color='#c60000')
        # plt.plot(time, [min_value.n for i in range(0, len(time))], color='#019120')
        # #plt.plot(time, [zero_line.n for i in range(0, len(time))], color='orange')
        # plt.scatter(exzenter_min, [min_value.n for i in range(0, len(exzenter_min))], color="purple")
        # plt.show()

    amplitudes[0] = 1
    np.savetxt('results/amplitude_' + str(d[0]) + 'mm.dat', np.column_stack((freq_quotient, amplitudes, amplitudes_error)))
    np.savetxt('results/phase_shift_' + str(d[0]) + 'mm.dat', np.column_stack((freq_quotient, shifts, shifts_error)))


omega_r_exp = np.array([0.970, 0.981, 0.971])
omega_r_exp = omega_r_exp*omega_0_total

omega_r = ma.uncertainty.Sheet('sqrt(o**2-2*b**2)', '\\omega_r')
omega_r.set_value('o', omega_0_total.n, omega_0_total.s, '\\omega_0')
omega_r.set_value('b', tex='\\beta')
omega_r_theo = omega_r.batch([[beta.n, beta.s] for beta in results_beta[1:]], 'b|b%')

deviation_omega_r = []
for i, om in enumerate(omega_r_theo):
    deviation_omega_r.append(ma.data.literature_value(om[0], omega_r_exp[i].n))