# -*- coding: utf-8 -*-
import scipy.stats as stats
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning
import math

matplotlib.rcParams.update({'font.size': 22})

def exponential_function(x, a, b, c):
    return a*np.exp(x * (-b))+c

#### Dämpfungskonstante ####
def analyze_oszillation(elongation, time, max, min):
    periods = []
    logarithmic_decrements = []
    for i, index in enumerate(max):
        if i + 1 < len(max):
            periods.append(time[max[i + 1]] - time[index])
            logarithmic_decrements.append(elongation[index] / elongation[max[i + 1]])

    for i, index in enumerate(min):
        if i + 1 < len(min):
            periods.append(time[min[i + 1]] - time[index])
            logarithmic_decrements.append(elongation[index] / elongation[min[i + 1]])

    omega = (2*math.pi)/ufloat(np.mean(periods), stats.sem(periods)) # Winkelgeschwindigkeit
    logarithmic_decrement = ufloat(np.mean(logarithmic_decrements), stats.sem(logarithmic_decrements)) # Logarithmisches Dekrement

    params_max, errors_max = curve_fit(exponential_function, time[max], elongation[max], p0=(1, 1e-6, 1))
    params_min, errors_min = curve_fit(exponential_function, time[min], elongation[min], p0=(1, -1e-6, 1))

    beta = np.mean([ufloat(params_max[1], math.sqrt(errors_max[1][1])), ufloat(params_min[1], math.sqrt(errors_min[1][1]))]) # Dämpfungskonstante

    return omega, logarithmic_decrement, beta, params_max, params_min


def find_peaks(array, xdelta, ydelta):
    results = []
    ismax = {}
    for i, x in enumerate(array):
        if i > xdelta and i < len(array)-xdelta:
            if array[i-xdelta] < x and x > array[i+xdelta]:
                results.append(i)
                ismax[i] = True
            elif array[i-xdelta] > x and x < array[i+xdelta]:
                results.append(i)
                ismax[i] = False

    previous = 0
    same_value = []
    final = []
    max = []
    min = []
    for result in results:
        if abs(array[result] - previous) < ydelta:
            same_value.append(result)
        else:
            if same_value:
                mean_value = int(round(np.mean(same_value)));
                final.append(mean_value)
                if not mean_value in ismax.keys():
                    if array[mean_value - xdelta] < x and x > array[mean_value + xdelta]:
                        max.append(mean_value)
                    elif array[mean_value - xdelta] > x and x < array[mean_value + xdelta]:
                        min.append(mean_value)
                elif ismax[mean_value]:
                    max.append(mean_value)
                else:
                    min.append(mean_value)
                same_value = []
            previous = array[result]

    return final, max, min

# Plot Setup
fig_free_osz, axarr = plt.subplots(2, 2, figsize=(30,20))
axarr[0, 0].set_title(u'Dämpfung: 0 mm')
axarr[0, 0].axis([0,160000,-150,150])
axarr[0, 0].set_ylabel(u'Amplitude [°]')
axarr[0, 1].set_title(u'Dämpfung: 6 mm')
axarr[0, 1].axis([0,25000,-80,80])
axarr[1, 0].set_title(u'Dämpfung: 4 mm')
axarr[1, 0].set_ylabel(u'Amplitude [°]')
axarr[1, 0].set_xlabel(u'Zeit [ms]')
axarr[1, 0].axis([0,35000,-100,100])
axarr[1, 1].set_title(u'Dämpfung: 8 mm')
axarr[1, 1].set_xlabel(u'Zeit [ms]')
axarr[1, 1].axis([0,20000,-60,60])

# Data Setup
# Dämpfung, xdelta, ydelta (für Peaks), axis
data = [
    [0, 30, 20, axarr[0, 0]],
    [4, 70, 8, axarr[1, 0]],
    [6, 100, 6, axarr[0, 1]],
    [8, 100, 4, axarr[1, 1]],
]

results_omega = []
results_log_decr = []
results_beta = []
for d in data:
    measurements = np.genfromtxt('messwerte/Frei/Messung_' + str(d[0]) + 'mm.txt', delimiter=' ', usecols=(0, 1), skip_header=2)
    elongation = measurements[:, 1] # second column is the amplitude column
    time = measurements[:,0]

    all, max, min = find_peaks(elongation, d[1], d[2])
    if d[0] == 8:
        dummya, add_max, add_min = find_peaks([0 for i in range(0, 4800)] + elongation.tolist()[4800:], 200, 0.5)
        min += add_min
    # print indices
    # indices_max = indices
    d[3].plot(time, elongation, color='orange')
    d[3].scatter(time[max], elongation[max], color='red')
    d[3].scatter(time[min], elongation[min], color='blue')
    #plt.show()

    omega, log_decr, beta, params_max, params_min = analyze_oszillation(elongation, time, max, min)

    results_omega.append(omega*1000) # mal 1000, da Milisekunden
    results_log_decr.append(log_decr)
    if beta.s == float('Inf'): beta = ufloat(beta.n, 0.0001)
    results_beta.append(beta*1000)

    d[3].plot(time, exponential_function(time, params_max[0], params_max[1], params_max[2]))
    d[3].plot(time, exponential_function(time, params_min[0], params_min[1], params_min[2]))

omega_0 = ma.uncertainty.Sheet('sqrt(o**2 + b**2)', '\\omega_0')
results_omega_0 = omega_0.batch([[results_omega[i].n, results_omega[i].s, results_beta[i].n, results_beta[i].s] for i, d in enumerate(results_omega)], 'o|o%|b|b%')

omega_0_total = ma.data.weighted_average(results_omega_0, 'ufloat')