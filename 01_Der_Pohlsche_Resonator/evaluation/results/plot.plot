reset
f(x)=G/(1+exp(-k*G*x)*(G/A-1))
fit f(x) "phase_shift_4mm.dat" via G,k,A
plot "phase_shift_4mm.dat", f(x)
