reset
f(x)= b*atan((a*((x-1)/pi))/(c-(((x-1)/pi)**2)))+0.25
b=0.15
a=200
c=10
fit f(x) "phase_shift_4mm.dat" via a,c,b
plot "phase_shift_4mm.dat", f(x)