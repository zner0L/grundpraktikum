reset
set terminal epslatex color lw 2 size 12cm,8cm
set output 'resonanz.tex'
set xlabel '$\omega/\omega_0$'
set ylabel '$\varphi_0(\omega)/\varphi_0(0)$'
set xrange [0.5:1.5]
set yrange [0:13]
set xtics 0.25


f(x)=a**2/(sqrt((a**2-x**2)**2+b**2*x**2))
g(x)=c**2/(sqrt((c**2-x**2)**2+d**2*x**2))
h(x)=e**2/(sqrt((e**2-x**2)**2+f**2*x**2))

fit f(x) 'amplitude_4mm.dat' using 1:2 via a,b
fit g(x) 'amplitude_6mm.dat' using 1:2 via c,d
fit h(x) 'amplitude_8mm.dat' using 1:2 via e,f

plot 'amplitude_4mm.dat' using 1:2:3 w yerrorbars title 'Dämpfung 4 mm' lc 1, f(x) notitle lc 1, \
	'amplitude_6mm.dat' using 1:2:3  w yerrorbars title 'Dämpfung 6 mm' lc 4, g(x) notitle lc 4, \
	'amplitude_8mm.dat' using 1:2:3 w yerrorbars title 'Dämpfung 8 mm' lc 6, h(x) notitle lc 6
set out