reset
set xrange [0.5:1.5]
f(x)=a/(1+exp(-b*a*x)*(a/c - 1))
a = 0.87987
b = 2.73091
c = 0.0117719
f(x)=a/(1+exp(-b*a*x)*(a/c - 1))
m(x)=d/(1+exp(-e*d*x)*(d/f - 1))
n(x)=g/(1+exp(-h*g*x)*(a/i - 1))
fit f(x) "phase_shift_4mm.dat" via a,b,c
fit m(x) "phase_shift_6mm.dat" via d,e,f
fit n(x) "phase_shift_8mm.dat" via g,h,i
plot "phase_shift_4mm.dat" u 1:2 w errorbars, "phase_shift_6mm.dat" u 1:2 w errorbars, "phase_shift_8mm.dat" u 1:2 w errorbars, f(x), m(x), n(x)