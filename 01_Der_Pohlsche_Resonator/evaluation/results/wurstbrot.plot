reset
set terminal epslatex color lw 2 size 12cm,8cm
set output 'phasenverschiebung.tex' 
set xrange [0.5:1.5]
set xtics 0.25
set ytics 0.25
set xlabel '$\omega/\omega_0$'
set ylabel '$\Phi/\pi$'
set key top left
set key box
i(x)= x<1 ? atan(g*x/(1-x**2))/pi : atan(g*x/(1-x**2))/pi+1
j(x)= x<1 ? atan(h*x/(1-x**2))/pi : atan(h*x/(1-x**2))/pi+1
k(x)= x<1 ? atan(i*x/(1-x**2))/pi : atan(i*x/(1-x**2))/pi+1

g=0.127; h=0.2; i=0.5

fit j(x) "phase_shift_6mm.dat" via h
fit k(x) "phase_shift_8mm.dat" via i
plot "phase_shift_4mm.dat" title 'Dämpfung 4 mm' w yerrorbars, "phase_shift_6mm.dat" title 'Dämpfung 6 mm' w yerrorbars, "phase_shift_8mm.dat" title 'Dämpfung 8 mm' w yerrorbars, \
	j(x) notitle, k(x) notitle
set out