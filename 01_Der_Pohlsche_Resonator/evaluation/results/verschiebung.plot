reset
f(x)=a/(1+exp(-b*a*x)*(a/c - 1))
a = 0.87987
b = 2.73091
c = 0.0117719
fit f(x) "phase_shift_4mm.dat" via a,b,c
plot "phase_shift_4mm.dat", f(x)