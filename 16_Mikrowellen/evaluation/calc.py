# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

def sin_refl(x, a, b, c, d, e):
        return a * np.sin(b*x + c) + d*x + e


def sin_lechl(x, a, b, c, d, e, f, g):
        return a * (np.exp(-g * x) + 0.2) * np.sin(b * x + c) + d + e*x + f*x**2

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

# Messreihen benennen, Daten einlesen aus Datei
data_refl = np.genfromtxt('data/standing_wave.csv', delimiter=' ', usecols=range(4))
data_lechl = np.genfromtxt('data/lecher.csv', delimiter=' ', usecols=range(4))

# Normalisierung

# Farben array anlegen für plots
cmap = plt.cm.get_cmap('Accent', 4)

#_________________________________________________________________________________________
# Plotstart 1
# speichern in Variable um speichern zu können
# Bildauflösung
fig1 = plt.figure(1, figsize=[10, 8])
# r macht txbar
plt.title(r'Reflexion')

# Fit
param, errors = curve_fit(sin_refl, data_refl[:, 0], data_refl[:, 1], [3, 0.46, 5, 0, 5])
l = np.linspace(50, 150, 200)

plt.errorbar(data_refl[:, 0], data_refl[:, 1], xerr=data_refl[:, 2], yerr=data_refl[:, 3], label='Messwerte', fmt='.', color=cmap(2))
plt.plot(l, sin_refl(l, param[0], param[1], param[2], param[3], param[4]),  color=cmap(3), label=r'\chi^2-Fit')
plt.axis([50, 150, 0, 8.5])
plt.xlabel(r'Ort $x$ [mm]')
plt.ylabel(r'Spannung $U \propto E$ [V]')
box = plt.axes().get_position()
plt.axes().set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
plt.legend(bbox_to_anchor=(0., -0.32, 1., .102), loc=3,
       ncol=2, mode="expand", borderaxespad=0.)
plt.close()

results_reflection = np.zeros((5, 2))
for i in range(len(param)):
        results_reflection[i, 0] = param[i]
        results_reflection[i, 1] = np.sqrt(errors[i, i])

# Plotende

#_________________________________________________________________________________________


# Plotstart 2
# speichern in Variable um speichern zu können
# Bildauflösung
fig2 = plt.figure(1, figsize=[10, 7])
# r macht txbar
plt.title(r'Lecherleitung')

# Fit
param, errors = curve_fit(sin_lechl, data_lechl[:, 0], data_lechl[:, 1], [1.8, 0.36, 0, 3, -0.0042, 0.00003, 0.2])
l = np.linspace(0, 150, 400)

plt.errorbar(data_lechl[:, 0], data_lechl[:, 1], xerr=data_lechl[:, 2], yerr=data_lechl[:, 3], fmt='.', color=cmap(0), label='Messwerte')
plt.plot(l, sin_lechl(l, param[0], param[1], param[2], param[3], param[4], param[5], param[6]),  color=cmap(1), label=r'\chi^2-Fit')
plt.xlabel(r'Ort $x$ [mm]')
plt.ylabel(r'Spannung $U \propto E$ [V]')
plt.axis([0, 150, 0, 7.5])
plt.legend(loc='upper right')
plt.close()

results_lechl = np.zeros((7, 2))
for i in range(len(param)):
        results_lechl[i, 0] = param[i]
        results_lechl[i, 1] = np.sqrt(errors[i, i])
# Plotende
#_________________________________________________________________________________________

wavelength = ma.uncertainty.Sheet('2*pi/b')
wavelength.set_value('b', results_reflection[1, 0], results_reflection[1, 1])
results_reflection[1, :] = wavelength.get_result()
wavelength.set_value('b', results_lechl[1, 0], results_lechl[1, 1])
results_lechl[1, :] = wavelength.get_result()

print('-- Ergebnisse der Reflexion --')
print(results_reflection)
print
print('-- Ergebnisse der Lecherleitung --')
print(results_lechl)
print
print('-- Literaturwerte --')
f = ma.uncertainty.Sheet('(c/2)*sqrt(4/a**2+4/b**2)', 'f')
f.set_value('c', 299792458e+3)
f.set_value('a', 23, 0.2)
f.set_value('b', 22, 0.2)
f = f.get_result()

print('f = ' + str(f))

wl_lit = ma.uncertainty.Sheet('c/f', '\lambda')
wl_lit.set_value('c', 299792458e+3)
wl_lit.set_value('f', f[0], f[1])
wl_lit = wl_lit.get_result()

print('lambda lit = ' + str(wl_lit))

print('--- Abweichungen ---')
print('Reflexion:' + str(ma.data.literature_value(wl_lit[0], results_reflection[1, 0], results_reflection[1, 1],  mode='ufloat')*100) + ' %')
print('Lecherleitung:' + str(ma.data.literature_value(wl_lit[0], results_lechl[1, 0], results_lechl[1, 1], mode='ufloat')*100) + ' %')

n_pvc = ma.uncertainty.Sheet('1 + d/D')
n_pvc.set_value('d', 3, 1)
n_pvc.set_value('D', 20)
n_pvc = n_pvc.get_result()

print

print('n_pvc = ' + str(n_pvc))

fig1.savefig('reflexion.png')
fig2.savefig('lecherleitung.png')