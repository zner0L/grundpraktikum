f(x) = a * sin(b*x+c)+d*x + e
a=2
d = 0.5
e = 4
b = 0.5

fit f(x) 'm1.dat' via a,b,c,d,e

plot 'm1.dat', f(x)
pause -1