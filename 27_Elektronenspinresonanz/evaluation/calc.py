# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

# Fitfunktion defnieren
def fit_linear(x, a, b):
        return a * x + b

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

figsize = [10, 8]
figs = {}

# Kontanten
mu_0 = 4*math.pi*1e-7
h = 6.62607004e-34
mu_B = 9.274009994e-24 # J/T

# Gleichungen
b_field = ma.uncertainty.Sheet('mu0 * 8 * n/(125)**(1/2) * i/r * 0.9')
b_field.set_value('mu0', mu_0)
b_field.set_value('n', 320)
b_field.set_value('r', 6.8e-2)

# Messreihen benennen, Daten einlesen aus Datei
for messung in ['alle']:
    data = np.genfromtxt('data/' + messung.lower().replace(' ', '_') + '.csv', delimiter=' ', usecols=range(4))
    data[:, [0, 1]] = data[:, [0, 1]] * 1e+6
    data[:, [2, 3]] = data[:, [2, 3]] * 1e-3
    bs = b_field.batch(data[:, [2, 3]], 'i|i%')

    print('---- Messung "' + messung + '" ----')

    l = np.linspace(bs[:, 0].min(), bs[:, 0].max())
    figs[messung.lower().replace(' ', '_') + '.png'] = {'fig': plt.figure(figsize=figsize), 'extra_artists': ()}
    plt.xlabel(r'Magnetfeld $B$ [mT]')
    plt.ylabel(r"Resonanzfrequenz $f$ [MHz]")
    params, error = curve_fit(fit_linear, bs[:, 0], data[:, 0], p0=[2e+10, 3])
    plt.errorbar(bs[:, 0]*1e+3, data[:, 0]*1e-6, yerr=data[:, 1]*1e-6, xerr=bs[:, 1]*1e+3, label=r'Messwerte', fmt='+')
    plt.plot(l*1e+3, fit_linear(l, params[0], params[1])*1e-6, label='Linearer Fit')
    figs[messung.lower().replace(' ', '_') + '.png']['extra_artists'] = (plt.legend(bbox_to_anchor=(0., -0.22, 1., .102), loc=3,
                                                               ncol=2, mode="expand", borderaxespad=0.),)
    plt.close()

    m = ufloat(params[0], np.sqrt(error[0][0]))
    gs = (m * h) / mu_B
    print(' - Steigung: ' + str(m))
    print(' - f-Achsenabschnitt: ' + str(ufloat(params[1], np.sqrt(error[1][1]))))
    print(' - Lande-Faktor: ' +str(gs))

for key in figs.keys():
    figs[key]['fig'].savefig(key, bbox_extra_artists=figs[key]['extra_artists'], bbox_inches='tight')