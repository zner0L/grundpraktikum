# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

# Fitfunktion defnieren
def fit_linear(x, a, b):
        return a * x + b

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

figsize = [10, 8]
figs = {}

# Messreihen benennen, Daten einlesen aus Datei
data_h = np.genfromtxt('data/xyz.csv', delimiter=' ', usecols=range(2))

for key in figs.keys():
    figs[key].savefig(key)