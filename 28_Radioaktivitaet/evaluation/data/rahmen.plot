reset
set terminal epslatex color solid
set format x '\num{%g}'
set format y '\num{%g}'
set key top right
set fit errorvariables

##################################################################################
### Teil I: lineare Graphen
##################################################################################

f0(t)=n0
f1(t)=a1*exp(-log(2)*t/ta)+b1*exp(-log(2)*t/tb)+n0
f2(t)=a2*exp(-log(2)*t/ta)+b2*exp(-log(2)*t/tb)+n0
f4(t)=a4*exp(-log(2)*t/ta)+b4*exp(-log(2)*t/tb)+n0
f8(t)=a8*exp(-log(2)*t/ta)+b8*exp(-log(2)*t/tb)+n0
f(x,y) = y==1 ? f1(x) : y==2 ? f2(x) : y==4 ? f4(x) : y==8 ? f8(x): f0(x)

ta=22; tb=117; n0=0.6
a1=433; a2=430; a4=750; a8=340
b1=39; b2=26; b4=112; b8=66

fit f(x,y) 'messwerte.dat' using 1:3:2:($2==0 ? 1 : sqrt($2)) via ta,tb,n0,a1,a2,a4,a8,b1,b2,b4,b8

set xlabel '$t$ [\si{\second}]'
set ylabel '$-\dot{N}$ [\si{\imp\per\fives}]'

set xrange [0:2000]
set yrange [0:12]
set output 'nullrate.tex'
plot './messwerte.dat' using 1:2 index 0 title 'Messwerte',\
     f0(x) title 'Gefittete Nullrate'
set output
!epstopdf 'nullrate.eps'
!rm 'nullrate.eps'

set xrange [0:300]
set yrange [0:700]
set output 'minute1.tex'
plot './messwerte.dat' using 1:2:(sqrt($2)) index 1 title 'Messwerte',\
     f1(x) title 'Gefitteter Verlauf',\
     a1*exp(-log(2)*x/ta) title 'Isotop A',\
     b1*exp(-log(2)*x/tb) title 'Isotop B',\
     f0(x) title 'Nullrate'
set output
!epstopdf 'minute1.eps'
!rm 'minute1.eps'

set xrange [0:300]
set yrange [0:800]
set output 'minute2.tex'
plot './messwerte.dat' using 1:2:(sqrt($2)) index 2 title 'Messwerte',\
     f2(x) title 'Gefitteter Verlauf',\
     a2*exp(-log(2)*x/ta) title 'Isotop A',\
     b2*exp(-log(2)*x/tb) title 'Isotop B',\
     f0(x) title 'Nullrate'
set output
!epstopdf 'minute2.eps'
!rm 'minute2.eps'

set xrange [0:300]
set yrange [0:800]
set output 'minute4.tex'
plot './messwerte.dat' using 1:2:(sqrt($2)) index 3 title 'Messwerte',\
     f4(x) title 'Gefitteter Verlauf',\
     a4*exp(-log(2)*x/ta) title 'Isotop A',\
     b4*exp(-log(2)*x/tb) title 'Isotop B',\
     f0(x) title 'Nullrate'
set output
!epstopdf 'minute4.eps'
!rm 'minute4.eps'

set xrange [0:450]
set yrange [0:800]
set output 'minute8.tex'
plot './messwerte.dat' using 1:2:(sqrt($2)) index 4 title 'Messwerte',\
     f8(x) title 'Gefitteter Verlauf',\
     a8*exp(-log(2)*x/ta) title 'Isotop A',\
     b8*exp(-log(2)*x/tb) title 'Isotop B',\
     f0(x) title 'Nullrate'
set output
!epstopdf 'minute8.eps'
!rm 'minute8.eps'

##################################################################################
### Teil II: logarithmische Graphen
##################################################################################

set xlabel '$t$ [\si{\second}]'
set ylabel '$\ln\left(-\frac{\dot{N}}{[\si{\imp\per\fives}]}\right)$'

set xrange [0:300]
set yrange [0:6.7]
set output 'minute1_.tex'
plot './messwerte.dat' using 1:(log($2)):(log(sqrt($2))) index 1 title 'Messwerte',\
     log(f1(x)) title 'Gefitteter Verlauf',\
     log(a1*exp(-log(2)*x/ta)) title 'Isotop A',\
     log(b1*exp(-log(2)*x/tb)) title 'Isotop B',\
     log(f0(x)) title 'Nullrate'
set output
!epstopdf 'minute1_.eps'
!rm 'minute1_.eps'

set xrange [0:300]
set yrange [0:6.7]
set output 'minute2_.tex'
plot './messwerte.dat' using 1:(log($2)):(log(sqrt($2))) index 2 title 'Messwerte',\
     log(f2(x)) title 'Gefitteter Verlauf',\
     log(a2*exp(-log(2)*x/ta)) title 'Isotop A',\
     log(b2*exp(-log(2)*x/tb)) title 'Isotop B',\
     log(f0(x)) title 'Nullrate'
set output
!epstopdf 'minute2_.eps'
!rm 'minute2_.eps'

set xrange [0:300]
set yrange [0:6.7]
set output 'minute4_.tex'
plot './messwerte.dat' using 1:(log($2)):(log(sqrt($2))) index 3 title 'Messwerte',\
     log(f4(x)) title 'Gefitteter Verlauf',\
     log(a4*exp(-log(2)*x/ta)) title 'Isotop A',\
     log(b4*exp(-log(2)*x/tb)) title 'Isotop B',\
     log(f0(x)) title 'Nullrate'
set output
!epstopdf 'minute4_.eps'
!rm 'minute4_.eps'

set xrange [0:450]
set yrange [0:6.7]
set output 'minute8_.tex'
plot './messwerte.dat' using 1:(log($2)):(log(sqrt($2))) index 4 title 'Messwerte',\
     log(f8(x)) title 'Gefitteter Verlauf',\
     log(a8*exp(-log(2)*x/ta)) title 'Isotop A',\
     log(b8*exp(-log(2)*x/tb)) title 'Isotop B',\
     log(f0(x)) title 'Nullrate'
set output
!epstopdf 'minute8_.eps'
!rm 'minute8_.eps'

##################################################################################
### Teil III: Aktivierungskurven
##################################################################################

set ylabel '$-\dot{N}_0$ [\si{\imp\per\fives}]'
set xlabel '$\tau$ [\si{\second}]'
set xrange [0:9]

set print 'parameter.txt'
set yrange [800:1050]
g(x)=A*(1-exp(-log(2)/tA*x))
A=100; tA=ta
print 1, a1, a1_err; print 2, a2, a2_err; print 4, a4, a4_err; print 8, a8, a8_err
fit g(x) 'parameter.txt' using 1:2:3 via A, tA
set output 'isotopA.tex'
plot 'parameter.txt' using 1:2:3 with errorbars title 'Messwerte',\
     g(x) title 'Aktivierungskurve'
set output
!epstopdf 'isotopA.eps'
!rm 'isotopA.eps'
!rm 'parameter.txt'

set print 'parameter.txt'
set key bottom right
set yrange [30:180]
h(x)=B*(1-exp(-log(2)/tB*x))
B=100; tB=tb
print 1, b1, b1_err; print 2, b2, b2_err; print 4, b4, b4_err; print 8, b8, b8_err
fit h(x) 'parameter.txt' using 1:2:3 via B, tB
set output 'isotopB.tex'
plot 'parameter.txt' using 1:2:3 with errorbars title 'Messwerte',\
     h(x) title 'Aktivierungskurve'
set output
!epstopdf 'isotopB.eps'
!rm 'isotopB.eps'
!rm 'parameter.txt'
