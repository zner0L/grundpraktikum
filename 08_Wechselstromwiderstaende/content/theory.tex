
Im Folgenden werden Wechselstromkreise mit Eingangsspannung $U_e = U_0 \cos \omega t$ betrachtet. Durch Induktivitäten $L$ und Kapazitäten $C$ treten Phasenverschiebungen zwischen Strom und Spannung auf. Die Spannung lässt sich komplex in der Form schreiben:
\begin{equation*}
	U_{(t)} = U_0 (\cos \omega t + i \sin \omega t) = U_0 e^{iwt} \; .
\end{equation*} 


\subsection{Induktiver Widerstand}
Für eine ideale Spule der Induktivität $L$ muss nach den Kirchhoffschen Regeln die angelegte Spannung $U_e = U_0 e^{iwt}$ entgegengesetzt gleich der induzierten Spannung $U_{ind} = L \cdot \text{d} I / \text{d} t$ sein. Umstellen nach $I$ und Integration über die Zeit liefert 
\begin{equation*}
	I(t) = \frac{U_0}{i \omega L} \,e^{iwt} = I_0 \, e^{iwt} \qquad \text{mit} \qquad I_0 = \frac{U_0}{i \omega L}, 
\end{equation*}
was die Definition des Induktiven Widerstandes motiviert \cite[Kap 5.4]{demtroeder06}:
\begin{equation*}
	Z_L = \frac{U_0}{I_0} = i \omega L \;.
\end{equation*}
Spannung und Strom sind in einer solchen Schaltung nicht mehr in Phase, die Spannung läuft dem Strom um eine viertel Periode voraus.  

\subsection{Kapazitiver Widerstand}
Für einen Stromkreis mit Kapazität $C$ und angelegter Wechselspannung $U_e$ folgt aus $U = Q/C$ durch zeitliche Differentiation und Umstellen nach $I$ dass
\begin{equation*}
	I(t) = iwCU_0 \, e^{iwt} = I_0 e^{iwt} \qquad \text{mit} \qquad I_0 = U_0 iwC.
\end{equation*}
Analog wird der kapazitive Widerstand mit
\begin{equation*}
	Z_C = \frac{U_0}{I_0} = \frac{1}{i \omega C}
\end{equation*}
definiert. Hier nun eilt der Strom der Spannung mit einer Phasenverschiebung von $\phi = \pi / 2 $ voraus. 

\subsection{Impedanz}
Werden Spulen, Kondensatoren und ohm'sche Widerstände gemeinsam in Schaltungen verbaut, so lässt sich aus den induktiven, kapazitiven und ohm'schen Widerständen $Z_L$, $Z_C$ und $R$ der gesamte Scheinwiderstand $Z$ berechnen, dessen Betrag auch als Impedanz $|Z|$ bezeichnet wird \cite[Kap 5.4]{demtroeder06}. Der Realteil der Impedanz heißt Wirkwiderstand $R_W = \text{Re} (Z)$, der Imaginärteil wird Blindwiderstand oder Reaktanz $X = \text{Im} (Z)$ genannt. Es gilt also \cite[Kap 8.4]{knetter16}
\begin{equation}
	Z^2 = {R_W}^2 + X^2 \; .
	\label{eq:impedanz}
\end{equation}
 
\subsubsection{Reihenschaltung}
In einer Reihenschaltung ergibt sich der Gesamtwiderstand als Summe der Einzelwiderstände 
\begin{equation}
	Z = R + Z_L + Z_C = R + i \omega L + \frac{1}{i\omega C} = R + i \left(\omega L - \frac{1}{\omega C}\right)
\end{equation}
und aus Gleichung \ref{eq:impedanz} folgt für die Phasenverschiebung 
\begin{equation}
	\phi = \arctan \left(\frac{X}{R}\right) = \arctan\left(\frac{\omega L - \frac{1}{\omega C}}{R}\right) \; .
	\label{eq:phase_shift}
\end{equation}
Die Resonanzfrequenz  $\omega_r$ wird erreicht für $X = 0$, also für
\begin{equation}
	\omega L - \frac{1}{\omega C} = 0 \qquad \Leftrightarrow \qquad \omega_r = \frac{1}{\sqrt{LC}} \; .
\end{equation}

\subsubsection{Parallelschaltung}
Werden die einzelnen Bauteile parallel geschaltet, so berechnet sich der Kehrwert des Gesamtwiderstandes als Summe der Kehrwerte der Einzelwiderstände: 
\begin{equation}
	\frac{1}{Z} = \frac{1}{R} + \frac{1}{Z_L} +\frac{1}{Z_C} 
	= \frac{1}{R} + \frac{1}{i \omega L} + i\omega C
	= \frac{1}{R} + i\left(\omega C - \frac{1}{\omega L}\right) \; .
\end{equation}
Analog ergibt sich für die Phasenverschiebung 
\begin{equation}
	\phi = \arctan \left(\frac{X}{R}\right) = \arctan\left(\frac{\omega C - \frac{1}{\omega L}}{R}\right) \; .
\end{equation}