# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import cmath
import math

def linear_function(x, m, b):
    return m * x + b

data_with_cap = np.genfromtxt('data/with_capacitor.csv', delimiter=' ', usecols=range(6))
data_without_cap = np.genfromtxt('data/without_capacitor.csv', delimiter=' ', usecols=range(4))
data_parallel = np.genfromtxt('data/parallel.csv', delimiter=' ', usecols=range(3))

data_with_cap[:, 2] = data_with_cap[:, 2]*1e-3
data_parallel[:, 2] = data_parallel[:, 2]*1e-3

ampere_from_volt = ma.uncertainty.Sheet('u/r')
ampere_from_volt.set_value('r', 10.2, 0.005)
ampere_from_volt.set_error('u', 0.02)
ampere_with = ampere_from_volt.batch(data_with_cap[:, 2], 'u')
ampere_from_volt.set_error('u', 0.2)
ampere_without = ampere_from_volt.batch(data_without_cap[:, 2], 'u')

omega = ma.uncertainty.Sheet('2*pi*f', name='\\omega')
omega_squared = ma.uncertainty.Sheet('(2*pi*f)^2', name='\\omega^2')
omegas_without = omega_squared.batch(data_without_cap[:, 0], 'f')
omegas_with = omega.batch(data_with_cap[:, 0], 'f')
omegas_parallel = omega.batch(data_parallel[:, 0], 'f')

impedance = ma.uncertainty.Sheet('u/i', name='z')
impedance_squared = ma.uncertainty.Sheet('(u/i)^2', name='z^2')
impedance_squared.set_error('u', 2)
impedance_results_without = impedance_squared.batch(np.column_stack([data_without_cap[:, 1], ampere_without]), 'u|i|i%')
impedance.set_error('u', 2)
impedance_results_with = impedance.batch(np.column_stack([data_with_cap[:, 1], ampere_with]), 'u|i|i%')

impedance.set_error('u', 0.05)
impedance.set_error('i', 0.05e-3)
impedance_parallel = impedance.batch(data_parallel[:, [1,2]], 'u|i')

phase_shift = ma.uncertainty.Sheet('t*1e-6*f*2*pi')
phase_shift.set_error('t', 30)
shifts_with = phase_shift.batch(np.column_stack([data_with_cap[:, 3], data_with_cap[:, 1]]), 't|f')
shifts_with[0:7, 0] = - shifts_with[0:7, 0]

params, errors = curve_fit(linear_function, omegas_without[:, 0], impedance_results_without[:, 0])
l = np.linspace(0,9e+6)

m_without = ufloat(params[0], np.sqrt(errors[0, 0]))
induction = umath.sqrt(m_without);
b_without = ufloat(params[1], np.sqrt(errors[1, 1]))

print 'Induktion L: ' + str(induction)
print 'ohmscher Spulenwiderstand R: ' + str(cmath.sqrt(b_without.n))

plt.errorbar(omegas_without[:, 0]/1e+6, impedance_results_without[:, 0]/1e+6, yerr=impedance_results_without[:, 1]/1e+6, fmt='+', color='red')
plt.plot(l/1e+6, linear_function(l, params[0], params[1])/1e+6, color='green', linestyle='--')
plt.title(r'ohne Kondensator')
plt.xlabel(r'$\omega^2 \;[10^6\, \mathrm{Hz}^2]$')
plt.ylabel(r'$z^2 \;[10^6\, \Omega^2]$')
plt.savefig('without_capacitor.png')
plt.close()

l = np.linspace(500, 2500)
spline = interp1d(omegas_with[:, 0], impedance_results_with[:, 0], kind='cubic')
minimum = minimize(spline, 1000)
omega_r = minimum.x[0]
print omega_r
resistance = minimum.fun
print resistance
plt.errorbar(omegas_with[:, 0], impedance_results_with[:, 0], yerr=impedance_results_with[:, 1], fmt='+', color='red')
plt.plot(l, spline(l), color='grey', label=r'Interpolation')
plt.scatter(minimum.x[0], minimum.fun, marker='o', color='orange', label=r'Minimum')
plt.title(r'mit Kondensator')
plt.xlabel(r'$\omega \;[\mathrm{Hz}]$')
plt.ylabel(r'$z \;[\Omega]$')
plt.legend()
plt.savefig('with_capacitor.png')
plt.close()

spline = interp1d(omegas_with[:, 0], shifts_with[:, 0], kind='cubic')
root = brentq(spline, 500, 2500)
l = np.linspace(1000,1500)
print 'Root: ' + str(root)
plt.errorbar(omegas_with[:, 0], shifts_with[:, 0], yerr=shifts_with[:, 1], fmt='+', color='red')
plt.plot(l, spline(l), color='grey', label=r'Interpolation')
plt.scatter(root, 0, marker='o', color='orange', label='Nullstelle')
plt.title(r'Phasenverschiebung im Serienresonanzkreis')
plt.xlabel(r'$\omega \;[\mathrm{Hz}]$')
plt.ylabel(r'$\phi \;[\mathrm{rad}]$')
plt.legend()
plt.savefig('phase_shift.png')
plt.close()

spline1 = interp1d(omegas_with[:, 0], -data_with_cap[:, 4], kind='cubic')
spline2 = interp1d(omegas_with[:, 0], -data_with_cap[:, 5], kind='cubic')
min1 = minimize(spline1, 1200)
min2 = minimize(spline2, 1200)

plt.errorbar(omegas_with[:, 0], data_with_cap[:, 1], yerr=2, fmt='+', color='red', label=r'$U$')
plt.errorbar(omegas_with[:, 0], data_with_cap[:, 4], yerr=0.05, fmt='+', color='blue', label=r'$U_C$')
plt.errorbar(omegas_with[:, 0], data_with_cap[:, 5], yerr=0.05, fmt='+', color='green', label=r'$U_{L+R}$')
plt.legend()
plt.title(r'Spannungen im Serienresonanzkreis')
plt.xlabel(r'$\omega \;[\mathrm{Hz}]$')
plt.ylabel(r'$U \;[\mathrm{V}]$')
plt.savefig('voltages.png')
plt.close()

print 'U_C: ' + str(-min1.fun)
print 'U_(R+L): ' + str(-min2.fun)
print 'phi: ' + str(np.rad2deg(np.arccos(10.2/-min2.x[0])))

plt.errorbar(omegas_parallel[:, 0], impedance_parallel[:, 0], yerr=impedance_parallel[:, 1], fmt='+', color='red')
plt.title(r'Impedanz im Parallelkreis')
plt.xlabel(r'$\omega \;[\mathrm{Hz}]$')
plt.ylabel(r'$z \;[\mathrm{\Omega}]$')
plt.savefig('parallel.png')
plt.close()
