# -*- coding: utf-8 -*-
import scipy.stats as stats
from scipy.interpolate import interp1d
import numpy as np
import maabara as ma
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
import uncertainties.umath as umath
from uncertainties import ufloat
from scipy.optimize import curve_fit, OptimizeWarning, minimize, brentq
import math

# Fitfunktion defnieren
def fit_linear(x, a, b):
        return a * x + b

# array für plots
rc_params = {'font.family' : 'Palatino',
        'font.weight' : 'light',
        'font.size'   : 22,
        'text.usetex' : True,
        'legend.fontsize': 22}

matplotlib.rcParams.update(rc_params)

figsize = [10, 8]
figs = {}

# Messreihen benennen, Daten einlesen aus Datei
data_h = np.genfromtxt('data/h.csv', delimiter=' ', usecols=range(3))
data_hg = np.genfromtxt('data/hg.csv', delimiter=' ', usecols=range(3))

# Normierung, Umrechung Deg -> Rad
data_hg[:, 0] = np.deg2rad(360 - data_hg[:, 0])
data_h[:, 0] = np.deg2rad(360 - data_h[:, 0])
data_hg[:, 1] = np.deg2rad(data_hg[:, 1])
data_h[:, 1] = np.deg2rad(data_h[:, 1])


# Gitterkonstante
sin = ma.uncertainty.Sheet('sin(a)')
data_hg[:, (0, 1)] = sin.batch(data_hg[:, (0, 1)], 'a|a%')
params, errors = curve_fit(fit_linear, data_hg[:, 0], data_hg[:, 2])
g = ufloat(params[0], np.sqrt(errors[0][0]))
print('Gitterkonstante: ' + str(g))

# Plot für Gitterkonstante
l = np.linspace(0.24, 0.40)
figs['gitterkonstante.png'] = plt.figure(figsize=figsize)
plt.title('Bestimung der Gitterkonstante')
plt.errorbar(data_hg[:, 0], data_hg[:, 2], xerr=data_hg[:, 1], fmt='.', label='Messwerte', zorder=1)
plt.plot(l, fit_linear(l, params[0], params[1]), label='Ausgleichsgerade')
plt.xlabel('$\\sin(\\alpha)$')
plt.ylabel('Wellenl\\"ange [nm]')
plt.axis([0.25, 0.40, 400, 650])
box = plt.axes().get_position()
plt.axes().set_position([box.x0, box.y0 + box.height * 0.2, box.width, box.height * 0.8])
plt.legend(bbox_to_anchor=(0., -0.32, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.)
plt.close()

# Wellenlängen Balmer
wavelength = ma.uncertainty.Sheet('g*sin(a)')
wavelength.set_value('g', g.n, g.s)
wl_balmer = wavelength.batch(data_h[:, (0, 1)], 'a|a%')

# Rydberg-Konstante
rydberg = ma.uncertainty.Sheet('1/(la*(0.25-1/(n**2)))')
rydies = rydberg.batch(np.column_stack((wl_balmer*1e-9, data_h[:, 2])), 'la|la%|n')

rydy = ma.data.weighted_average(rydies, 'ufloat')
print('Mittelwert Rydberg: ' + str(rydy))

# Tabellen
tbl = ma.latex.Table()
tbl.add_column(data_h, 'num($0,$1)', 'Winkel $\\alpha_n$ [rad]')
tbl.add_column(wl_balmer, 'num($0,$1)', 'Wellenlänge $\\lambda_n$ [nm]')
tbl.add_column(rydies*1e-6, 'num($0,$1)', 'Rydberg-Konstante $R_{\\infty}$ [$10^6 \\, \\text{m}^{-1}$]')
tbl.set_caption('Berechnete Wellenlängen der Balmer-Serie.')
tbl.set_label('tab:balmer')
tbl.set_mode('hline')
tbl.set_placement('[h!]')
tbl.export('balmer.tex')


for key in figs.keys():
    figs[key].savefig(key)